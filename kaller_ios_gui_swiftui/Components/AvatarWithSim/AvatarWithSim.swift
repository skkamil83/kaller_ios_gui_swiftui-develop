//
//  RecentsAvatar.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 04/12/2020.
//

import SwiftUI

struct AvatarWithSim: View {
    let props: AvatarWithSimProps

    var body: some View {
        ZStack {
            avatar(props: props)
            if !props.isSimHidden {
                sim(props: props)
            }
        }
                .frame(width: props.size, height: props.size)
    }

    private func avatar(props: AvatarWithSimProps) -> some View {
        AvatarView(
                props: AvatarViewProps(
                        size: props.size,
                        style: props.avatar
                )
        )
    }

    private func sim(props: AvatarWithSimProps) -> some View {
        HStack {
            Spacer()
            VStack {
                Spacer()
                SimIcon(
                        props: SimIconProps(
                                number: props.simNumber,
                                size: CGSize(width: props.size * 14 / 44, height: props.size * 18 / 44)
                        )
                )
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: props.size * -3 / 44, trailing: props.size * -3 / 44))
            }
        }
    }
}

struct RecentAvatar_Previews: PreviewProvider {
    static var previews: some View {
        AvatarWithSim(
                props: AvatarWithSimProps(
                        avatar: .name(firstName: "Jay", lastName: "D", company: "New"),
                        simNumber: 1,
                        isSimHidden: false,
                        size: 44
                )
        )
    }
}
