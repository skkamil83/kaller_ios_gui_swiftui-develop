import SwiftUI

struct AvatarWithSimProps {
    let avatar: AvatarViewProps.Style
    let simNumber: Int
    let isSimHidden: Bool
    let size: CGFloat
}
