import SwiftUI

struct BottomTabbarActionView: View {
    let props: BottomTabbarActionViewProps

    var body: some View {
        VStack {
            Spacer()
            view(props: props)
        }
    }

    private func view(props: BottomTabbarActionViewProps) -> some View {
        let bottomHeight = UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0
        return HStack {
            ForEach(props.items, id: \.self.id) { item in
                itemView(item: item, alignment: getAlignmentOfItem(items: props.items, targetItem: item))
                        .frame(minWidth: 0, maxWidth: .infinity)
            }
        }
                .frame(height: 45)
                .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: bottomHeight, trailing: Spacing.spacing16Pt))
                .background(background())
    }

    private func background() -> some View {
        Color.navigationBarBackgroundChrome
                .background(Color.white)
                .edgesIgnoringSafeArea(.bottom)
    }

    private func itemView(item: BottomTabbarActionViewModel, alignment: TextAlignment) -> some View {
        Button(action: {
            item.action(item.id)
        }) {
            HStack {
                if alignment == .trailing {
                    Spacer()
                }
                Text(item.title)
                        .font(TextStyle.body3Button1Light5Blue.font)
                        .foregroundColor(item.color)
                if alignment == .leading {
                    Spacer()
                }
            }
        }
                .disabled(item.isDisable)
    }

    func getAlignmentOfItem(items: [BottomTabbarActionViewModel], targetItem: BottomTabbarActionViewModel) -> TextAlignment {
        if targetItem.id == items.first?.id {
            return .leading
        } else if targetItem.id == items.last?.id {
            return .trailing
        } else {
            return .center
        }
    }
}

struct ChatPageEditBottomView_Previews: PreviewProvider {
    static var previews: some View {
        BottomTabbarActionView(
                props: BottomTabbarActionViewProps(
                        items: [
                            .init(
                                    id: "read",
                                    title: "Readdsad",
                                    isDisable: true,
                                    color: nil,
                                    action: { _ in }
                            ),
                            .init(id: "archive", title: "Archive", isDisable: false, color: nil, action: { _ in }),
                            .init(id: "delete", title: "Delete", isDisable: false, color: .red, action: { _ in })
                        ]
                )
        )
    }
}
