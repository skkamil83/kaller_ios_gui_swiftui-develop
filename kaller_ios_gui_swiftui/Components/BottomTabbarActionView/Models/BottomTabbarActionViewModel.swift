import SwiftUI

struct BottomTabbarActionViewModel: Equatable {
    let id: String
    let title: String
    let isDisable: Bool
    let color: Color?
    let action: (_ id: String) -> Void
    
    static func == (lhs: BottomTabbarActionViewModel, rhs: BottomTabbarActionViewModel) -> Bool {
        return lhs.id == rhs.id && lhs.title == rhs.title && lhs.isDisable == rhs.isDisable && lhs.color == rhs.color
    }
    
}
