import SwiftUI

struct ConversationRowStatusView: View {
    let props: ConversationRowStatusViewProps

    var body: some View {
        Image(props.status.imageName)
                .frame(width: 24, height: 24)
    }
}
