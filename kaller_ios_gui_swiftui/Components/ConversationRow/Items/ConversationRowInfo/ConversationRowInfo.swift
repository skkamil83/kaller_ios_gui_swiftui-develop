import SwiftUI

struct ConversationRowInfo: View {
    let props: ConversationRowInfoProps
    var body: some View {
        info(props: props)
    }

    private func info(props: ConversationRowInfoProps) -> some View {
        VStack(spacing: 0) {
            HStack(spacing: 0) {
                name(props: props)
                Spacer()
                status(props: props)
                time(props: props)
            }
                    .frame(height: 22)
                    .padding(.top, 7)
                    .padding(.trailing, Spacing.spacing16Pt)
            HStack(spacing: 0) {
                VStack(spacing: 0) {
                    message(props: props)
                    Spacer()
                }
                Spacer()
                VStack(spacing: 0) {
                    icons(props: props)
                    Spacer()
                }
            }
            Divider()
        }
                .frame(height: 76)
                .padding(.leading, Spacing.spacing16Pt)
    }

    private func name(props: ConversationRowInfoProps) -> some View {
        Text(props.title)
                .textStyle(.body2Bold1Light1LabelColor)
    }

    private func status(props: ConversationRowInfoProps) -> some View {
        if let status = props.status {
            return AnyView(
                    ConversationRowStatusView(
                            props: ConversationRowStatusViewProps(
                                    status: status
                            )
                    )
            )
        } else {
            return AnyView(EmptyView())
        }
    }

    private func time(props: ConversationRowInfoProps) -> some View {
        ConversationRowTime(
                props: ConversationRowTimeProps(
                        date: props.time
                )
        )
    }

    private func icons(props: ConversationRowInfoProps) -> some View {
        HStack(spacing: Spacing.spacing4Pt) {
            if props.isMuted {
                Image("messagesMute")
            }
            if props.isPinned {
                Image("messagesPinned")
            }
            if props.isUnread {
                VStack {
                    Text("\(props.unreadCount == 0 ? "" : "\(props.unreadCount)")")
                            .textStyle(.subheadline1Default2Dark1LabelColor)
                            .padding(EdgeInsets(top: 0, leading: 4.5, bottom: 0, trailing: 4.5))
                }
                        .frame(minWidth: 20, minHeight: 20, maxHeight: 20)
                        .background(Color.primaryBlue)
                        .cornerRadius(10)
            }
        }
                .padding(EdgeInsets(top: 2, leading: 0, bottom: 0, trailing: Spacing.spacing16Pt))
    }

    private func message(props: ConversationRowInfoProps) -> some View {
        if let iconName = props.messageType?.iconName {
            return AnyView(
                    (Text(Image(systemName: iconName))
                            + Text(" \(props.message)")
                    )
                            .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                            .lineLimit(2)
                            .padding(.trailing, Spacing.spacing16Pt)
            )
        } else {
            return AnyView(
                    Text(props.message)
                            .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                            .lineLimit(2)
                            .padding(.trailing, Spacing.spacing16Pt)
            )
        }
    }
}

struct ChatListRowInfo_Previews: PreviewProvider {
    static var previews: some View {
        ConversationRowInfo(
                props: ConversationRowInfoProps(
                        title: "aaaaa",
                        status: .none,
                        time: Date(),
                        isMuted: true,
                        isPinned: true,
                        isUnread: true,
                        unreadCount: 0,
                        message: "Lorem efref ffeffffffffrf frfrff rfrf frfff rf rf rf j4r rr",
                        messageType: .text
                )
        )
    }
}
