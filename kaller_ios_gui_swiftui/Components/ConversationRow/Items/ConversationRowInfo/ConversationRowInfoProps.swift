import Foundation

struct ConversationRowInfoProps {
    let title: String
    let status: MessageStatus?
    let time: Date
    let isMuted: Bool
    let isPinned: Bool
    let isUnread: Bool
    let unreadCount: Int
    let message: String
    let messageType: MessageType?
}
