import SwiftUI

struct ConversationRowTime: View {
    let props: ConversationRowTimeProps

    var body: some View {
        Text(props.date.formattedDate())
                .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
    }
}
