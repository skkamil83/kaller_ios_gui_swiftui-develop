import SwiftUI

struct ConversationRow: View {
    let props: ConversationRowProps

    var body: some View {
        HStack(spacing: 0) {
            avatar(props: props)
            info(props: props)
        }
                .frame(height: 76)
                .background(Color.white)
    }

    private func avatar(props: ConversationRowProps) -> some View {
        AvatarWithSim(
                props: AvatarWithSimProps(
                        avatar: props.conversation.avatar,
                        simNumber: props.conversation.numberOfSim,
                        isSimHidden: !props.conversation.isShowSimNumber,
                        size: 44
                )
        )
                .padding(.leading, Spacing.spacing16Pt)
    }

    private func info(props: ConversationRowProps) -> some View {
        ConversationRowInfo(
                props: ConversationRowInfoProps(
                        title: props.conversation.title,
                        status: props.conversation.messageStatus,
                        time: props.conversation.date,
                        isMuted: props.conversation.isMuted,
                        isPinned: props.conversation.isPinned,
                        isUnread: props.conversation.isUnread,
                        unreadCount: props.conversation.unreadCount,
                        message: props.conversation.message,
                        messageType: props.conversation.messageType
                )
        )
    }

}

struct ConversationRow_Previews: PreviewProvider {
    static var previews: some View {
        ConversationRow(
                props: ConversationRowProps(
                        conversation: ConversationViewModel(
                                id: "2",
                                avatar: .name(firstName: "Alex", lastName: "Sir", company: ""),
                                title: "Alex Sir",
                                conversationType: .dual,
                                numberOfSim: 1,
                                isShowSimNumber: true,
                                message: "Hello!",
                                messageType: .text,
                                messageStatus: .deliverd,
                                date: Date(),
                                isArchived: false,
                                isUnread: true,
                                unreadCount: 0,
                                dateOfPin: nil,
                                isMuted: false)
                )
        )
    }
}
