//
//  MultiaccountSelectionContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 24/11/2020.
//

import SwiftUI

struct MultiaccountSelectionContainerProps{
    let style: MultiaccountSelectionStyle
    let onClose: () -> Void
    let onMakeCall: (PhoneInfo?) -> Void
    let onMakeMessage: (PhoneInfo?) -> Void
}

struct MultiaccountSelectionContainer: View {
    let props: MultiaccountSelectionContainerProps

    @EnvironmentObject private var userInfoState: UserInfoState

    var body: some View {
        VStack {
            if let userInfo = userInfoState.accountInfo {
                MultiaccountSelectionView(
                    props: MultiaccountSelectionViewProps(
                        style: props.style,
                        accountInfo: userInfo,
                        onOutSideTap: props.onClose,
                        onButtonCallTap: props.onClose,
                        onSelectedCallingContact: { phoneSelected in
                            props.onMakeCall(phoneSelected)
                        },
                        onSelectedMessageContact: { phoneSelected in
                            props.onMakeMessage(phoneSelected)
                        }, onSelectedDefalutNumber: {
                            
                        }
                    )
                ).ignoresSafeArea()
            } else {
                Text("NO USER INFO IN STATE")
            }
        }.onAppear {
            userInfoState.accountInfo = AccountInfo(
                phones: [
                    PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                account: Account(id: 1, name: "Account name", createDate: Date()),
                devices: []
            )
        }
    }
}

struct MultiaccountSelectionContainer_Previews: PreviewProvider {
    static var previews: some View {
        MultiaccountSelectionContainer(
            props: MultiaccountSelectionContainerProps(
                style: .selectPhoneInfo(true),
                onClose: {
                    print("close")
                },
                onMakeCall: { phone in
    
                },
                onMakeMessage: { phone in
        
                }
            )
        )
    }
}
