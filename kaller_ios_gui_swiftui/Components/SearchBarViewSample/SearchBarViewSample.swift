import SwiftUI

struct SearchBarViewSample: View {
    @State private var query = ""

    var body: some View {
        VStack {
            SearchBarView(
                    props: SearchBarViewProps(
                            query: $query,
                            placeHolder: "hint",
                            onFocus: { isResponding in

                            }
                    )
            )

            Text("search query: \(query)")
        }.padding()
    }
}