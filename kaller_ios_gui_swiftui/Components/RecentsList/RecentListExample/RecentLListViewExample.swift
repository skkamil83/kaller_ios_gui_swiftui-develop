//
//  RecentLListViewALLViewExample.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 07/12/2020.
//

import SwiftUI

struct RecentLListViewExample: View {
    @State private var accountInfo: AccountInfo = AccountInfo(
        phones: [
            PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
            PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
            PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
        account: Account(id: 1, name: "Account name", createDate: Date()),
        devices: []
    )
    @State private var recentCalls: [RecentViewModel] = []
    @State private var filterType: RecentListFilterType
    @State private var state: RecentListState
    @State private var isEmpty: Bool
    
    init(filterType: RecentListFilterType, state: RecentListState, isEmpty: Bool) {
        _filterType = .init(wrappedValue: filterType)
        _state = .init(wrappedValue: state)
        _isEmpty = .init(wrappedValue: isEmpty)
    }
    
    var body: some View {
        RecentsListView(
            props: RecentsListViewProps(
                filterType: filterType,
                recentCalls: isEmpty ? [] : recentCalls,
                accountInfo: accountInfo,
                state: state,
                onDelete: { (_) in
                    print("delete")
                },
                onShowInfo: { (_) in
                    print("show Info")
                },
                onSelected: {_ in
                    print("Selected")
                },
                onFilter: {
                    print("Filter")
                }
            )
        )
        .onAppear(){
            let recentCall1 = RecentCall( callId: "1", callType: .outcoming, phoneFromNumber: "+71111234567", phoneToNumber: "+44 20 1234 5678", callStartDate: Date(),
                                          callEndDate: Date(), isExtension: false, isMissed: false, isCanceled: false, fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Dad", lastName: "", company: nil, avatarUrl: nil, phones: []),
                                         billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
            let recentCallViewModel = RecentViewModel( calls: [recentCall1], displayDate: "18:21", isMissed: false, isCanceled: false, callType: .outcoming,
                                                       contact: Contact(firstName: "Dad",lastName: "", company: nil,avatarUrl: nil,
                                                                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: "", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "Mobile",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
                                                       isExtension: false, place: "Mobile")
            recentCalls.append(recentCallViewModel)
            
            let recentCall2 = RecentCall( callId: "2", callType: .incoming, phoneFromNumber: "+44 20 1234 5678", phoneToNumber: "+71111234567", callStartDate: Date(),
                                          callEndDate: Date(), isExtension: false, isMissed: false, isCanceled: false,fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Dad", lastName: "", company: nil, avatarUrl: nil, phones: []),
                                         billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
            let recentCallViewModel2 = RecentViewModel( calls: [recentCall2], displayDate: "17:56", isMissed: false, isCanceled: false, callType: .incoming,
                                                        contact: Contact(firstName: "Dad",lastName: "", company: nil,avatarUrl: nil,
                                                                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: "", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "Home",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
                                                       isExtension: false, place: "Home")
            recentCalls.append(recentCallViewModel2)
            

            let recentCallViewModel3 = RecentViewModel( calls: [recentCall1, recentCall1], displayDate: "17:26", isMissed: true, isCanceled: false, callType: .outcoming,
                                                        contact: Contact(firstName: "Dad",lastName: "", company: nil,avatarUrl: nil,
                                                                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: "", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "Work",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
                                                       isExtension: false, place: "Work")
            recentCalls.append(recentCallViewModel3)
            
            let recentCall4 = RecentCall( callId: "3", callType: .outcoming, phoneFromNumber: "+11111234567", phoneToNumber: "+44 20 1234 5678", callStartDate: Date(),
                                          callEndDate: Date(), isExtension: false, isMissed: false, isCanceled: false,fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Richard", lastName: "Miles", company: nil, avatarUrl: nil, phones: []),
                                         billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
            let recentCallViewModel4 = RecentViewModel( calls: [recentCall4], displayDate: "16:03", isMissed: false, isCanceled: false, callType: .outcoming,
                                                        contact: Contact(firstName: "Richard",lastName: "Miles", company: nil,avatarUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640",
                                                                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Richard", lastName: "Miles", avatarImageUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640"), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "Mobile",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
                                                       isExtension: false, place: "Mobile")
            recentCalls.append(recentCallViewModel4)
            
            let recentCall5 = RecentCall( callId: "4", callType: .incoming, phoneFromNumber: "+44 20 1234 5678", phoneToNumber: "+11111234567", callStartDate: Date(),
                                          callEndDate: Date(), isExtension: false, isMissed: true, isCanceled: false,fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Alex", lastName: "Kazakevich", company: nil, avatarUrl: nil, phones: []),
                                         billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
            let recentCallViewModel5 = RecentViewModel( calls: [recentCall5], displayDate: "15:21", isMissed: true, isCanceled: false, callType: .incoming,
                                                        contact: Contact(firstName: "Alex",lastName: "Kazakevich", company: nil,avatarUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640",
                                                                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Alex", lastName: "Kazakevich", avatarImageUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640"), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "Work",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
                                                       isExtension: false, place: "Work")
            recentCalls.append(recentCallViewModel5)
            
            let recentCall6 = RecentCall( callId: "5", callType: .incoming, phoneFromNumber: "+33 1 23 45 67 89", phoneToNumber: "+21111234567", callStartDate: Date(),
                                          callEndDate: Date(), isExtension: false, isMissed: true, isCanceled: false,fromCountry: nil, fromExt: nil, contact: nil,
                                         billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
            let recentCallViewModel6 = RecentViewModel( calls: [recentCall6, recentCall6], displayDate: "Thursday", isMissed: true, isCanceled: false, callType: .incoming,
                                                       contact: nil,
                                                       isExtension: false, place: "France")
            recentCalls.append(recentCallViewModel6)
            
            
            let recentCall7 = RecentCall( callId: "6", callType: .outcoming, phoneFromNumber: "+21111234567", phoneToNumber: "+71111234567", callStartDate: Date(),
                                          callEndDate: Date(), isExtension: false, isMissed: false, isCanceled: false,fromCountry: nil, fromExt: nil, contact: Contact( firstName: "John", lastName: "Doe", company: nil, avatarUrl: nil, phones: []),
                                         billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
            let recentCallViewModel7 = RecentViewModel( calls: [recentCall7], displayDate: "23.08.2018", isMissed: false, isCanceled: false, callType: .outcoming,
                                                        contact: Contact(firstName: "John",lastName: "Doe", company: nil,avatarUrl: nil,
                                                                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "John", lastName: "Doe", avatarImageUrl: nil), phoneNumber: "+71111234567",nickName: "@jayd",place: "Extension 11",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
                                                       isExtension: false, place: "Extension 11")
            recentCalls.append(recentCallViewModel7)
        }
    }
    
}
