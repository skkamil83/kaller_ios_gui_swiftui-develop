//
//  RecentPageExample.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 09/12/2020.
//

import SwiftUI

struct RecentPageExample: View {
    @EnvironmentObject private var userInfoState: UserInfoState
    @EnvironmentObject private var recentsState: RecentsState
    @EnvironmentObject private var contactsState: ContactsState
    
    var body: some View {
        RecentsPageContainer(
            props: RecentsPageContainerProps(
                openRecentDetails: {_ in }
            )
        )
        .environmentObject(userInfoState)
        .environmentObject(recentsState)
        .environmentObject(contactsState)
        .onAppear(){
            userInfoExample()
            contactExample()
            recentCallsExample()
        }
    }
    
    private func userInfoExample(){
        userInfoState.accountInfo = AccountInfo(
            phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
            account: Account(id: 1, name: "Account name", createDate: Date()),
            devices: []
        )
    }
    
    private func contactExample(){
        let contacts = [
            Contact(firstName: "Alex", lastName: "Gray", company: nil, avatarUrl: nil, phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Alex", lastName: "Sir", avatarImageUrl: nil), phoneNumber: "+12221234567", nickName: "@alex", place: "home", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Alex", lastName: "Sir", avatarImageUrl: nil), phoneNumber: "+12221234568", nickName: "@alex", place: "work", sortOrder: 1, phoneCode: 1, locationCode: 222, ext: nil)
            ]),
            Contact(firstName: "David", lastName: nil, company: nil, avatarUrl: nil, phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "David", lastName: nil, avatarImageUrl: nil), phoneNumber: "+12221234567", nickName: "@Dav", place: "work", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil)
            ]),
            Contact(firstName: "Dad", lastName: nil, company: nil, avatarUrl: nil, phones: [
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "David", lastName: nil, avatarImageUrl: nil), phoneNumber: "+12221237890", nickName: "@Dad", place: "Mobile", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil),
            ]),
            Contact(firstName: "Richard", lastName: "Miles", company: nil, avatarUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640", phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Richard", lastName: "Miles", avatarImageUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640"), phoneNumber: "+12221233333", nickName: "@Richard", place: "Mobile", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil),
            ]),
            Contact(firstName: "Alex", lastName: "Kazakevich", company: nil, avatarUrl: "https://images.unsplash.com/photo-1563351672-62b74891a28a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=aejaz-memon-6erzQwfnCuo-unsplash.jpg&w=640", phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Alex", lastName: "Kazakevich", avatarImageUrl: "https://images.unsplash.com/photo-1563351672-62b74891a28a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=aejaz-memon-6erzQwfnCuo-unsplash.jpg&w=640"), phoneNumber: "+12221234444", nickName: "@Alex", place: "Work", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil),
            ])
        ]
        contactsState.localContacts = contacts
    }
    
    private func recentCallsExample(){
        recentsState.recents = [
            RecentCall(callId: "1", callType: .outcoming, phoneFromNumber: "+71111234567", phoneToNumber: "+12221237890", callStartDate: Date(), callEndDate: Date(), isExtension: false, isMissed: false, isCanceled: false,fromCountry: "United States", fromExt: nil, contact: Contact(firstName: "Dad", lastName: nil, company: nil, avatarUrl: nil, phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: nil, avatarImageUrl: nil), phoneNumber: "+12221237890", nickName: "@Dad", place: "Mobile", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil)
            ]), billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "2", callType: .incoming, phoneFromNumber: "+12221237890", phoneToNumber: "+71111234567", callStartDate: Date(timeIntervalSince1970: 1607455563), callEndDate: Date(timeIntervalSince1970: 1607455563), isExtension: false, isMissed: false, isCanceled: false,fromCountry: "United States", fromExt: nil, contact: Contact(firstName: "Dad", lastName: nil, company: nil, avatarUrl: nil, phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: nil, avatarImageUrl: nil), phoneNumber: "+12221237890", nickName: "@Dad", place: "Mobile", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil)
            ]), billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "3", callType: .incoming, phoneFromNumber: "+12221237890", phoneToNumber: "+11111234567", callStartDate: Date(timeIntervalSince1970: 1607378400), callEndDate: Date(timeIntervalSince1970: 1607378400), isExtension: false, isMissed: true, isCanceled: false,fromCountry: "United States", fromExt: nil, contact: Contact(firstName: "Dad", lastName: nil, company: nil, avatarUrl: nil, phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: nil, avatarImageUrl: nil), phoneNumber: "+12221237890", nickName: "@Dad", place: "Mobile", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil)
            ]), billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "4", callType: .incoming, phoneFromNumber: "+12221237890", phoneToNumber: "+11111234567", callStartDate: Date(timeIntervalSince1970: 1607378400), callEndDate: Date(timeIntervalSince1970: 1607378400), isExtension: false, isMissed: true, isCanceled: true,fromCountry: "United States", fromExt: nil, contact: Contact(firstName: "Dad", lastName: nil, company: nil, avatarUrl: nil, phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: nil, avatarImageUrl: nil), phoneNumber: "+12221237890", nickName: "@Dad", place: "Mobile", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil)
            ]), billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "5", callType: .outcoming, phoneFromNumber: "+11111234567", phoneToNumber: "+12221233333", callStartDate: Date(timeIntervalSince1970: 1607230800), callEndDate: Date(timeIntervalSince1970: 1607230800), isExtension: false, isMissed: false, isCanceled: false,fromCountry: "United States", fromExt: nil, contact: Contact(firstName: "Richard", lastName: "Miles", company: nil, avatarUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640", phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Richard", lastName: "Miles", avatarImageUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640"), phoneNumber: "+12221233333", nickName: "@Richard", place: "Mobile", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil),
            ]), billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "6", callType: .incoming, phoneFromNumber: "+12221234444", phoneToNumber: "+11111234567", callStartDate: Date(timeIntervalSince1970: 1607230800), callEndDate: Date(timeIntervalSince1970: 1607230800), isExtension: false, isMissed: true, isCanceled: false,fromCountry: "United States", fromExt: nil, contact: Contact(firstName: "Alex", lastName: "Kazakevich", company: nil, avatarUrl: "https://images.unsplash.com/photo-1563351672-62b74891a28a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=aejaz-memon-6erzQwfnCuo-unsplash.jpg&w=640", phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Alex", lastName: "Kazakevich", avatarImageUrl: "https://images.unsplash.com/photo-1563351672-62b74891a28a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=aejaz-memon-6erzQwfnCuo-unsplash.jpg&w=640"), phoneNumber: "+12221234444", nickName: "@Alex", place: "Work", sortOrder: 0, phoneCode: 1, locationCode: 222, ext: nil),
            ]), billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "7", callType: .incoming, phoneFromNumber: "+33123456789", phoneToNumber: "+21111234567", callStartDate: Date(timeIntervalSince1970: 1606802400), callEndDate: Date(timeIntervalSince1970: 1606802400), isExtension: false, isMissed: true, isCanceled: false,fromCountry: "France", fromExt: nil, contact: nil, billingInfo: BillingInfo(type: .phoneToApp, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "8", callType: .incoming, phoneFromNumber: "+442012345678", phoneToNumber: "+21111234567", callStartDate: Date(timeIntervalSince1970: 1601535600), callEndDate: Date(timeIntervalSince1970: 1601535600), isExtension: false, isMissed: false, isCanceled: false,fromCountry: "United Kingdom", fromExt: nil, contact: nil, billingInfo: BillingInfo(type: .phoneToApp, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD"))),
            
            RecentCall(callId: "9", callType: .outcoming, phoneFromNumber: "+11111234567", phoneToNumber: "+21111444444", callStartDate: Date(timeIntervalSince1970: 1598968800), callEndDate: Date(timeIntervalSince1970: 1598968800), isExtension: true, isMissed: false, isCanceled: false,fromCountry: nil, fromExt: "Extension 11", contact: nil, billingInfo: BillingInfo(type: .phoneToApp, pricePerMinute: 2, minutes: 60, currency: .usd(symbol: "$", code: "USD")))
        ]
    }
}

struct RecentPageExample_Previews: PreviewProvider {
    static var previews: some View {
        RecentPageExample()
    }
}
