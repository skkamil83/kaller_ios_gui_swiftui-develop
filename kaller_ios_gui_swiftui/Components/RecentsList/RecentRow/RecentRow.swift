//
//  RecentRow.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 04/12/2020.
//

import SwiftUI

struct RecentRow: View {
    let props: RecentRowProps
    let deletingButtonWidth: CGFloat = 74
    @State private var offset: CGSize = .zero
    @State private var isShowedDeleting = false
    
    var body: some View {
        ZStack{
            deletingView(props: props)
            VStack(spacing: 0){
                HStack(spacing: 0){
                    if(props.state == .edit){
                        deleteIcon(props: props)
                    }
                    avatar(props: props)
                    infoView(props: props)
                    Spacer()
                    timeText(props: props)
                    if(props.state == .view){
                        infoButton(props: props)
                    }
                }
                .frame(height: 59.5)
                divider(props: props)
            }
            .frame(width: UIScreen.main.bounds.width, height: 60)
            .background(Color.white)
            .offset(x: offset.width)
            .onTapGesture {
                props.onSelected()
            }
            .gesture(
                DragGesture()
                    .onChanged({ (gesture) in
                        if(props.state == .edit){
                            withAnimation{
                                isShowedDeleting = false
                                self.offset = .zero
                            }
                            return
                        }
                        if (isShowedDeleting){
                            let newOffsetWidth = -deletingButtonWidth + gesture.translation.width
                            if( newOffsetWidth <= 0){
                                offset.width = newOffsetWidth
                            }
                        }else{
                            if(gesture.translation.width < 0 && abs(gesture.translation.width) > abs(gesture.translation.height)){
                                offset = gesture.translation
                            }
                        }
                    })
                    .onEnded({ (_) in
                        if offset.width < -deletingButtonWidth/2 && props.state != .edit{
                            withAnimation{
                                isShowedDeleting = true
                                offset = CGSize(width: -deletingButtonWidth, height: 0)
                            }
                        } else {
                            withAnimation{
                                isShowedDeleting = false
                                self.offset = .zero
                            }
                        }
                    })
            )
            
        }
    }
    
    private func deletingView(props: RecentRowProps) -> some View{
        VStack(spacing: 0){
            HStack(spacing: 0){
                Color.tintsSystemRedLight
                Button(action: {
                    props.onDelete()
                    isShowedDeleting = false
                    self.offset = .zero
                }){
                    Text("Delete")
                        .font(TextStyle.subheadline1Default2Dark1LabelColor.font)
                        .foregroundColor(TextStyle.subheadline1Default2Dark1LabelColor.color)
                }
                .frame(width: deletingButtonWidth, height: 60)
                .background(Color.tintsSystemRedLight)
            }
        }
        .frame(width: UIScreen.main.bounds.width, height: 60)
        .background(Color.white)
    }
    
    private func deleteIcon(props: RecentRowProps) -> some View{
        Button(action: props.onDelete){
            Image("EditingDelete")
        }
        .padding(.leading, Spacing.spacing16Pt)
    }
    
    private func avatar(props: RecentRowProps) -> some View{
        AvatarWithSim(
            props: AvatarWithSimProps(
                avatar: RecentRowHelper.getAvatarStyle(contact: props.recentViewModel.contact),
                simNumber: RecentRowHelper.getSimNumber(recentViewModel: props.recentViewModel, accountInfo: props.accountInfo),
                isSimHidden: props.filterType == .defaultNumber,
                size: 44
            )
        )
        .padding(.leading, Spacing.spacing16Pt)
    }
    
    private func infoButton(props: RecentRowProps) -> some View{
        return
            Button(action: props.onShowInfo){
                Image("symbolInfo")
            }
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: Spacing.spacing16Pt))
    }
    
    private func timeText(props: RecentRowProps) -> some View{
        Text(props.recentViewModel.displayDate)
            .font(TextStyle.subheadline1Default1Light2SecondaryLabelColor.font)
            .foregroundColor(TextStyle.subheadline1Default1Light2SecondaryLabelColor.color)
            .padding(EdgeInsets(top: 0, leading: 17, bottom: 0, trailing: props.state == .view ? 10 : 6))
    }
    
    private func infoView(props: RecentRowProps) -> some View{
        let redTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.tintsSystemRedLight)
        let nameTextStyle = props.recentViewModel.isMissed ? redTextStyle : TextStyle.body2Bold1Light1LabelColor
        return
            VStack(alignment: .leading, spacing: 0){
                HStack(spacing: 0){
                    Text(RecentRowHelper.getNameOfPartner(recentViewModel: props.recentViewModel))
                        .font(nameTextStyle.font)
                        .foregroundColor(nameTextStyle.color)
                    Text(getNumberOfCall(props: props))
                        .font(nameTextStyle.font)
                        .foregroundColor(nameTextStyle.color)
                }
                .frame(height: 22)
                .padding(.top, Spacing.spacing10Pt)
                HStack(spacing: 4){
                    Image(getCallTypeImageName(props: props))
                        .frame(width: 16, height: 16)
                    Text(props.recentViewModel.place)
                        .font(TextStyle.subheadline1Default1Light2SecondaryLabelColor.font)
                        .foregroundColor(TextStyle.subheadline1Default1Light2SecondaryLabelColor.color)
                }
                .frame(height: 20)
                .padding(.bottom, Spacing.spacing8Pt)
            }
            .padding(.leading, 16)
    }
    
    private func divider(props: RecentRowProps) -> some View{
        Color.iOsSystemSeparatorsLightSeparatorCol
            .frame(height: 0.5)
            .padding(.leading, props.state == .edit ? 116 : Spacing.spacing76Pt)
    }
    
    private func getCallTypeImageName(props: RecentRowProps) -> String{
        guard let recentCall = props.recentViewModel.calls.first else {return ""}
        let callType = recentCall.billingInfo.type
        if props.recentViewModel.isMissed{
            return callType == .appToApp ? "CallLightMiss" : "CallLightMissGsm"
        }
        if props.recentViewModel.callType == .outcoming{
            return callType == .appToApp ? "CallLightOutgoing" : "CallLightOutgoingGsm"
        }else{
            return callType == .appToApp ? "CallLightIn" : "CallLightInGsm"
        }
    }
    
    private func getNumberOfCall(props: RecentRowProps) -> String{
        let count = props.recentViewModel.calls.count
        return count > 1 ? " (\(count))" : ""
    }
    
}

struct RecentRow_Previews: PreviewProvider {
    static var previews: some View {
        let accountInfo = AccountInfo(
            phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
            account: Account(id: 1, name: "Account name", createDate: Date()),
            devices: []
        )
        
        let recentCall = RecentCall( callId: "1", callType: .incoming, phoneFromNumber: "+44 20 1234 5678", phoneToNumber: "+11111234567", callStartDate: Date(),
                                     callEndDate: Date(), isExtension: false, isMissed: true, isCanceled: false,fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCallViewModel = RecentViewModel( calls: [recentCall, recentCall], displayDate: "16:04", isMissed: true, isCanceled: false, callType: .incoming,
                                                   contact: Contact(firstName: "Jayyyyyyyy",lastName: "Daaaaaaaaaaa", company: nil,avatarUrl: nil,
                             phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Jayyyyyyyy", lastName: "Daaaaaaaaaaa", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "United Kingdom",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]), isExtension: false, place: "United Kingdom")
        return RecentRow(
            props: RecentRowProps(
                recentViewModel: recentCallViewModel,
                accountInfo: accountInfo,
                state: .view,
                filterType: .defaultNumber,
                onDelete: {},
                onShowInfo: {},
                onSelected: {}
            )
        )
    }
}
