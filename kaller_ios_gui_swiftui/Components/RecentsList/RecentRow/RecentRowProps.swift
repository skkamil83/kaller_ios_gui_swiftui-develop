//
//  RecentRowProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 04/12/2020.
//

import Foundation

struct RecentRowProps{
    let recentViewModel: RecentViewModel
    let accountInfo: AccountInfo
    let state: RecentListState
    let filterType: RecentListFilterType
    let onDelete: () -> Void
    let onShowInfo: () -> Void
    let onSelected: () -> Void
}
