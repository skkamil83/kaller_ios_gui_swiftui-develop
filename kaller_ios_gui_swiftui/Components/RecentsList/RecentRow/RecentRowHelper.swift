//
//  RecentRowHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 04/12/2020.
//

import Foundation

class RecentRowHelper{
    static func getAvatarStyle(contact: Contact?) -> AvatarViewProps.Style {
        guard let contact = contact else {
            return .image(imageName: "avatarUnknown")
        }
        if let url = contact.avatarUrl{
            return .bitmapImage(urlString: url)
        }else{
            if contact.firstName != nil || contact.lastName != nil {
                return .name(firstName: contact.firstName ?? "", lastName: contact.lastName ?? "", company: "")
            }
        }
        return .image(imageName: "avatarUnknown")
    }
    
    static func getSimNumber(recentViewModel: RecentViewModel, accountInfo: AccountInfo) -> Int{
        guard let recentCall = recentViewModel.calls.first else {return 0}
        let phone = recentViewModel.callType == .incoming ? recentCall.phoneToNumber : recentCall.phoneFromNumber
        if let phoneInfo = accountInfo.phones.first(where: {$0.phoneNumber == phone}){
            return accountInfo.getSimNumber(phone: phoneInfo) ?? 0
        }
        return 0
    }
    
    static func getNameOfPartner(recentViewModel: RecentViewModel) -> String{
        var result = ""

        if let contact = recentViewModel.contact{
            if let firstName = contact.firstName{
                result += firstName.isEmpty ? "" : "\(firstName) "
            }
            
            if let lastName = contact.lastName{
                result += lastName
            }
        }
        
        if result == ""{
            if let recentCall = recentViewModel.calls.first{
                return recentViewModel.callType == .incoming ? recentCall.phoneFromNumber : recentCall.phoneToNumber
            }
        }
        
        return result
    }
}
