//
//  RecentsListViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 05/12/2020.
//

import Foundation

struct RecentsListViewProps{
    let filterType: RecentListFilterType
    let recentCalls: [RecentViewModel]
    let accountInfo: AccountInfo
    let state: RecentListState
    let onDelete: (RecentViewModel) -> Void
    let onShowInfo: (RecentViewModel) -> Void
    let onSelected: (RecentCall) -> Void
    let onFilter: () -> Void
}
