//
//  RecentsListView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 05/12/2020.
//

import SwiftUI

struct RecentsListView: View {
    let props: RecentsListViewProps
    
    @State private var defaultListFrame: CGRect = .zero
    @State private var isShowFilteredView = false
    
    private let filteredViewHeight: CGFloat = 44
    
    var body: some View {
        if(props.recentCalls.count > 0){
            list(props: props)
        }else{
            emptyText(props: props)
        }
    }
    
    private func list(props: RecentsListViewProps) -> some View{
        ScrollView{
            VStack(spacing: 0){
                filteredView(props: props)
                Divider()
                LazyVStack(spacing: 0){
                    ForEach(0..<props.recentCalls.count, id: \.self) { index in
                        let recentCall = props.recentCalls[index]
                        RecentRow(
                            props: RecentRowProps(
                                recentViewModel: recentCall,
                                accountInfo: props.accountInfo,
                                state: props.state,
                                filterType: props.filterType,
                                onDelete: {
                                    props.onDelete(recentCall)
                                },
                                onShowInfo: {
                                    props.onShowInfo(recentCall)
                                },
                                onSelected: {
                                    if let call = recentCall.calls.first{
                                        props.onSelected(call)
                                    }
                                }
                            )
                        )
                    }
                }
            }
            .background(collectListInfo())
        }
        .frame(width: UIScreen.main.bounds.width)
    }
    
    private func emptyText(props: RecentsListViewProps) -> some View{
        Text("No Recents")
            .font(TextStyle.title21Default1Light2SecondaryLabelColor.font)
            .foregroundColor(TextStyle.title21Default1Light2SecondaryLabelColor.color)
    }
    
    private func filteredView(props: RecentsListViewProps) -> some View{
        HStack(spacing: 0){
            AvatarView(
                props: AvatarViewProps(
                    size: 34,
                    style: .blueGlyph(systemImageName: "tray.full.fill"
                    )
                )
            )
                .padding(.leading, Spacing.spacing16Pt)
            Text("Filtered by Default Number")
                .font(TextStyle.body1Default1Light5Blue.font)
                .foregroundColor(TextStyle.body1Default1Light5Blue.color)
                .padding(.leading, Spacing.spacing8Pt)
            Spacer()
        }
        .frame(height: isShowFilteredView ? filteredViewHeight : 0)
        .opacity(isShowFilteredView ? 1 : 0)
        .animation(.easeIn)
        .onTapGesture {
            props.onFilter()
        }
    }
    
    private func collectListInfo() -> some View{
        return GeometryReader{ geometry -> Color in
            let rect = geometry.frame(in: .global)
            DispatchQueue.main.async {
                if(defaultListFrame.origin.y <= 0){
                    defaultListFrame = rect
                }else{
                    if(rect.origin.y - defaultListFrame.origin.y > filteredViewHeight){
                        isShowFilteredView = true
                    }else if (rect.origin.y - defaultListFrame.origin.y < -filteredViewHeight){
                        isShowFilteredView = false
                    }
                }
            }
            return Color.clear
        }
    }
}

struct RecentsListView_Previews: PreviewProvider {
    static var previews: some View {
        let accountInfo = AccountInfo(
            phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
            account: Account(id: 1, name: "Account name", createDate: Date()),
            devices: []
        )
        let recentCall = RecentCall( callId: "1", callType: .incoming, phoneFromNumber: "+44 20 1234 5678", phoneToNumber: "+11111234567", callStartDate: Date(),
                                     callEndDate: Date(), isExtension: false, isMissed: true,
                                     isCanceled: false,
                                     fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCallViewModel = RecentViewModel( calls: [recentCall, recentCall], displayDate: "16:04", isMissed: true, isCanceled: false, callType: .incoming,
                                                   contact: Contact(firstName: "Jayyyyyyyy",lastName: "Daaaaaaaaaaa", company: nil,avatarUrl: nil,
                             phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Jayyyyyyyy", lastName: "Daaaaaaaaaaa", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "United Kingdom",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]), isExtension: false, place: "United Kingdom")
        
        return RecentsListView(
            props: RecentsListViewProps(
                filterType: .defaultNumber,
                recentCalls: [recentCallViewModel, recentCallViewModel],
                accountInfo: accountInfo,
                state: .view,
                onDelete: {_ in},
                onShowInfo: {_ in},
                onSelected: {_ in},
                onFilter: {}
            )
        )
    }
}
