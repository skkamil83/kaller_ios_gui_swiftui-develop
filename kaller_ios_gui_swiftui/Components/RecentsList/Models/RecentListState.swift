//
//  RecentListState.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 05/12/2020.
//

import Foundation

enum RecentListState{
    case view
    case edit
}
