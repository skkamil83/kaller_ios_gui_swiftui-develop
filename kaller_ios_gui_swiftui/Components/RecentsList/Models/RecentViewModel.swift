struct RecentViewModel {
    let calls: [RecentCall]
    let displayDate: String
    let isMissed: Bool
    let isCanceled: Bool
    let callType: CallType
    let contact: Contact?
    let isExtension: Bool
    let place: String
}
