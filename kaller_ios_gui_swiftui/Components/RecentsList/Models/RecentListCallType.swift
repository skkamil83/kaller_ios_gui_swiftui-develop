//
//  RecentListCallType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 08/12/2020.
//

import Foundation

enum RecentListCallType: Int{
    case all = 0
    case missed = 1
}
