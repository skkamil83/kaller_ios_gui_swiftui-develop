import SwiftUI

struct SearchBarView: View {
    let props: SearchBarViewProps
    @State private var isResponding = false

    var body: some View {
        VStack {
            HStack {
                Image(systemName: "magnifyingglass")
                        .font(TextStyle.body1Default1Light2SecondaryLabelColor.font)
                        .foregroundColor(TextStyle.body1Default1Light2SecondaryLabelColor.color)
                TextFiledWithCleaner(
                        props: TextFiledWithCleanerProps(
                                text: props.query,
                                placeHolder: props.placeHolder,
                                onFocus: { isFocus in
                                    isResponding = isFocus
                                    props.onFocus(isFocus)
                                }
                        )
                )
                        .keyboardType(.webSearch)
            }
                    .padding(7)
                    .background(Color.fillsLightTertiaryFillColor.cornerRadius(10))
                    .modifier(ClearSearchButton(text: props.query, isResponding: isResponding))
        }
                .accessibility(identifier: "searchField")
    }
}
