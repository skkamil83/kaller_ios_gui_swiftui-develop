import SwiftUI

struct ClearSearchButton: ViewModifier {
    @Binding var text: String
    var isResponding: Bool

    public func body(content: Content) -> some View {
        HStack {
            content
                    .padding(.trailing, 4)

            if isResponding || !text.isEmpty {
                Button(action: {
                    self.text = ""
                    UIApplication.shared.endEditing()
                }) {
                    Text("Cancel")
                            .font(TextStyle.body3Button1Light5Blue.font)
                            .foregroundColor(isResponding ? TextStyle.body3Button1Light5Blue.color : Color(UIColor.opaqueSeparator))
                }
            }
        }
    }
}

