import SwiftUI

struct SearchBarViewProps {
    let query: Binding<String>
    let placeHolder: String
    let onFocus: (Bool) -> Void
}
