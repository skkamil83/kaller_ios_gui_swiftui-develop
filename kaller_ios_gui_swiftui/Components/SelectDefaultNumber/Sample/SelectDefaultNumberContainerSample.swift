import SwiftUI

struct SelectDefaultNumberContainerSample: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject private var userInfoState: UserInfoState

    var body: some View {
        view()
                .onAppear() {
                    updateAccountInfo()
                }
    }

    private func view() -> some View {
        if userInfoState.accountInfo != nil {
            return AnyView(
                    SelectDefaultNumberContainer(
                            props: SelectDefaultNumberContainerProps(
                                    onClose: {
                                        presentationMode.wrappedValue.dismiss()
                                    }
                            )
                    )
                            .environmentObject(userInfoState)
            )
        } else {
            return AnyView(EmptyView())
        }
    }

    private func updateAccountInfo() {
        userInfoState.accountInfo = AccountInfo(
                phones: [
                    PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                account: Account(id: 1, name: "Account name", createDate: Date()),
                devices: []
        )
    }
}

struct SelectDefaultNumberContainerSample_Previews: PreviewProvider {
    static var previews: some View {
        SelectDefaultNumberContainerSample()
    }
}
