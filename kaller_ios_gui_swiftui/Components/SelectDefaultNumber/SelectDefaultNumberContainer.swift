import SwiftUI

struct SelectDefaultNumberContainerProps {
    let onClose: () -> Void
}

struct SelectDefaultNumberContainer: View {
    let props: SelectDefaultNumberContainerProps
    @EnvironmentObject private var userInfoState: UserInfoState

    var body: some View {
        SelectDefaultNumberView(
                props: SelectDefaultNumberViewProps(
                        accountInfo: userInfoState.accountInfo!,
                        onSelect: setPhoneInfoIsDefault(phoneInfo:),
                        onAddSimNumber: onAddSimNumber,
                        onGetLocalNumber: onGetLocalNumber,
                        onClose: onClose,
                        onSorted: onSorted,
                        onViewInfo: onViewInfo
                )
        )
    }

    private func setPhoneInfoIsDefault(phoneInfo: PhoneInfo) {
        if let _ = userInfoState.accountInfo {
            if let defaultPhone = userInfoState.accountInfo!.defaultPhone {
                if let indexPhoneInfo = userInfoState.accountInfo!.phones.firstIndex(where: { $0.phoneNumber == phoneInfo.phoneNumber }) {
                    if let indexDefaultPhone = userInfoState.accountInfo!.phones.firstIndex(where: { $0.phoneNumber == defaultPhone.phoneNumber }) {
                        userInfoState.accountInfo!.phones[indexPhoneInfo].isDefault = true
                        userInfoState.accountInfo!.phones[indexDefaultPhone].isDefault = false
                    }
                }
            }
        }
        onClose()
    }

    private func onAddSimNumber() {
        print(#function)
    }

    private func onGetLocalNumber() {
        print(#function)
    }

    private func onClose() {
        props.onClose()
    }

    private func onSorted(phones: [PhoneInfo]) {
        userInfoState.accountInfo?.phones = phones
    }

    private func onViewInfo(phone: PhoneInfo) {
        print("view info: \(phone.phoneNumber)")
    }
}

struct SelectDefaultNumberContainer_Previews: PreviewProvider {
    static var previews: some View {
        let userInfoState = UserInfoState()
        userInfoState.accountInfo = AccountInfo(phones: [
            PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
            PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
            PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                account: Account(id: 1, name: "Account name", createDate: Date()), devices: []
        )
        return SelectDefaultNumberContainer(
                props: SelectDefaultNumberContainerProps(
                        onClose: {}
                )
        )
                .environmentObject(userInfoState)
    }
}
