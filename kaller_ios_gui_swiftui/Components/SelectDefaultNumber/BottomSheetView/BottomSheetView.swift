import SwiftUI

struct BottomSheetView<Content: View>: View {

    @State private var lastDragPosition: DragGesture.Value?
    @Binding private var spacingTop: CGFloat
    private let heightToClose: CGFloat = 122
    private let maxHeight: CGFloat = UIScreen.main.bounds.height - 54
    private let midleHeight: CGFloat = UIScreen.main.bounds.height / 2
    private let content: Content
    private let speed: CGFloat = 500
    let props: BottomSheetViewProps

    init(props: BottomSheetViewProps, @ViewBuilder content: () -> Content) {
        self.props = props
        self.content = content()
        self._spacingTop = props.spacingTop
    }

    var body: some View {
        ZStack {
            Color.black.opacity(0.4)
            VStack(spacing: 0) {
                Spacer()
                headerView(props: props)
                Divider()
                content
            }
                    .padding(.top, spacingTop)
        }
                .edgesIgnoringSafeArea(.all)
    }

    private func headerView(props: BottomSheetViewProps) -> some View {
        ZStack {
            dragIndicator()
            VStack {
                Spacer()
                ZStack {
                    leadingItem(props: props)
                    title(props: props)
                    trailingItem(props: props)
                }
                        .padding(.bottom, 11)
            }
                    .frame(width: UIScreen.main.bounds.width)
        }
                .frame(width: UIScreen.main.bounds.width, height: 57)
                .background(backgroundHeader())
                .gesture(
                        DragGesture()
                                .onChanged(onDrag(gesture:))
                                .onEnded(onEndDrag(gesture:))
                )
    }

    private func backgroundHeader() -> some View {
        RoundedCorners(
                color: .navigationBarBackgroundChrome,
                topLeft: 14,
                topRight: 14,
                bottomLeft: 0,
                bottomRight: 0
        )
    }

    private func dragIndicator() -> some View {
        VStack {
            Color.graysSystemGray4Light
                    .frame(width: 51, height: 4, alignment: .center)
                    .cornerRadius(2.5)
                    .padding(.top, 8)
            Spacer()
        }
    }

    private func title(props: BottomSheetViewProps) -> some View {
        Text(props.title)
                .textStyle(.body2Bold1Light1LabelColor)
    }

    private func leadingItem(props: BottomSheetViewProps) -> some View {
        if let title = props.leadingItemTitle {
            return AnyView(
                    HStack {
                        Button(action: {
                            props.onLeadingItemSelected?()
                        }) {
                            Text(title)
                                    .textStyle(.body3Button1Light5Blue)
                        }
                                .padding(.leading, 16)
                        Spacer()
                    }
            )
        } else {
            return AnyView(EmptyView())
        }
    }

    private func trailingItem(props: BottomSheetViewProps) -> some View {
        if let title = props.trailingItemTitle {
            return AnyView(
                    HStack {
                        Spacer()
                        Button(action: {
                            props.onTrailingItemSelected?()
                        }) {
                            Text(title)
                                    .textStyle(.body3Button1Light5Blue)
                        }
                                .padding(.trailing, 16)
                    }
            )
        } else {
            return AnyView(EmptyView())
        }
    }

    private func onDrag(gesture: DragGesture.Value) {
        let newSpacing = gesture.translation.height + spacingTop
        if newSpacing >= UIScreen.main.bounds.height - maxHeight {
            spacingTop = newSpacing
        }
        lastDragPosition = gesture
    }

    private func onEndDrag(gesture: DragGesture.Value) {
        let timeDiff = gesture.time.timeIntervalSince(self.lastDragPosition?.time ?? Date())
        let speed: CGFloat = CGFloat(gesture.translation.height - self.lastDragPosition!.translation.height) / CGFloat(timeDiff)
        if spacingTop > UIScreen.main.bounds.height - heightToClose {
            withAnimation {
                spacingTop = UIScreen.main.bounds.height + 122 + 58
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    props.onClose()
                }
            }
        } else if speed > self.speed {
            withAnimation {
                spacingTop = UIScreen.main.bounds.height - maxHeight
            }
        } else if speed < -self.speed {
            withAnimation {
                spacingTop = UIScreen.main.bounds.height - midleHeight
            }
        } else if spacingTop < (UIScreen.main.bounds.height - midleHeight) / 2 {
            withAnimation {
                spacingTop = UIScreen.main.bounds.height - maxHeight
            }
        } else {
            withAnimation {
                spacingTop = UIScreen.main.bounds.height - midleHeight
            }
        }
    }
}

struct BottomSheetView_Previews: PreviewProvider {
    static var previews: some View {
        BottomSheetView(
                props: BottomSheetViewProps(
                        spacingTop: .constant(54),
                        title: "My Phone Numbers",
                        leadingItemTitle: nil,
                        trailingItemTitle: nil,
                        onLeadingItemSelected: {},
                        onTrailingItemSelected: {},
                        onClose: {})
        ) {
            VStack {
                Color.red
                Color.blue
            }
        }

    }
}
