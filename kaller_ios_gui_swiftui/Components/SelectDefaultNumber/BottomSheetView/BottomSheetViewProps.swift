import SwiftUI

struct BottomSheetViewProps {
    let spacingTop: Binding<CGFloat>
    let title: String
    let leadingItemTitle: String?
    let trailingItemTitle: String?
    let onLeadingItemSelected: (() -> Void)?
    let onTrailingItemSelected: (() -> Void)?
    let onClose: () -> Void
}
