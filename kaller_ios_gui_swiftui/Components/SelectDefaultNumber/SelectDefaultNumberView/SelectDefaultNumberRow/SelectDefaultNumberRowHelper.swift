class SelectDefaultNumberRowHelper {
    static func getAvatarStyle(phoneInfo: PhoneInfo) -> AvatarViewProps.Style {
        if phoneInfo.userInfo.avatarImageUrl?.isEmpty ?? true {
            return .name(firstName: phoneInfo.userInfo.firstName ?? "", lastName: phoneInfo.userInfo.lastName ?? "", company: "")
        } else {
            return .bitmapImage(urlString: phoneInfo.userInfo.avatarImageUrl ?? "")
        }
    }
}
