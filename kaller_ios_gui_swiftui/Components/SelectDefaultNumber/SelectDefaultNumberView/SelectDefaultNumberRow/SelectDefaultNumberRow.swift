import SwiftUI

struct SelectDefaultNumberRow: View {
    let props: SelectDefaultNumberRowProps

    var body: some View {
        VStack(spacing: 0) {
            HStack(spacing: 16) {
                avatarWithSim(props: props)
                content(props: props)
                Spacer()
                checkMark(props: props)
            }
                    .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
            Divider()
                    .padding(.leading, 16)
        }
                .frame(height: 72)
                .background(Color.white)
    }

    private func avatarWithSim(props: SelectDefaultNumberRowProps) -> some View {
        AvatarWithSim(
                props: AvatarWithSimProps(
                        avatar: SelectDefaultNumberRowHelper.getAvatarStyle(phoneInfo: props.phoneInfo),
                        simNumber: props.accountInfo.getSimNumber(phone: props.phoneInfo) ?? 1,
                    isSimHidden: false,
                    size: 44
                )
        )
    }

    private func content(props: SelectDefaultNumberRowProps) -> some View {
        let phoneNumberTextStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        if let nickName = props.phoneInfo.nickName {
            return AnyView(
                    ZStack(alignment: .leading) {
                        VStack {
                            Text(props.phoneInfo.place)
                                    .frame(height: 20)
                                    .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                                    .padding(.top, 6)
                            Spacer()
                        }
                        VStack {
                            Text(props.phoneInfo.phoneNumber)
                                    .frame(height: 22)
                                    .textStyle(phoneNumberTextStyle)
                                    .padding(.top, 24)
                            Spacer()
                        }
                        VStack {
                            Text(nickName)
                                    .frame(height: 18)
                                    .textStyle(.footnote1Default1Light2SecondaryLabelColor)
                                    .padding(.top, 44)
                            Spacer()
                        }
                    }
                            .frame(height: 71)
            )
        } else {
            return AnyView(
                    VStack(alignment: .leading, spacing: 0) {
                        Text(props.phoneInfo.place)
                                .frame(height: 20)
                                .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                                .padding(.top, 13)
                        Text(props.phoneInfo.phoneNumber)
                                .frame(height: 22)
                                .textStyle(phoneNumberTextStyle)
                        Spacer()
                    }
                            .frame(height: 71)
            )
        }
    }

    private func checkMark(props: SelectDefaultNumberRowProps) -> some View {
        if let defaultPhone = props.accountInfo.defaultPhone {
            if defaultPhone.phoneNumber == props.phoneInfo.phoneNumber {
                return AnyView(Image("checkmark"))
            }
        }
        return AnyView(EmptyView())
    }

}

struct SelectDefaultNumberRow_Previews: PreviewProvider {
    static var previews: some View {
        SelectDefaultNumberRow(
                props: SelectDefaultNumberRowProps(
                        accountInfo: AccountInfo(
                            phones: [
                                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                                account: Account(id: 1, name: "Account name", createDate: Date()), devices: []
                        ),
                    phoneInfo: PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: nil), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil)
                )
        )
    }
}
