import SwiftUI

struct SelectDefaultNumberView: View {
    @Environment(\.editMode) var editMode
    @State private var isCalculatedHeight = false
    @State private var spacingTop: CGFloat = UIScreen.main.bounds.height / 2
    @State private var phonesEditMode = [PhoneInfo]()
    @State private var defaultSortOrder: Int = 0
    let props: SelectDefaultNumberViewProps

    var body: some View {
        BottomSheetView(
                props: BottomSheetViewProps(
                        spacingTop: $spacingTop,
                        title: editMode?.wrappedValue == .active ? "My Phone Numbers" : "Set default number",
                        leadingItemTitle: editMode?.wrappedValue == .active ? "Cancel" : nil,
                        trailingItemTitle: editMode?.wrappedValue == .active ? "Done" : "Edit",
                        onLeadingItemSelected: onLeadingItemTap,
                        onTrailingItemSelected: onTrailingItemTap,
                        onClose: props.onClose
                )
        ) {
            view(props: props)
        }
    }

    private func view(props: SelectDefaultNumberViewProps) -> some View {
        ZStack {
            VStack(spacing: 0) {
                if editMode?.wrappedValue == .active {
                    listEditMode(props: props)
                } else {
                    list(props: props)
                }
                Color.white
                        .frame(height: 122)
            }
            if editMode?.wrappedValue == .inactive {
                actionsView(props: props)
            }
        }
    }

    private func list(props: SelectDefaultNumberViewProps) -> some View {
        ScrollView {
            LazyVStack(spacing: 0) {
                ForEach(props.accountInfo.sortedPhones, id: \.phoneNumber) { phoneInfo in
                    SelectDefaultNumberRow(
                            props: SelectDefaultNumberRowProps(
                                    accountInfo: props.accountInfo,
                                    phoneInfo: phoneInfo
                            )
                    )
                            .onTapGesture {
                                props.onSelect(phoneInfo)
                            }
                }
            }
                    .background(calculateHeightOnStartUp())
        }
                .background(Color.white)
    }

    private func listEditMode(props: SelectDefaultNumberViewProps) -> some View {
        List {
            ForEach(phonesEditMode, id: \.phoneNumber) { phoneInfo in
                SortDefaultNumberRow(
                        props: SortDefaultNumberRowProps(
                                numberOfSim: PhoneHelper.getSimNumber(phones: phonesEditMode, phone: phoneInfo) ?? 1,
                                phoneInfo: phoneInfo,
                                onViewInfo: {
                                    props.onViewInfo(phoneInfo)
                                }
                        )
                )
                        .listRowInsets(EdgeInsets(.init(top: -1, leading: -1, bottom: -1, trailing: -1)))
                        .background(Color.white)
            }
                    .onMove(perform: move)
        }
                .id(UUID())
    }

    private func actionsView(props: SelectDefaultNumberViewProps) -> some View {
        VStack {
            Spacer()
            VStack {
                HStack {
                    Button(action: props.onAddSimNumber) {
                        Text("Add SIM Number")
                                .textStyle(.body1Default1Light5Blue)
                    }
                            .padding(EdgeInsets(top: 12, leading: 16, bottom: 10, trailing: 0))
                    Spacer()
                }
                HStack {
                    Button(action: props.onGetLocalNumber) {
                        Text("Get Local Number")
                                .textStyle(.body1Default1Light5Blue)
                    }
                            .padding(EdgeInsets(top: 12, leading: 16, bottom: 10, trailing: 0))
                    Spacer()
                }
                Spacer()
            }
                    .frame(width: UIScreen.main.bounds.width, height: 122)
                    .background(Color.white)
        }
    }

    private func move(from source: IndexSet, to destination: Int) {
        phonesEditMode.move(fromOffsets: source, toOffset: destination)
        for i in 0..<phonesEditMode.count {
            phonesEditMode[i].sortOrder = defaultSortOrder + i
        }
    }

    private func onLeadingItemTap() {
        if editMode?.wrappedValue == .active {
            onCancelSorting()
        }
    }

    private func onTrailingItemTap() {
        if editMode?.wrappedValue == .active {
            onDoneSorting()
        } else {
            onEdit()
        }
    }

    private func onCancelSorting() {
        self.editMode?.wrappedValue = .inactive
    }

    private func onDoneSorting() {
        props.onSorted(phonesEditMode)
        self.editMode?.wrappedValue = .inactive
    }

    private func onEdit() {
        defaultSortOrder = props.accountInfo.defaultPhone?.sortOrder ?? 0
        phonesEditMode = props.accountInfo.sortedPhones
        withAnimation {
            self.editMode?.wrappedValue = .active
        }
    }

    private func calculateHeightOnStartUp() -> some View {
        return GeometryReader { geometry -> Color in
            if !isCalculatedHeight {
                let rect = geometry.frame(in: .global)
                DispatchQueue.main.async {
                    isCalculatedHeight = true
                    let height = rect.height + 58 + 122
                    if (height > UIScreen.main.bounds.height / 2) {
                        self.spacingTop = 54
                    } else {
                        self.spacingTop = UIScreen.main.bounds.height / 2
                    }
                }
            }
            return Color.clear
        }
    }
}

struct SelectDefaultNumberView_Previews: PreviewProvider {
    static var previews: some View {
        SelectDefaultNumberView(
                props: SelectDefaultNumberViewProps(
                        accountInfo: AccountInfo(
                                phones: [
                                    PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640"), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil),
                                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+211234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)
                                ],

                                account: Account(id: 1, name: "Account name", createDate: Date()),
                                devices: []
                        ),
                        onSelect: { _ in },
                        onAddSimNumber: {},
                        onGetLocalNumber: {},
                        onClose: {},
                        onSorted: { _ in },
                        onViewInfo: { _ in }
                )
        )
    }
}
