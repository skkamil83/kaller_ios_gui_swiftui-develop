struct SelectDefaultNumberViewProps {
    let accountInfo: AccountInfo
    let onSelect: (PhoneInfo) -> Void
    let onAddSimNumber: () -> Void
    let onGetLocalNumber: () -> Void
    let onClose: () -> Void
    let onSorted: ([PhoneInfo]) -> Void
    let onViewInfo: (PhoneInfo) -> Void
}
