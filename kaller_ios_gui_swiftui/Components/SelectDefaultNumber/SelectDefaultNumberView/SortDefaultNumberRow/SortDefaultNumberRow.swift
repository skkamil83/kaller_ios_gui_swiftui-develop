import SwiftUI

struct SortDefaultNumberRow: View {
    let props: SortDefaultNumberRowProps

    var body: some View {
        ZStack {
            HStack(spacing: 0) {
                sim(props: props)
                Spacer()
            }
                    .offset(x: -24)
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    avatar(props: props)
                    content(props: props)
                    Spacer()
                }
                Divider()
            }
                    .offset(x: 6)
        }
                .frame(width: UIScreen.main.bounds.width, height: 74)
                .onTapGesture {
                    props.onViewInfo()
                }
    }

    private func sim(props: SortDefaultNumberRowProps) -> some View {
        SimIcon(
                props: SimIconProps(
                        number: props.numberOfSim,
                        size: CGSize(width: 14, height: 18
                        )
                )
        )
    }

    private func avatar(props: SortDefaultNumberRowProps) -> some View {
        AvatarView(
                props: AvatarViewProps(
                        size: 44,
                        style: SelectDefaultNumberRowHelper.getAvatarStyle(phoneInfo: props.phoneInfo)
                )
        )
    }

    private func content(props: SortDefaultNumberRowProps) -> some View {
        if let nickName = props.phoneInfo.nickName {
            return AnyView(
                    contentWithNickname(props: props, nickName: nickName)
            )
        } else {
            return AnyView(
                    contentWithoutNickName(props: props)
            )
        }
    }

    private func contentWithNickname(props: SortDefaultNumberRowProps, nickName: String) -> some View {
        ZStack(alignment: .leading) {
            VStack {
                Text(props.phoneInfo.place)
                        .frame(height: 20)
                        .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                        .padding(.top, 6)
                Spacer()
            }
            VStack {
                Text(props.phoneInfo.phoneNumber)
                        .frame(height: 22)
                        .textStyle(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black))
                        .padding(.top, 24)
                Spacer()
            }
            VStack {
                Text(nickName)
                        .frame(height: 18)
                        .textStyle(.footnote1Default1Light2SecondaryLabelColor)
                        .padding(.top, 44)
                Spacer()
            }
        }
                .frame(height: 71)
                .padding(.leading, 16)
    }

    private func contentWithoutNickName(props: SortDefaultNumberRowProps) -> some View {
        VStack(alignment: .leading, spacing: 0) {
            Text(props.phoneInfo.place)
                    .frame(height: 20)
                    .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                    .padding(.top, 13)
            Text(props.phoneInfo.phoneNumber)
                    .frame(height: 22)
                    .textStyle(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black))
            Spacer()
        }
                .frame(height: 71)
                .padding(.leading, 16)
    }

}

struct SortDefaultNumberRow_Previews: PreviewProvider {
    static var previews: some View {
        SortDefaultNumberRow(
                props: SortDefaultNumberRowProps(
                        numberOfSim: 1,
                    phoneInfo: PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                        onViewInfo: {}
                )
        )
    }
}
