import SwiftUI

struct PhoneNumberFilterView: View {
    
    let props: PhoneNumberFilterViewProps
    
    var body: some View {
        HStack(spacing: Spacing.spacing8Pt) {
            icon()
            title(props: props)
            Spacer()
        }
                .frame(height: 44)
                .animation(.easeIn)
                .onTapGesture {
                    props.onSelected()
                }
    }
    
    private func icon() -> some View {
        AvatarView(
                props: AvatarViewProps(
                        size: 34,
                        style: .blueGlyph(systemImageName: "tray.full.fill"
                        )
                )
        )
                .padding(.leading, Spacing.spacing16Pt)
    }
    
    private func title(props: PhoneNumberFilterViewProps) -> some View {
        Text(props.filterType.title)
                .textStyle(.body1Default1Light5Blue)
    }
}

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        PhoneNumberFilterView(
            props: PhoneNumberFilterViewProps(
                filterType: .allNumbers,
                onSelected: {
                    
                }
            )
        )
    }
}
