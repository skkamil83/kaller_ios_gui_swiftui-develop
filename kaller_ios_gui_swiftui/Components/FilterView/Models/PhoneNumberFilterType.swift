enum PhoneNumberFilterType {
    case allNumbers
    case defaultNumber

    var title: String {
        switch self {
        case .allNumbers:
            return "Filtered by All Numbers"
        case .defaultNumber:
            return "Filtered by Default Numbers"
        }
    }
}
