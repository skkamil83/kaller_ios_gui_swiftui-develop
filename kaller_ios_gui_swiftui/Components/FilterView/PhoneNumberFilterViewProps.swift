struct PhoneNumberFilterViewProps {
    let filterType: PhoneNumberFilterType
    let onSelected: () -> Void
}
