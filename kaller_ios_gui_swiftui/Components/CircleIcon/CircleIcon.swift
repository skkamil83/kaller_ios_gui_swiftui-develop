//
//  FlagView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 16/11/2020.
//

import SwiftUI

struct CircleIcon: View {
    let props: CircleIconProps
    
    var body: some View {
        getView(props: props)
            
    }
    
    private func getView(props: CircleIconProps) -> some View{
        switch props.style{
        
        case .filled(let imageName):
            return AnyView(
                Image(imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: props.size, height: props.size)
                    .cornerRadius(props.size/2)
            )
        case .systemImage(let systemImageName, let font, let backgroundColor, let foregroundColor):
            return AnyView(
                Image(systemName: systemImageName)
                    .frame(width: props.size, height: props.size)
                    .font(font)
                    .foregroundColor(foregroundColor)
                    .background(backgroundColor)
                    .cornerRadius(props.size/2)
            )
        }
    }
}

struct CircleIcon_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 10){
            CircleIcon(props: CircleIconProps(size: 168, style: .filled(imageName: "united-kingdom")))
            CircleIcon(props: CircleIconProps(size: 32, style: .systemImage(systemImageName: "info", font: .custom("SFProDisplay-Regular", size: 17), backgroundColor: Color.black.opacity(0.5), foregroundColor: .white)))
        }
    }
}
