//
//  FileIconProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Farid Guliyev on 07.12.2020.
//

import SwiftUI

struct FileIconProps {
    let color: Color
    let text: String
    let size: CGFloat
}
