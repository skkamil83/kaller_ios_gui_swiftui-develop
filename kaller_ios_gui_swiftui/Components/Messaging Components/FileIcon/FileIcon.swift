//
//  FileIcon.swift
//  kaller_ios_gui_swiftui
//
//  Created by Farid Guliyev on 07.12.2020.
//

import SwiftUI

struct FileIcon: View {
    
    let props: FileIconProps
    
    var body: some View {
        ZStack {
            fileIcon(color: props.color)
            fileExtension(fontSize: props.size / 2)
        }.frame(width: props.size, height: props.size)
    }
    
    
    private func fileIcon(color: Color) -> some View{
        Image("messagingFileIcon")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .foregroundColor(color)
    }
    
    
    private func fileExtension(fontSize: CGFloat) -> some View {
        Text(props.text)
            .multilineTextAlignment(.center)
            .foregroundColor(.white)
            .font(.system(size: fontSize, weight: .semibold))
            .padding(props.size * 20 / 120)
            .lineLimit(1)
            .minimumScaleFactor(0.1)
    }
}

struct FileIcon_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 10){
            FileIcon(props: FileIconProps(color: .blue, text: "DOCX", size: 120))
            FileIcon(props: FileIconProps(color: .green, text: "TIFF", size: 30))
            FileIcon(props: FileIconProps(color: .gray, text: "SVG", size: 24))
        }
    }
}
