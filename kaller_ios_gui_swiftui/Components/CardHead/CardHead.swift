//
//  CardHead.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 16/11/2020.
//

import SwiftUI

struct CardHead: View {
    let props: CardHeadProps
    
    var body: some View {
        HStack(spacing: Spacing.spacing8Pt){
            flag(props: props)
            ZStack(alignment: .leading){
                locationName(props: props)
                HStack(spacing: Spacing.spacing4Pt){
                    phoneNumber(props: props)
                    if(!props.isEnable){
                        exclamationMark()
                    }
                }
                .padding(.top, 13)
            }
        }
        .frame(height: 35)
    }
    
    private func flag(props: CardHeadProps) -> some View{
        CircleIcon(props: CircleIconProps(size: Spacing.spacing30Pt, style: .filled(imageName: props.flagImageName)))
            .opacity(props.isEnable ? 1 : 0.5)
    }
    
    private func locationName(props: CardHeadProps) -> some View{
        let textStyle = props.isEnable ? TextStyle.caption11Default1Light2SecondaryLabelColor : TextStyle.caption11Default1Light3TertiaryLabelColor
        return Text(CardHeadHelper.getLocationText(countryName: props.countryName, cityName: props.locationName))
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.bottom, 19)
    }
    
    private func phoneNumber(props: CardHeadProps) -> some View{
        let textStyle = props.isEnable ? TextStyle.body2Bold1Light1LabelColor : TextStyle.body2Bold1Light3TertiaryLabelColor
        return Text(props.phoneNumber)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
    }
    
    private func exclamationMark() -> some View{
        CircleIcon(props: CircleIconProps(size: 18, style: .systemImage(systemImageName: "exclamationmark.circle.fill", font: TextStyle.subheadline2Bold1Light6SystemRed.font, backgroundColor: .white, foregroundColor: TextStyle.subheadline2Bold1Light6SystemRed.color)))
    }
}

struct CardHead_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            CardHead(props: CardHeadProps(isEnable: true, flagImageName: "united-kingdom", countryName: "UNITED STATES", locationName: "WASHINGTON", phoneNumber: "+1 210 000 0000"))
            CardHead(props: CardHeadProps(isEnable: false, flagImageName: "united-kingdom", countryName: "UNITED STATES", locationName: "WASHINGTON", phoneNumber: "+1 210 000 0000"))
        }
    }
}
