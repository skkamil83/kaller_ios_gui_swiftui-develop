//
//  CardHeadProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 16/11/2020.
//

import Foundation

struct CardHeadProps{
    let isEnable: Bool
    let flagImageName: String
    let countryName: String
    let locationName: String
    let phoneNumber: String
}
