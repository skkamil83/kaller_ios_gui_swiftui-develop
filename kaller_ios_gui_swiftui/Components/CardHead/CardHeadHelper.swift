//
//  CardHeadHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 16/11/2020.
//

import Foundation

class CardHeadHelper{
    static func getLocationText(countryName: String, cityName: String) -> String {
        return "\(countryName)\(cityName.isEmpty ? "" : ", \(cityName)")"
    }
}
