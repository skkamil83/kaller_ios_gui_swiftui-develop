//
//  UpperTab.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 03/12/2020.
//

import SwiftUI

struct UpperTab: View {
    let props: UpperTabProps
    
    var body: some View {
        Text(props.upperTab.title)
            .font(textStyle(props: props).font)
            .foregroundColor(textStyle(props: props).color)
    }
    
    private func textStyle(props: UpperTabProps) -> TextStyle{
        return props.isSelected ? TextStyle.subheadline1Default1Light5Blue : TextStyle.subheadline1Default1Light2SecondaryLabelColor
    }
}

struct UpperTab_Previews: PreviewProvider {
    static var previews: some View {
        UpperTab(props: UpperTabProps(upperTab: UpperTabModel(id: "1", title: "Clients"), isSelected: true))
    }
}
