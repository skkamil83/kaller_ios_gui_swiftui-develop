//
//  UpperTabbarProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 02/12/2020.
//

import Foundation

struct UpperTabbarProps{
    let tabs: [UpperTabModel]
    let onSelected: (_ id: String) -> Void
}
