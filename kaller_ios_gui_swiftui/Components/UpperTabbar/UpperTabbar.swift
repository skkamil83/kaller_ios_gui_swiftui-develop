//
//  UpperTabbar.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 02/12/2020.
//

import SwiftUI

struct UpperTabbar: View {
    let props: UpperTabbarProps
    @State private var selectedId: String = ""
    @State private var frameDict: [String: CGRect] = [:]
    
    init(props: UpperTabbarProps) {
        self.props = props
        _selectedId = .init(wrappedValue: props.tabs.first?.id ?? "")
    }
    
    var body: some View {
        VStack(spacing: 0){
            tabsView(props: props)
            bottomLine(frameDict: frameDict, selectedId: selectedId)
        }
        .frame(width: UIScreen.main.bounds.width)
    }
    
    private func tabsView(props: UpperTabbarProps) -> some View{
        HStack(spacing: 0){
            ForEach(props.tabs, id: \.self.id) { model in
                Spacer()
                Button(action: {
                    withAnimation {
                        selectedId = model.id
                        props.onSelected(model.id)
                    }
                }){
                    UpperTab(
                        props: UpperTabProps(
                            upperTab: model,
                            isSelected: selectedId == model.id
                        )
                    )
                }
                .background(collectTabFrameInfo(id: model.id))
                Spacer()
            }
        }
    }
    
    private func bottomLine(frameDict: [String: CGRect], selectedId: String) -> some View{
        VStack(spacing: 0){
            Spacer()
                .frame(height: Spacing.spacing4Pt)
            Color.primaryBlue
                .cornerRadius(Spacing.spacing4Pt/2)
                .frame(height: Spacing.spacing4Pt)
        }
        .frame(height: Spacing.spacing4Pt)
        .padding(
            EdgeInsets(
                top: Spacing.spacing10Pt,
                leading: frameDict[selectedId]?.origin.x ?? 0,
                bottom: 0,
                trailing: UIScreen.main.bounds.width - (frameDict[selectedId]?.origin.x ?? 0) - (frameDict[selectedId]?.width ?? 0)
            )
        )
        .clipped()
    }
    
    private func collectTabFrameInfo(id: String) -> some View{
        return GeometryReader{ geometry -> Color in
            let rect = geometry.frame(in: .global)
            DispatchQueue.main.async {
                frameDict[id] = rect
            }
            return Color.clear
        }
    }
}

struct UpperTabbar_Previews: PreviewProvider {
    static var previews: some View {
        UpperTabbar(
            props: UpperTabbarProps(
                tabs: [.init(id: "0", title: "My Contacts"), .init(id: "1", title: "Employees"), .init(id: "2", title: "Clients")],
                onSelected: {_ in}
            )
        )
    }
}
