//
//  UpperTabModel.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 03/12/2020.
//

import Foundation

struct UpperTabModel{
    let id: String
    let title: String
}
