//
//  SettingsListRowProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by ar_ko on 18.01.2021.
//

struct SettingsListRowProps {
    let image: String
    let text: String
    let trailing: Insert
    
    init(image: String, text: String, trailing: Insert = .none) {
        self.image = image
        self.text = text
        self.trailing = trailing
    }
    
    enum Insert {
        case text(String)
        case present
        case none
    }
}
