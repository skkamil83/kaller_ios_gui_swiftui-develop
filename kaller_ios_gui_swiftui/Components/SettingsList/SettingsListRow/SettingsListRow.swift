//
//  SettingsListRow.swift
//  kaller_ios_gui_swiftui
//
//  Created by ar_ko on 12.01.2021.
//

import SwiftUI

struct SettingsListRow: View {
    var props: SettingsListRowProps
    
    var body: some View {
            HStack {
                icon(props: props)
                title(props: props)
                Spacer()
                trailingView(props: props)
        }
    }
    
    private func icon(props: SettingsListRowProps) -> some View {
        Image(props.image)
            .frame(width: 29, height: 29)
    }
    
    private func title(props: SettingsListRowProps) -> some View {
        Text(props.text)
            .textStyle(.body1Default1Light1LabelColor2CenterAligned)
    }
    
    private func trailingView(props: SettingsListRowProps) -> some View {
        switch props.trailing {
        case .none:
            return AnyView(EmptyView())
        case .present:
            return AnyView(Image(systemName: "gift.fill")
                            .textStyle(.body1Default1Light2SecondaryLabelColor)
            )
        case .text(let text):
            return AnyView(Text(text)
                            .textStyle(.body1Default1Light2SecondaryLabelColor)
            )
        }
    }
}

struct ControlsTablesRowCompactLightDefault_Previews: PreviewProvider {
    static var previews: some View {
        SettingsListRow(props: SettingsListRowProps(image: "Invite", text: "Invite Friends", trailing: .text("3")))
    }
}
