//
//  SettingsListContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by ar_ko on 22.01.2021.
//

import SwiftUI

struct SettingsListContainer: View {
    var body: some View {
        NavigationView {
            SettingsListView()
        }
    }
}

struct SettingsContainer_Previews: PreviewProvider {
    static var previews: some View {
        SettingsListContainer()
    }
}
