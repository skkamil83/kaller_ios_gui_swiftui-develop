//
//  SettingsListView.swift
//  kaller_ios_gui_swiftui
//
//  Created by ar_ko on 18.01.2021.
//

import SwiftUI

struct SettingsListView: View {
    var body: some View {
        List {
            Section() {
                NavigationLink(destination: Text("Active Devices")) {
                    SettingsListRow(props: SettingsListRowProps(image: "Devices", text: "Active Devices", trailing: .text("3")))
                }
                NavigationLink(destination: Text("Privacy and Security")) {
                    SettingsListRow(props: SettingsListRowProps(image: "Privacy", text: "Privacy and Security"))
                }
                NavigationLink(destination: Text("Data and Storage")) {
                    SettingsListRow(props: SettingsListRowProps(image: "Data", text: "Data and Storage"))
                }
                NavigationLink(destination: Text("Notifications")) {
                    SettingsListRow(props: SettingsListRowProps(image: "Notifications",text: "Notifications"))
                }
                NavigationLink(destination: Text("Language")) {
                    SettingsListRow(props: SettingsListRowProps(image: "Language", text: "Language"))
                }
            }
            
            Section() {
                NavigationLink(destination: Text("Invite Friends")) {
                    SettingsListRow(props: SettingsListRowProps(image: "Invite",text: "Invite Friends", trailing: .present))
                }
                NavigationLink(destination: Text("Help")) {
                    SettingsListRow(props: SettingsListRowProps(image: "Help", text: "Help"))
                }
            }
        }
        .navigationBarTitle("Settings")
        .textStyle(.largeTitle2Bold1Light1LabelColor)
        .listStyle(GroupedListStyle())
    }
}

struct SettingsListView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsListView()
    }
}
