//
//  RecentsInfoSample.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 24/12/2020.
//

import SwiftUI

struct RecentDetailsContainerSample: View {
    @Environment(\.presentationMode) var presentationMode
    let example: Example
    
    enum Example{
        case incoming, outcoming, missed, canceled
    }
    
    var body: some View {
        let userInfoState = UserInfoState()
        userInfoState.accountInfo = AccountInfo(
            phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
            account: Account(id: 1, name: "Account name", createDate: Date()),
            devices: []
        )
        
        switch example {
        case .incoming:
            return AnyView(incoming()
                .environmentObject(userInfoState))
        case .outcoming:
            return AnyView(outcoming()
                .environmentObject(userInfoState))
        case .missed:
            return AnyView(missed()
                .environmentObject(userInfoState))
        case .canceled:
            return AnyView(canceled()
                .environmentObject(userInfoState))
        }
    }
    
    private func incoming() -> some View{

        let recentCall = RecentCall(
            callId: "1",
            callType: .incoming,
            phoneFromNumber: "+44 20 1234 5678",
            phoneToNumber: "+11111234567",
            callStartDate: Date(),
            callEndDate: Date(),
            isExtension: false,
            isMissed: false,
            isCanceled: false,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 0, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCallViewModel = RecentViewModel(
            calls: [recentCall],
            displayDate: "16:04",
            isMissed: false,
            isCanceled: false,
            callType: .incoming,
            contact: Contact(
                firstName: "Jayyyyyyyy",
                lastName: "Daaaaaaaaaaa", company: nil,
                avatarUrl: nil,
                phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Jayyyyyyyy", lastName: "Daaaaaaaaaaa", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "United Kingdom",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
            isExtension: false,
            place: "United Kingdom")
        
        return RecentDetailsPageContainer(
            props: RecentDetailsPageContainerProps(
                recentViewModel: recentCallViewModel,
                close: {
                    presentationMode.wrappedValue.dismiss()
                }
            )
        )
    }
    
    private func outcoming() -> some View{
        let recentCall = RecentCall(
            callId: "1",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(),
            callEndDate: Date(),
            isExtension: false,
            isMissed: false,
            isCanceled: false,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .phoneToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCall2 = RecentCall(
            callId: "2",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(timeIntervalSince1970: 1607378400),
            callEndDate: Date(),
            isExtension: false,
            isMissed: false,
            isCanceled: false,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .phoneToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCallViewModel = RecentViewModel(
            calls: [recentCall, recentCall2],
            displayDate: "16:04",
            isMissed: false,
            isCanceled: false,
            callType: .outcoming,
            contact: Contact(
                firstName: "Jayyyyyyyy",
                lastName: "Daaaaaaaaaaa", company: nil,
                avatarUrl: nil,
                phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Jayyyyyyyy", lastName: "Daaaaaaaaaaa", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "United Kingdom",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
            isExtension: false,
            place: "United Kingdom")
        
        return RecentDetailsPageContainer(
            props: RecentDetailsPageContainerProps(
                recentViewModel: recentCallViewModel,
                close: {
                    presentationMode.wrappedValue.dismiss()
                }
            )
        )
    }
    
    private func missed() -> some View{
        let recentCall = RecentCall(
            callId: "1",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(),
            callEndDate: Date(),
            isExtension: false,
            isMissed: true,
            isCanceled: false,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCall2 = RecentCall(
            callId: "2",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(timeIntervalSince1970: 1607378400),
            callEndDate: Date(),
            isExtension: false,
            isMissed: true,
            isCanceled: false,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCall3 = RecentCall(
            callId: "3",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(timeIntervalSince1970: 1607378400),
            callEndDate: Date(),
            isExtension: false,
            isMissed: true,
            isCanceled: false,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCallViewModel = RecentViewModel(
            calls: [recentCall, recentCall2, recentCall3],
            displayDate: "16:04",
            isMissed: true,
            isCanceled: false,
            callType: .outcoming,
            contact: Contact(
                firstName: "Jayyyyyyyy",
                lastName: "Daaaaaaaaaaa", company: nil,
                avatarUrl: nil,
                phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Jayyyyyyyy", lastName: "Daaaaaaaaaaa", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "United Kingdom",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
            isExtension: false,
            place: "United Kingdom")
        
        return RecentDetailsPageContainer(
            props: RecentDetailsPageContainerProps(
                recentViewModel: recentCallViewModel,
                close: {
                    presentationMode.wrappedValue.dismiss()
                }
            )
        )
    }
    
    private func canceled() -> some View{
        let recentCall = RecentCall(
            callId: "1",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(),
            callEndDate: Date(),
            isExtension: false,
            isMissed: true,
            isCanceled: true,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCall2 = RecentCall(
            callId: "2",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(timeIntervalSince1970: 1607378400),
            callEndDate: Date(),
            isExtension: false,
            isMissed: true,
            isCanceled: true,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCall3 = RecentCall(
            callId: "3",
            callType: .outcoming,
            phoneFromNumber: "+11111234567",
            phoneToNumber: "+44 20 1234 5678",
            callStartDate: Date(timeIntervalSince1970: 1607378400),
            callEndDate: Date(),
            isExtension: false,
            isMissed: true,
            isCanceled: true,
            fromCountry: nil,
            fromExt: nil,
            contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
            billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCallViewModel = RecentViewModel(
            calls: [recentCall, recentCall2, recentCall3],
            displayDate: "16:04",
            isMissed: true,
            isCanceled: true,
            callType: .outcoming,
            contact: Contact(
                firstName: "Jayyyyyyyy",
                lastName: "Daaaaaaaaaaa", company: nil,
                avatarUrl: nil,
                phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Jayyyyyyyy", lastName: "Daaaaaaaaaaa", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "United Kingdom",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
            isExtension: false,
            place: "United Kingdom")
        
        return RecentDetailsPageContainer(
            props: RecentDetailsPageContainerProps(
                recentViewModel: recentCallViewModel,
                close: {
                    presentationMode.wrappedValue.dismiss()
                }
            )
        )
    }
}
