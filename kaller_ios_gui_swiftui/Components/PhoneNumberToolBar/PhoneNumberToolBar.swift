import SwiftUI

struct PhoneNumberToolBar: View {
    let props: PhoneNumberToolBarProps

    var body: some View {
        HStack {
            if let number = props.phoneNumber {
                if let flagImg = props.flagImageName {
                    flagIcon(flagImg)
                }
                phoneNumber(number)
                selectNumberBtn(props: props)
            } else {
                EmptyView()
            }
        }
                .onTapGesture {
                    props.onSelectPhoneNumber()
                }
    }

    private func flagIcon(_ flagImgName: String) -> some View {
        CircleIcon(
                props: CircleIconProps(
                        size: Spacing.keypadHorizontalPhoneNumberSelectionFlag,
                        style: .filled(imageName: flagImgName)
                )
        )
    }

    private func phoneNumber(_ number: String) -> some View {
        Text(number)
                .font(TextStyle.subheadline1Default1Light1LabelColor.font)
                .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)

    }

    private func selectNumberBtn(props: PhoneNumberToolBarProps) -> some View {
        Button(action: props.onSelectPhoneNumber) {
            Image("dropDown")
                    .frame(height: Spacing.keypadHorizontalArrow9Pt)
        }
    }
}
