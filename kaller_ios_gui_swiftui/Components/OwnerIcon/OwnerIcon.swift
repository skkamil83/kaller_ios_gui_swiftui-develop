//
//  OwnerIcon.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 03/12/2020.
//

import SwiftUI

struct OwnerIcon: View {
    let props: OwnerIconProps
    
    var body: some View {
        HStack(spacing: Spacing.spacing16Pt){
            avatar(props: props)
            info(props: props)
        }
    }
    
    private func avatar(props: OwnerIconProps) -> some View{
        AvatarView(props: AvatarViewProps(size: 44, style: props.avatarStyle))
            .padding(EdgeInsets(top: Spacing.spacing8Pt, leading: 0, bottom: Spacing.spacing8Pt, trailing: 0))
    }
    
    private func info(props: OwnerIconProps) -> some View{
        let nameTextStyle = TextStyle.body2Bold1Light1LabelColor
        let roleTextStyle = TextStyle.footnote1Default1Light2SecondaryLabelColor
        return
            VStack(alignment: .leading){
                Text(OwnerIconHelper.getfullName(firstName: props.firstName, lastName: props.lastName))
                    .frame(height: 22)
                    .font(nameTextStyle.font)
                    .foregroundColor(nameTextStyle.color)
                    .padding(.top, Spacing.spacing10Pt)
                Text(props.role)
                    .frame(height: 18)
                    .font(roleTextStyle.font)
                    .foregroundColor(roleTextStyle.color)
                    .padding(.bottom, Spacing.spacing10Pt)
                    
            }
    }
}

struct OwnerIcon_Previews: PreviewProvider {
    static var previews: some View {
        OwnerIcon(
            props: OwnerIconProps(
                avatarStyle: AvatarViewProps.Style.name(
                    firstName: "Jay",
                    lastName: "Doctor",
                    company: "Hospital"
                ),
                firstName: "Jay",
                lastName: "Doctor",
                role: "Friend"
            )
        )
    }
}
