//
//  OwnerIconHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 03/12/2020.
//

import Foundation

class OwnerIconHelper {
    
    static func getfullName(firstName: String, lastName: String) -> String {
        var result = ""

        if firstName != "" {
            result += "\(firstName) "
        }

        if lastName != "" {
            result += lastName
        }

        return result
    }

}
