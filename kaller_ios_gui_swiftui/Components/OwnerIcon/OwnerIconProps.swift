//
//  OwnerIconProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 03/12/2020.
//

import Foundation

struct OwnerIconProps{
    let avatarStyle: AvatarViewProps.Style
    let firstName: String
    let lastName: String
    let role: String
}
