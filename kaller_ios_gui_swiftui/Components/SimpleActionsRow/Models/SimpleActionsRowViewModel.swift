import SwiftUI

struct SimpleActionsRowViewModel {
    let id: String
    let title: String
    let color: Color
    let icon: SimpleActionsImageType
    let action: () -> Void
}
