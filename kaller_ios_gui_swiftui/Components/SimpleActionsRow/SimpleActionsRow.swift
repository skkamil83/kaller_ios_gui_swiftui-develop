import SwiftUI

struct SimpleActionsRow: View {
    let props: SimpleActionsRowProps

    var body: some View {
        view(props: props)
                .frame(height: 76)
                .onAppear {
                    props.contentViewWidth(CGFloat(74 * props.items.count))
                }
    }

    private func view(props: SimpleActionsRowProps) -> some View {
        HStack(spacing: 0) {
            if props.alignment == .trailing {
                props.items.first?.color
            }
            ForEach(props.items, id: \.self.id) { item in
                button(item: item)
            }
            if props.alignment == .leading {
                props.items.last?.color
            }
        }
    }

    private func button(item: SimpleActionsRowViewModel) -> some View {
        Button(action: item.action) {
            VStack(spacing: Spacing.spacing4Pt) {
                icon(type: item.icon)
                Text(item.title)
                        .textStyle(.subheadline1Default2Dark1LabelColor)
            }
        }
                .frame(width: 74, height: 76)
                .background(item.color)
    }

    private func icon(type: SimpleActionsImageType) -> some View {
        switch type {
        case .imageName(let name):
            return AnyView(Image(name))
        case .sytemImageName(let name):
            return AnyView(
                    Image(systemName: name)
                            .textStyle(TextStyle(fontName: "SFProDisplay-Regular", fontSize: 21, color: .white))
            )
        }
    }
}
