import SwiftUI

struct SimpleActionsRowProps {
    let items: [SimpleActionsRowViewModel]
    let alignment: Alignment
    let contentViewWidth: (CGFloat) -> Void
}
