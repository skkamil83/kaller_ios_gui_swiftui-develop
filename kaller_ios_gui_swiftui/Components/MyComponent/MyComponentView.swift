import SwiftUI

struct MyComponentView: View {
    let props: MyComponentViewProps

    var body: some View {
        Circle()
                .foregroundColor(props.color)
                .frame(width: props.size, height: props.size)
    }
}


struct MyComponentView_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView(showsIndicators: false) {
            VStack {
                MyComponentView(
                        props: MyComponentViewProps(size: 76, color: .red)
                )
                Divider()
                MyComponentView(
                        props: MyComponentViewProps(size: 34, color: .red)
                )
                Divider()
            }
        }
    }
}
