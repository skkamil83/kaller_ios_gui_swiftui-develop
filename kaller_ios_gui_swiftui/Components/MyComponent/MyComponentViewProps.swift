import SwiftUI

struct MyComponentViewProps {
    let size: CGFloat
    let color: Color
}