class MyComponentViewHelper {
    static func getInitials(firstName: String, lastName: String, company: String) -> String {
        var result = ""

        if firstName != "" {
            result += firstName.prefix(1)
        }

        if lastName != "" {
            result += lastName.prefix(1)
        }

        if result == "" && company != "" {
            result += company.prefix(1)
        }

        return result.uppercased()
    }
}
