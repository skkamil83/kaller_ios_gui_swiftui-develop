//
//  CardNumberPreview.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 18/11/2020.
//

import SwiftUI

struct CardNumberPreview: View {
    let props: CardNumberPreviewProps
    
    var body: some View {
        let size = getSize()
        ZStack{
            locationPreview(url: props.locationPreviewImageURL, size: size)
            cardHead(props: props)
            trash(onTap: props.onTrashTap)
        }
        .frame(width: size.width, height: size.height)
    }

    private func locationPreview(url: String, size: CGSize) -> some View {
        URLImage(
                props: URLImageProps(
                        urlString: props.locationPreviewImageURL,
                        contentMode: .fill,
                        size: size
                )
        )
    }
    
    private func cardHead(props: CardNumberPreviewProps) -> some View{
        VStack{
            HStack{
                CardHead(
                        props: CardHeadProps(
                                isEnable: props.isEnable,
                                flagImageName: props.flagImageName,
                                countryName: props.countryName,
                                locationName: props.locationName,
                                phoneNumber: props.phoneNumber
                        )
                )
                        .padding(
                                EdgeInsets(
                                        top: Spacing.spacing16Pt,
                                        leading: Spacing.spacing16Pt,
                                        bottom: 0,
                                        trailing: 0
                                )
                        )
                Spacer()
            }
            Spacer()
        }
    }
    
    private func trash(onTap: @escaping () -> Void) -> some View{
        let textStyle = TextStyle(
                fontName: "SFProText-Regular",
                fontSize: 17,
                color: Color.white
        )

        return VStack(){
            Spacer()
            HStack{
                Spacer()
                Button(action: onTap) {
                    CircleIcon(
                            props: CircleIconProps(
                                    size: 32,
                                    style: .systemImage(
                                            systemImageName: "trash",
                                            font: textStyle.font,
                                            backgroundColor: Color.black.opacity(0.5),
                                            foregroundColor: textStyle.color
                                    )
                            )
                    )
                }
                .padding(
                        EdgeInsets(
                                top: 0,
                                leading: 0,
                                bottom: Spacing.spacing10Pt,
                                trailing: Spacing.spacing16Pt
                        )
                )
            }
        }
    }
    
    private func getSize() -> CGSize{
        let width =  UIScreen.main.bounds.width
        let height = width / 1.6180
        return CGSize(width: width, height: height)
    }
}

struct CardNumberPreview_Previews: PreviewProvider {
    static var previews: some View {
        CardNumberPreview(
                props: CardNumberPreviewProps(
                        locationPreviewImageURL: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640",
                        isEnable: true,
                        flagImageName: "united-states-of-america",
                        countryName: "UNITED STATES",
                        locationName: "WASHINGTON",
                        phoneNumber: "1 210 000 0000",
                        onTrashTap: {}
                )
        )
    }
}
