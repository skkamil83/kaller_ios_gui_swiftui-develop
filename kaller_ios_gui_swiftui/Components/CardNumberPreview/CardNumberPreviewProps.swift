//
//  CardNumberPreviewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 18/11/2020.
//

import SwiftUI

struct CardNumberPreviewProps{
    let locationPreviewImageURL: String
    let isEnable: Bool
    let flagImageName: String
    let countryName: String
    let locationName: String
    let phoneNumber: String
    let onTrashTap: () -> Void
}
