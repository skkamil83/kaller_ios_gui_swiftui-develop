//
//  SimIconColorType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 18/11/2020.
//

import Foundation

enum SimIconColorType{
    case blue, green, purple, yellow
    var imageName: String{
        switch self{
        case .blue:
            return "avatarXSim1"
        case .green:
            return "avatarXSim2"
        case .purple:
            return "avatarXSim3"
        case .yellow:
            return "avatarXSim4"
        }
    }
}
