//
//  SimIconProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 18/11/2020.
//

import SwiftUI

struct SimIconProps{
    let number: Int
    let size: CGSize
}
