//
//  SimIcon.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 18/11/2020.
//

import SwiftUI

struct SimIcon: View {
    let props: SimIconProps
    
    var body: some View {
        let textStyle = getTextStyle(props: props)
        let colorType = getColorType(props: props)
        ZStack{
            Image(colorType.imageName)
                .resizable()
            if(props.number > 0){
                Text("\(props.number)")
                    .font(textStyle.font)
                    .foregroundColor(textStyle.color)
                    .tracking(getTracking(props: props))
                    .minimumScaleFactor(0.1)
                    .lineLimit(1)
                    .multilineTextAlignment(.center)
            }
        }
        .frame(width: props.size.width, height: props.size.height)
    }
    
    private func getColorType(props: SimIconProps) -> SimIconColorType{
        switch props.number % 4 {
        case 0:
            return .yellow
        case 1:
            return .blue
        case 2:
            return .green
        case 3:
            return .purple
        default:
            return .blue
        }
    }
    
    private func getTextStyle(props: SimIconProps) -> TextStyle{
        switch "\(props.number)".count{
        case 1:
            return TextStyle(
                fontName: "SFProText-Medium",
                fontSize: Float(props.size.width),
                color: .white
            )
        case 2:
            return TextStyle(
                fontName: "SFProText-Medium",
                fontSize: Float(props.size.width) * 10 / 14,
                color: .white
            )
        default:
            return TextStyle(
                fontName: "SFCompactText-Medium",
                fontSize: Float(props.size.width) * 7 / 14,
                color: .white
            )
            
        }
    }
    
    private func getTracking(props: SimIconProps) -> CGFloat{
        switch "\(props.number)".count{
        case 1:
            return 0.14
        case 2:
            return -0.4
        default:
            return -0.28
            
        }
    }
}

struct SimIcon_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView{
            LazyVStack(spacing: 10){
                ForEach(1...999, id: \.self) {
                    SimIcon(props: SimIconProps(number: $0, size: CGSize(width: 14, height: 18)))
                }
            }
        }
    }
}
