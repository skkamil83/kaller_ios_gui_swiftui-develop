import SwiftUI

struct ComponentsContainer: View {
    var body: some View {
        NavigationView {
            List {
                Group {
                    myComponentSection()
                    avatars()
                    circleImages()
                    cardHeads()
                    cardNumberPreview()
                    simIcons()
                    urlImages()
                    multiaccountSelection()
                    upperTabbar()
                    ownerIcon()
                }
                Group {
                    messagingComponents()
                    searchBar()
                    recentCallList()
                    countriesList()
                    selectDefaultNumber()
                    recentsInfo()
                    avatarLoader()
                    settings()
                }
            }
            .navigationBarTitle("Components", displayMode: .inline)
        }
    }
    
    private func messagingComponents() -> some View {
        Section(header: Text("Messaging Components")) {
            NavigationLink(
                destination:
                    VStack(spacing: 10) {
                        FileIcon(props: FileIconProps(color: .blue, text: "DOCX", size: 120))
                        FileIcon(props: FileIconProps(color: .green, text: "TIFF", size: 30))
                        FileIcon(props: FileIconProps(color: .gray, text: "SVG", size: 24))
                    }
            ) {
                Text("File Icons")
            }
        }
    }
    
    private func myComponentSection() -> some View {
        Section(header: Text("My component")) {
            NavigationLink(
                destination: MyComponentView(
                    props: MyComponentViewProps(size: 200, color: .green)
                )
            ) {
                Text("MyComponent_big")
            }
            
            NavigationLink(
                destination: MyComponentView(
                    props: MyComponentViewProps(size: 33, color: .blue)
                )
            ) {
                Text("MyComponent_small")
            }
        }
    }
    
    private func avatars() -> some View {
        Section(header: Text("Avatars")) {
            NavigationLink(
                destination:
                    HStack(spacing: 10){
                        VStack{
                            AvatarView(props: AvatarViewProps(size: 30, style: .name(firstName: "Jay", lastName: "Doctor", company: "")))
                            Text("Size: 30")
                        }
                        VStack{
                            AvatarView(props: AvatarViewProps(size: 168, style: .name(firstName: "Jay", lastName: "Doctor", company: "")))
                            Text("Size: 168")
                        }
                    }
            ) {
                Text("Name")
            }
            
            NavigationLink(
                destination:
                    ScrollView(showsIndicators: false){
                        VStack(spacing: 10){
                            HStack(spacing: 10){
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 30, style: .image(imageName: "avatarUnknown")))
                                    Text("Size: 30")
                                }
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 168, style: .image(imageName: "avatarUnknown")))
                                    Text("Size: 168")
                                }
                            }
                            HStack(spacing: 10){
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 30, style: .image(imageName: "avatarChannel")))
                                    Text("Size: 30")
                                }
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 168, style: .image(imageName: "avatarChannel")))
                                    Text("Size: 168")
                                }
                            }
                            HStack(spacing: 10){
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 30, style: .image(imageName: "avatarLocation")))
                                    Text("Size: 30")
                                }
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 168, style: .image(imageName: "avatarLocation")))
                                    Text("Size: 168")
                                }
                            }
                            HStack(spacing: 10){
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 30, style: .image(imageName: "flag_AO")))
                                    Text("Size: 30")
                                }
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 168, style: .image(imageName: "flag_AO")))
                                    Text("Size: 168")
                                }
                            }
                            HStack(spacing: 10){
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 30, style: .image(imageName: "avatarGroup")))
                                    Text("Size: 30")
                                }
                                VStack{
                                    AvatarView(props: AvatarViewProps(size: 168, style: .image(imageName: "avatarGroup")))
                                    Text("Size: 168")
                                }
                            }
                        }
                    }
            ) {
                Text("Static Image")
            }
            
            NavigationLink(
                destination: HStack(spacing: 10){
                    VStack{
                        AvatarView(props: AvatarViewProps(size: 30, style: .bitmapImage(urlString: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640")))
                        Text("Size: 30")
                    }
                    VStack{
                        AvatarView(props: AvatarViewProps(size: 168, style: .bitmapImage(urlString: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640")))
                        Text("Size: 168")
                    }
                }
            ) {
                Text("Bitmap Image")
            }
            
            NavigationLink(
                destination: HStack(spacing: 10){
                    VStack{
                        AvatarView(props: AvatarViewProps(size: 30, style: .whiteGlyph(systemImageName: "star.fill")))
                        Text("Size: 30")
                    }
                    VStack{
                        AvatarView(props: AvatarViewProps(size: 168, style: .whiteGlyph(systemImageName: "star.fill")))
                        Text("Size: 168")
                    }
                }
            ) {
                Text("White Glyph")
            }
            
            NavigationLink(
                destination: HStack(spacing: 10){
                    VStack{
                        AvatarView(props: AvatarViewProps(size: 30, style: .blueGlyph(systemImageName: "person.2.fill")))
                        Text("Size: 30")
                    }
                    VStack{
                        AvatarView(props: AvatarViewProps(size: 168, style: .blueGlyph(systemImageName: "person.2.fill")))
                        Text("Size: 168")
                    }
                }
            ) {
                Text("Blue Glyph")
            }
            
        }
    }
    
    private func circleImages() -> some View {
        Section(header: Text("Circle Icons")) {
            NavigationLink(
                destination:  CircleIcon(props: CircleIconProps(size: 168, style: .filled(imageName: "flag_GB")))
            ) {
                Text("Filled")
            }
            
            NavigationLink(
                destination:  CircleIcon(props: CircleIconProps(size: 32, style: .systemImage(systemImageName: "info", font: .custom("SFProDisplay-Regular", size: 17), backgroundColor: Color.black.opacity(0.5), foregroundColor: .white)))
            ) {
                Text("Info")
            }
            
            NavigationLink(
                destination:  CircleIcon(props: CircleIconProps(size: 32, style: .systemImage(systemImageName: "trash", font: .custom("SFProDisplay-Regular", size: 17), backgroundColor: Color.black.opacity(0.5), foregroundColor: .white)))
            ) {
                Text("Trash")
            }
        }
    }
    
    private func cardHeads() -> some View {
        Section(header: Text("Card Head")) {
            NavigationLink(
                destination:  CardHead(props: CardHeadProps(isEnable: true, flagImageName: "flag_US", countryName: "UNITED STATES", locationName: "WASHINGTON", phoneNumber: "+1 210 000 0000"))
            ) {
                Text("Enable")
            }
            
            NavigationLink(
                destination:  CardHead(props: CardHeadProps(isEnable: false, flagImageName: "flag_US", countryName: "UNITED STATES", locationName: "WASHINGTON", phoneNumber: "+1 210 000 0000"))
            ) {
                Text("Disable")
            }
        }
    }
    
    private func cardNumberPreview() -> some View {
        Section(header: Text("Card Number Preview")) {
            NavigationLink(
                destination: CardNumberPreview(props: CardNumberPreviewProps(locationPreviewImageURL: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640", isEnable: true, flagImageName: "flag_US", countryName: "UNITED STATES", locationName: "WASHINGTON", phoneNumber: "1 210 000 0000", onTrashTap: {
                    print("onTrashTap")
                }))
            ) {
                Text("Card Number Preview")
            }
        }
    }
    
    private func simIcons() -> some View {
        Section(header: Text("Sim Icons")) {
            NavigationLink(
                destination:
                    ScrollView{
                        LazyVStack(spacing: 10){
                            ForEach(1...999, id: \.self) {
                                SimIcon(props: SimIconProps(number: $0, size: CGSize(width: 14, height: 18)))
                            }
                        }
                    }
            ) {
                Text("14x18")
            }
            
            NavigationLink(
                destination:
                    ScrollView{
                        LazyVStack(spacing: 10){
                            ForEach(1...999, id: \.self) {
                                SimIcon(props: SimIconProps(number: $0, size: CGSize(width: 16, height: 21)))
                            }
                        }
                    }
            ) {
                Text("16x21")
            }
            
            NavigationLink(
                destination:
                    ScrollView{
                        LazyVStack(spacing: 10){
                            ForEach(1...999, id: \.self) {
                                SimIcon(props: SimIconProps(number: $0, size: CGSize(width: 28, height: 36)))
                            }
                        }
                    }
            ) {
                Text("28x36")
            }
        }
    }
    
    private func urlImages() -> some View {
        Section(header: Text("URL Images")) {
            NavigationLink(
                destination:  URLImage(props: URLImageProps(urlString: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=640", contentMode: .fit, size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
            ) {
                Text("Small")
            }
            
            NavigationLink(
                destination:  URLImage(props: URLImageProps(urlString: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=1920", contentMode: .fit, size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
            ) {
                Text("Medium")
            }
            
            NavigationLink(
                destination:  URLImage(props: URLImageProps(urlString: "https://images.unsplash.com/photo-1604942980447-8155eab1b2e2?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=marius-cern-zwLbYhSKj_0-unsplash.jpg&w=2400", contentMode: .fit, size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
            ) {
                Text("Large")
            }
        }
    }
    
    private func multiaccountSelection() -> some View {
        return Section(header: Text("Multiaccount Selection")) {
            NavigationLink(
                destination: MultiaccountSelectionContainer(props: MultiaccountSelectionContainerProps(style: .selectPhoneInfo(true), onClose: {}, onMakeCall: {_ in}, onMakeMessage: {_ in}))
            ) {
                Text("Select Phone Info (free)")
            }
            
            NavigationLink(
                destination: MultiaccountSelectionContainer(props: MultiaccountSelectionContainerProps(style: .selectPhoneInfo(false), onClose: {}, onMakeCall: {_ in}, onMakeMessage: {_ in}))
            ) {
                Text("Select Phone Info (not free)")
            }
            
            NavigationLink(
                destination: MultiaccountSelectionContainer(props: MultiaccountSelectionContainerProps(style: .selectDefaultPhoneInfo, onClose: {}, onMakeCall: {_ in}, onMakeMessage: {_ in}))
            ) {
                Text("Select Default Phone Info")
            }
        }
    }

    private func searchBar() -> some View {
        Section(header: Text("SearchBar")) {
            NavigationLink(
                    destination: SearchBarViewSample()
            ) {
                Text("Search bar")
            }
        }
    }
    
    private func upperTabbar() -> some View {
        Section(header: Text("Upper Tabbar")) {
            NavigationLink(
                destination: UpperTabbar(
                    props: UpperTabbarProps(
                        tabs: [.init(id: "0", title: "My Contacts"), .init(id: "1", title: "Employees"), .init(id: "2", title: "Clients")],
                        onSelected: { selectedId in
                            print("id: \(selectedId)")
                        }
                    )
                )
            ) {
                Text("Default")
            }
            
            NavigationLink(
                destination: UpperTabbar(
                    props: UpperTabbarProps(
                        tabs: [.init(id: "0", title: "My Contacts"), .init(id: "1", title: "Employees"), .init(id: "2", title: "Clients"), .init(id: "3", title: "Users")],
                        onSelected: { selectedId in
                            print("id: \(selectedId)")
                        }
                    )
                )
            ) {
                Text("4 tabs")
            }
        }
    }
    
    private func ownerIcon() -> some View {
        Section(header: Text("Owner Icon")) {
            NavigationLink(
                destination: OwnerIcon(props: OwnerIconProps(avatarStyle: .name(firstName: "Jay", lastName: "Don", company: "Company"), firstName: "Jay", lastName: "Don", role: "Friend"))
            ) {
                Text("Owner Icon")
            }
        }
    }

    private func recentCallList() -> some View {
        
        return Section(header: Text("Recent Call List")) {
            NavigationLink(
                destination: RecentLListViewExample(filterType: .all, state: .view, isEmpty: true)
            ) {
                Text("No Recents")
            }
            NavigationLink(
                destination: RecentLListViewExample(filterType: .all, state: .view, isEmpty: false)
            ) {
                Text("Filter: All, State: View")
            }
            
            NavigationLink(
                destination: RecentLListViewExample(filterType: .defaultNumber, state: .edit, isEmpty: false)
            ) {
                Text("Filter: Default Number, State: Edit")
            }
        }
    }

    private func countriesList() -> some View {
        Section(header: Text("Countries list")) {
            NavigationLink(destination: CountriesSample()) {
                Text("Countries")
            }
        }
    }
    
    private func selectDefaultNumber() -> some View {
        Section(header: Text("Select Default Number")) {
            NavigationLink(
                destination: SelectDefaultNumberContainerSample()
            ) {
                Text("Select Default Number")
            }
        }
    }
    
    private func recentsInfo() -> some View {
        Section(header: Text("Recents Info")) {
            NavigationLink(
                destination: RecentDetailsContainerSample(example: .incoming)
            ) {
                Text("Incoming")
            }
            NavigationLink(
                destination: RecentDetailsContainerSample(example: .outcoming)
            ) {
                Text("Outcoming")
            }
            NavigationLink(
                destination: RecentDetailsContainerSample(example: .missed)
            ) {
                Text("Missed")
            }
            NavigationLink(
                destination: RecentDetailsContainerSample(example: .canceled)
            ) {
                Text("Canceled")
            }
        }
    }
    
    private func avatarLoader() -> some View {
        Section(header: Text("Avatar Loader")) {
            NavigationLink(
                destination: AvatarLoader(
                    props: AvatarLoaderProps(
                        size: 86,
                        firstName: "David",
                        lastName: "Bon",
                        avatarImage: nil,
                        avatarUrl: nil,
                        onEditPhoto: {})
                )
            ) {
                Text("86 x 86")
            }
            NavigationLink(
                destination: AvatarLoader(
                    props: AvatarLoaderProps(
                        size: 150,
                        firstName: "David",
                        lastName: "Bon",
                        avatarImage: nil,
                        avatarUrl: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640",
                        onEditPhoto: {})
                )
            ) {
                Text("150 x 150")
            }
        }
    }
    
    private func settings() -> some View {
        Section(header: Text("Settings")) {
            NavigationLink(
                destination: SettingsListView()
            ) {
                Text("Settings")
            }
        }
    }
    
}
