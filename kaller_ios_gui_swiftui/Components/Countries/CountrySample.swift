import SwiftUI

struct CountriesSample: View {
    @State private var countries = CountryFetcher().countries
    var body: some View {
        List {
            ForEach(countries, id: \.self.iso) { country in
                countryView(country)
            }
        }
    }

    private func countryView(_ country: Country) -> some View {
        HStack {
            if let phoneCode = country.phoneCode {
                Text("+\(phoneCode)")
            }
            CircleIcon(props: CircleIconProps(size: 21, style: .filled(imageName: country.flagImageName)))
            Text(country.iso)
            Text(country.localizedName)
            Spacer()
            if let countryCode = country.countryCode {
                Text("\(countryCode)")
            }
        }
    }
}