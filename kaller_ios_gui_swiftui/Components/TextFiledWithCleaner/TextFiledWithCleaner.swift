import SwiftUI

struct TextFiledWithCleaner: View {
    let props: TextFiledWithCleanerProps
    @Binding private var text: String

    init(props: TextFiledWithCleanerProps) {
        self.props = props
        self._text = props.text
    }

    var body: some View {
        textField(props: props)
    }

    private func textField(props: TextFiledWithCleanerProps) -> some View {
        TextField(
                props.placeHolder,
                text: $text,
                onEditingChanged: props.onFocus
        )
                .textStyle(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black))
                .modifier(ClearInputIcon(text: props.text))
    }
}
