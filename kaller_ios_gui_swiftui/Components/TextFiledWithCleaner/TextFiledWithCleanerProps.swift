import SwiftUI

struct TextFiledWithCleanerProps {
    let text: Binding<String>
    let placeHolder: String
    let onFocus: (Bool) -> Void
}
