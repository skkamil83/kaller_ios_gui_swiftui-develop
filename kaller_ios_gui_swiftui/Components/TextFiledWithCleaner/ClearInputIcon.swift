import SwiftUI

struct ClearInputIcon: ViewModifier {
    @Binding var text: String

    public func body(content: Content) -> some View {
        HStack {
            ZStack(alignment: .trailing) {
                content

                if !text.isEmpty {
                    clearButton()
                }
            }
        }
    }

    private func clearButton() -> some View {
        return Button(action: { self.text = "" }) {
            Image(systemName: "xmark.circle.fill")
                    .font(TextStyle.body1Default1Light2SecondaryLabelColor.font)
                    .foregroundColor(TextStyle.body1Default1Light2SecondaryLabelColor.color)
        }
    }
}
