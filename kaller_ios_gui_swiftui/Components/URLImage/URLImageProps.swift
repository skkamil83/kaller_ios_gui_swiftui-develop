//
//  URLImageProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 18/11/2020.
//

import SwiftUI

struct URLImageProps{
    let urlString: String
    let contentMode: ContentMode
    let size: CGSize
}
