//
//  URLImage.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 18/11/2020.
//

import SwiftUI

struct URLImage: View {
    let props: URLImageProps
    @State private var image: UIImage? = nil
    
    var body: some View {
        imageView(props: props)
            .clipped()
    }
    
    private func imageView(props: URLImageProps) -> some View{
        if let image = image{
            return AnyView(
                Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: props.contentMode)
                    .frame(maxWidth: props.size.width, maxHeight: props.size.height)
            )
        }else{
            DispatchQueue.global(qos: .userInitiated).async {
                loadImage(props: props)
            }
            return AnyView(
                EmptyView()
                    .frame(maxWidth: props.size.width, maxHeight: props.size.height)
            )
        }
    }
    
    private func loadImage(props: URLImageProps){
        guard let url = URL(string: props.urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                    let data = data,
                    let image = UIImage(data: data)
                    else {
                return
            }
            DispatchQueue.main.async {
                self.image = image
            }
        }
        .resume()
    }
}

struct URLImage_Previews: PreviewProvider {
    static var previews: some View {
        URLImage(
                props: URLImageProps(
                    urlString: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640",
                    contentMode: .fill,
                    size: CGSize(width: 50, height: 50)
                )
        )
    }
}
