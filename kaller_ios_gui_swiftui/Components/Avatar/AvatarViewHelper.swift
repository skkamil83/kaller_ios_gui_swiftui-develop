class AvatarViewHelper {
    
    static func getInitials(firstName: String, lastName: String, company: String) -> String {
        var result = ""

        if firstName != "" {
            result += firstName.prefix(1)
        }

        if lastName != "" {
            result += lastName.prefix(1)
        }

        if result == "" && company != "" {
            result += company.prefix(1)
        }

        return result.uppercased()
    }
    
    static func getAvatarStyle(from phoneInfo: PhoneInfo?) -> AvatarViewProps.Style {
        guard let phoneInfo = phoneInfo else {
            return .image(imageName: "avatarUnknown")
        }
        if phoneInfo.userInfo.avatarImageUrl?.isEmpty ?? true{
            return .name(firstName: phoneInfo.userInfo.firstName ?? "", lastName: phoneInfo.userInfo.lastName ?? "", company: "")
        }else{
            return .bitmapImage(urlString: phoneInfo.userInfo.avatarImageUrl ?? "")
        }
    }
    
    static func getAvatarStyle(from contact: Contact?) -> AvatarViewProps.Style {
        guard let contact = contact else {
            return .image(imageName: "avatarUnknown")
        }
        if let url = contact.avatarUrl{
            return .bitmapImage(urlString: url)
        }else{
            if contact.firstName != nil || contact.lastName != nil {
                return .name(firstName: contact.firstName ?? "", lastName: contact.lastName ?? "", company: "")
            }
        }
        return .image(imageName: "avatarUnknown")
    }
    
    static func getAvatarStyle(url: String, firstName: String, lastName: String) -> AvatarViewProps.Style {
        if !url.isEmpty {
            return .bitmapImage(urlString: url)
        }
        if !firstName.isEmpty || !lastName.isEmpty {
            return .name(firstName: firstName, lastName: lastName, company: "")
        }
        return .image(imageName: "avatarUnknown")
    }

}
