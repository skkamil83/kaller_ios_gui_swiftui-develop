import SwiftUI

struct AvatarViewProps {
    let size: CGFloat
    let style: Style
    
    enum Style{
        case name(firstName: String, lastName: String, company: String)
        case image(imageName: String)
        case bitmapImage(urlString: String)
        case whiteGlyph(systemImageName: String)
        case blueGlyph(systemImageName: String)
    }
}
