import SwiftUI

struct AvatarView: View {
    let props: AvatarViewProps

    var body: some View {
        VStack{
            getContent(props: props)
        }
        .frame(width: props.size, height: props.size)
        .background(getBackground(props: props))
        .cornerRadius(props.size/2)
    }
    
    
    private func getBackground(props: AvatarViewProps) -> some View{
        switch props.style{
        case .name, .whiteGlyph:
            return AnyView(
                LinearGradient(
                    gradient: Gradient(
                        colors: [Color.graysSystemGray4Light, Color.avatarGradient]),
                    startPoint: .top,
                    endPoint: .bottom
                )
            )
            
        case .blueGlyph:
            return AnyView(
                Color(red: 0, green: 0, blue: 0, opacity: 0.1).opacity(0.5)
            )
        
        case .bitmapImage(let urlString):
            return AnyView(
                URLImage(
                    props: URLImageProps(
                        urlString: urlString,
                        contentMode: .fill,
                        size: CGSize(
                            width: props.size,
                            height: props.size
                        )
                    )
                )
            )
        case .image:
            return AnyView(EmptyView())
        }
    }
    
    private func getContent(props: AvatarViewProps) -> some View{
        switch props.style{
        case .name(let firstName, let lastName, let company):
            let initials = AvatarViewHelper.getInitials(firstName: firstName, lastName: lastName, company: company)
            return AnyView(
                Text(initials)
                    .font(.custom("SFProRounded-Medium", size: 76 * props.size / 168))
                    .foregroundColor(.white)
                    .minimumScaleFactor(0.1)
                    .lineLimit(1)
                    .padding(props.size * 39 / 168)
                    .multilineTextAlignment(.center)
            )
            
        case .bitmapImage:
            return AnyView(EmptyView())
            
        case .blueGlyph(let systemImageName):
            let textStyle = TextStyle(fontName: "SFProRounded-Medium", fontSize: Float(props.size) * 73.6 / 168, color: Color.primaryBlue)
            return AnyView(
                Image(systemName: systemImageName)
                    .font(textStyle.font)
                    .foregroundColor(textStyle.color)
            )
            
        case .whiteGlyph(let systemImageName):
            let textStyle = TextStyle(fontName: "SFProDisplay-Light", fontSize: Float(props.size) * 76 / 168, color: Color.white)
            return AnyView(
                Image(systemName: systemImageName)
                    .font(textStyle.font)
                    .foregroundColor(textStyle.color)
            )
            
        case .image(imageName: let imageName):
            return AnyView(
                Image(imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            )
        }
    }
    
}

struct AvatarView_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView{
            VStack (spacing: 10){
                AvatarView(props: AvatarViewProps(size: 76, style: .name(firstName: "JC", lastName: "DC", company: "Company")))
                AvatarView(props: AvatarViewProps(size: 76, style: .image(imageName: "avatarUnknown")))
                AvatarView(props: AvatarViewProps(size: 76, style: .bitmapImage(urlString: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640")))
                AvatarView(props: AvatarViewProps(size: 76, style: .whiteGlyph(systemImageName: "star.fill")))
                AvatarView(props: AvatarViewProps(size: 76, style: .blueGlyph(systemImageName: "person.2.fill")))
                AvatarView(props: AvatarViewProps(size: 76, style: .image(imageName: "avatarChannel")))
                AvatarView(props: AvatarViewProps(size: 76, style: .image(imageName: "avatarLocation")))
                AvatarView(props: AvatarViewProps(size: 76, style: .image(imageName: "flag_AO")))
                AvatarView(props: AvatarViewProps(size: 76, style: .image(imageName: "avatarGroup")))
            }
        }
    }
}
