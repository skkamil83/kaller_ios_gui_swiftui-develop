import SwiftUI

struct AvatarLoaderProps {
    let size: CGFloat
    let firstName: String
    let lastName: String
    let avatarImage: UIImage?
    let avatarUrl: String?
    let onEditPhoto: () -> Void
}
