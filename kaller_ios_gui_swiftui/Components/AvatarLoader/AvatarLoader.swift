import SwiftUI

struct AvatarLoader: View {
    let props: AvatarLoaderProps

    var body: some View {
        avatarView(props: props)
                .frame(width: props.size, height: props.size)
    }

    private func avatarView(props: AvatarLoaderProps) -> some View {
        ZStack {
            avatar(props: props)
            newPhotoButton(props: props)
        }
                .onTapGesture {
                    props.onEditPhoto()
                }
    }

    private func avatar(props: AvatarLoaderProps) -> some View {
        if let image = props.avatarImage {
            return AnyView(
                    Image(uiImage: image)
                            .resizable()
                            .frame(width: props.size, height: props.size)
                            .aspectRatio(contentMode: .fill)
            )
        } else {
            return AnyView(
                    AvatarView(
                            props: AvatarViewProps(
                                    size: props.size,
                                    style: AvatarViewHelper.getAvatarStyle(
                                            url: props.avatarUrl ?? "",
                                            firstName: props.firstName,
                                            lastName: props.lastName
                                    )
                            )
                    )
            )
        }
    }

    private func newPhotoButton(props: AvatarLoaderProps) -> some View {
        HStack {
            Spacer()
            VStack {
                Spacer()
                AvatarView(
                        props: AvatarViewProps(
                                size: props.size * 28 / 86,
                                style: .image(imageName: "photo")
                        )
                )
            }
        }
                .offset(x: props.size * 1.5 / 86, y: props.size * 1.5 / 86)
    }
}

struct AvatarLoader_Previews: PreviewProvider {
    static var previews: some View {
        AvatarLoader(
                props: AvatarLoaderProps(
                        size: 86,
                        firstName: "aaa",
                        lastName: "bbbb",
                        avatarImage: nil,
                        avatarUrl: nil,
                        onEditPhoto: {}
                )
        )
    }
}
