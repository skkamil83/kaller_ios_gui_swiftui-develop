//
//  RegistrationEmailView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import SwiftUI

struct RegistrationEmailView: View {
    let props: RegistrationEmailViewProps

    var body: some View {
        VStack(spacing: 0){
            yourEmailText()
            pleaseEnterEmailText()
            emailView(props: props)
            Spacer()
                .frame(maxHeight: 84)
            usePhoneNumberButton(props: props)
            Spacer()
        }
    }
    
    private func yourEmailText() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 28, color: .black)
        return Text("Your Email")
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.top, -28)
    }
    
    private func pleaseEnterEmailText() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return Text("Please enter your email address.")
            .frame(height: 22)
            .multilineTextAlignment(.center)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.top, 16)
    }
    
    private func emailView(props: RegistrationEmailViewProps) -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 20, color: .black)
        return VStack(spacing: 4){
            Divider()
            TextField("someone@example.com", text: props.email)
                .frame(height: 40)
                .font(textStyle.font)
                .foregroundColor(textStyle.color)
            Divider()
        }
        .padding(EdgeInsets(top: 96, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func usePhoneNumberButton(props: RegistrationEmailViewProps) -> some View{
        Button(action: props.onSelectUsePhoneNumber){
            Text("Use a phone number instead")
                .font(TextStyle.subheadline1Default1Light5Blue.font)
                .foregroundColor(TextStyle.subheadline1Default1Light5Blue.color)
        }
    }
}

struct RegistrationEmailView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationEmailView(
            props: RegistrationEmailViewProps(
                email: .constant(""),
                onSelectUsePhoneNumber: {}
            )
        )
    }
}
