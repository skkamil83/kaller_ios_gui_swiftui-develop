//
//  RegistrationEmailViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import SwiftUI

struct RegistrationEmailViewProps{
    let email: Binding<String>
    let onSelectUsePhoneNumber: () -> Void
}
