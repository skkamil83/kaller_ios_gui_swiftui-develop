//
//  RegistrationCountryCodeSelectionProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import SwiftUI

struct RegistrationCountryCodeSelectionProps{
    let country: Country
    let phoneNumberWithoutPhoneCode: Binding<String>
    let onSelectCountryCode: () -> Void
}
