//
//  RegistrationCountryCodeSelection.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import SwiftUI

struct RegistrationCountryCodeSelection: View {
    let props: RegistrationCountryCodeSelectionProps

    var body: some View {
        phoneNumberSelectionView(props: props)
    }
    
    private func phoneNumberSelectionView(props: RegistrationCountryCodeSelectionProps) -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 20, color: Color.black)
        
        return VStack(spacing: 4){
            Divider()
            HStack(spacing: 7){
                Button(action: props.onSelectCountryCode){
                    countryCodeView(props: props)
                }
                TextField("Your phone number", text: props.phoneNumberWithoutPhoneCode)
                    .font(textStyle.font)
                    .foregroundColor(textStyle.color)
                    .keyboardType(.numberPad)
                    .minimumScaleFactor(0.1)
                    .lineLimit(1)
            }
            Divider()
        }
    }
    
    private func countryCodeView(props: RegistrationCountryCodeSelectionProps) -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 20, color: .black)
        return HStack(spacing: 0){
            CircleIcon(
                props: CircleIconProps(
                    size: 21,
                    style: .filled(imageName: props.country.flagImageName)
                )
            )
            .padding(.leading, 8)
            Text(RegistrationCountryCodeSelectionHelper.getPhoneCode(country: props.country))
                .font(textStyle.font)
                .foregroundColor(textStyle.color)
                .lineLimit(1)
                .minimumScaleFactor(0.1)
                .padding(.leading, 8)
            Image("controlsRowRightLight")
                .padding(EdgeInsets(top: 0, leading: 7, bottom: 0, trailing: 7))
        }
        .frame(height: 40)
        .background(Color.graysSystemGray5Light.opacity(0.33))
        .cornerRadius(8)
    }
}

struct RegistrationCountryCodeSelection_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationCountryCodeSelection(
            props: RegistrationCountryCodeSelectionProps(
                country: Country(
                    localizedName: "Afghanistan",
                    iso: "AF",
                    phoneCode: 93,
                    countryCode: 4,
                    flagImageName: "flag_AF"),
                phoneNumberWithoutPhoneCode: .constant(""),
                onSelectCountryCode: {}
            )
        )
    }
}
