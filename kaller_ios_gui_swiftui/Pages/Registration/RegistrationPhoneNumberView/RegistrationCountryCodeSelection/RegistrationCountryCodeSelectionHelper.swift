//
//  RegistrationCountryCodeSelectionHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import Foundation

class RegistrationCountryCodeSelectionHelper{
    static func getPhoneCode(country: Country) -> String {
        guard let phoneCode = country.phoneCode else {return ""}
        return "+\(phoneCode)"
    }
}
