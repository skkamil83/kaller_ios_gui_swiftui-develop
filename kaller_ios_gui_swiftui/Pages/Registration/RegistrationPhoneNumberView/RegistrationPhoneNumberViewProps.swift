//
//  RegistrationPhoneNumberViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import SwiftUI

struct RegistrationPhoneNumberViewProps{
    let phoneNumberWithoutCode: Binding<String>
    let country: Country
    let onSelectCountryCode: () -> Void
    let onSelectUsingEmail: () -> Void
}
