//
//  RegistrationPhoneNumberView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import SwiftUI

struct RegistrationPhoneNumberView: View {
    let props: RegistrationPhoneNumberViewProps
    
    var body: some View {
        VStack(spacing: 0){
            yourPhoneText()
            confirmText()
            phoneNumberView(props: props)
            Spacer()
                .frame(maxHeight: 84)
            useEmailButton(props: props)
            Spacer()
        }
    }
    
    private func yourPhoneText() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 28, color: .black)
        return Text("Your Phone")
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.top, -28)
    }
    
    private func confirmText() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return Text("Please confirm your country code\nand enter your phone number.")
            .frame(height: 44)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .lineLimit(2)
            .minimumScaleFactor(0.1)
            .multilineTextAlignment(.center)
            .padding(.top, 16)
    }
    
    private func phoneNumberView(props: RegistrationPhoneNumberViewProps) -> some View{
        return RegistrationCountryCodeSelection(
            props: RegistrationCountryCodeSelectionProps(
                country: props.country,
                phoneNumberWithoutPhoneCode: props.phoneNumberWithoutCode,
                onSelectCountryCode: props.onSelectCountryCode
            )
        )
        .padding(EdgeInsets(top: 74, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func useEmailButton(props: RegistrationPhoneNumberViewProps) -> some View{
        Button(action: props.onSelectUsingEmail){
            Text("Use an email address instead")
                .font(TextStyle.subheadline1Default1Light5Blue.font)
                .foregroundColor(TextStyle.subheadline1Default1Light5Blue.color)
        }
    }

}

struct RegistrationPhoneNumberView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationPhoneNumberView(
            props: RegistrationPhoneNumberViewProps(
                phoneNumberWithoutCode: .constant(""),
                country: Country(
                    localizedName: "Afghanistan",
                    iso: "AF",
                    phoneCode: 93,
                    countryCode: 4,
                    flagImageName: "flag_AF"),
                onSelectCountryCode: {},
                onSelectUsingEmail: {}
            )
        )
    }
}
