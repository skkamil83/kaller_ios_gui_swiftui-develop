//
//  RegistrationPhoneNumberContainerHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 13/12/2020.
//

import Foundation

class RegistrationContainerHelper{
    static func getFullPhoneNumber(country: Country, phoneNumberWithoutPhoneCode: String) -> String{
        guard let phoneCode = country.phoneCode else {return ""}
        return "+\(phoneCode) \(phoneNumberWithoutPhoneCode)"
    }
}
