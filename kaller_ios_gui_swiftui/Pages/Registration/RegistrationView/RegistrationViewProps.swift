//
//  RegistrationViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import Foundation

struct RegistrationViewProps{
    let country: Country
    let onNextWithPhoneNumber: (_ country: Country, _ phoneNumberWithouCode: String) -> Void
    let onNextWithEmail: (String) -> Void
    let onSelectCountryCode: () -> Void
}
