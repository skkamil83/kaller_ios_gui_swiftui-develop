//
//  RegistrationView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import SwiftUI

struct RegistrationView: View {
    let props: RegistrationViewProps
    @State private var email = ""
    @State private var phoneNumberWithoutCode = ""
    @State private var viewType: RegistrationViewType = .byPhoneNumber
    
    var body: some View {
        registration()
            .navigationBarItems(
                trailing: nextButton(props: props)
            )
            .navigationBarTitleDisplayMode(.large)
    }
    
    private func registration() -> some View{
        if(viewType == .byPhoneNumber)
        {
            return AnyView(phoneNumber(props: props))
        }else{
            return AnyView(email(props: props))
        }
    }
    
    private func phoneNumber(props: RegistrationViewProps) -> some View{
        RegistrationPhoneNumberView(
            props: RegistrationPhoneNumberViewProps(
                phoneNumberWithoutCode: $phoneNumberWithoutCode,
                country: props.country,
                onSelectCountryCode: props.onSelectCountryCode,
                onSelectUsingEmail: {
                    viewType = .byEmail
                }
            )
        )
    }
    
    private func email(props: RegistrationViewProps) -> some View{
        RegistrationEmailView(
            props: RegistrationEmailViewProps(
                email: $email,
                onSelectUsePhoneNumber: {
                    viewType = .byPhoneNumber
                }
            )
        )
    }
    
    private func nextButton(props: RegistrationViewProps) ->  some View{
        let grayTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.tintsDisabledLight)
        let blueTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.primaryBlue)
        
        let ableToNext = RegistrationViewHelper.checkAbleToNext(
            viewType: viewType,
            phoneNumberWithoutCode: phoneNumberWithoutCode,
            email: email
        )
        
        return Button(action: {
            onNext(props: props)
        }){
            Text("Next")
                .font(ableToNext ? blueTextStyle.font : grayTextStyle.font)
                .foregroundColor(ableToNext ? blueTextStyle.color : grayTextStyle.color)
        }
        .disabled(!ableToNext)
    }
    
    private func onNext(props: RegistrationViewProps){
        if viewType == .byPhoneNumber{
            props.onNextWithPhoneNumber(props.country, phoneNumberWithoutCode)
        }else{
            props.onNextWithEmail(email)
        }
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView(
            props: RegistrationViewProps(
                country: Country(
                    localizedName: "Afghanistan",
                    iso: "AF",
                    phoneCode: 93,
                    countryCode: 4,
                    flagImageName: "flag_AF"
                ),
                onNextWithPhoneNumber: {_,_ in},
                onNextWithEmail: {_ in},
                onSelectCountryCode: {}
            )
        )
    }
}
