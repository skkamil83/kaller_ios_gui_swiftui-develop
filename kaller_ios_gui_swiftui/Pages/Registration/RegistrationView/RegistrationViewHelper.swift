//
//  RegistrationViewHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import Foundation

class RegistrationViewHelper{
    static func checkAbleToNext(viewType: RegistrationViewType, phoneNumberWithoutCode: String, email: String) -> Bool{
        if( viewType == .byPhoneNumber){
            return !phoneNumberWithoutCode.isEmpty
        }else{
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: email)
        }
    }
}
