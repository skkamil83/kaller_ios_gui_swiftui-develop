//
//  RegistrationPhoneNumberContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import SwiftUI

struct RegistrationContainerProps{
    
}

struct RegistrationContainer: View {
    let props: RegistrationContainerProps
    @State private var country: Country
    @State private var phoneNumber = ""
    @State private var email = ""
    @State private var isPresentCountrySelection = false
    @State private var alert: RegistrationMessageType?
    @State private var phushToVerifyNewUser = false
    @State private var phushToVerifyOldUserUser = false
    @State private var phushToEnterPassword = false
    
    init(props: RegistrationContainerProps) {
        self.props = props
        _country = .init(wrappedValue: Country(
                            localizedName: "Afghanistan",
                            iso: "AF",
                            phoneCode: 93,
                            countryCode: 4,
                            flagImageName: "flag_AF")
        )
    }
    
    var body: some View {
        RegistrationView(
            props: RegistrationViewProps(
                country: country,
                onNextWithPhoneNumber: onNextWithPhoneNumber,
                onNextWithEmail: onNextWithEmail,
                onSelectCountryCode: onSelectCountryCode
            )
        )
        .sheet(isPresented: $isPresentCountrySelection, content: {
            CountrySelectionContainer(
                props: CountrySelectionContainerProps(
                    onSelected: {country in
                        self.country = country
                    }
                )
            )
        })
        .alert(item: $alert) { (item) -> Alert in
            switch item{
            case .confirm:
                return alertConfirm()
            case .invalid:
                return alertInvalid()
            case .loggedIn:
                return alertLoggedIn()
            }
        }
        verifyNewUser()
        enterPassword()
    }
    
    private func onSelectCountryCode(){
        isPresentCountrySelection.toggle()
    }
    
    private func onNextWithPhoneNumber(country: Country, phoneNumberWithoutCode: String){
        self.phoneNumber = phoneNumberWithoutCode
        alert = .confirm
    }
    
    private func onNextWithEmail(email: String){
        self.email = email
        phushToEnterPassword = true
    }
    
    private func verifyNewUser() -> some View{
        NavigationLink(
            destination: VerifyPhoneNumberNewUserContainer(
                props: VerifyPhoneNumberNewUserContainerProps(
                    phoneNumber: RegistrationContainerHelper.getFullPhoneNumber(country: country, phoneNumberWithoutPhoneCode: self.phoneNumber
                    )
                )
            ),
            isActive: $phushToVerifyNewUser
        ) {
            
        }
    }
    
    private func enterPassword() -> some View{
        NavigationLink(
            destination: EnterPasswordContainer(
                props: EnterPasswordContainerProps(
                    email: email
                )
            ),
            isActive: $phushToEnterPassword
        ) {
            
        }
    }
    
    private func alertConfirm() -> Alert{
        let phoneNumber = RegistrationContainerHelper.getFullPhoneNumber(country: country, phoneNumberWithoutPhoneCode: self.phoneNumber)
        return Alert(
            title: Text(phoneNumber),
            message: Text("Is your phone number above correct?"),
            primaryButton: .default(Text("Edit")),
            secondaryButton: .default(Text("Yes")){
                confrimPhoneNumber()
            })
    }
    
    private func alertInvalid() -> Alert{
        Alert(
            title: Text("Invalid phone number.\nSMS is not supported."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func alertLoggedIn() -> Alert{
        Alert(
            title: Text("This phone number is currently logged in on this device."),
            message: nil,
            dismissButton: .default(Text("OK")){
                phushToVerifyOldUserUser = true
            }
        )
    }
    
    private func confrimPhoneNumber(){
        //send phone number to Backend
        let isCorrect = true
        let isNewUser = true
        if isCorrect{
            if isNewUser{
                phushToVerifyNewUser = true
            }else{
                alert = .loggedIn
            }
        }else{
            alert = .invalid
        }
    }

}

struct RegistrationPhoneNumberContainer_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationContainer(
            props: RegistrationContainerProps()
        )
    }
}
