//
//  RegistrationPhoneNumberMessageType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 13/12/2020.
//

import SwiftUI

enum RegistrationMessageType: Identifiable{
    var id: String{
        switch self {
        case .confirm:
            return "confirm"
        case .invalid:
            return "invalid"
        case .loggedIn:
            return "loggedIn"
        }
    }
    case confirm
    case invalid
    case loggedIn
}
