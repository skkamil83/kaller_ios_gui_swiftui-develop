enum AuthVerificationResult {
    case notVerified
    case inProgress
    case failedWithError
    case emailOrPassIncorrect
    case otpIncorrect
    case verificationSuccess
}