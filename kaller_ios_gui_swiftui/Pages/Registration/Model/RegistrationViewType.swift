//
//  RegistrationViewType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import Foundation

enum RegistrationViewType{
    case byPhoneNumber
    case byEmail
}
