import Foundation
class CallKitViewHelper{
    
    static func getTitleForSim(calls: [CallKitInfo]) -> String {
        var call: CallKitInfo?
        if(calls.count == 1){
            call = calls.first
        }else{
            call = calls.first(where: {$0.status != .hold && $0.status != .callEnded}) ?? calls.first(where: {$0.status == .hold})
        }
        if let call = call{
            return call.callType == .incoming ? "Call to" : "Call from"
        }
        return ""
    }
    
    static func getSimNumber(calls: [CallKitInfo], accountInfo: AccountInfo) -> Int {
        var call: CallKitInfo?
        if(calls.count == 1){
            call = calls.first
        }else{
            call = calls.first(where: {$0.status != .hold && $0.status != .callEnded}) ?? calls.first(where: {$0.status == .hold})
        }
        if let call = call{
            let phoneNumber = call.callType == .incoming ? call.toPhoneNumber : call.fromPhoneNumber
            
            if let phoneInfo = accountInfo.phones.first(where: { $0.phoneNumber == phoneNumber }) {
                return accountInfo.getSimNumber(phone: phoneInfo) ?? 0
            }
        }
        return 0
    }
    
    static func getStatusMessage(calls: [CallKitInfo]) -> String {
        var callOpt: CallKitInfo?
        if(calls.count == 1){
            callOpt = calls.first
        }else{
            callOpt = calls.first(where: {$0.status != .hold})
        }
        guard let call = callOpt else {
            return ""
        }

        let phoneNumber = call.callType == .incoming
                ? call.fromPhoneNumber
                : call.toPhoneNumber

        let place = call.contact?.phones
                .first(where: { $0.phoneNumber == phoneNumber })?
                .place ?? ""
        
        var mess = ""

        switch call.status {
        case .connecting:
            if let extensionNumber = call.connectWithExt {
                mess = "Connecting extension \(extensionNumber)…"
            } else {
                mess = "Connecting\(place.isEmpty ? "" : " \(place)")…"
            }
        case .reconnecting:
            mess = "Reconnecting…"
        case .callEnded:
            mess = "Call ended"
        case .calling:
            mess = "Calling\(place.isEmpty ? "" : " \(place)")…"
        case .userBusy:
            mess = "User Busy"
        case .inCall:
            if(calls.count == 1){
                switch call.billingType{
                case .appToApp:
                    mess = "Kaller to Kaller"
                case .appToPhone:
                    mess = "Kaller to Phone"
                case .phoneToApp:
                    mess = "Phone to Kaller"
                case .phoneToPhone:
                    mess = "Phone to Phone"
                }
                mess = "\(mess) - \(call.seconds.secondsToHoursMinutes())"
            } else {
                mess = "\(call.seconds.secondsToHoursMinutes())"
            }
            
        case .hold:
            return "HOLD"
        }
        
        return calls.count == 1
                ? mess
                : mess.lowercased()
    }
    
    static func getPrice(calls: [CallKitInfo]) -> String {
        var callOpt: CallKitInfo?
        if(calls.count == 1){
            callOpt = calls.first
        }else{
            callOpt = calls.first(where: {$0.status != .hold && $0.status != .callEnded}) ?? calls.first(where: {$0.status == .hold})
        }
        guard let call = callOpt else {
            return ""
        }

        var cost = ""
        if call.callType == .outcoming {
            if(call.billingType == .appToApp) {
                cost = "Free"
            } else {
                cost = "\(call.currency.symbol)\(call.pricePerMinute)\u{200A}/\u{200A}min."
            }
        }

        return cost
    }
    
    static func disableAddCall(calls: [CallKitInfo]) -> Bool {
        
        var callOpt: CallKitInfo?
        if(calls.count == 1){
            callOpt = calls.first
        }else{
            callOpt = calls.first(where: {$0.status != .hold && $0.status != .callEnded}) ?? calls.first(where: {$0.status == .hold})
        }
        guard let call = callOpt else {
            return true
        }

        return call.status == .inCall
    }
    
    static func disableMergeCalls(calls: [CallKitInfo]) -> Bool {
        guard let call = calls.first(where: {$0.status != .hold}) else {
            return true
        }

        return call.status != .inCall
    }
    
    static func disableSwap(calls: [CallKitInfo]) -> Bool {
        guard let call = calls.first(where: {$0.status != .hold}) else {
            return true
        }

        return call.status != .inCall
    }
    
    static func showHoldButton(calls: [CallKitInfo]) -> Bool {
        guard let _ = calls.first(where: {$0.status == .hold}) else {
            return false
        }
        if(calls.count == 1){
            return true
        }else{
            return calls.first(where: {$0.status == .callEnded}) != nil
        }
    }
    
    static func disableKeypad(calls: [CallKitInfo]) -> Bool {
        var callOpt: CallKitInfo?
        if(calls.count == 1){
            callOpt = calls.first
        }else{
            callOpt = calls.first(where: {$0.status != .hold && $0.status != .callEnded}) ?? calls.first(where: {$0.status == .hold})
        }
        guard let call = callOpt else {
            return true
        }

        return call.billingType == .appToApp
    }
    
    static func disableVideo(calls: [CallKitInfo]) -> Bool {
        var callOpt: CallKitInfo?
        if(calls.count == 1){
            callOpt = calls.first
        }else{
            callOpt = calls.first(where: {$0.status != .hold && $0.status != .callEnded}) ?? calls.first(where: {$0.status == .hold})
        }
        guard let call = callOpt else {
            return true
        }

        return call.billingType != .appToApp
    }
    
    static func getCurrentRecipientPhoneNumber(calls: [CallKitInfo]) -> String {
        var callOpt: CallKitInfo?
        if(calls.count == 1){
            callOpt = calls.first
        }else{
            callOpt = calls.first(where: {$0.status != .hold})
        }
        guard let call = callOpt else {
            return ""
        }

        return call.callType == .incoming
                ? call.fromPhoneNumber
                : call.toPhoneNumber
    }
    
    static func getCurrentRecipientContact(calls: [CallKitInfo]) -> Contact? {
        var call: CallKitInfo?
        if(calls.count == 1){
            call = calls.first
        }else{
            call = calls.first(where: {$0.status != .hold})
        }
        return call?.contact
    }
    
    static func getCurrentRecipientName(calls: [CallKitInfo]) -> String {
        var callOpt: CallKitInfo?
        if(calls.count == 1){
            callOpt = calls.first
        }else{
            callOpt = calls.first(where: {$0.status != .hold})
        }
        guard let call = callOpt else {
            return ""
        }

        if let contact = call.contact {
            var result = ""
            
            if let firstName = contact.firstName {
                result += firstName.isEmpty ? "" : "\(firstName) "
            }
            
            if let lastName = contact.lastName {
                result += lastName
            }
            
            return result
        }else{
            return getCurrentRecipientPhoneNumber(calls: calls)
        }
    }
    
    static func getHoldPhoneNumber(calls: [CallKitInfo]) -> String {
        guard let call = calls.first(where: { $0.status == .hold }) else {
            return ""
        }

        return call.callType == .incoming
                ? call.fromPhoneNumber
                : call.toPhoneNumber
    }
    
    static func getHoldContact(calls: [CallKitInfo]) -> Contact? {
        calls
                .first(where: { $0.status == .hold })?
                .contact
    }
    
    static func getHoldCallingName(calls: [CallKitInfo]) -> String {
        guard let call = calls.first(where: { $0.status == .hold }) else {
            return ""
        }

        if let contact = call.contact {
            var result = ""
            
            if let firstName = contact.firstName {
                result += firstName.isEmpty ? "" : "\(firstName) "
            }
            
            if let lastName = contact.lastName {
                result += lastName
            }
            
            return result
        } else {
            return getHoldPhoneNumber(calls: calls)
        }
    }
}
