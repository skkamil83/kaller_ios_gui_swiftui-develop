//
//  CallKitFunctionButtons.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 25/12/2020.
//

import SwiftUI

struct CallKitFunctionButtons: View {
    let props: CallKitFunctionButtonsProps
    
    var body: some View {
        VStack(spacing: 18) {

            HStack(spacing: 29) {
                if props.isShowHoldButton{
                    hold(props: props)
                }else{
                    mute(props: props)
                }
                keypad(props: props)
                audio(props: props)
            }

            HStack(spacing: 29) {
                if props.isInHold {
                    mergeCallS(props: props)
                    swap(props: props)
                } else {
                    addCall(props: props)
                    video(props: props)
                }

                contacts(props: props)
            }
        }
    }
    
    private func mute(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .systemImage(systemImageName: "mic.slash.fill"),
                buttonName: "mute",
                buttonSize: props.buttonSize,
                isSelected: props.isMuted,
                onClick: props.onClickedMute
            )
        )
    }
    
    private func keypad(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .image(imageName: "callkitButtonKeypad"),
                buttonName: "keypad",
                buttonSize: props.buttonSize,
                isSelected: false,
                onClick: props.onClickedKeypad
            )
        )
                .disabled(props.isKeypadDisable)
    }
    
    private func audio(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .image(imageName: "callkitButtonAudio"),
                buttonName: "audio",
                buttonSize: props.buttonSize,
                isSelected: props.isAudioSelected,
                onClick: props.onClickedAudio
            )
        )
                .animation(nil)
    }
    
    private func addCall(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .systemImage(systemImageName: "plus"),
                buttonName: "add call",
                buttonSize: props.buttonSize,
                isSelected: false,
                onClick: props.onClickedAddCall
            )
        )
                .disabled(props.isAddCallDisable)
    }
    
    private func video(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .image(imageName: "callkitXButtonVideo"),
                buttonName: "video",
                buttonSize: props.buttonSize,
                isSelected: false,
                onClick: props.onClickedVideo
            )
        )
                .disabled(props.isVideoDisable)
    }
    
    private func contacts(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .systemImage(systemImageName: "person.crop.circle"),
                buttonName: "contacts",
                buttonSize: props.buttonSize,
                isSelected: false,
                onClick: props.onClickedContacts
            )
        )
    }
    
    private func mergeCallS(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .systemImage(systemImageName: "arrow.triangle.merge"),
                buttonName: "merge calls",
                buttonSize: props.buttonSize,
                isSelected: false,
                onClick: props.onMergeCalls
            )
        )
                .disabled(props.isDisableMerge)
    }
    
    private func swap(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
            props: CallKitMultiStatesButtonProps(
                buttonType: .systemImage(systemImageName: "arrow.triangle.swap"),
                buttonName: "swap",
                buttonSize: props.buttonSize,
                isSelected: false,
                onClick: props.onSwap
            )
        )
                .disabled(props.isDisableSwap)
    }
    
    private func hold(props: CallKitFunctionButtonsProps) -> some View {
        CallKitMultiStatesButton(
                props: CallKitMultiStatesButtonProps(
                        buttonType: .systemImage(systemImageName: "pause.fill"),
                        buttonName: "hold",
                        buttonSize: props.buttonSize,
                        isSelected: true,
                        onClick: props.onStopHold
                )
        )
    }
}

struct CallKitFunctionButtons_Previews: PreviewProvider {
    static var previews: some View {
        CallKitFunctionButtons(
            props: CallKitFunctionButtonsProps(
                isInHold: true,
                buttonSize: 72,
                isMuted: true,
                isAudioSelected: true,
                isKeypadDisable: false,
                isAddCallDisable: false,
                isVideoDisable: false,
                isDisableMerge: false,
                isDisableSwap: false,
                isShowHoldButton: false,
                onClickedMute: {},
                onClickedKeypad: {},
                onClickedAudio: {},
                onClickedAddCall: {},
                onClickedVideo: {},
                onClickedContacts: {},
                onMergeCalls: {},
                onSwap: {},
                onStopHold: {}
            )
        )
                .background(Color.gray)
    }
}
