import SwiftUI

struct CallKitFunctionButtonsProps {
    let isInHold: Bool
    let buttonSize: CGFloat
    let isMuted: Bool
    let isAudioSelected: Bool
    let isKeypadDisable: Bool
    let isAddCallDisable: Bool
    let isVideoDisable: Bool
    let isDisableMerge: Bool
    let isDisableSwap: Bool
    let isShowHoldButton: Bool
    let onClickedMute: () -> Void
    let onClickedKeypad: () -> Void
    let onClickedAudio: () -> Void
    let onClickedAddCall: () -> Void
    let onClickedVideo: () -> Void
    let onClickedContacts: () -> Void
    let onMergeCalls: () -> Void
    let onSwap: () -> Void
    let onStopHold: () -> Void
}
