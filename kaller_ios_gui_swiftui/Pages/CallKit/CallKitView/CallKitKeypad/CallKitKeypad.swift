//
//  CallKitKeypad.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 27/12/2020.
//

import SwiftUI

struct CallKitKeypad: View {
    let props: CallKitKeypadProps
    
    var body: some View {
        let buttonSize = getButtonSize()

        return
            VStack(spacing: Spacing.keyHorizontalFix) {
                HStack(spacing: Spacing.keyVerticalFix) {
                    numberButton(props: props, buttonSize: buttonSize, title: "1", subtitle: "")
                    numberButton(props: props, buttonSize: buttonSize, title: "2", subtitle: "A B C")
                    numberButton(props: props, buttonSize: buttonSize, title: "3", subtitle: "D E F")
                }
                HStack(spacing: Spacing.keyVerticalFix) {
                    numberButton(props: props, buttonSize: buttonSize, title: "4", subtitle: "G H I")
                    numberButton(props: props, buttonSize: buttonSize, title: "5", subtitle: "J K L")
                    numberButton(props: props, buttonSize: buttonSize, title: "6", subtitle: "M N O")
                }
                HStack(spacing: Spacing.keyVerticalFix) {
                    numberButton(props: props, buttonSize: buttonSize, title: "7", subtitle: "P Q R S")
                    numberButton(props: props, buttonSize: buttonSize, title: "8", subtitle: "T U V")
                    numberButton(props: props, buttonSize: buttonSize, title: "9", subtitle: "W X Y Z")
                }
                HStack(spacing: Spacing.keyVerticalFix) {
                    asterisk(props: props, buttonSize: buttonSize)
                    zeroButton(props: props, buttonSize: buttonSize)
                    octocorp(props: props, buttonSize: buttonSize)
                }
            }
    }
    
    private func numberButton(props: CallKitKeypadProps, buttonSize: CGFloat, title: String, subtitle: String) -> some View {
        KeypadButtonView(
            props: KeypadButtonProps(
                size: buttonSize,
                style: .titleWithSubTitle(
                    title: title,
                    subtitle: subtitle,
                    normalColor: Color.white.opacity(0.1),
                    highlightColor: Color.white.opacity(0.5),
                    textColor: .white
                ),
                isHandleLongPress: false,
                isShadowWhenLongPress: false,
                onAction: { props.onNumTap(title) }
            )
        )
    }
    
    private func asterisk(props: CallKitKeypadProps, buttonSize: CGFloat) -> some View {
        KeypadButtonView(
            props: KeypadButtonProps(
                size: buttonSize,
                style: .image(
                        normalState: "systemCallkitButtonAsterisk",
                        highlightState: "systemCallkitButtonAsteriskPressed",
                        isResizeAble: true
                ),
                isHandleLongPress: false,
                isShadowWhenLongPress: false,
                onAction: { props.onNumTap("*") }
            )
        )
    }
    
    private func octocorp(props: CallKitKeypadProps, buttonSize: CGFloat) -> some View {
        KeypadButtonView(
            props: KeypadButtonProps(
                size: buttonSize,
                style: .image(
                    normalState: "systemCallkitButtonOctocorp",
                    highlightState: "systemCallkitButtonOctocorpPressed",
                    isResizeAble: true
                ),
                isHandleLongPress: false,
                isShadowWhenLongPress: false,
                onAction: { props.onNumTap("#") }
            )
        )
    }
    
    private func zeroButton(props: CallKitKeypadProps, buttonSize: CGFloat) -> some View {
        KeypadButtonView(
            props: KeypadButtonProps(
                size: buttonSize,
                style: .titleWithSystemImage(
                    title: "0",
                    systemImageName: "plus",
                    normalColor: Color.white.opacity(0.1),
                    highlightColor: Color.white.opacity(0.5),
                    textColor: .white
                ),
                isHandleLongPress: true,
                isShadowWhenLongPress: false,
                onAction: { props.onNumTap("0") },
                onLongPressAction: { props.onNumTap("+") }
            )
        )
    }
    
    private func getButtonSize() -> CGFloat {
        (UIScreen.main.bounds.width - ((Spacing.keyVerticalFix + Spacing.keypadSidesFix) * 2)) / 3
    }
}

struct CallKitKeypad_Previews: PreviewProvider {
    static var previews: some View {
        CallKitKeypad(
            props: CallKitKeypadProps(
                onNumTap: {_ in}
            )
        )
        .background(Color(red: 11/255, green: 42/255, blue: 69/255, opacity: 1))
    }
}
