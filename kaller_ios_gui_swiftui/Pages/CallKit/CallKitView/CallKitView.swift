import SwiftUI

struct CallKitView: View {
    @State private var safeAreaHeight: CGFloat = 0
    @State private var showAudioSelector = false
    @State private var isMuted = false
    @State private var showKeypadView = false
    @State private var keypadText = ""
    
    let props: CallKitViewProps
    
    var body: some View {
        VStack {
            ZStack {
                if props.calls.count > 1 {
                    holdView(props: props)
                } else {
                    if keypadText.isEmpty {
                        recipient(props: props)
                    } else {
                        keypadLabel(props: props)
                    }
                }

                if showKeypadView {
                    keypadView(props: props)
                } else {
                    buttons(props: props)
                }

                endCallButtonsAndOther(props: props)

                if showAudioSelector {
                    audioSelectionView(props: props)
                }
            }
        }
                .background(backgroundImage())
                .navigationBarBackButtonHidden(true)
                .navigationBarTitleDisplayMode(.large)
                .navigationBarItems(
                    leading: backButton(props: props),
                    trailing: simView(props: props)
                )
    }
    
    private func backgroundImage() -> some View {
        Image("callkitBackground")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: UIScreen.main.bounds.width)
            .edgesIgnoringSafeArea(.all)
    }
    
    private func simView(props: CallKitViewProps) -> some View {
        HStack {
            Text(CallKitViewHelper.getTitleForSim(calls: props.calls))
                    .font(TextStyle.subheadline1Default2Dark1LabelColor.font)
                    .foregroundColor(TextStyle.subheadline1Default2Dark1LabelColor.color)

            SimIcon(
                    props: SimIconProps(
                            number: CallKitViewHelper.getSimNumber(calls: props.calls, accountInfo: props.accountInfo),
                            size: CGSize(width: 14, height: 18)
                    )
            )
        }
    }
    
    private func backButton(props: CallKitViewProps) -> some View {
        Button(action: props.onBack) {
            Image(systemName: "chevron.left")
                    .font(TextStyle(fontName: "SFProText-Medium", fontSize: 24, color: .primaryBlue).font)
                    .foregroundColor(.white)
        }
    }
    
    private func recipient(props: CallKitViewProps) -> some View {
        VStack(spacing: 0) {
            CallKitRecipient(
                props: CallKitRecipientProps(
                    contact: CallKitViewHelper.getCurrentRecipientContact(calls: props.calls),
                    phoneNumber: CallKitViewHelper.getCurrentRecipientPhoneNumber(calls: props.calls),
                    status: CallKitViewHelper.getStatusMessage(calls: props.calls)
                )
            )
                    .padding(.top, -28)

            Spacer()
        }
    }
    
    private func keypadLabel(props: CallKitViewProps) -> some View {
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 34, color: .white)

        return VStack(spacing: 0) {
            Text(keypadText)
                .font(textStyle.font)
                .foregroundColor(textStyle.color)
                .lineLimit(1)
                .truncationMode(.head)
                .padding(EdgeInsets(top: -32, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))

            Spacer()
        }
    }
    
    private func holdView(props: CallKitViewProps) -> some View {
        VStack(spacing: 0) {
            CallKitRecipientHolding(
                props: CallKitRecipientHoldingProps(
                    holdingName: CallKitViewHelper.getHoldCallingName(calls: props.calls),
                    callingName: CallKitViewHelper.getCurrentRecipientName(calls: props.calls),
                    callingStatus: CallKitViewHelper.getStatusMessage(calls: props.calls)
                )
            )
            .padding(.top, -30)

            Spacer()
        }
    }
    
    private func buttons(props: CallKitViewProps) -> some View {
        VStack {
            Spacer()
            CallKitFunctionButtons(
                props: CallKitFunctionButtonsProps(
                    isInHold: props.calls.count > 1,
                    buttonSize: getButtonSize(),
                    isMuted: isMuted,
                    isAudioSelected: showAudioSelector,
                    isKeypadDisable: CallKitViewHelper.disableKeypad(calls: props.calls),
                    isAddCallDisable: CallKitViewHelper.disableAddCall(calls: props.calls),
                    isVideoDisable: CallKitViewHelper.disableVideo(calls: props.calls),
                    isDisableMerge: CallKitViewHelper.disableMergeCalls(calls: props.calls),
                    isDisableSwap: CallKitViewHelper.disableSwap(calls: props.calls),
                    isShowHoldButton: CallKitViewHelper.showHoldButton(calls: props.calls),
                    onClickedMute: onClickedMute,
                    onClickedKeypad: onShowKeypad,
                    onClickedAudio: onClickedAudio,
                    onClickedAddCall: props.addCall,
                    onClickedVideo: props.enableVideo,
                    onClickedContacts: props.openContacts,
                    onMergeCalls: props.onMergeCalls,
                    onSwap: {
                        withAnimation{
                            props.onSwap()
                        }
                    },
                    onStopHold: props.onStopHold
                )
            )

            Spacer()
        }
                .edgesIgnoringSafeArea(.all)
    }
    
    private func endCallButtonsAndOther(props: CallKitViewProps) -> some View {
        VStack {
            Spacer()
            HStack(spacing: 0){
                pricePerMinute(props: props)
                        .frame(width: getButtonSize())
                        .padding(.trailing, Spacing.keyVerticalFix)
                endCallButton(props: props)
                rightButton(props: props)
                        .frame(width: getButtonSize())
                        .padding(.leading, Spacing.keyVerticalFix)
            }
                    .padding(.bottom, getCallButtonPaddingBottom())
        }
                .background(collectSafeAreaHeight())
    }
    
    private func endCallButton(props: CallKitViewProps) -> some View {
        KeypadButtonView(
            props: KeypadButtonProps(
                size: getButtonSize(),
                style: .image(
                        normalState: "systemCallkitFinish",
                        highlightState: "systemCallkitFinishPressed",
                        isResizeAble: true
                ),
                isHandleLongPress: false,
                isShadowWhenLongPress: false,
                onAction: props.onEndCall
            )
        )
    }
    
    private func rightButton(props: CallKitViewProps) -> some View {
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .white)

        if showKeypadView {
            return AnyView(
                Button(action: {
                    showKeypadView = false
                    keypadText = ""
                }){
                    Text("Hide")
                        .font(textStyle.font)
                        .foregroundColor(textStyle.color)
                }
            )
        } else {
            return AnyView(
                Button(action: props.transfer){
                    Text("Transfer")
                        .font(textStyle.font)
                        .foregroundColor(textStyle.color)
                        .fixedSize()
                }
            )
        }
    }
    
    private func pricePerMinute(props: CallKitViewProps) -> some View {
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .white)

        return Text(CallKitViewHelper.getPrice(calls: props.calls))
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
    }
    
    private func audioSelectionView(props: CallKitViewProps) -> some View {
        VStack {
            HStack {
                Spacer()
                CallKitAudioSelector(
                    props: CallKitAudioSelectorProps(
                        viewModels: props.speakers,
                        onSelected: { viewModel in
                            props.onSelectSpeaker(viewModel)
                            showAudioSelector = false
                        }
                    )
                )
                    .padding(.trailing, Spacing.spacing16Pt)
            }
                    .padding(.top, UIScreen.main.bounds.height/2 - 9)

            Spacer()
        }
                .background(Color.white.opacity(0))
                .edgesIgnoringSafeArea(.all)
                .contentShape(Rectangle())
                .onTapGesture {
                    showAudioSelector = false
                }
    }
    
    private func keypadView(props: CallKitViewProps) -> some View{
        VStack {
            Spacer()
            CallKitKeypad(
                props: CallKitKeypadProps(
                    onNumTap: { character in
                        props.onNumPad(character)
                        keypadText += character
                    }
                )
            )
                    .padding(.bottom, getCallButtonPaddingBottom() + getButtonSize() + 14)
        }
    }
    
    private func onClickedMute() {
        isMuted.toggle()
        props.updateMuteStatus(isMuted)
    }
    
    private func onClickedAudio(){
        withAnimation{
            showAudioSelector = true
        }
    }
    
    private func onShowKeypad() {
        withAnimation{
            showKeypadView = true
        }
    }
    
    private func getButtonSize() -> CGFloat {
        (UIScreen.main.bounds.width - (46 * 2) - (29 * 2))/3
    }
    
    private func getKeypadHeight() -> CGFloat {
        getButtonSize() * 5 + 14 * 4
    }
    
    private func getCallButtonPaddingBottom() -> CGFloat {
        (safeAreaHeight - getKeypadHeight() - 72) * 5 / 17 + 50
    }
    
    private func collectSafeAreaHeight() -> some View {
        GeometryReader{ geometry -> Color in
            DispatchQueue.main.async {
                safeAreaHeight = geometry.size.height
            }
            return Color.clear
        }
    }
}
