import Foundation

class CallKitRecipientHelper {
    static func getName(contact: Contact?, phoneNumber: String) -> String{
        if let contact = contact {
            var result = ""
            
            if let firstName = contact.firstName {
                result += firstName.isEmpty ? "" : "\(firstName) "
            }
            
            if let lastName = contact.lastName {
                result += lastName
            }
            
            return result
        } else {
            return phoneNumber
        }
    }
}
