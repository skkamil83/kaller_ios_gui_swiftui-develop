import Foundation

struct CallKitRecipientProps {
    let contact: Contact?
    let phoneNumber: String
    let status: String
}
