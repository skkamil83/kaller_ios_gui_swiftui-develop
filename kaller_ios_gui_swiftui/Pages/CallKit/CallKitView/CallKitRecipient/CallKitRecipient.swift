import SwiftUI

struct CallKitRecipient: View {
    let props: CallKitRecipientProps

    private let maxLength = UIScreen.main.bounds.width - 128

    var body: some View {
        HStack(spacing: Spacing.spacing16Pt) {
            avatar(props: props)
            VStack(alignment: .leading, spacing: 0) {
                name(props: props)
                Spacer()
                status(props: props)
            }
        }
        .frame(height: 63)
        .padding(EdgeInsets(top: 0, leading: 36, bottom: 0, trailing: Spacing.spacing16Pt))
    }
    
    private func avatar(props: CallKitRecipientProps) -> some View {
        AvatarView(
            props: AvatarViewProps(
                size: 60,
                style: AvatarViewHelper.getAvatarStyle(from: props.contact)
            )
        )
    }
    
    private func name(props: CallKitRecipientProps) -> some View {
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 35, color: .white)
        let text =  CallKitRecipientHelper.getName(contact: props.contact, phoneNumber: props.phoneNumber)
        
        return
            ZStack{
                ScrollAroundView(
                    props: ScrollAroundViewProps(
                        viewSize: CGSize(width: maxLength, height: 33),
                        spacing: 50,
                        scrolling: {_ in }
                    )
                ){
                    Text(text)
                        .frame(height: 33)
                        .font(textStyle.font)
                        .foregroundColor(textStyle.color)
                }
            }
            .frame(width: maxLength, height: 33)
            .clipped()
    }
    
    private func status(props: CallKitRecipientProps) -> some View {
        Text(props.status)
            .font(TextStyle.body2Dark2SecondaryLabelColor.font)
            .foregroundColor(TextStyle.body2Dark2SecondaryLabelColor.color)
    }
}

struct CallKitRecipient_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            CallKitRecipient(
                props: CallKitRecipientProps(
                    contact: nil,
                    phoneNumber: "+1 210 000 1000 000",
                    status: "Connecting…"
                )
            )
            Spacer()
        }
        .background(Color.gray)
    }
}
