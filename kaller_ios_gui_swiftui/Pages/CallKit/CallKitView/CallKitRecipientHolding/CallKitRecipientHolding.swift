//
//  CallKitRecipientHolding.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 27/12/2020.
//

import SwiftUI

struct CallKitRecipientHolding: View {
    @State private var firstNameLength: CGFloat = 0
    @State private var secondNameLength: CGFloat = 0
    
    let props: CallKitRecipientHoldingProps
    
    var body: some View {
        VStack(spacing: 11) {
            firstContact(props: props)
            secondContact(props: props)
        }
            .padding(EdgeInsets(top: 0, leading: 40, bottom: 0, trailing: 40))
    }
    
    private func firstContact(props: CallKitRecipientHoldingProps) -> some View{
        let nameTextStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 24, color: Color.secondaryLabelDark)
        let holdTextStyle = TextStyle(fontName: "SFProDisplay-Light", fontSize: 24, color: Color.tertiaryLabelDark)
        
        return
            HStack(spacing: 0) {
                ZStack {
                    ScrollAroundView(
                        props: ScrollAroundViewProps(
                            viewSize: CGSize(width: firstNameLength, height: 28),
                            spacing: 50,
                            scrolling: {_ in}
                        )
                    ){
                        Text(props.holdingName)
                                .font(nameTextStyle.font)
                                .foregroundColor(nameTextStyle.color)
                    }
                }

                Text("HOLD")
                        .font(holdTextStyle.font)
                        .foregroundColor(holdTextStyle.color)
                        .fixedSize()
                        .padding(.leading, Spacing.spacing16Pt)
                        .background(collectFirstNameLength())
            }
                    .frame(height: 28)
    }
    
    private func secondContact(props: CallKitRecipientHoldingProps) -> some View {
        let nameTextStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 24, color: .white)
        let holdTextStyle = TextStyle(fontName: "SFProDisplay-Light", fontSize: 24, color: Color.secondaryLabelDark)
        
        return
            HStack(spacing: 0) {
                ZStack {
                    ScrollAroundView(
                        props: ScrollAroundViewProps(
                            viewSize: CGSize(width: secondNameLength, height: 28),
                            spacing: 50,
                            scrolling: {_ in }
                        )
                    ){
                        Text(props.callingName)
                                .font(nameTextStyle.font)
                                .foregroundColor(nameTextStyle.color)
                    }
                }

                Text(props.callingStatus)
                        .font(holdTextStyle.font)
                        .foregroundColor(holdTextStyle.color)
                        .fixedSize()
                        .padding(.leading, Spacing.spacing16Pt)
                        .background(collectSecondNameLength())
            }
                    .frame(height: 28)
    }
    
    private func collectFirstNameLength() -> some View {
        GeometryReader{ geometry -> Color in
            let newLength = UIScreen.main.bounds.width - 80 - 16 - geometry.size.width

            if firstNameLength != newLength {
                DispatchQueue.main.async{
                    firstNameLength = newLength
                }
            }

            return Color.clear
        }
    }
    
    private func collectSecondNameLength() -> some View {
        GeometryReader { geometry -> Color in
            let newLength = UIScreen.main.bounds.width - 80 - 16 - geometry.size.width

            if secondNameLength != newLength {
                DispatchQueue.main.async {
                    if abs(newLength - secondNameLength) > 20 {
                        secondNameLength = newLength
                    }
                }
            }

            return Color.clear
        }
    }
}

struct CallKitRecipientHolding_Previews: PreviewProvider {
    static var previews: some View {
        CallKitRecipientHolding(
            props: CallKitRecipientHoldingProps(
                holdingName: "Richard Miles",
                callingName: "+1 200 100 0001",
                callingStatus: "Calling...")
        )
        .background(Color.gray)
    }
}
