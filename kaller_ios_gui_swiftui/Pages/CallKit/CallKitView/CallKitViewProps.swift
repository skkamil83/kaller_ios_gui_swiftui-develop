import Foundation

struct CallKitViewProps{
    let calls: [CallKitInfo]
    let accountInfo: AccountInfo
    let speakers: [CallKitAudioSelectorViewModel]
    let onBack: () -> Void
    let onEndCall: () -> Void
    let updateMuteStatus: (_ isMuted: Bool) -> Void
    let onSelectSpeaker: (CallKitAudioSelectorViewModel) -> Void
    let addCall: () -> Void
    let enableVideo: () -> Void
    let openContacts: () -> Void
    let transfer: () -> Void
    let onNumPad: (String) -> Void
    let onMergeCalls: () -> Void
    let onSwap: () -> Void
    let onStopHold: () -> Void
}
