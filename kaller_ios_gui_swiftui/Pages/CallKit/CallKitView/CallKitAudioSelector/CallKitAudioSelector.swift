//
//  CallKitAudioSelector.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 26/12/2020.
//

import SwiftUI

struct CallKitAudioSelector: View {
    let props: CallKitAudioSelectorProps

    var body: some View {
        list(props: props)
    }
    
    private func list(props: CallKitAudioSelectorProps) -> some View {
        VStack(spacing: 0) {
            ForEach(props.viewModels, id: \.id) { item in
                audioSourceListItem(model: item)
                Divider()
            }
        }
                .frame(width: 250)
                .background(Color.white)
                .padding(.bottom, -1)
                .cornerRadius(12)
    }

    private func audioSourceListItem(model: CallKitAudioSelectorViewModel) -> some View {
        HStack(spacing: 0) {
            Image(systemName: "checkmark")
                    .font(TextStyle.body2Bold1Light1LabelColor.font)
                    .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
                    .frame(width: 44)
                    .opacity(model.isSelected ? 1 : 0)
            Text(model.name)
            Spacer()
            Image(systemName: model.icon.rawValue)
                    .font(TextStyle.body1Default1Light1LabelColor2CenterAligned.font)
                    .foregroundColor(TextStyle.body1Default1Light1LabelColor2CenterAligned.color)
                    .frame(width: 57)
        }
                .contentShape(Rectangle())
                .onTapGesture { props.onSelected(model) }
                .padding(EdgeInsets(top: 11, leading: 0, bottom: 11, trailing: 0))
    }
}

struct CallKitAudioSelector_Previews: PreviewProvider {
    static var previews: some View {
        CallKitAudioSelector(
            props: CallKitAudioSelectorProps(
                viewModels: [
                    .init(id: "1", isSelected: true, name: "iPhone", icon: .iPhone),
                    .init(id: "2", isSelected: false, name: "Speaker", icon: .speaker),
                    .init(id: "3", isSelected: false, name: "Alexander's MacBook Pro", icon: .macbook)
                ],
                onSelected: {_ in}
            )
        )
        .background(Color.gray)
    }
}
