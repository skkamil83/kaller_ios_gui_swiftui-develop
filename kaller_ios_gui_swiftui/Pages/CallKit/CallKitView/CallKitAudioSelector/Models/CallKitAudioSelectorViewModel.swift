import Foundation

struct CallKitAudioSelectorViewModel {
    let id: String
    let isSelected: Bool
    let name: String
    let icon: CallKitAudioIconType
}

enum CallKitAudioIconType: String {
    case iPhone = "iphone.homebutton"
    case speaker = "speaker.wave.3"
    case macbook = "laptopcomputer"
}
