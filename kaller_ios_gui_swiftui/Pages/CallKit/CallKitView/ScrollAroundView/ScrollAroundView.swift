//
//  ScrollAroundView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 27/12/2020.
//

import SwiftUI

struct ScrollAroundView<Content: View>: View {
    @State private var offset: CGFloat = 0
    @State private var isScrolling = false
    private let props: ScrollAroundViewProps
    private let content: Content
    private let textAnimation = Animation.linear(duration: 5).repeatForever(autoreverses: false)

    init(props: ScrollAroundViewProps, @ViewBuilder content: () -> Content){
        self.props = props
        self.content = content()
    }
    
    var body: some View {
        ZStack{
            HStack(spacing: 0) {
                content
                    .fixedSize()
                    .background(collectContentWidth())
                content
                    .fixedSize()
                    .padding(.leading, props.spacing)
                    .opacity(isScrolling ? 1 : 0)
            }
                    .frame(maxWidth: props.viewSize.width, maxHeight: props.viewSize.height, alignment: .leading)
                    .offset(x: isScrolling ? -offset : 0)
                    .clipped()
        }
    }
    
    private func collectContentWidth() -> some View {
        GeometryReader{ geometry -> Color in
            let contentWidth = geometry.size.width
            let newOffset = contentWidth + props.spacing
            if isScrolling {
                if(contentWidth < props.viewSize.width){
                    DispatchQueue.main.async{
                        props.scrolling(false)
                        withAnimation(.default){
                            offset = 0
                            isScrolling = false
                        }
                    }
                } else {
                    if offset != newOffset {
                        DispatchQueue.main.async {
                            withAnimation(textAnimation) {
                                offset = newOffset
                            }
                        }
                    }
                }
            } else {
                if contentWidth > props.viewSize.width {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        props.scrolling(true)
                        withAnimation(textAnimation) {
                            offset = newOffset
                            isScrolling = true
                        }
                    }
                }
            }
            return Color.clear
        }
    }
}
