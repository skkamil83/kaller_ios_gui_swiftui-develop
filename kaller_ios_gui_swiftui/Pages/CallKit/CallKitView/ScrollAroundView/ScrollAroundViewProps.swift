import SwiftUI

struct ScrollAroundViewProps {
    let viewSize: CGSize
    let spacing: CGFloat
    let scrolling: (Bool) -> Void
}
