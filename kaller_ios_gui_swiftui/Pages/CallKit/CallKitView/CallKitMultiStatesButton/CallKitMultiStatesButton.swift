//
//  CallKitButtonMultiStates.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 25/12/2020.
//

import SwiftUI

struct CallKitMultiStatesButton: View {
    let props: CallKitMultiStatesButtonProps
    @Environment(\.isEnabled) private var isEnabled
    
    var body: some View {
        VStack(spacing: 8) {
            button(props: props)
            name(props: props)
        }
    }
    
    private func button(props: CallKitMultiStatesButtonProps) -> some View {
        Button(action: props.onClick) { }
                .buttonStyle(CallKitMultiStatesButtonStyle(
                    props: CallKitMultiStatesButtonStyleProps(
                        size: props.buttonSize,
                        type: props.buttonType,
                        isEnable: isEnabled,
                        isSelected: props.isSelected
                    )
                ))
    }
    
    private func name(props: CallKitMultiStatesButtonProps) -> some View {
        let enableTextStyle = TextStyle.subheadline1Default2Dark1LabelColor
        let disableTextStyle = TextStyle.subheadline1Default2Dark3TertiaryLabelColor

        return Text(props.buttonName)
            .font(isEnabled
                    ? enableTextStyle.font
                    : disableTextStyle.font
            )
            .foregroundColor(isEnabled
                    ? enableTextStyle.color
                    : disableTextStyle.color
            )
    }
}

struct CallKitButtonMultiStates_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            CallKitMultiStatesButton(
                props: CallKitMultiStatesButtonProps(
                    buttonType: .systemImage(systemImageName: "person.circle"),
                    buttonName: "mute",
                    buttonSize: 75,
                    isSelected: false,
                    onClick: {}
                )
            )
                    .disabled(false)

            CallKitMultiStatesButton(
                props: CallKitMultiStatesButtonProps(
                    buttonType: .systemImage(systemImageName: "video.fill"),
                    buttonName: "video",
                    buttonSize: 75,
                    isSelected: false,
                    onClick: {}
                )
            )
                    .disabled(true)

            CallKitMultiStatesButton(
                props: CallKitMultiStatesButtonProps(
                    buttonType: .image(imageName: "callkitButtonAudio"),
                    buttonName: "audio",
                    buttonSize: 75,
                    isSelected: false,
                    onClick: {}
                )
            )
                    .disabled(false)

            CallKitMultiStatesButton(
                props: CallKitMultiStatesButtonProps(
                    buttonType: .image(imageName: "callkitButtonKeypad"),
                    buttonName: "contacts",
                    buttonSize: 75,
                    isSelected: false,
                    onClick: {}
                )
            )
                    .disabled(true)
        }
                .background(Color.gray)
    }
}
