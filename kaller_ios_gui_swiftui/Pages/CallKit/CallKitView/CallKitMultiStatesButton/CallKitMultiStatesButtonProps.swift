import SwiftUI

struct CallKitMultiStatesButtonProps {
    let buttonType: CallKitMultiStatesButtonStyleImageType
    let buttonName: String
    let buttonSize: CGFloat
    let isSelected: Bool
    let onClick: () -> Void
}
