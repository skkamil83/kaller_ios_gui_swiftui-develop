enum CallKitMultiStatesButtonStyleImageType {
    case image(imageName: String)
    case systemImage(systemImageName: String)
}
