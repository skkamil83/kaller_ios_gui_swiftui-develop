import SwiftUI

struct CallKitMultiStatesButtonStyle: ButtonStyle {
    let props: CallKitMultiStatesButtonStyleProps
    
    func makeBody(configuration: Configuration) -> some View {
        view(configuration: configuration, props: props)
    }
    
    private func view(configuration: Configuration, props: CallKitMultiStatesButtonStyleProps) -> some View {
        ZStack {
            if props.isSelected {
                Color.white
                    .opacity(0.5)
            } else {
                Color.white
                    .opacity(configuration.isPressed ? 0.5 : 0.1)
            }

            image(props: props)
                .opacity(props.isEnable ? 1 : 0.5)
        }
                .frame(width: props.size, height: props.size)
                .cornerRadius(props.size/2)
    }
    
    private func image(props: CallKitMultiStatesButtonStyleProps) -> some View {
        switch props.type {
        case .image(let imageName):
            return AnyView(
                Image(imageName)
                    .resizable()
            )
        case .systemImage(let systemImageName):
            let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: Float(props.size) * 32 / 75, color: .white)
            return AnyView(
                Image(systemName: systemImageName)
                    .font(textStyle.font)
                    .foregroundColor(Color.white)
            )
        }
    }
}
