import SwiftUI

struct CallKitMultiStatesButtonStyleProps {
    let size: CGFloat
    let type: CallKitMultiStatesButtonStyleImageType
    let isEnable: Bool
    let isSelected: Bool
}
