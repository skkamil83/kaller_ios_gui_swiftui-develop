//
//  CallKitContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 25/12/2020.
//

import SwiftUI

struct CallKitContainerProps{

}

struct CallKitContainer: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject private var userInfoState: UserInfoState
    @State var calls: [CallKitInfo] // will be environmentObject
    
    var props: CallKitContainerProps
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        CallKitView(
            props: CallKitViewProps(
                calls: calls,
                accountInfo: userInfoState.accountInfo!,
                speakers: fetchSpeakers(),
                onBack: back,
                onEndCall: endCall,
                updateMuteStatus: switchMute(isMuted:),
                onSelectSpeaker: selectSpeaker(speaker:),
                addCall: addCall,
                enableVideo: enableVideo,
                openContacts: openContacts,
                transfer: transfer,
                onNumPad: onNumPad(character:),
                onMergeCalls: onMergeCalls,
                onSwap: onSwap,
                onStopHold: onStopHold
            )
        )
        .onReceive(timer) { _ in timeTicker() }
        .onAppear{ callExample() }
    }
    
    private func switchMute(isMuted: Bool) {
        print("isMuted: \(isMuted)")
    }
    
    private func selectSpeaker(speaker: CallKitAudioSelectorViewModel){
        print("\(#function): \(speaker.name)")
    }
    
    private func endCall() {
        if let index = calls.firstIndex(where: { $0.status != .hold }) {
            calls[index].status = .callEnded
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if calls.count > 1 {
                    calls.remove(at: index)
                } else {
                    back()
                }
            }
        }
    }
    
    private func back() {
        presentationMode.wrappedValue.dismiss()
    }
    
    private func addCall() {
        print(#function)
    }
    
    private func enableVideo() {
        print(#function)
    }
    
    private func openContacts() {
        print(#function)
    }
    
    private func transfer() {
        print(#function)
    }
    
    private func onNumPad(character: String) {
        print("\(#function): \(character)")
    }
    
    private func onMergeCalls() {
        print(#function)
    }
    
    private func onSwap() {
        if let index = calls.firstIndex(where: {$0.status != .hold}){
            if let holdIndex = calls.firstIndex(where: {$0.status == .hold}){
                calls[holdIndex].status = .inCall
            }
            calls[index].status = .hold
        }
    }
    
    private func onStopHold(){
        if calls.count == 1{
            if let holdIndex = calls.firstIndex(where: {$0.status == .hold}){
                calls[holdIndex].status = .inCall
            }
        }
    }
    
    private func fetchSpeakers() -> [CallKitAudioSelectorViewModel] {
        [
            .init(id: "1", isSelected: true, name: "iPhone", icon: .iPhone),
            .init(id: "2", isSelected: false, name: "Speaker", icon: .speaker),
            .init(id: "3", isSelected: false, name: "Alexander's MacBook Pro", icon: .macbook)
        ]
    }
    
    private func timeTicker() {
        if let index = calls.firstIndex(where: { $0.status == .inCall }) {
            calls[index].seconds += 1
        }
    }
    
    //Example Calling
    private func callExample() {
        if let index = calls.firstIndex(where: { $0.status != .hold }) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                calls[index].status = .connecting
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    calls[index].status = .inCall
                }
            }
        }
    }
}
