import SwiftUI

struct CallKitContainerSample: View {
    
    enum ExampleType{
        case incomingPhoneToApp
        case incomingAppToApp
        case outgoingAppToPhone
        case outgoingAppToApp
        case hold
    }
    
    let exampleType: ExampleType
    
    var body: some View {
        let userInfoState = UserInfoState()
        userInfoState.accountInfo = AccountInfo(
            phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
            account: Account(id: 1, name: "Account name", createDate: Date()),
            devices: []
        )
        let contact = Contact(firstName: "Richard",lastName: "Miles", company: nil,avatarUrl: "https://images.unsplash.com/photo-1583512603805-3cc6b41f3edb?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=karsten-winegeart-NE0XGVKTmcA-unsplash.jpg&w=640",
                              phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Richard", lastName: "Miles", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "Mobile",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)])
        switch exampleType {
        case .incomingPhoneToApp:
            return AnyView(incomingPhoneToApp()
                            .environmentObject(userInfoState))
        case .incomingAppToApp:
            return AnyView(incomingAppToApp(contact: contact)
                            .environmentObject(userInfoState))
        case .outgoingAppToPhone:
            return AnyView(outgoingAppToPhone()
                            .environmentObject(userInfoState))
        case .outgoingAppToApp:
            return AnyView(outgoingAppToApp(contact: contact)
                            .environmentObject(userInfoState))
        case .hold:
            return AnyView(hold(contact: contact)
                            .environmentObject(userInfoState))
        }
    }
    
    private func incomingPhoneToApp() -> some View{
        return CallKitContainer(
            calls: [CallKitInfo(
                callType: .incoming,
                status: .connecting,
                contact: nil,
                fromPhoneNumber: "+44 20 1234 5678",
                toPhoneNumber: "+11111234567",
                connectWithExt: nil,
                billingType: .phoneToApp,
                pricePerMinute: 0.05,
                currency: .usd(symbol: "$", code: "USD"),
                seconds: 0
            )],
            props: CallKitContainerProps()
        )
    }
    
    private func incomingAppToApp(contact: Contact) -> some View{
        return CallKitContainer(
            calls: [
                CallKitInfo(
                callType: .incoming,
                status: .connecting,
                contact: contact,
                fromPhoneNumber: "+44 20 1234 5678",
                toPhoneNumber: "+71111234567",
                connectWithExt: 12,
                billingType: .appToApp,
                pricePerMinute: 0.05,
                currency: .usd(symbol: "$", code: "USD"),
                seconds: 0)
            ],
            props: CallKitContainerProps()
        )
    }
    
    private func outgoingAppToPhone() -> some View{
        return CallKitContainer(
            calls: [
                CallKitInfo(
                callType: .outcoming,
                status: .calling,
                contact: nil,
                fromPhoneNumber: "+71111234567",
                toPhoneNumber: "+44 20 1234 5678",
                connectWithExt: nil,
                billingType: .appToPhone,
                pricePerMinute: 0.05,
                currency: .usd(symbol: "$", code: "USD"),
                seconds: 0)
            ],
            props: CallKitContainerProps()
        )
    }
    
    private func outgoingAppToApp(contact: Contact) -> some View{
        return CallKitContainer(
            calls: [
                CallKitInfo(
                    callType: .outcoming,
                    status: .calling,
                    contact: contact,
                    fromPhoneNumber: "+21111234567",
                    toPhoneNumber: "+44 20 1234 5678",
                    connectWithExt: nil,
                    billingType: .appToApp,
                    pricePerMinute: 0.05,
                    currency: .usd(symbol: "$", code: "USD"),
                    seconds: 0)
            ],
            props: CallKitContainerProps()
        )
    }
    
    private func hold(contact: Contact) -> some View{
        return CallKitContainer(
            calls: [
                CallKitInfo(
                    callType: .outcoming,
                    status: .calling,
                    contact: contact,
                    fromPhoneNumber: "+21111234567",
                    toPhoneNumber: "+44 20 1234 5678",
                    connectWithExt: nil,
                    billingType: .appToApp,
                    pricePerMinute: 0.05,
                    currency: .usd(symbol: "$", code: "USD"),
                    seconds: 0),
                CallKitInfo(
                    callType: .incoming,
                    status: .hold,
                    contact: nil,
                    fromPhoneNumber: "+84 999 777 555 444",
                    toPhoneNumber: "+71111234567",
                    connectWithExt: nil,
                    billingType: .appToApp,
                    pricePerMinute: 0.05,
                    currency: .usd(symbol: "$", code: "USD"),
                    seconds: 0),
            ],
            props: CallKitContainerProps()
        )
    }
}
