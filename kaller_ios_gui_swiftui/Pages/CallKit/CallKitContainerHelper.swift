class CallKitContainerHelper {
    static func getContact(from phoneNumber: String, contacts: [Contact]) -> Contact? {
        for contact in contacts{
            for phone in contact.phones{
                if phone.phoneNumber.trimmingCharacters(in: .whitespaces) == phoneNumber.trimmingCharacters(in: .whitespaces) {
                    return contact
                }
            }
        }
        return nil
    }
}
