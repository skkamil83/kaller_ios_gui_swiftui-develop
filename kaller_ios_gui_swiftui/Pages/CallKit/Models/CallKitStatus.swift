enum CallKitStatus {
    case calling
    case connecting
    case reconnecting
    case callEnded
    case userBusy
    case inCall
    case hold
}
