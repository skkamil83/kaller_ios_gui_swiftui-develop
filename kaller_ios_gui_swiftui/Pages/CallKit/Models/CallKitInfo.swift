struct CallKitInfo {
    let callType: CallType
    var status: CallKitStatus
    let contact: Contact?
    let fromPhoneNumber: String
    let toPhoneNumber: String
    let connectWithExt: Int?
    let billingType: BillingType
    let pricePerMinute: Double
    let currency: CurrencyType
    var seconds: Int
}
