import SwiftUI

struct MyNumbersContainerProps {
    let myNumbers: [MyNumberViewModel]
}

struct MyNumbersContainer: View {
    let props: MyNumbersContainerProps
    var body: some View {
        MyNumbersView(
            props: MyNumbersViewProps(
                myNumbers: props.myNumbers,
                onEdit: onEdit,
                onAddPhoneNumber: onAddPhoneNumber,
                onGetLocalNumber: onGetLocalNumber,
                onSelected: onSelected(myNumber:)
            )
        )
    }
    
    private func onEdit() -> Void {
        print(#function)
    }
    
    private func onAddPhoneNumber() -> Void {
        print(#function)
    }
    
    private func onGetLocalNumber() -> Void {
        print(#function)
    }
    
    private func onSelected(myNumber: MyNumberViewModel) {
        print("\(#function): \(myNumber.phoneNumber)")
    }
}
