import SwiftUI

struct MyNumbersView: View {
    @State private var isShowDetails = false
    
    let props: MyNumbersViewProps
    
    var body: some View {
        ScrollView{
            LazyVStack(spacing: 0) {
                list(props: props)
                if(props.myNumbers.count <= 1) {
                    alwaysBeAvailable()
                }else{
                    moreDetails()
                }
                addPhoneNumber(props: props)
                getLocalNumber(props: props)
            }
        }
        .navigationTitle("My Numbers")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(
            trailing: editButton(props: props)
        )
        .alert(
            isPresented: $isShowDetails,
            content: { detailsAlert() }
        )
    }
    
    private func list(props: MyNumbersViewProps) -> some View {
        ForEach(props.myNumbers, id: \.phoneNumber){ number in
            CardNumber(
                props: CardNumberProps(
                    myNumber: number,
                    onSelected: {
                        props.onSelected(number)
                    }
                )
            )
            .padding(.top, Spacing.spacing16Pt)
        }
    }
    
    private func alwaysBeAvailable() -> some View {
        VStack(alignment: .leading, spacing: 0){
            titleDetail()
                .padding(.bottom, 10)
            VStack(alignment: .leading, spacing: 8){
                Text("Attach any number of your SIM cards to the application. Leave unnecessary SIM cards at home, you do not need them anymore.")
                    .font(TextStyle.subheadline1Default1Light1LabelColor.font)
                    .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)
                Text("In addition, get any amount of landline, mobile and national virtual phone numbers of Kaller from more than 3000 cities.")
                    .font(TextStyle.subheadline1Default1Light1LabelColor.font)
                    .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)
                Text("Organize free of charge multifunctional cloud telephony for your business on the go for calls, instant information exchange and solving any business problems.")
                    .font(TextStyle.subheadline1Default1Light1LabelColor.font)
                    .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)
            }
        }
        .padding(EdgeInsets(top: 28, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))
    }
    
    private func moreDetails() -> some View {
        Button(action: {
            isShowDetails.toggle()
        }){
            (Text("A new level of cloud communications in your smartphone for solving any business problems on the go. ")
                .font(TextStyle.caption11Default1Light2SecondaryLabelColor.font)
                .foregroundColor(TextStyle.caption11Default1Light2SecondaryLabelColor.color)
                + Text("More details")
                .font(TextStyle.caption11Default1Light2SecondaryLabelColor.font)
                .foregroundColor(TextStyle.caption11Default1Light2SecondaryLabelColor.color)
                .underline()
            )
            .padding(EdgeInsets(top: Spacing.spacing16Pt, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))
        }
    }
    
    private func addPhoneNumber(props: MyNumbersViewProps) -> some View {
        HStack{
            Button(action: props.onAddPhoneNumber){
                Text("Add Phone Number")
                    .font(TextStyle.body1Default1Light5Blue.font)
                    .foregroundColor(TextStyle.body1Default1Light5Blue.color)
            }
            .padding(.leading, Spacing.spacing16Pt)
            Spacer()
        }
        .frame(height: 44)
        .padding(.top, Spacing.spacing12Pt)
    }
    
    private func getLocalNumber(props: MyNumbersViewProps) -> some View {
        HStack{
            Button(action: props.onGetLocalNumber){
                Text("Get Local Number")
                    .font(TextStyle.body1Default1Light5Blue.font)
                    .foregroundColor(TextStyle.body1Default1Light5Blue.color)
            }
            .padding(.leading, Spacing.spacing16Pt)
            Spacer()
        }
        .frame(height: 44)
        .padding(.bottom, Spacing.spacing35Pt)
    }
    
    private func editButton(props: MyNumbersViewProps) ->  some View {
        return Button(action: props.onEdit){
            Text("Edit")
                .font(TextStyle.body3Button1Light5Blue.font)
        }
    }
    
    private func detailsAlert() -> Alert {
        Alert(
            title: titleDetail(),
            message: descriptionDetail(),
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func titleDetail() -> Text {
        Text("Always be available")
            .font(TextStyle.title32Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.title32Bold1Light1LabelColor.color)
    }
    
    private func descriptionDetail() -> Text {
        Text("Attach any number of your SIM cards to the application. Leave unnecessary SIM cards at home, you do not need them anymore.\n\nIn addition, get any amount of landline, mobile and national virtual phone numbers of Kaller from more than 3000 cities.\n\nOrganize free of charge multifunctional cloud telephony for your business on the go for calls, instant information exchange and solving any business problems.")
            .font(TextStyle.subheadline1Default1Light1LabelColor.font)
            .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)
    }
}

struct MyNumbersView_Previews: PreviewProvider {
    static var previews: some View {
        MyNumbersView(
            props: MyNumbersViewProps(
                myNumbers: [
                    .init(isEnable: true,
                          flagImageName: "flag_AD",
                          countryName: "VN",
                          locationName: "aaaaaa",
                          phoneNumber: "+123235334",
                          locationImageURL: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640")
                    ,
                    .init(isEnable: true,
                          flagImageName: "flag_AD",
                          countryName: "VN",
                          locationName: "aaaaaa",
                          phoneNumber: "+1232353344444",
                          locationImageURL: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640")
                ],
                onEdit: {},
                onAddPhoneNumber: {},
                onGetLocalNumber: {},
                onSelected: {_ in}
            )
        )
    }
}
