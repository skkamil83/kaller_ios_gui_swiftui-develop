struct MyNumbersViewProps {
    let myNumbers: [MyNumberViewModel]
    let onEdit: () -> Void
    let onAddPhoneNumber: () -> Void
    let onGetLocalNumber:() -> Void
    let onSelected: (MyNumberViewModel) -> Void
}
