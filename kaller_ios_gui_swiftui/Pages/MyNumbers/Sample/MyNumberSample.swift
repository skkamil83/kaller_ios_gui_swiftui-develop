import SwiftUI

struct MyNumberSample: View {
    enum ExampleType{
        case one
        case moreThanOne
    }
    let sampleType: ExampleType
    
    var body: some View {
        view()
    }
    
    private func view() -> some View {
        switch sampleType {
        case .one:
            return AnyView(
                MyNumbersContainer(
                    props: .init(
                        myNumbers: [
                            .init(isEnable: true,
                                  flagImageName: "flag_AX",
                                  countryName: "AAAAAA",
                                  locationName: "aaaaaa",
                                  phoneNumber: "+1232353344444",
                                  locationImageURL: "https://images.unsplash.com/photo-1552620888-a4553dd93b5e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80")
                        ]
                    )
                )
            )
        case .moreThanOne:
            return AnyView(
                MyNumbersContainer(
                    props: .init(
                        myNumbers: [
                            .init(isEnable: true,
                                  flagImageName: "flag_AD",
                                  countryName: "BBBBB",
                                  locationName: "aaaaaa",
                                  phoneNumber: "+12323533499999",
                                  locationImageURL: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640")
                            ,
                            .init(isEnable: true,
                                  flagImageName: "flag_AX",
                                  countryName: "AAAAAA",
                                  locationName: "aaaaaa",
                                  phoneNumber: "+1232353344444",
                                  locationImageURL: "https://images.unsplash.com/photo-1552620888-a4553dd93b5e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80")
                        ]
                    )
                )
            )
        }
    }
}
