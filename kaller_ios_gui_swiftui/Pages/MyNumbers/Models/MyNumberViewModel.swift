struct MyNumberViewModel {
    let isEnable: Bool
    let flagImageName: String
    let countryName: String
    let locationName: String
    let phoneNumber: String
    let locationImageURL: String
}
