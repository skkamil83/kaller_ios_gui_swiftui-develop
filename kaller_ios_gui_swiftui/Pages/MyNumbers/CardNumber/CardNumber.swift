import SwiftUI

struct CardNumber: View {
    let props: CardNumberProps
    
    var body: some View {
        let size = getSize()
        return ZStack{
            location(props: props)
            cardHead(props: props)
            info(props: props)
        }
        .frame(width: size.width, height: size.height)
        .cornerRadius(10)
        .shadow(radius: 8)
        .contentShape(Rectangle())
        .onTapGesture { props.onSelected() }
    }
    
    private func location(props: CardNumberProps) -> some View {
        URLImage(
            props: URLImageProps(
                urlString: props.myNumber.locationImageURL,
                contentMode: .fill,
                size: getSize()
            )
        )
    }
    
    private func cardHead(props: CardNumberProps) -> some View {
        VStack{
            HStack{
                CardHead(
                    props: CardHeadProps(
                        isEnable: props.myNumber.isEnable,
                        flagImageName: props.myNumber.flagImageName,
                        countryName: props.myNumber.countryName,
                        locationName: props.myNumber.locationName,
                        phoneNumber: props.myNumber.phoneNumber
                    )
                )
                Spacer()
            }
            Spacer()
        }
        .padding(EdgeInsets(top: Spacing.spacing14Pt, leading: Spacing.spacing16Pt, bottom: 0, trailing: 0))
    }
    
    private func info(props: CardNumberProps) -> some View {
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .white)
        
        return
            HStack{
                Spacer()
                VStack{
                    Spacer()
                    CircleIcon(
                        props: CircleIconProps(
                            size: 32,
                            style: .systemImage(
                                systemImageName: "info",
                                font: textStyle.font,
                                backgroundColor: Color.black.opacity(0.5),
                                foregroundColor: textStyle.color
                            )
                        )
                    )
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: Spacing.spacing8Pt, trailing: Spacing.spacing8Pt))
                }
            }
    }
    
    private func getSize() -> CGSize {
        let width = UIScreen.main.bounds.width - Spacing.spacing16Pt * 2
        let height = width * 213/343
        return CGSize(width: width, height: height)
    }
}

struct MyNumbersCard_Previews: PreviewProvider {
    static var previews: some View {
        CardNumber(
            props: CardNumberProps(
                myNumber: MyNumberViewModel(
                    isEnable: true,
                    flagImageName: "flag_AD",
                    countryName: "VN",
                    locationName: "aaaaaa",
                    phoneNumber: "+123235334",
                    locationImageURL: "https://images.unsplash.com/photo-1500630417200-63156e226754?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=lilly-rum-eE4YBIxSP60-unsplash.jpg&w=640"),
                onSelected: {}
            )
        )
    }
}
