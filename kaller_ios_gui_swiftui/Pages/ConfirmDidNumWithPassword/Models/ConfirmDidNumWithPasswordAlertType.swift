//
//  EnterPasswordOldUserAlertType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

import Foundation

enum ConfirmDidNumWithPasswordAlertType: Identifiable{
    var id: String{
        switch self {
        case .inCorrectPassword:
            return "inCorrectPassword"
        case .waitForFewMinutes:
            return "waitForFewMinutes"
        }
    }
    
    case inCorrectPassword
    case waitForFewMinutes
}
