//
//  EnterPasswordOldUserView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

import SwiftUI

struct ConfirmDidNumWithPasswordView: View {
    let props: ConfirmDidNumWithPasswordViewProps
    @State private var password = ""
    @State private var isShowPassword = false
    
    var body: some View {
        VStack(spacing: 0){
            enterPassWordText()
            descriptionText()
            textFieldPassword()
            Spacer()
                .frame(maxHeight: 84)
            forgotPassword(props: props)
            Spacer()
        }
        .navigationBarTitleDisplayMode(.large)
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                phoneNumberToolbar(phoneNumber: props.phoneNumber)
            }
        })
        .navigationBarItems(
            trailing: nextButton(props: props)
        )
    }
    
    private func enterPassWordText() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 28, color: .black)
        return Text("Enter password")
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.top, -28)
    }
    
    private func descriptionText() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return Text("The password is used by the admin to access the phone number.")
            .frame(height: 44)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .lineLimit(2)
            .multilineTextAlignment(.center)
            .padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
    }
    
    private func textFieldPassword() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 20, color: .black)
        return
            VStack(spacing: 0){
                Spacer()
                HStack(spacing: 0){
                    Spacer()
                        .frame(width: 42.4)
                    
                    if isShowPassword {
                        TextField("Password", text: $password)
                            .multilineTextAlignment(.center)
                            .font(textStyle.font)
                            .foregroundColor(textStyle.color)
                            .disableAutocorrection(true)
                    } else {
                        SecureField("Password", text: $password)
                            .multilineTextAlignment(.center)
                            .font(textStyle.font)
                            .foregroundColor(textStyle.color)
                    }
                    
                    Button(action: {
                        isShowPassword.toggle()
                    }){
                        Image(systemName: isShowPassword ? "eye" : "eye.slash")
                            .font(TextStyle.body1Default1Light2SecondaryLabelColor.font)
                            .foregroundColor(TextStyle.body1Default1Light2SecondaryLabelColor.color)
                    }
                    .frame(width: 26)
                    .padding(.trailing, 16.4)
                }
                Spacer()
                Divider()
            }
            .frame(height: 44)
            .padding(EdgeInsets(top: 79, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func forgotPassword(props: ConfirmDidNumWithPasswordViewProps) -> some View{
        NavigationLink(
            destination: ConfirmEmailWithOtpContainer(
                props: ForgotPasswordContainerProps(
                    phoneNumber: props.phoneNumber
                )
            )
        ) {
            Text("Forgot password?")
                .font(TextStyle.subheadline1Default1Light5Blue.font)
                .foregroundColor(TextStyle.subheadline1Default1Light5Blue.color)
        }
    }
    
    
    private func phoneNumberToolbar(phoneNumber: String) -> some View{
        Text(phoneNumber)
            .font(TextStyle.body2Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
    }
    
    private func nextButton(props: ConfirmDidNumWithPasswordViewProps) ->  some View{
        let textStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: .primaryBlue)
        let isCodeValid = !password.isEmpty
        let isInProgress = props.verificationResult == .inProgress
        
        return Button(action: {
            props.onNext(password)
        }){
            Text("Next")
                .font(textStyle.font)
        }
        .disabled(!isCodeValid || isInProgress || props.waitingTime > 0)
    }
}

struct EnterPasswordOldUserView_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmDidNumWithPasswordView(
            props: ConfirmDidNumWithPasswordViewProps(
                phoneNumber: "+972 50 999 9999",
                waitingTime: 0,
                verificationResult: .notVerified,
                onNext: {_ in}
            )
        )
    }
}
