//
//  EnterPasswordOldUserContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

import SwiftUI

struct ConfirmDidNumWithPasswordContainerProps{
    let phoneNumber: String
}

struct ConfirmDidNumWithPasswordContainer: View {
    @State private var alert: ConfirmDidNumWithPasswordAlertType?
    @State private var didEnterWrongPassword = false
    @State private var waitingTimeCountDown = 0
    
    let props: ConfirmDidNumWithPasswordContainerProps
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    let timeToWait: Int = 180
    
    var body: some View {
        ConfirmDidNumWithPasswordView(
            props: ConfirmDidNumWithPasswordViewProps(
                phoneNumber: props.phoneNumber,
                waitingTime: waitingTimeCountDown,
                verificationResult: .notVerified,
                onNext: onNext(password:)
            )
        )
        .alert(item: $alert) { item  in alertView(type: item)}
        .onReceive(timer){ _ in onTimerTick()}
    }
    
    private func onNext(password: String){
        onWrongPassword()
    }
    
    private func onWrongPassword(){
        if(didEnterWrongPassword){
            waitingTimeCountDown = timeToWait
            alert = .waitForFewMinutes
            didEnterWrongPassword = false
        }else{
            alert = .inCorrectPassword
            didEnterWrongPassword = true
        }
    }
    
    private func alertView(type: ConfirmDidNumWithPasswordAlertType) -> Alert{
        switch type{
        case .inCorrectPassword:
            return alertIncorrectPassword()
        case .waitForFewMinutes:
            return alertWaitForFewMinutes()
        }
    }
    
    private func alertIncorrectPassword() -> Alert{
        Alert(
            title: Text("You have entered an invalid password. Please try again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func alertWaitForFewMinutes() -> Alert{
        Alert(
            title: Text("Please wait a few minutes to submit your password before trying again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func onTimerTick() {
        if(waitingTimeCountDown > 0){
            waitingTimeCountDown -= 1
        }
    }
}

struct EnterPasswordOldUserContainer_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmDidNumWithPasswordContainer(
            props: ConfirmDidNumWithPasswordContainerProps(
                phoneNumber: "+972 50 999 9999"
            )
        )
    }
}
