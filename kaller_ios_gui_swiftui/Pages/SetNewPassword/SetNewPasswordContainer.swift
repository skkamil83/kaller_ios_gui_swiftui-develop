import SwiftUI

struct SetNewPasswordContainerProps{
    let phoneNumber: String
}

struct SetNewPasswordContainer: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var alert: SetNewPasswordAlertType?
    
    let props: SetNewPasswordContainerProps
    
    var body: some View {
        SetNewPasswordView(
            props: SetNewPasswordViewProps(
                phoneNumber: props.phoneNumber,
                onNext: onNext(password:))
        )
        .alert(item: $alert) { item in alertView(type: item)}
    }
    
    private func onNext(password: String){
        alert = .recoveryCodeExpired
    }
    
    private func alertView(type: SetNewPasswordAlertType) -> Alert{
        switch type{
        case .recoveryCodeExpired:
            return alertRecoveryCodeHasExpired()
        }
    }
    
    private func alertRecoveryCodeHasExpired() -> Alert{
        Alert(
            title: Text("Your recovery code has expired. Please request a new one."),
            message: nil,
            dismissButton: .default(Text("OK")){
                presentationMode.wrappedValue.dismiss()
            }
        )
    }
    
}
