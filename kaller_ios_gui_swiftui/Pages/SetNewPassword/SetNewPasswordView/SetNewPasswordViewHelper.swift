class SetNewPasswordViewHelper{
    static func containsLowerCase(text: String) -> Bool{
        return text.first(where: {$0.isLowercase}) != nil
    }
    
    static func containsUpperCase(text: String) -> Bool{
        return text.first(where: {$0.isUppercase}) != nil
    }
    
    static func containsNumeric(text: String) -> Bool{
        return text.rangeOfCharacter(from: .decimalDigits) != nil
    }
    
    static func containsSixCharacters(text: String) -> Bool{
        return text.count >= 6
    }
    
    static func passwordIsValid(text: String) -> Bool{
        return containsLowerCase(text: text) &&
            containsUpperCase(text: text) &&
            containsNumeric(text: text) &&
            containsSixCharacters(text: text)
    }
}
