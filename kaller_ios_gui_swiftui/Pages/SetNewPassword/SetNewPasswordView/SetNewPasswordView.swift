import SwiftUI

struct SetNewPasswordView: View {
    let props: SetNewPasswordViewProps
    @State private var password = ""
    @State private var isShowPassword = false
    
    var body: some View {
        VStack(spacing: 0){
            createNewPasswordText()
            descriptionText()
            textFieldPassword()
            Spacer()
                .frame(maxHeight: 52)
            passwordValid()
            Spacer()
        }
        .navigationBarTitleDisplayMode(.large)
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                phoneNumberToolbar(phoneNumber: props.phoneNumber)
            }
        })
        .navigationBarItems(
            trailing: nextButton(props: props)
        )
    }
    
    private func createNewPasswordText() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 28, color: .black)
        return Text("Create new password")
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.top, -28)
    }
    
    private func descriptionText() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return Text("Please create a new password which will be used to access your phone number.")
            .frame(height: 44)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .multilineTextAlignment(.center)
            .lineLimit(2)
            .padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
    }
    
    private func textFieldPassword() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 20, color: .black)
        return
            VStack(spacing: 0){
                Spacer()
                HStack{
                    Spacer()
                        .frame(width: 42.4)
                    
                    if isShowPassword {
                        TextField("New password", text: $password)
                            .multilineTextAlignment(.center)
                            .font(textStyle.font)
                            .foregroundColor(textStyle.color)
                            .disableAutocorrection(true)
                    } else {
                        SecureField("New password", text: $password)
                            .multilineTextAlignment(.center)
                            .font(textStyle.font)
                            .foregroundColor(textStyle.color)
                    }
                    
                    Button(action: {
                        isShowPassword.toggle()
                    }){
                        Image(systemName: isShowPassword ? "eye" : "eye.slash")
                            .font(TextStyle.body1Default1Light2SecondaryLabelColor.font)
                            .foregroundColor(TextStyle.body1Default1Light2SecondaryLabelColor.color)
                    }
                    .frame(width: 26)
                    .padding(.trailing, 16.4)
                }
                Spacer()
                Divider()
            }
            .frame(height: 44)
            .padding(EdgeInsets(top: 79, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func passwordValid() -> some View{
        VStack(alignment: .leading){
            Text("Password must contain:")
                .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                .foregroundColor(.black)
                .padding(.leading, 9)
            Text("• a minimum of 1 lower case letter [a-z],")
                .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                .foregroundColor(SetNewPasswordViewHelper.containsLowerCase(text: password) ? .black : .secondaryLabel)
            Text("• a minimum of 1 upper case letter [A-Z],")
                .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                .foregroundColor(SetNewPasswordViewHelper.containsUpperCase(text: password) ? .black : .secondaryLabel)
            Text("• a minimum of 1 numeric character [0-9],")
                .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                .foregroundColor(SetNewPasswordViewHelper.containsNumeric(text: password) ? .black : .secondaryLabel)
            Text("• at least 6 characters in length and longer.")
                .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                .foregroundColor(SetNewPasswordViewHelper.containsSixCharacters(text: password) ? .black : .secondaryLabel)
        }
    }
    
    private func phoneNumberToolbar(phoneNumber: String) -> some View{
        Text(phoneNumber)
            .font(TextStyle.body2Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
    }
    
    private func nextButton(props: SetNewPasswordViewProps) ->  some View{
        let textStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: .primaryBlue)
        let isCodeValid = SetNewPasswordViewHelper.passwordIsValid(text: password)
        
        return Button(action: {
            props.onNext(password)
        }){
            Text("Next")
                .font(textStyle.font)
        }
        .disabled(!isCodeValid)
    }
}

struct SetNewPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        SetNewPasswordView(
            props: SetNewPasswordViewProps(
                phoneNumber: "+972 50 999 9999",
                onNext: {_ in}
            )
        )
    }
}
