import Foundation

struct Account {
    let id: Int
    let name: String
    let createDate: Date
}