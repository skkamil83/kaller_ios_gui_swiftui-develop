import Foundation

struct AccountInfo {
    var phones: [PhoneInfo]
    let account: Account
    let devices: [DeviceInfo]
    var defaultPhone: PhoneInfo? {
        get { phones.first(where: {$0.isDefault}) }
    }
    
    var sortedPhones: [PhoneInfo]{
        phones.sorted {$0.sortOrder < $1.sortOrder}
    }
    
    func getSimNumber(phone: PhoneInfo) -> Int? {
        PhoneHelper.getSimNumber(phones: phones, phone: phone)
    }
}
