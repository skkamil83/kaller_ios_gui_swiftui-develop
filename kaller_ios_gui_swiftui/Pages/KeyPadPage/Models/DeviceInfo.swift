struct DeviceInfo {
    let deviceId: Int
    let name: String
    let isActive: Bool
}