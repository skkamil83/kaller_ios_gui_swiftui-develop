//
//  UserInfo.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 17/12/2020.
//

import Foundation

struct UserInfo{
    let firstName: String?
    let lastName: String?
    let avatarImageUrl: String?
    
    var fullName: String {
        guard let firstName = firstName else {
            return lastName ?? ""
        }
        guard let lastName = lastName else {
            return firstName
        }
        return "\(firstName) \(lastName)"
    }
}
