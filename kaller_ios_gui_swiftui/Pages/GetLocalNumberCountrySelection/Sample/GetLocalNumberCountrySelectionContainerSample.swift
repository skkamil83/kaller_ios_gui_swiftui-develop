import SwiftUI

struct GetLocalNumberCountrySelectionContainerSample: View {

    @State private var countrySelected: Country? = Country(localizedName: "United Kingdom", iso: "GB", phoneCode: 44, countryCode: 826, flagImageName: "flag_GB"
    )

    var body: some View {
        GetLocalNumberCountrySelectionContainer(
                props: GetLocalNumberCountrySelectionContainerProps(
                        countrySelected: $countrySelected
                )
        )
    }
}

struct GetLocalNumberCountrySelectionContainerSample_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberCountrySelectionContainerSample()
    }
}
