import SwiftUI

struct GetLocalNumberCountrySelectionContainerProps {
    let countrySelected: Binding<Country?>
}

struct GetLocalNumberCountrySelectionContainer: View {
    let props: GetLocalNumberCountrySelectionContainerProps
    let countryFetcher = CountryFetcher()
    
    var body: some View {
        GetLocalNumberCountrySelectionView(
                props: GetLocalNumberCountrySelectionViewProps(
                    mostPopularCountries: countryFetcher.countriesAvailableToWorkWith,
                    allCountries: countryFetcher.countries,
                    countrySelected: props.countrySelected
                )
        )
    }
}

struct GetLocalNumberCountrySelectionContainer_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberCountrySelectionContainer(
                props: GetLocalNumberCountrySelectionContainerProps(
                        countrySelected: .constant(nil)
                )
        )
    }
}
