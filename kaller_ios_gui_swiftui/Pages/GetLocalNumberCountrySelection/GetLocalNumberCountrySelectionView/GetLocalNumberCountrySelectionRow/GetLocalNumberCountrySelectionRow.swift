import SwiftUI

struct GetLocalNumberCountrySelectionRow: View {
    let props: GetLocalNumberCountrySelectionRowProps

    var body: some View {
        row(props: props)
    }

    private func row(props: GetLocalNumberCountrySelectionRowProps) -> some View {
        VStack(spacing: 0) {
            HStack(spacing: Spacing.spacing8Pt) {
                CircleIcon(
                        props: CircleIconProps(
                                size: 21,
                                style: .filled(imageName: props.country.flagImageName)
                        )
                )
                        .padding(.leading, Spacing.spacing16Pt)
                Text(props.country.localizedName)
                        .textStyle(.body1Default1Light1LabelColor2CenterAligned)
                if let phoneCode = props.country.phoneCode {
                    Text("+\(String(phoneCode))")
                            .textStyle(.body1Default1Light2SecondaryLabelColor)
                }
                Spacer()
                if props.isSelected {
                    Image("checkmark")
                            .padding(.trailing, Spacing.spacing16Pt)
                }
            }
                    .frame(height: 44)
            Divider()
                .padding(.leading, props.isLastRow ? 0 : Spacing.spacing16Pt)
        }
                .background(Color.white)
                .onTapGesture {
                    props.onSelected()
                }
    }
}

struct GetLocalNumberCountrySelectionRow_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberCountrySelectionRow(
                props: GetLocalNumberCountrySelectionRowProps(
                        country: Country(
                                localizedName: "Afghanistan",
                                iso: "AF",
                                phoneCode: 1193,
                                countryCode: 4,
                                flagImageName: "flag_AF"
                        ),
                        isSelected: true,
                        isLastRow: true,
                        onSelected: {}
                )
        )
    }
}
