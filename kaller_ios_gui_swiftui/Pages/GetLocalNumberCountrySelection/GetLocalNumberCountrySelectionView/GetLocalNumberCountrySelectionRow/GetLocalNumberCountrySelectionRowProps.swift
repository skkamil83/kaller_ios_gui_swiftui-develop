struct GetLocalNumberCountrySelectionRowProps {
    let country: Country
    let isSelected: Bool
    let isLastRow: Bool
    let onSelected: () -> Void
}
