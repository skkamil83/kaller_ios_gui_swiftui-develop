import SwiftUI

struct GetLocalNumberCountrySelectionView: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var searchText = ""
    @State private var isShowTitle = true
    @Binding private var countrySelected: Country?

    let props: GetLocalNumberCountrySelectionViewProps

    init(props: GetLocalNumberCountrySelectionViewProps) {
        self.props = props
        self._countrySelected = props.countrySelected
    }

    var body: some View {
        ZStack {
            background()
            VStack(spacing: 0) {
                header()
                if searchText.isEmpty {
                    list(props: props)
                } else {
                    searchList(props: props)
                }
            }
        }
    }

    private func background() -> some View {
        Color.navigationBarBackgroundChrome
                .edgesIgnoringSafeArea(.all)
    }

    private func header() -> some View {
        VStack(spacing: 0) {
            if isShowTitle {
                titleView()
                        .padding(.top, Spacing.spacing16Pt)
            }
            searchView()
                    .padding(EdgeInsets(top: Spacing.spacing16Pt, leading: 0, bottom: Spacing.spacing16Pt, trailing: 0))
            Divider()
        }
    }

    private func searchView() -> some View {
        SearchBarView(
                props: SearchBarViewProps(
                        query: $searchText,
                        placeHolder: "Search",
                        onFocus: { onFocus in
                            withAnimation {
                                isShowTitle = !onFocus
                            }
                        }
                )
        )
                .frame(height: 36)
                .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))
    }

    private func titleView() -> some View {
        ZStack {
            HStack(spacing: 0) {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
                            .textStyle(.body3Button1Light5Blue)
                }
                        .padding(.leading, 17)
                Spacer()
            }
            Text("Country Selection")
                    .textStyle(.body2Bold1Light1LabelColor)
        }
    }

    private func list(props: GetLocalNumberCountrySelectionViewProps) -> some View {
        ScrollView {
            VStack(spacing: 0) {
                if !props.mostPopularCountries.isEmpty {
                    mostPopular(props: props)
                }
                allCountries(props: props)
            }
        }
    }
    
    private func searchList(props: GetLocalNumberCountrySelectionViewProps) -> some View {
        let countries = GetLocalNumberCountrySelectionViewHelper.filterCountries(countries: props.allCountries, searchText: searchText)
        
        return ScrollView {
            LazyVStack(spacing: 0) {
                ForEach(countries, id: \.self.iso) { country in
                    GetLocalNumberCountrySelectionRow(
                            props: GetLocalNumberCountrySelectionRowProps(
                                    country: country,
                                    isSelected: country == countrySelected,
                                    isLastRow: country == countries.last,
                                    onSelected: {
                                        countrySelected = country
                                        presentationMode.wrappedValue.dismiss()
                                    }
                            )
                    )
                }
            }
        }
    }

    private func mostPopular(props: GetLocalNumberCountrySelectionViewProps) -> some View {
        LazyVStack(spacing: 0) {
            section(title: "MOST POPULAR")
            ForEach(props.mostPopularCountries, id: \.self.iso){ country in
                GetLocalNumberCountrySelectionRow(
                    props: GetLocalNumberCountrySelectionRowProps(
                        country: country,
                        isSelected: country == countrySelected,
                        isLastRow: country == props.mostPopularCountries.last,
                        onSelected: {
                            countrySelected = country
                            presentationMode.wrappedValue.dismiss()
                        }
                    )
                )
            }
        }
    }

    private func allCountries(props: GetLocalNumberCountrySelectionViewProps) -> some View {

        if props.allCountries.isEmpty {
            return AnyView(moreCountriesText())
        }

        return AnyView(
                LazyVStack(spacing: 0) {
                    section(title: "ALL COUNTRIES")
                    ForEach(props.allCountries, id: \.self.iso) { country in
                        GetLocalNumberCountrySelectionRow(
                                props: GetLocalNumberCountrySelectionRowProps(
                                        country: country,
                                        isSelected: country == countrySelected,
                                        isLastRow: country == props.allCountries.last,
                                        onSelected: {
                                            countrySelected = country
                                            presentationMode.wrappedValue.dismiss()
                                        }
                                )
                        )
                    }
                }
        )
    }

    private func section(title: String) -> some View {
        VStack(alignment: .leading, spacing: 0) {
            Spacer()
            Text(title)
                    .textStyle(.footnote1Default1Light6DarkGray)
                    .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: 7, trailing: 0))
            Divider()
        }
                .frame(width: UIScreen.main.bounds.width, height: 56)
    }

    private func moreCountriesText() -> some View {
        VStack(spacing: 0) {
            Text("More countries will be added soon")
                    .textStyle(.footnote1Default1Light2SecondaryLabelColor)
                    .padding(EdgeInsets(top: Spacing.spacing8Pt, leading: Spacing.spacing16Pt, bottom: 13, trailing: 0))
        }
                .frame(width: UIScreen.main.bounds.width, height: 37, alignment: .leading)
    }
}

struct GetLocalNumberCountrySelectionView_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberCountrySelectionView(
            props: GetLocalNumberCountrySelectionViewProps(
                mostPopularCountries: [],
                allCountries: CountryFetcher().countries,
                countrySelected: .constant(nil)
            )
        )
    }
}
