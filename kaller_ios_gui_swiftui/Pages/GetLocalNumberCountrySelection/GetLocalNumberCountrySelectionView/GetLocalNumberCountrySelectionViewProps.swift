import SwiftUI

struct GetLocalNumberCountrySelectionViewProps {
    let mostPopularCountries: [Country]
    let allCountries: [Country]
    let countrySelected: Binding<Country?>
}
