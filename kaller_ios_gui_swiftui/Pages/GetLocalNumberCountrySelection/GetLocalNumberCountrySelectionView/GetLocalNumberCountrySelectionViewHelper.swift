class GetLocalNumberCountrySelectionViewHelper {
    static func filterCountries(countries: [Country], searchText: String) -> [Country] {
        let searchString = searchText.replacingOccurrences(of: "+", with: "").lowercased().trimmingCharacters(in: .whitespaces)
        if searchString.isEmpty {
            return countries
        }
        return countries.filter { country -> Bool in
            
            if country.localizedName.lowercased().hasPrefix(searchString) {
                return true
            }
            if let phoneCode = country.phoneCode {
                if "\(phoneCode)".hasPrefix(searchString) {
                    return true
                }
            }
            return false
        }
    }
}
