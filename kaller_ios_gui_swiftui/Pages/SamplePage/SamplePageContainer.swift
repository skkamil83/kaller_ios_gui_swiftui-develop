import SwiftUI

struct SamplePageContainerProps {
    let sampleItemId: Int
}

// CONTAINER is dedicated to map STATE to PROPS and map ACTIONS to PROPS
// and pass PROPS to it's own root view
struct SamplePageContainer: View {
    let props: SamplePageContainerProps
    private let dataStorage = [
        "sample item 1",
        "sample item 2"
    ]

    var body: some View {
        SamplePageView(
                props: SamplePageViewProps(
                        sampleItem: dataStorage[props.sampleItemId] ?? "",
                        title: "My sample page",
                        sampleAction: sampleAction
                )
        )
    }

    private func sampleAction() {
        print(#function)
    }
}
