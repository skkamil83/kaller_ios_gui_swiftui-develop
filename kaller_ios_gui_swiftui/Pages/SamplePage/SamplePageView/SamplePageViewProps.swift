struct SamplePageViewProps {
    let sampleItem: String
    let title: String
    let sampleAction: () -> Void
}