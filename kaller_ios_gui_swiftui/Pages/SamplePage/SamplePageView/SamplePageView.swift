import SwiftUI

// PAGE VIEW is dedicated to handle user interactions and draw VIEW according to PROPS
struct SamplePageView: View {
    let props: SamplePageViewProps

    var body: some View {
        VStack {
            Text("SamplePageView")
                    .font(.headline)
                    .padding()

            Text(props.title)
                    .font(.title)
                    .padding()

            Button(action: props.sampleAction) {
                Text(props.sampleItem)
            }
        }
    }
}