import SwiftUI

struct ArchivedConversationsPageContainerProps {
    let onSwitchEditMode: (_ isEditMode: Bool, _ items: [BottomTabbarActionViewModel]) -> Void
}

struct ArchivedConversationsPageContainer: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject private var userInfoState: UserInfoState
    @EnvironmentObject private var countryState: CountryState
    @EnvironmentObject private var chatState: ChatState

    let props: ArchivedConversationsPageContainerProps

    var body: some View {
        ArchivedConversationsPageView(
                props: ArchivedConversationsPageViewProps(
                        conversations: ConversationsHelper.buildConversationViewModels(conversations: chatState.archivedConversations?.conversations ?? [], isArchived: true, filterType: .allNumbers, defaultPhone: userInfoState.accountInfo?.defaultPhone, accountInfo: userInfoState.accountInfo!),
                        onMute: onMute,
                        onDelete: onDelete,
                        onClear: onClear,
                        onUnarchive: onUnarchived,
                        onRead: onRead,
                        onUnread: onUnread,
                        onSwitchEditMode: props.onSwitchEditMode,
                        onSelectedConversation: onSelectedConversation
                )
        )
                .onDisappear {
                    props.onSwitchEditMode(false, [])
                }
    }

    private func onMute(isMuted: Bool, id: String) {
        if let index = chatState.archivedConversations?.conversations.firstIndex(where: { $0.id == id }) {
            chatState.archivedConversations?.conversations[index].isMuted = isMuted
        }
    }

    private func onDelete(ids: [String]) {
        for id in ids {
            if let index = chatState.archivedConversations?.conversations.firstIndex(where: { $0.id == id }) {
                chatState.archivedConversations?.conversations.remove(at: index)
            }
        }
        updateStateOfArchivedConersations()
    }

    private func onClear(ids: [String]) {
        for id in ids {
            if let index = chatState.archivedConversations?.conversations.firstIndex(where: { $0.id == id }) {
                chatState.archivedConversations?.conversations[index].messages.removeAll()
            }
        }
        updateStateOfArchivedConersations()
    }

    private func onUnarchived(ids: [String]) {
        for id in ids {
            if let index = chatState.archivedConversations?.conversations.firstIndex(where: { $0.id == id }) {
                if let conversation = chatState.archivedConversations?.conversations[index] {
                    chatState.archivedConversations?.conversations.remove(at: index)
                    chatState.conversations?.append(conversation)
                }
            }
        }
        if chatState.archivedConversations?.conversations.isEmpty == true {
            presentationMode.wrappedValue.dismiss()
        }
        updateStateOfArchivedConersations()
    }

    private func onRead(ids: [String]) {
        for id in ids {
            if let index = chatState.archivedConversations?.conversations.firstIndex(where: { $0.id == id }) {
                chatState.archivedConversations?.conversations[index].isUnread = false
                chatState.archivedConversations?.conversations[index].unreadCount = 0
                for i in 0..<chatState.archivedConversations!.conversations[index].messages.count {
                    if !chatState.archivedConversations!.conversations[index].messages[i].phoneNumbersHaveRead.contains(chatState.archivedConversations!.conversations[index].userPhoneInfo.phoneNumber) {
                        chatState.archivedConversations!.conversations[index].messages[i].phoneNumbersHaveRead.append(chatState.archivedConversations!.conversations[index].userPhoneInfo.phoneNumber)
                    }
                }
            }
        }
        updateStateOfArchivedConersations()
    }

    private func onUnread(id: String) {
        if let index = chatState.archivedConversations?.conversations.firstIndex(where: { $0.id == id }) {
            chatState.archivedConversations?.conversations[index].isUnread = true
        }
        updateStateOfArchivedConersations()
    }
    
    private func updateStateOfArchivedConersations() {
        if chatState.archivedConversations?.conversations.first?.isUnread != nil {
            chatState.archivedConversations?.isUnread = true
        }
        
        var unreadCount = 0
        for conversation in chatState.archivedConversations?.conversations ?? [] {
            unreadCount += conversation.unreadCount
        }
        chatState.archivedConversations?.unreadCount = unreadCount
    }

    private func onSelectedConversation(id: String) {
        print("\(#function): \(id)")
    }
}
