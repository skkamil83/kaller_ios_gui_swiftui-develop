import SwiftUI

struct ArchivedConversationsPageView: View {
    @State private var isInEditMode = false {
        didSet {
            props.onSwitchEditMode(isInEditMode, bottomActions)
        }
    }
    @State private var conversationIdsSelected = [String]() {
        didSet {
            props.onSwitchEditMode(isInEditMode, bottomActions)
        }
    }
    @State private var bottomActions: [BottomTabbarActionViewModel] = []{
        didSet {
            if oldValue != bottomActions {
                props.onSwitchEditMode(isInEditMode, bottomActions)
            }
        }
    }
    @State private var actionSheet: ArchivedConversationsPageActionSheetType?

    let props: ArchivedConversationsPageViewProps
    var body: some View {
        ZStack{
            updateBottomActions(props: props)
            list(props: props)
                    .navigationBarTitle("Archived Conversations", displayMode: .inline)
                    .navigationBarItems(
                            trailing: trailingButton()
                    )
                    .actionSheet(item: $actionSheet) { item in
                        switch item {
                        case .delete(let ids):
                            return deleteActionSheet(props: props, ids: ids)
                        }
                    }
        }
    }
    
    private func updateBottomActions(props: ArchivedConversationsPageViewProps) -> some View{
        let actions = getBottomActions(props: props)
        DispatchQueue.main.async {
            self.bottomActions = actions
        }
        return EmptyView()
    }

    private func list(props: ArchivedConversationsPageViewProps) -> some View {
        ScrollView {
            LazyVStack(spacing: 0) {
                ForEach(props.conversations, id: \.self.id) { conversation in
                    ChatListRow(
                            props: ChatListRowProps(
                                    conversation: conversation,
                                    state: isInEditMode ? .edit(conversationIdsSelected.contains(conversation.id)) : .normal,
                                    onMute: { isMute in
                                        props.onMute(isMute, conversation.id)
                                    },
                                    onDelete: {
                                        actionSheet = .delete([conversation.id])
                                    },
                                    onArchive: { isArchived in
                                        if !isArchived {
                                            props.onUnarchive([conversation.id])
                                        }
                                    },
                                    onRead: {
                                        props.onRead([conversation.id])
                                    },
                                    onUnread: {
                                        props.onUnread(conversation.id)
                                    },
                                    onPin: { _ in }
                            )
                    )
                            .onTapGesture {
                                if isInEditMode {
                                    if let index = conversationIdsSelected.firstIndex(where: { $0 == conversation.id }) {
                                        conversationIdsSelected.remove(at: index)
                                    } else {
                                        conversationIdsSelected.append(conversation.id)
                                    }
                                } else {
                                    props.onSelectedConversation(conversation.id)
                                }
                            }
                }
            }
        }
    }

    private func readAllBottomAction(props: ArchivedConversationsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ArchivedConversationsPageEditActionType.readAll.id,
                title: ArchivedConversationsPageEditActionType.readAll.title,
                isDisable: false,
                color: nil,
                action: { _ in
                    props.onRead(props.conversations.filter({ $0.isUnread }).map({ $0.id }))
                }
        )
    }

    private func readBottomAction(props: ArchivedConversationsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ArchivedConversationsPageEditActionType.read.id,
                title: ArchivedConversationsPageEditActionType.read.title,
                isDisable: ConversationsHelper.checkReadButtonInTabbarIsDisable(conversations: props.conversations, idsSelected: conversationIdsSelected),
                color: nil,
                action: { _ in
                    props.onRead(conversationIdsSelected)
                    conversationIdsSelected.removeAll()
                }
        )
    }

    private func unarchiveBottomAction(props: ArchivedConversationsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ArchivedConversationsPageEditActionType.unarchive.id,
                title: ArchivedConversationsPageEditActionType.unarchive.title,
                isDisable: conversationIdsSelected.isEmpty,
                color: nil,
                action: { _ in
                    props.onUnarchive(conversationIdsSelected)
                    conversationIdsSelected.removeAll()
                }
        )
    }

    private func deleteBottomAction(props: ArchivedConversationsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ArchivedConversationsPageEditActionType.delete.id,
                title: ArchivedConversationsPageEditActionType.delete.title,
                isDisable: conversationIdsSelected.isEmpty,
                color: nil,
                action: { _ in
                    actionSheet = .delete(conversationIdsSelected)
                }
        )
    }

    private func trailingButton() -> some View {
        Button(action: {
            isInEditMode = !isInEditMode
            if !isInEditMode {
                conversationIdsSelected.removeAll()
            }
        }) {
            Text(isInEditMode ? "Done" : "Edit")
                    .textStyle(.body3Button1Light5Blue)
        }
    }

    private func deleteActionSheet(props: ArchivedConversationsPageViewProps, ids: [String]) -> ActionSheet {
        ActionSheet(
                title: Text(ConversationsHelper.getTitleOfDeleteActionSheet(conversations: props.conversations, idsSelected: ids)),
                buttons: [
                    .default(Text("Clear")) {
                        props.onClear(ids)
                        conversationIdsSelected.removeAll()
                    },
                    .destructive(Text("Delete")) {
                        props.onDelete(ids)
                        conversationIdsSelected.removeAll()
                    },
                    .cancel()
                ]
        )
    }

    private func getBottomActions(props: ArchivedConversationsPageViewProps) -> [BottomTabbarActionViewModel] {
        if ConversationsHelper.checkIsShowReadAllInTabbar(conversations: props.conversations, idsSelected: conversationIdsSelected) {
            return bottomActionsWithReadAll()
        } else {
            return bottomActionWithRead()
        }
    }

    private func bottomActionsWithReadAll() -> [BottomTabbarActionViewModel] {
        [readAllBottomAction(props: props),
         unarchiveBottomAction(props: props),
         deleteBottomAction(props: props)]
    }

    private func bottomActionWithRead() -> [BottomTabbarActionViewModel] {
        [readBottomAction(props: props),
         unarchiveBottomAction(props: props),
         deleteBottomAction(props: props)]
    }
}

struct ArchivedConversationsPageView_Previews: PreviewProvider {
    static var previews: some View {
        ArchivedConversationsPageView(
                props: ArchivedConversationsPageViewProps(
                        conversations: [],
                        onMute: { _, _ in },
                        onDelete: { _ in },
                        onClear: { _ in },
                        onUnarchive: { _ in },
                        onRead: { _ in },
                        onUnread: { _ in },
                        onSwitchEditMode: { _, _ in },
                        onSelectedConversation: { _ in }
                )
        )
    }
}
