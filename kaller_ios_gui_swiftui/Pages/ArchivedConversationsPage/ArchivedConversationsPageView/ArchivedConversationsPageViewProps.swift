struct ArchivedConversationsPageViewProps {
    let conversations: [ConversationViewModel]
    let onMute: (_ isMute: Bool, _ id: String) -> Void
    let onDelete: (_ ids: [String]) -> Void
    let onClear: (_ ids: [String]) -> Void
    let onUnarchive: (_ ids: [String]) -> Void
    let onRead: (_ ids: [String]) -> Void
    let onUnread: (_ id: String) -> Void
    let onSwitchEditMode: (_ isEditMode: Bool, _ items: [BottomTabbarActionViewModel]) -> Void
    let onSelectedConversation: (_ id: String) -> Void
}
