enum ArchivedConversationsPageEditActionType: Identifiable {

    var id: String {
        return title.lowercased()
    }

    case read
    case readAll
    case unarchive
    case delete

    var title: String {
        switch self {
        case .read:
            return "Read"
        case .readAll:
            return "Read All"
        case .unarchive:
            return "Unarchive"
        case .delete:
            return "Delete"
        }
    }
}
