enum ArchivedConversationsPageActionSheetType: Identifiable {
    var id: String {
        switch self {
        case .delete(_):
            return "delete"
        }
    }

    case delete(_ ids: [String])
}
