import SwiftUI

struct LoginProfileContainerProps {
    let flagImageName: String
    let phoneNumber: String
    let locationImageUrl: String
}

struct LoginProfileContainer: View {
    @State private var avatar: UIImage? = nil
    let props: LoginProfileContainerProps

    var body: some View {
        LoginProfileView(
                props: LoginProfileViewProps(
                        avatar: avatar,
                        flagImageName: props.flagImageName,
                        phoneNumber: props.phoneNumber,
                        locationImageUrl: props.locationImageUrl,
                        onDone: done,
                        onCancel: cancel,
                        onTakePhoto: takePhoto,
                        onChoosePhoto: choosePhoto)
        )
    }

    private func cancel() {
        print(#function)
    }

    private func takePhoto() {
        print(#function)
    }

    private func choosePhoto() {
        print(#function)
    }

    private func done(firstName: String, lastName: String) {
        print("\(#function)  \(firstName)  \(lastName)")
    }
}
