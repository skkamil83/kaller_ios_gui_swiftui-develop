import SwiftUI

struct LoginProfileContainerSample: View {
    var body: some View {
        LoginProfileContainer(
                props: LoginProfileContainerProps(
                        flagImageName: "flag_AD",
                        phoneNumber: "+972 50 999 9999",
                        locationImageUrl: "https://images.unsplash.com/photo-1602741996025-18d802e46a0f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80"
                )
        )
    }
}
