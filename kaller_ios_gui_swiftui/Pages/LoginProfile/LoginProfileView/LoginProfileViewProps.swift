import SwiftUI

struct LoginProfileViewProps {
    let avatar: UIImage?
    let flagImageName: String
    let phoneNumber: String
    let locationImageUrl: String
    let onDone: (_ firstName: String, _ lastName: String) -> Void
    let onCancel: () -> Void
    let onTakePhoto: () -> Void
    let onChoosePhoto: () -> Void
}
