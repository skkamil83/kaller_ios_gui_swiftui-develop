import SwiftUI

struct LoginProfileView: View {
    let props: LoginProfileViewProps
    @State private var isShowPhotoActionSheet = false
    @State private var firstName = ""
    @State private var lastName = ""

    var body: some View {
        VStack(spacing: 18) {
            location(props: props)
            infosView(props: props)
            Spacer()
        }
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        header(props: props)
                    }
                }
                .navigationBarItems(
                        leading: cancel(props: props),
                        trailing: done(props: props)
                )
                .navigationBarBackButtonHidden(true)
                .actionSheet(isPresented: $isShowPhotoActionSheet) {
                    photoActionSheet(props: props)
                }
    }

    private func location(props: LoginProfileViewProps) -> some View {
        let size = locationImageSize()
        return URLImage(
                props: URLImageProps(
                        urlString: props.locationImageUrl,
                        contentMode: .fill,
                        size: size
                )
        )
                .frame(width: size.width, height: size.height)
    }

    private func infosView(props: LoginProfileViewProps) -> some View {
        HStack(spacing: 16) {
            avatarView(props: props)
                    .padding(.leading, Spacing.spacing14Pt)
            VStack(spacing: 0) {
                VStack(spacing: 0) {
                    Spacer()
                    TextFiledWithCleaner(
                            props: TextFiledWithCleanerProps(
                                    text: $firstName,
                                    placeHolder: "First Name",
                                    onFocus: {_ in}
                            )
                    )
                            .padding(.trailing, 15)
                    Spacer()
                    Divider()
                }
                        .frame(height: 44)
                VStack(spacing: 0) {
                    Spacer()
                    TextFiledWithCleaner(
                            props: TextFiledWithCleanerProps(
                                    text: $lastName,
                                    placeHolder: "Last Name",
                                    onFocus: {_ in}
                            )
                    )
                            .padding(.trailing, 15)
                    Spacer()
                    Divider()
                }
                        .frame(height: 44)
            }
        }
    }

    private func avatarView(props: LoginProfileViewProps) -> some View {
        AvatarLoader(
                props: AvatarLoaderProps(
                        size: 86,
                        firstName: firstName,
                        lastName: lastName,
                        avatarImage: props.avatar,
                        avatarUrl: nil,
                        onEditPhoto: {
                            isShowPhotoActionSheet.toggle()
                        }
                )
        )
    }

    private func header(props: LoginProfileViewProps) -> some View {
        VStack(spacing: 1) {
            Text("Profile Info")
                    .textStyle(.body2Bold1Light1LabelColor)
            HStack(spacing: Spacing.spacing4Pt) {
                CircleIcon(
                        props: CircleIconProps(
                                size: 13,
                                style: .filled(imageName: props.flagImageName)
                        )
                )
                Text(props.phoneNumber)
                        .textStyle(.footnote1Default1Light2SecondaryLabelColor)
            }
        }
    }

    private func done(props: LoginProfileViewProps) -> some View {
        Button(action: {
            props.onDone(firstName, lastName)
        }) {
            Text("Done")
                    .font(TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: .tintsDisabledLight).font)
        }
                .disabled(firstName.isEmpty || lastName.isEmpty)
    }

    private func cancel(props: LoginProfileViewProps) -> some View {
        Button(action: props.onCancel) {
            Text("Cancel")
                    .font(TextStyle.body3Button1Light5Blue.font)
        }
    }

    private func photoActionSheet(props: LoginProfileViewProps) -> ActionSheet {
        ActionSheet(
                title: Text("Add profile photo."),
                message: nil,
                buttons: [
                    .default(Text("Take Photo")) {
                        props.onTakePhoto()
                    },
                    .default(Text("Chosse Photo")) {
                        props.onChoosePhoto()
                    },
                    .cancel()
                ]
        )
    }

    private func locationImageSize() -> CGSize {
        let width = UIScreen.main.bounds.width
        let height = width * 232 / 375
        return CGSize(width: width, height: height)
    }
}

struct LoginProfileView_Previews: PreviewProvider {
    static var previews: some View {
        LoginProfileView(
                props: LoginProfileViewProps(
                        avatar: nil,
                        flagImageName: "",
                        phoneNumber: "",
                        locationImageUrl: "",
                        onDone: { _, _ in },
                        onCancel: {},
                        onTakePhoto: {},
                        onChoosePhoto: {}
                )
        )
    }
}
