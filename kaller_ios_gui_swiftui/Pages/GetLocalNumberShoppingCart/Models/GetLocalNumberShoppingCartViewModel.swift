struct GetLocalNumberShoppingCartViewModel {
    let country: Country
    let location: Location
    let phoneNumber: String
    let plan: PBXPlan
    let seconds: Int
}
