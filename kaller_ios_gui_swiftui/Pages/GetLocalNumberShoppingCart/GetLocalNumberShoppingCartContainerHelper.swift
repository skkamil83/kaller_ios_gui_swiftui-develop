class GetLocalNumberShoppingCartContainerHelper {
    static func buildShoppingCartViewModel(country: Country, location: Location, phoneNumber: String, plan: PBXPlan, seconds: Int) -> GetLocalNumberShoppingCartViewModel {
        return GetLocalNumberShoppingCartViewModel(
            country: country,
            location: location,
            phoneNumber: phoneNumber,
            plan: plan,
            seconds: seconds
        )
    }
}
