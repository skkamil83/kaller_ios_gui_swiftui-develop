class GetLocalNumberShoppingCartViewHelper {
    static func getSetupPrice(plan: PBXPlanInfo) -> String {
        return "\(plan.currency.symbol)\(plan.setupPrice.toFitString())"
    }

    static func getMonthlyPrice(plan: PBXPlanInfo) -> String {
        return "\(plan.currency.symbol)\(plan.numberPricePerMonth.toFitString())"
    }

    static func getCloudMonthlyPrice(plan: PBXPlan) -> String {
        let price = plan.phones.isEmpty ? plan.type.info.cloudPricePetMonth : 0
        return "\(plan.type.info.currency.symbol)\(price.toFitString())"
    }

    static func getTotalMonthlyPrice(plan: PBXPlan) -> String {
        let price = plan.phones.isEmpty ? (plan.type.info.cloudPricePetMonth + plan.type.info.numberPricePerMonth) : plan.type.info.numberPricePerMonth
        return "\(plan.type.info.currency.symbol)\(price.toFitString())"
    }

    static func getTotalCheckOutPrice(plan: PBXPlan) -> String {
        let price = plan.phones.isEmpty ? (plan.type.info.cloudPricePetMonth + plan.type.info.numberPricePerMonth) : plan.type.info.numberPricePerMonth
        return "\(plan.type.info.currency.symbol)\(price.toString(decimalCount: 2))"
    }

    static func getPhoneNumbersJoined(phones: [PhoneInfo]) -> String {
        return phones.map({ $0.phoneNumber }).joined(separator: ", ")
    }
}
