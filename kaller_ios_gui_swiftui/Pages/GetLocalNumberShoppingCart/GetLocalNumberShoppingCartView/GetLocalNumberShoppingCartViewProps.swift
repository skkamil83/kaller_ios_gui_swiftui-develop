struct GetLocalNumberShoppingCartViewProps {
    let viewModel: GetLocalNumberShoppingCartViewModel
    let onRemove: () -> Void
    let onCheckOut: () -> Void
}
