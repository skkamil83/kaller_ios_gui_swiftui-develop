import SwiftUI

struct GetLocalNumberShoppingCartView: View {
    @State private var isShowClearAlert = false
    let props: GetLocalNumberShoppingCartViewProps

    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                cardNumberPreviewer(props: props)
                offerInfo(props: props)
                offerDescription()
                addNumberHeader()
                pbxDetails(props: props)
                totalPayment(props: props)
                checkOutButton(props: props)
                Spacer()
            }
        }
                .background(background())
                .navigationTitle("Shopping Cart")
                .navigationBarTitleDisplayMode(.inline)
                .alert(isPresented: $isShowClearAlert, content: {
                    clearCartAlert(props: props)
                })
    }

    private func background() -> some View {
        Color.navigationBarBackgroundChrome
                .edgesIgnoringSafeArea(.all)
    }

    private func cardNumberPreviewer(props: GetLocalNumberShoppingCartViewProps) -> some View {
        CardNumberPreview(
                props: CardNumberPreviewProps(
                        locationPreviewImageURL: props.viewModel.location.previewImageUrl,
                        isEnable: props.viewModel.seconds > 0,
                        flagImageName: props.viewModel.country.flagImageName,
                        countryName: props.viewModel.country.localizedName,
                        locationName: props.viewModel.location.localizedName,
                        phoneNumber: props.viewModel.phoneNumber,
                        onTrashTap: {
                            isShowClearAlert.toggle()
                        }
                )
        )
                .background(Color.white)
    }

    private func offerInfo(props: GetLocalNumberShoppingCartViewProps) -> some View {
        VStack(spacing: 0) {
            HStack(spacing: 0) {
                pricingInfo(props: props)
                Spacer()
                offerTimer(props: props)
            }
                    .frame(height: 44)
            Divider()
        }
                .background(Color.white)
    }
    
    private func offerTimer(props: GetLocalNumberShoppingCartViewProps) -> some View {
        Text(props.viewModel.seconds.secondsToHoursMinutes())
                .textStyle(props.viewModel.seconds > 0 ? .subheadline1Default1Light3TertiaryLabelColor : .subheadline1Default1Light6Red)
                .padding(.trailing, Spacing.spacing16Pt)
    }
    
    private func pricingInfo(props: GetLocalNumberShoppingCartViewProps) -> some View {
        HStack(spacing: 0) {
            (Text("Setup: ")
                    + Text(GetLocalNumberShoppingCartViewHelper.getSetupPrice(plan: props.viewModel.plan.type.info))
            )
                    .textStyle(.subheadline1Default1Light1LabelColor)
                    .padding(.leading, Spacing.spacing16Pt)
            (Text("Monthly: ")
                    + Text(GetLocalNumberShoppingCartViewHelper.getMonthlyPrice(plan: props.viewModel.plan.type.info))
            )
                    .textStyle(.subheadline1Default1Light1LabelColor)
                    .padding(.leading, Spacing.spacing16Pt)
        }
    }

    private func offerDescription() -> some View {
        Text("The number will be active immediately after payment.\nThe selected number is reserved for 30 minutes.")
                .textStyle(.footnote21Default1Light2SecondaryLabelColor)
                .padding(EdgeInsets(top: Spacing.spacing8Pt, leading: 18, bottom: 13, trailing: 18))
                .frame(width: UIScreen.main.bounds.width, alignment: .leading)
    }

    private func addNumberHeader() -> some View {
        VStack(alignment: .leading, spacing: 0) {
            Text("ADD NUMBER TO PBX VERSION")
                    .textStyle(.footnote1Default1Light6DarkGray)
                    .padding(EdgeInsets(top: 7, leading: Spacing.spacing16Pt, bottom: 7, trailing: 0))
            Divider()
        }
    }

    private func pbxDetails(props: GetLocalNumberShoppingCartViewProps) -> some View {
        NavigationLink(
                destination: Text("PBX Selector")
        ) {
            pbxDetailsTitle(props: props)
        }
    }

    private func pbxDetailsTitle(props: GetLocalNumberShoppingCartViewProps) -> some View {
        VStack(spacing: 0) {
            HStack(spacing: 0) {
                AvatarView(
                        props: AvatarViewProps(
                                size: 34,
                                style: .blueGlyph(
                                        systemImageName: "cloud.fill"
                                )
                        )
                )
                        .padding(.leading, Spacing.spacing16Pt)
                Text(props.viewModel.plan.type.info.name)
                        .textStyle(.body2Bold1Light1LabelColor)
                        .padding(.leading, Spacing.spacing8Pt)
                Spacer()
                Text(props.viewModel.plan.phones.isEmpty ? "New" : "Existing")
                        .textStyle(.body1Default1Light2SecondaryLabelColor)
                        .padding(.trailing, Spacing.spacing12Pt)
                Image("sortChevronLight")
                        .padding(.trailing, Spacing.spacing16Pt)

            }
                    .frame(height: 44)
            if props.viewModel.plan.phones.isEmpty {
                pbxDetailsForNew(props: props)
            } else {
                pbxDetailsForExisting(props: props)
            }
            Divider()
        }
                .background(Color.white)
    }

    private func pbxDetailsForNew(props: GetLocalNumberShoppingCartViewProps) -> some View {
        HStack(spacing: 0) {
            (Text("Setup: ")
                    + Text(GetLocalNumberShoppingCartViewHelper.getSetupPrice(plan: props.viewModel.plan.type.info))
            )
                    .textStyle(.subheadline1Default1Light1LabelColor)
            (Text("Monthly: ")
                    + Text(GetLocalNumberShoppingCartViewHelper.getCloudMonthlyPrice(plan: props.viewModel.plan))
            )
                    .textStyle(.subheadline1Default1Light1LabelColor)
                    .padding(.leading, Spacing.spacing24Pt)
            Spacer()
        }
                .padding(EdgeInsets(top: 1, leading: Spacing.spacing16Pt, bottom: 6, trailing: 0))
    }

    private func pbxDetailsForExisting(props: GetLocalNumberShoppingCartViewProps) -> some View {
        Text(GetLocalNumberShoppingCartViewHelper.getPhoneNumbersJoined(phones: props.viewModel.plan.phones))
                .textStyle(.subheadline1Default1Light1LabelColor)
                .lineLimit(1)
                .padding(EdgeInsets(top: 1, leading: Spacing.spacing16Pt, bottom: 6, trailing: Spacing.spacing24Pt))
                .frame(width: UIScreen.main.bounds.width, alignment: .leading)
    }

    private func totalPayment(props: GetLocalNumberShoppingCartViewProps) -> some View {
        VStack(alignment: .leading, spacing: 0) {
            Text("The number will be added to a new cloud PBX with the ability to create new settings.")
                    .textStyle(.footnote21Default1Light2SecondaryLabelColor)
                    .padding(EdgeInsets(top: Spacing.spacing8Pt, leading: 18, bottom: 13, trailing: 18))
            (Text("One-time payment: ")
                    + Text(GetLocalNumberShoppingCartViewHelper.getSetupPrice(plan: props.viewModel.plan.type.info)))
                    .textStyle(.subheadline1Default1Light1LabelColor)
                    .padding(.leading, Spacing.spacing16Pt)
            (Text("Monthly payment: ")
                    + Text(GetLocalNumberShoppingCartViewHelper.getTotalMonthlyPrice(plan: props.viewModel.plan)))
                    .textStyle(.subheadline1Default1Light1LabelColor)
                    .padding(EdgeInsets(top: Spacing.spacing8Pt, leading: Spacing.spacing16Pt, bottom: 0, trailing: 0))
        }
    }

    private func checkOutButton(props: GetLocalNumberShoppingCartViewProps) -> some View {
        Button(action: props.onCheckOut) {
            VStack(spacing: 0) {
                Text("Proceed to Checkout")
                        .textStyle(.body2Bold2Dark1LabelColor)
                (Text("Total Amount: ")
                        + Text(GetLocalNumberShoppingCartViewHelper.getTotalCheckOutPrice(plan: props.viewModel.plan)))
                        .textStyle(.subheadline1Default2Dark1LabelColor)
            }
        }
                .frame(width: UIScreen.main.bounds.width - 32, height: 55)
                .background(props.viewModel.seconds > 0 ? Color.primaryBlue : Color.graysSystemGray4Light)
                .cornerRadius(8)
                .padding(EdgeInsets(top: 40, leading: 0, bottom: 17, trailing: 0))
                .disabled(props.viewModel.seconds == 0)
    }

    private func clearCartAlert(props: GetLocalNumberShoppingCartViewProps) -> Alert {
        Alert(
                title: Text("Clear Shopping Cart"),
                message: Text("Are you sure you want to remove this item from the shopping cart?"),
                primaryButton: .cancel(),
                secondaryButton: .default(Text("Remove"), action: props.onRemove)
        )
    }
}

struct GetLocalNumberShoppingCartView_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberShoppingCartView(
                props: GetLocalNumberShoppingCartViewProps(
                        viewModel: GetLocalNumberShoppingCartViewModel(
                                country: Country(
                                        localizedName: "Afghanistan",
                                        iso: "AF",
                                        phoneCode: 93,
                                        countryCode: 4,
                                        flagImageName: "flag_AF"
                                ),
                                location: Location(
                                        localizedName: "Mobile",
                                        phoneCode: 93,
                                        locationCode: 2,
                                        previewImageUrl: "https://images.unsplash.com/photo-1552620888-a4553dd93b5e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"
                                ),
                                phoneNumber: "+972 00-000-0000",
                                plan: PBXPlan(
                                        type: .extended,
                                        phones: []
                                ),
                                seconds: 100
                        ),
                        onRemove: {},
                        onCheckOut: {}
                )
        )
    }
}
