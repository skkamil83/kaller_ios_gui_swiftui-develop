import SwiftUI

struct GetLocalNumberShoppingCartContainerSample: View {

    enum SampleType {
        case newPlan
        case existPlan
    }

    let sample: SampleType

    var body: some View {
        switch sample {
        case .newPlan:
            return AnyView(newPlan())
        case .existPlan:
            return AnyView(existPlan())
        }
    }

    private func newPlan() -> some View {
        GetLocalNumberShoppingCartContainer(
                props: GetLocalNumberShoppingCartContainerProps(
                        country: Country(
                                localizedName: "Afghanistan",
                                iso: "AF",
                                phoneCode: 93,
                                countryCode: 4,
                                flagImageName: "flag_AF"
                        ),
                        location: Location(
                                localizedName: "Mobile",
                                phoneCode: 93,
                                locationCode: 2,
                                previewImageUrl: "https://images.unsplash.com/photo-1552620888-a4553dd93b5e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"
                        ),
                        phoneNumber: "+869054695485",
                        plan: PBXPlan(type: .basic, phones: [])
                )
        )
    }

    private func existPlan() -> some View {
        GetLocalNumberShoppingCartContainer(
                props: GetLocalNumberShoppingCartContainerProps(
                        country: Country(
                                localizedName: "Afghanistan",
                                iso: "AF",
                                phoneCode: 93,
                                countryCode: 4,
                                flagImageName: "flag_AF"
                        ),
                        location: Location(
                                localizedName: "Mobile",
                                phoneCode: 93,
                                locationCode: 2,
                                previewImageUrl: "https://images.unsplash.com/photo-1552620888-a4553dd93b5e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"
                        ),
                        phoneNumber: "+34543953492",
                        plan: PBXPlan(
                                type: .maximum,
                                phones: [
                                    .init(isDefault: false, userInfo: UserInfo(firstName: "", lastName: "", avatarImageUrl: ""), phoneNumber: "+43543897878", nickName: nil, place: "", sortOrder: 0, phoneCode: 0, locationCode: 0, ext: nil),
                                    .init(isDefault: false, userInfo: UserInfo(firstName: "", lastName: "", avatarImageUrl: ""), phoneNumber: "+589485954", nickName: nil, place: "", sortOrder: 0, phoneCode: 0, locationCode: 0, ext: nil),
                                    .init(isDefault: false, userInfo: UserInfo(firstName: "", lastName: "", avatarImageUrl: ""), phoneNumber: "+958469546", nickName: nil, place: "", sortOrder: 0, phoneCode: 0, locationCode: 0, ext: nil),
                                    .init(isDefault: false, userInfo: UserInfo(firstName: "", lastName: "", avatarImageUrl: ""), phoneNumber: "+45466450000", nickName: nil, place: "", sortOrder: 0, phoneCode: 0, locationCode: 0, ext: nil)
                                ]
                        )
                )
        )
    }
}

