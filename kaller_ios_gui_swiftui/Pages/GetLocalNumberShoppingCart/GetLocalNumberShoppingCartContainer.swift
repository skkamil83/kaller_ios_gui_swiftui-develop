import SwiftUI

struct GetLocalNumberShoppingCartContainerProps {
    let country: Country
    let location: Location
    let phoneNumber: String
    let plan: PBXPlan
}

struct GetLocalNumberShoppingCartContainer: View {
    @State private var seconds: Int = 1800
    let props: GetLocalNumberShoppingCartContainerProps
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()

    var body: some View {
        GetLocalNumberShoppingCartView(
                props: GetLocalNumberShoppingCartViewProps(
                        viewModel: GetLocalNumberShoppingCartContainerHelper.buildShoppingCartViewModel(
                                country: props.country,
                                location: props.location,
                                phoneNumber: props.phoneNumber,
                                plan: props.plan,
                                seconds: seconds
                        ),
                        onRemove: onRemove,
                        onCheckOut: onCheckOut
                )
        )
                .onReceive(timer) { _ in
                    timeTicker()
                }
    }

    private func onRemove() {
        print(#function)
    }

    private func onCheckOut() {
        print(#function)
    }

    private func timeTicker() {
        if seconds > 0 {
            seconds -= 1
        }
    }
}

struct GetLocalNumberShoppingCartContainer_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberShoppingCartContainer(
                props: GetLocalNumberShoppingCartContainerProps(
                        country: Country(
                                localizedName: "Afghanistan",
                                iso: "AF",
                                phoneCode: 93,
                                countryCode: 4,
                                flagImageName: "flag_AF"
                        ),
                        location: Location(
                                localizedName: "Mobile",
                                phoneCode: 93,
                                locationCode: 2,
                                previewImageUrl: ""
                        ),
                        phoneNumber: "+972 00-000-0000",
                        plan: PBXPlan(
                                type: .maximum,
                                phones: []
                        )
                )
        )
    }
}
