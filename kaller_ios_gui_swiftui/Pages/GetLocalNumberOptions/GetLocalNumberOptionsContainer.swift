import SwiftUI

struct GetLocalNumberOptionsContainerProps {
    let localNumber: LocalNumberViewModel
}

struct GetLocalNumberOptionsContainer: View {
    
    let props: GetLocalNumberOptionsContainerProps
    
    var body: some View {
        GetLocalNumberOptionsView(
            props:
                GetLocalNumberOptionsViewProps(
                    viewModel: props.localNumber,
                    onSelectNumber: selectNumber
                )
        )
    }
    
    private func selectNumber() {
        print(#function)
    }
}
