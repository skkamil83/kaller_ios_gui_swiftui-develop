//
//  GetLocalNumberOptionsViewSample.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 30/12/2020.
//

import SwiftUI

struct GetLocalNumberOptionsViewSample: View {
    enum SampleType {
        case newPlan
        case existPlan
    }
    
    let sample: SampleType
    
    var body: some View {
        switch sample {
        case .newPlan:
            return AnyView(newPlan())
        case .existPlan:
            return AnyView(existPlan())
        }
    }
    
    private func newPlan() -> some View {
        GetLocalNumberOptionsContainer(
            props: GetLocalNumberOptionsContainerProps(
                localNumber: LocalNumberViewModel(
                    country: Country(
                        localizedName: "Afghanistan",
                        iso: "AF",
                        phoneCode: 93,
                        countryCode: 4,
                        flagImageName: "flag_AF"
                    ),
                    location: Location(
                        localizedName: "Mobile",
                        phoneCode: 93,
                        locationCode: 2,
                        previewImageUrl: "https://images.unsplash.com/photo-1552620888-a4553dd93b5e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"
                    ),
                    plan: PBXPlan(
                        type: .basic,
                        phones: []
                    )
                )
            )
        )
    }
    
    private func existPlan() -> some View {
        GetLocalNumberOptionsContainer(
            props: GetLocalNumberOptionsContainerProps(
                localNumber: LocalNumberViewModel(
                    country: Country(
                        localizedName: "Afghanistan",
                        iso: "AF",
                        phoneCode: 93,
                        countryCode: 4,
                        flagImageName: "flag_AF"
                    ),
                    location: Location(
                        localizedName: "Mobile",
                        phoneCode: 93,
                        locationCode: 2,
                        previewImageUrl: "https://images.unsplash.com/photo-1552620888-a4553dd93b5e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"
                    ),
                    plan: PBXPlan(
                        type: .maximum,
                        phones: [
                            .init(isDefault: false, userInfo: UserInfo(firstName: "", lastName: "", avatarImageUrl: ""), phoneNumber: "+543y56348", nickName: "", place: "", sortOrder: 0, phoneCode: 0, locationCode: 0, ext: nil)
                        ]
                    )
                )
            )
        )
    }
    
}

struct GetLocalNumberOptionsViewSample_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberOptionsViewSample(sample: .newPlan)
    }
}
