struct GetLocalNumberOptionsViewProps {
    let viewModel: LocalNumberViewModel
    let onSelectNumber: () -> Void
}
