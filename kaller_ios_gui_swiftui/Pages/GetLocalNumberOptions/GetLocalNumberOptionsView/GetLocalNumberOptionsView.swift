import SwiftUI

struct GetLocalNumberOptionsView: View {
    let props: GetLocalNumberOptionsViewProps
    var body: some View {
        ScrollView{
            VStack(spacing: 0){
                header()
                country(props: props)
                location(props: props)
                cloudPBX(props: props)
                image(props: props)
                price(props: props)
                selectNumberButton(props: props)
                Spacer()
            }
        }
        .navigationTitle("Get Local Number")
        .navigationBarTitleDisplayMode(.inline)
    }
    
    private func header() -> some View {
        VStack(spacing: 0){
            Spacer()
            HStack{
                Text("CHOOSE YOUR NUMBER")
                    .font(TextStyle.footnote1Default1Light6DarkGray.font)
                    .foregroundColor(TextStyle.footnote1Default1Light6DarkGray.color)
                    .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: 7, trailing: 0))
                Spacer()
            }
            Divider()
        }
        .frame(width: UIScreen.main.bounds.width, height: 56)
        .background(Color.navigationBarBackgroundChrome)
    }
    
    private func country(props: GetLocalNumberOptionsViewProps) -> some View {
        NavigationLink( destination: Text("Country") ) {
            contentLink(
                title: "Country",
                content: GetLocalNumberOptionsViewHelper.getCountryText(
                    country: props.viewModel.country
                )
            )
        }
    }
    
    private func location(props: GetLocalNumberOptionsViewProps) -> some View {
        NavigationLink( destination: Text("Location") ) {
            contentLink(
                title: "Location",
                content: GetLocalNumberOptionsViewHelper.getLocationText(
                    location: props.viewModel.location
                )
            )
        }
    }
    
    private func cloudPBX(props: GetLocalNumberOptionsViewProps) -> some View {
        NavigationLink( destination: Text("Cloud PBX") ) {
            contentLink(
                title: "Cloud PBX",
                content: GetLocalNumberOptionsViewHelper.getPBXText(
                    plan: props.viewModel.plan
                )
            )
        }
    }
    
    private func image(props: GetLocalNumberOptionsViewProps) -> some View {
        let size = getImageSize()
        return VStack(spacing: 0){
            Divider()
            URLImage(
                props: URLImageProps(
                    urlString: GetLocalNumberOptionsViewHelper.getPreviewImage(
                        location: props.viewModel.location
                    ),
                    contentMode: .fill,
                    size: size
                )
            )
        }
        .frame(height: size.height)
        .contentShape(Rectangle())
    }
    
    private func contentLink(title: String, content: String) -> some View {
        VStack(spacing: 0){
            VStack(spacing: 0){
                Spacer()
                HStack(spacing: 0){
                    Text(title)
                        .font(TextStyle.body1Default1Light1LabelColor2CenterAligned.font)
                        .foregroundColor(TextStyle.body1Default1Light1LabelColor2CenterAligned.color)
                    Spacer()
                    Text(content)
                        .font(TextStyle.body1Default1Light2SecondaryLabelColor.font)
                        .foregroundColor(TextStyle.body1Default1Light2SecondaryLabelColor.color)
                    Image("sortChevronLight")
                        .padding(.leading, Spacing.spacing12Pt)
                }
                Spacer()
            }
            .frame(height: 45)
            .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))
            Divider()
                .padding(.leading, Spacing.spacing16Pt)
        }
        .background(Color.white)
    }
    
    private func price(props: GetLocalNumberOptionsViewProps) -> some View {
        HStack(spacing: 0){
            priceItem(
                title: "Setup",
                price: GetLocalNumberOptionsViewHelper.getSetupPrice(
                    plan: props.viewModel.plan.type.info
                )
            )
            priceItem(
                title: "Number",
                price: GetLocalNumberOptionsViewHelper.getNumberPrice(
                    plan: props.viewModel.plan.type.info
                )
            )
                .padding(.leading, 32)
            priceItem(
                title: "Cloud PBX",
                price: GetLocalNumberOptionsViewHelper.getCloudPrice(
                    plan: props.viewModel.plan
                )
            )
                .padding(.leading, 32)
            Spacer()
            Text(GetLocalNumberOptionsViewHelper.getTotalPrice(
                    plan: props.viewModel.plan))
                .font(TextStyle.title11Default1Light1LabelColor2CenterAligned.font)
                .foregroundColor(TextStyle.title11Default1Light1LabelColor2CenterAligned.color)
                .lineLimit(1)
                .minimumScaleFactor(0.1)
        }
        .padding(EdgeInsets(top: 8, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))
    }
    
    private func priceItem(title: String, price: String) -> some View {
        VStack(alignment: .leading, spacing: 0){
            Text(title)
                .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                .foregroundColor(TextStyle.footnote1Default1Light2SecondaryLabelColor.color)
                .lineLimit(1)
                .fixedSize()
            Text(price)
                .font(TextStyle.subheadline1Default1Light1LabelColor.font)
                .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)
                .lineLimit(1)
                .fixedSize()
        }
    }
    
    private func selectNumberButton(props: GetLocalNumberOptionsViewProps) -> some View {
        Button(action: props.onSelectNumber){
            Text("Select Number")
                .font(TextStyle.body2Bold2Dark1LabelColor2CenterAligned.font)
                .foregroundColor(TextStyle.body2Bold2Dark1LabelColor2CenterAligned.color)
        }
        .frame(width: UIScreen.main.bounds.width - Spacing.spacing16Pt * 2, height: 50)
        .background(Color.primaryBlue)
        .cornerRadius(8)
        .padding(EdgeInsets(top: 34, leading: 0, bottom: 34, trailing: 0))
    }
    
    private func getImageSize() -> CGSize {
        let width = UIScreen.main.bounds.width
        let height = width * 232 / 375
        return CGSize(width: width, height: height)
    }
}

struct GetLocalNumberOptionsView_Previews: PreviewProvider {
    static var previews: some View {
        GetLocalNumberOptionsView(
            props: GetLocalNumberOptionsViewProps(
                viewModel: LocalNumberViewModel(
                    country: Country(
                        localizedName: "Afghanistan",
                        iso: "AF",
                        phoneCode: 93,
                        countryCode: 4,
                        flagImageName: "flag_AF"
                    ),
                    location: Location(
                        localizedName: "Mobile",
                        phoneCode: 93,
                        locationCode: 2,
                        previewImageUrl: ""
                    ),
                    plan: PBXPlan(
                        type: .basic,
                        phones: []
                    )
                ),
                onSelectNumber: {}
            )
        )
    }
}
