class GetLocalNumberOptionsViewHelper {
    static func getCountryText(country: Country) -> String {
        var text = "\(country.localizedName)"
        if let countryCode = country.countryCode{
            text += " (+\(countryCode))"
        }
        return text
    }
    
    static func getLocationText(location: Location) -> String {
        var text = "\(location.localizedName)"
        if let locationCode = location.locationCode{
            text += " (+\(locationCode))"
        }
        return text
    }
    
    static func getPreviewImage(location: Location) -> String {
        return location.previewImageUrl
    }
    
    static func getPBXText(plan: PBXPlan) -> String {
        var text = "\(plan.type.info.name)"
        if plan.phones.isEmpty {
            text += " (New)"
        }else{
            text += " (Existing)"
        }
        return text
    }
    
    static func getSetupPrice(plan: PBXPlanInfo) -> String {
        return "\(plan.currency.symbol)\(plan.setupPrice.toFitString())"
    }
    
    static func getNumberPrice(plan: PBXPlanInfo) -> String {
        return "\(plan.currency.symbol)\(plan.numberPricePerMonth.toFitString())\u{200A}/\u{200A}mo."
    }
    
    static func getCloudPrice(plan: PBXPlan) -> String {
        if plan.phones.isEmpty{
            return "\(plan.type.info.currency.symbol)\(plan.type.info.cloudPricePetMonth.toFitString())\u{200A}/\u{200A}mo."
        }else{
            return "Included"
        }
    }
    
    static func getTotalPrice(plan: PBXPlan) -> String {
        var price: Double = 0
        if plan.phones.isEmpty {
            price = plan.type.info.setupPrice + plan.type.info.numberPricePerMonth + plan.type.info.cloudPricePetMonth
        }else{
            price = plan.type.info.setupPrice + plan.type.info.numberPricePerMonth
        }
        return "\(plan.type.info.currency.symbol)\(price.toString(decimalCount: 2))"
    }
}
