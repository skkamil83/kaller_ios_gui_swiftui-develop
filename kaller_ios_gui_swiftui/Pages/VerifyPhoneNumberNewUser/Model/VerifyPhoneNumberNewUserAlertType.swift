//
//  VerifyPhoneNumberNewUserAlertType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 15/12/2020.
//

import Foundation

enum VerifyPhoneNumberNewUserAlertType: Identifiable{
    var id: String{
        switch self {
        case .incorrectCode:
            return "incorrectCode"
        case .expiredCode:
            return "expiredCode"
        case .setupEmail:
            return "setupEmail"
        case .stopVerification:
            return "stopVerification"
        }
    }
    
    case incorrectCode
    case expiredCode
    case setupEmail
    case stopVerification
}
