//
//  VerifyPhoneNumberNewUserViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 13/12/2020.
//

import Foundation

struct VerifyPhoneNumberNewUserViewProps{
    let phoneNumber: String
    let countDownSeconds: Int
    let onNext: (_ code: String) -> Void
    let haveNotReceivedCode: () -> Void
    let onBack: () -> Void
}
