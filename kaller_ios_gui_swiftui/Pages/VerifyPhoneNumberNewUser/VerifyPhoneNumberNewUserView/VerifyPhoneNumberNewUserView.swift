//
//  VerifyPhoneNumberNewUserView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 13/12/2020.
//

import SwiftUI

struct VerifyPhoneNumberNewUserView: View {
    let props: VerifyPhoneNumberNewUserViewProps
    @State private var code: String = ""
    
    init(props: VerifyPhoneNumberNewUserViewProps) {
        self.props = props
    }
    
    var body: some View {
        VStack(spacing: 0){
            verifyPhoneNumberText()
            desscriptionText()
            textFieldCode(props: props)
            Spacer()
                .frame(maxHeight: 84)
            countDown(props: props)
            Spacer()
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitleDisplayMode(.large)
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                phoneNumberToolbar(props: props)
            }
        })
        .navigationBarItems(
            leading: backButton(props: props),
            trailing: nextButton(props: props)
        )
    }
    
    private func verifyPhoneNumberText() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 28, color: .black)
        return Text("Verify phone number")
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.top, -28)
    }
    
    private func desscriptionText() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return Text("We have sent you an SMS with a code to the number above.")
            .frame(height: 44)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .multilineTextAlignment(.center)
            .padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
    }
    
    private func textFieldCode(props: VerifyPhoneNumberNewUserViewProps) -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 20, color: .black)
        return VStack(spacing: 0){
            Spacer()
            TextField("Code", text: $code)
                .multilineTextAlignment(.center)
                .font(textStyle.font)
                .foregroundColor(textStyle.color)
                .textContentType(.oneTimeCode)
                .keyboardType(.numberPad)
            Spacer()
            Divider()
        }
        .frame(height: 44)
        .padding(EdgeInsets(top: 79, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func countDown(props: VerifyPhoneNumberNewUserViewProps) -> some View{
        let countDownText = props.countDownSeconds.secondsToHoursMinutesSeconds()
        
        if props.countDownSeconds <= 0{
            return AnyView(
                Button(action: props.haveNotReceivedCode){
                    Text("Have not received the code?")
                        .font(TextStyle.subheadline1Default1Light5Blue.font)
                        .foregroundColor(TextStyle.subheadline1Default1Light5Blue.color)
                }
            )
        }else{
            return AnyView(
                Text("Kaller will call you in \(countDownText)")
                    .font(TextStyle.subheadline1Default1Light1LabelColor.font)
                    .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)
            )
        }
    }
    
    private func phoneNumberToolbar(props: VerifyPhoneNumberNewUserViewProps) -> some View{
        Text(props.phoneNumber)
            .font(TextStyle.body2Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
    }
    
    private func nextButton(props: VerifyPhoneNumberNewUserViewProps) ->  some View{
        let grayTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.tintsDisabledLight)
        let blueTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.primaryBlue)
        let ableToNext = !code.isEmpty
        
        return Button(action: {
            props.onNext(code)
        }){
            Text("Next")
                .font(ableToNext ? blueTextStyle.font : grayTextStyle.font)
                .foregroundColor(ableToNext ? blueTextStyle.color : grayTextStyle.color)
        }
        .disabled(!ableToNext)
    }
    
    private func backButton(props: VerifyPhoneNumberNewUserViewProps) -> some View{
        Button(action: props.onBack){
            HStack(spacing: 2){
                Image(systemName: "chevron.left")
                    .font(TextStyle(fontName: "SFProText-Medium", fontSize: 24, color: .primaryBlue).font)
                    .foregroundColor(.primaryBlue)
                Text("Back")
                    .font(TextStyle.body3Button1Light5Blue.font)
                    .foregroundColor(TextStyle.body3Button1Light5Blue.color)
            }
        }
    }
}

struct VerifyPhoneNumberNewUserView_Previews: PreviewProvider {
    static var previews: some View {
        VerifyPhoneNumberNewUserView(
            props: VerifyPhoneNumberNewUserViewProps(
                phoneNumber: "+972 50 999 9999",
                countDownSeconds: 3600,
                onNext: {_ in},
                haveNotReceivedCode: {},
                onBack: {}
            )
        )
    }
}
