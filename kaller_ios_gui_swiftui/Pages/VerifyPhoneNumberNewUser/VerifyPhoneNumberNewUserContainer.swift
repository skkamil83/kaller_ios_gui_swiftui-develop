//
//  VerifyPhoneNumberNewUserContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 13/12/2020.
//

import SwiftUI

struct VerifyPhoneNumberNewUserContainerProps{
    let phoneNumber: String
}

struct VerifyPhoneNumberNewUserContainer: View {
    let props: VerifyPhoneNumberNewUserContainerProps
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    @Environment(\.presentationMode) var presentationMode
    @State private var countDownTime: Int = 120
    @State private var alertType: VerifyPhoneNumberNewUserAlertType?
    
    var body: some View {
        VerifyPhoneNumberNewUserView(
            props: VerifyPhoneNumberNewUserViewProps(
                phoneNumber: props.phoneNumber,
                countDownSeconds: countDownTime,
                onNext: onNext(code:),
                haveNotReceivedCode: {
                    sendEmail(rops: props)
                },
                onBack: onBack
            )
        )
        .onReceive(timer) { time in
            if self.countDownTime >= 0 {
                self.countDownTime -= 1
            }
            
            if countDownTime == 60{
                resendSecondSMS()
            }else if countDownTime == 0 {
                callMyCode()
            }
        }
        .alert(item: $alertType) { (item) -> Alert in
            switch item{
            case .incorrectCode:
                return alertIncorrectCode()
            case .expiredCode:
                return alertExpiredCode()
            case .setupEmail:
                return alertSetUpEmail()
            case .stopVerification:
                return alertStopVerification()
            }
        }
    }
    
    private func onNext(code: String){
        //call back end here
        
        //if the code is incorrect
        //alertType = .incorrectCode
        
        //if the code is expired
        alertType = .expiredCode
    }
    
    private func onBack(){
        alertType = .stopVerification
    }
    
    private func sendEmail(rops: VerifyPhoneNumberNewUserContainerProps){
        let email = "sms@kaller.io"
        let subject = "\(props.phoneNumber), no code"
        let bodyText = "My phone number is:\n\(props.phoneNumber)\nI am having trouble receiving an activation code for Kaller."
        
        let coded = "mailto:\(email)?subject=\(subject)&body=\(bodyText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let emailURL = URL(string: coded!) {
            if UIApplication.shared.canOpenURL(emailURL) {
                UIApplication.shared.open(emailURL, options: [:], completionHandler: { (result) in
                    if !result {
                        print("Unable to send email.")
                    }
                })
            }else{
                alertType = .setupEmail
            }
        }
    }
    
    private func resendSecondSMS(){
        print(#function)
    }
    
    private func callMyCode(){
        print(#function)
    }
    
    private func alertIncorrectCode() -> Alert{
        Alert(
            title: Text("You have entered an invalid code. Please try again."),
            message: nil,
            dismissButton: .default(
                Text("OK")
            )
        )
    }
    
    private func alertExpiredCode() -> Alert{
        Alert(
            title: Text("The code has expired. Please login again."),
            message: nil,
            dismissButton: .default(
                Text("OK")
            )
        )
    }
    
    private func alertSetUpEmail() -> Alert{
        Alert(
            title: Text("Set up an email address"),
            message: Text("To report details about the issue, go to your device's settings > Passwords & Accounts > Add account > Switch on 'mail’."),
            dismissButton: .default(
                Text("OK")
            )
        )
    }
    
    private func alertStopVerification() -> Alert{
        Alert(
            title: Text("Would you like to stop the verification process?"),
            message: nil,
            primaryButton: .default(Text("Continue")),
            secondaryButton: .default(Text("Stop")){
                presentationMode.wrappedValue.dismiss()
            }
        )
    }
}

struct VerifyPhoneNumberNewUserContainer_Previews: PreviewProvider {
    static var previews: some View {
        VerifyPhoneNumberNewUserContainer(
            props: VerifyPhoneNumberNewUserContainerProps(
                phoneNumber: ""
            )
        )
    }
}
