import SwiftUI

struct PageContainerProps {
    let onOpenApp: ()->Void
}
struct PagesContainer: View {
    let props: PageContainerProps
    
    var body: some View {
        NavigationView {
            List {
                Group{
                    openApp()
                    samplePage()
                    keypadPage()
                    recentPage()
                    countrySelection()
                    registration()
                    confirmSimNumWithChatOtp()
                    confirmDidNumWithChatOtp()
                    callKit()
                    myNumbers()
                }
                Group{
                    getLocalNumberOptions()
                    loginProfile()
                    shoppingCart()
                    numberSelection()
                    contacts()
                    chatPage()
                }
            }
            .navigationBarTitle("Components", displayMode: .inline)
        }
    }
    
    private func openApp() -> some View {
        Button(action: props.onOpenApp) {
            Text("Open app")
        }
    }
    
    private func samplePage() -> some View {
        NavigationLink(
            destination: SamplePageContainer(
                props: SamplePageContainerProps(sampleItemId: 1)
            )
        ) {
            Text("Sample page")
        }
    }
    
    private func keypadPage() -> some View {
        let userInfoState = UserInfoState()
        userInfoState.accountInfo = AccountInfo(
            phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
            account: Account(id: 1, name: "Account name", createDate: Date()),
            devices: []
        )
        
        
        let countryState = CountryState()
        countryState.countries = CountryFetcher().countries
        
        return NavigationLink(
            destination: KeypadPageContainer(
                props: KeypadPageContainerProps(
                    openSelectDefaultNumberPage: {_,_  in },
                    openSelectCurrentNumber: {_,_,_  in }
                )
            )
            .environmentObject(userInfoState)
            .environmentObject(ContactsState())
            .environmentObject(countryState)
            
        ) {
            Text("KeyPad page")
        }
    }
    
    private func recentPage() -> some View {
        NavigationLink(
            destination: RecentPageExample()
        ) {
            Text("Recent Page")
        }
    }
    
    private func countrySelection() -> some View {
        NavigationLink(
            destination: CountrySelectionContainer(
                props: CountrySelectionContainerProps(
                    onSelected: { (country) in
                        print(country)
                    }
                )
            )
        ) {
            Text("Country Selection")
        }
    }
    
    private func registration() -> some View {
        NavigationLink(
            destination: RegistrationContainer(
                props: RegistrationContainerProps()
            )
        ) {
            Text("Registration")
        }
    }
    
    private func confirmSimNumWithChatOtp() -> some View {
        NavigationLink(
            destination: ConfirmSimNumWithChatOtpContainer(
                props: ConfirmSimNumWithChatOtpContainerProps(
                    phoneNumber: "+11111234567"
                )
            )
        ) {
            Text("Confirm Sim Num With Chat Otp")
        }
    }
    
    private func confirmDidNumWithChatOtp() -> some View {
        NavigationLink(
            destination: ConfirmDidNumWithChatOtpContainer(
                props: ConfirmDidNumWithChatOtpContainerProps(
                    phoneNumber: "+11111234567"
                )
            )
        ) {
            Text("Confirm Did Num With Chat Otp")
        }
    }
    
    private func callKit() -> some View {
        Section(header: Text("Callkit")) {
            NavigationLink(
                destination: CallKitContainerSample(exampleType: .incomingAppToApp)
            ) {
                Text("Incoming App To App")
            }
            NavigationLink(
                destination: CallKitContainerSample(exampleType: .incomingPhoneToApp)
            ) {
                Text("Incoming Phone To App")
            }
            NavigationLink(
                destination: CallKitContainerSample(exampleType: .outgoingAppToApp)
            ) {
                Text("Outgoing App To App")
            }
            NavigationLink(
                destination: CallKitContainerSample(exampleType: .outgoingAppToPhone)
            ) {
                Text("Outgoing App To Phone")
            }
            NavigationLink(
                destination: CallKitContainerSample(exampleType: .hold)
            ) {
                Text("Hold")
            }
        }
    }
    
    private func myNumbers() -> some View {
        Section(header: Text("My Numbers")) {
            NavigationLink(
                destination: MyNumberSample(sampleType: .one)
            ) {
                Text("One number")
            }
            NavigationLink(
                destination: MyNumberSample(sampleType: .moreThanOne)
            ) {
                Text("More than one")
            }
        }
    }
    
    private func getLocalNumberOptions() -> some View {
        Section(header: Text("Get Local Number Options")) {
            NavigationLink(
                destination: GetLocalNumberOptionsViewSample(
                    sample: .newPlan
                )
            ) {
                Text("New Plan")
            }
            NavigationLink(
                destination: GetLocalNumberOptionsViewSample(sample: .existPlan)
            ) {
                Text("ExistPlan")
            }
            NavigationLink(
                destination: GetLocalNumberCountrySelectionContainerSample()
            ) {
                Text("Country Selection")
            }
        }
    }
    
    private func loginProfile() -> some View {
        Section(header: Text("Profile Info")) {
            NavigationLink(
                destination: LoginProfileContainerSample()
            ) {
                Text("Login")
            }
        }
    }

    private func shoppingCart() -> some View {
        Section(header: Text("Shopping Cart")) {
            NavigationLink(
                destination: GetLocalNumberShoppingCartContainerSample(sample: .newPlan)
            ) {
                Text("New")
            }
            NavigationLink(
                destination: GetLocalNumberShoppingCartContainerSample(sample: .existPlan)
            ) {
                Text("Existing")
            }
        }
    }
    
    private func contacts() -> some View {
        Section(header: Text("Contacts")) {
            NavigationLink(
                destination: ContactsPageContainerSample()
            ) {
                Text("Contacts")
            }
            
            NavigationLink(
                destination: blockedList()
            ) {
                Text("Blocked contacts")
            }
        }
    }
    
    
    
  
    
    private func numberSelection() -> some View {
        Section(header: Text("Number Selection")) {
            NavigationLink(
                destination: NumberSelectionContainer(
                    props: NumberSelectionContainerProps(
                        country: Country(
                            localizedName: "Afghanistan",
                            iso: "AF",
                            phoneCode: 93,
                            countryCode: 4,
                            flagImageName: "flag_AF"
                        ),
                        location: Location(
                            localizedName: "Mobile",
                            phoneCode: nil,
                            locationCode: nil,
                            previewImageUrl: ""),
                        plan: PBXPlan(type: .basic, phones: [])
                    )
                )
            ) {
                Text("Number Selection")
            }
        }
    }
    
    private func chatPage() -> some View {
        Section(header: Text("ChatPage")) {
            NavigationLink(
                destination: ChatsPageContainerExample()
            ) {
                Text("Chat Page")
            }
        }
    }
}

