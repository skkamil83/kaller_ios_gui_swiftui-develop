import SwiftUI

struct ChatsPageContainerExample: View {

    var body: some View {

        ChatsPageContainer(
                props: ChatsPageContainerProps(
                        onSelectDefaultNumber: {
                            print("onSelectDefaultPhoneNumber")
                        },
                        onSwitchEditMode: {_,_  in }
                )
        )
                .environmentObject(getUserInfoState())
                .environmentObject(getChatState())
                .environmentObject(getCountryState())
                .navigationBarHidden(true)

    }

    private func getUserInfoState() -> UserInfoState {
        let userInfoState = UserInfoState()
        userInfoState.accountInfo = AccountInfo(
                phones: [
                    PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                account: Account(id: 1, name: "Account name", createDate: Date()),
                devices: []
        )
        return userInfoState
    }

    private func getCountryState() -> CountryState {
        let countryState = CountryState()
        countryState.countries = CountryFetcher().countries
        return countryState
    }

    private func getChatState() -> ChatState {
        let chatState = ChatState()
        let archivedConversations = ArchivedConversations(
                isUnread: true,
                unreadCount: 1,
                isPinned: true,
                conversations: [
                    Conversation(id: "1", type: .dual, members: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil), .init(isDefault: false, userInfo: UserInfo(firstName: "Alex", lastName: "Sir", avatarImageUrl: nil), phoneNumber: "+33333333", nickName: nil, place: "Mobile", sortOrder: 0, phoneCode: 0, locationCode: 0, ext: nil)], userPhoneInfo: PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil), title: "aaaa", iconUrl: nil, messages: [.init(id: "1", message: "Hello", messageType: .text, messageStatus: .notSent, date: Date(), sendFromPhoneNumber: "+11111234567", phoneNumbersHaveRead: [])], isUnread: true, unreadCount: 5, dateOfPin: Date(), isMuted: false, createdDate: Date(timeIntervalSince1970: 1606802400))
                ])
        chatState.archivedConversations = archivedConversations

        let conversations: [Conversation] = [
            Conversation(
                    id: "1",
                    type: .dual,
                    members: [
                        PhoneInfo(
                                isDefault: true,
                                userInfo: UserInfo(
                                        firstName: "Vasya",
                                        lastName: "Pupkin",
                                        avatarImageUrl: ""),
                                phoneNumber: "+11111234567",
                                nickName: "@vpCanada",
                                place: "mobile",
                                sortOrder: 0,
                                phoneCode: 1,
                                locationCode: 111,
                                ext: nil),
                        PhoneInfo(
                                isDefault: false,
                                userInfo: UserInfo(
                                        firstName: "Alex",
                                        lastName: "Sir",
                                        avatarImageUrl: nil),
                                phoneNumber: "+33333333",
                                nickName: nil,
                                place: "Mobile",
                                sortOrder: 0,
                                phoneCode: 0,
                                locationCode: 0,
                                ext: nil)
                    ],
                    userPhoneInfo: PhoneInfo(
                            isDefault: true,
                            userInfo: UserInfo(
                                    firstName: "Vasya",
                                    lastName: "Pupkin",
                                    avatarImageUrl: ""),
                            phoneNumber: "+11111234567",
                            nickName: "@vpCanada",
                            place: "mobile",
                            sortOrder: 0,
                            phoneCode: 1,
                            locationCode: 111,
                            ext: nil),
                    title: "",
                    iconUrl: nil,
                    messages: [
                        MessageInfo(
                                id: "1",
                                message: "Hello",
                                messageType: .text,
                                messageStatus: .notSent,
                                date: Date(timeIntervalSince1970: 1607230800),
                                sendFromPhoneNumber: "+11111234567",
                                phoneNumbersHaveRead: []
                        )
                    ],
                    isUnread: true,
                    unreadCount: 5,
                    dateOfPin: nil,
                    isMuted: false,
                    createdDate: Date(timeIntervalSince1970: 1606802400)),
            Conversation(
                    id: "2",
                    type: .dual,
                    members: [
                        PhoneInfo(
                                isDefault: false,
                                userInfo: UserInfo(
                                        firstName: "Kevin",
                                        lastName: "Rocker",
                                        avatarImageUrl: ""),
                                phoneNumber: "+71111234567",
                                nickName: "@vpRus",
                                place: "mobile",
                                sortOrder: 1,
                                phoneCode: 7,
                                locationCode: 111,
                                ext: nil),
                        PhoneInfo(
                                isDefault: false,
                                userInfo: UserInfo(
                                        firstName: "Now",
                                        lastName: "Ok",
                                        avatarImageUrl: ""),
                                phoneNumber: "+787797868",
                                nickName: "@fffff",
                                place: "mobile",
                                sortOrder: 1,
                                phoneCode: 7,
                                locationCode: 111,
                                ext: nil)],
                    userPhoneInfo: PhoneInfo(
                            isDefault: false,
                            userInfo: UserInfo(
                                    firstName: "Kevin",
                                    lastName: "Rocker",
                                    avatarImageUrl: ""),
                            phoneNumber: "+71111234567",
                            nickName: "@vpRus",
                            place: "mobile",
                            sortOrder: 1,
                            phoneCode: 7,
                            locationCode: 111,
                            ext: nil),
                    title: "",
                    iconUrl: nil,
                    messages: [
                        MessageInfo(
                                id: "4",
                                message: "Lorem ipsum dolor sit amet, consectetur adipiscing elibf vfubhfdh fdfdb",
                                messageType: .location,
                                messageStatus: .seen,
                                date: Date(timeIntervalSince1970: 1606802400),
                                sendFromPhoneNumber: "+71111234567",
                                phoneNumbersHaveRead: ["+71111234567", "+787797868"])
                    ],
                    isUnread: true,
                    unreadCount: 10,
                    dateOfPin: Date(),
                    isMuted: false,
                    createdDate: Date(timeIntervalSince1970: 1606802400)),
            Conversation(
                    id: "3",
                    type: .dual,
                    members: [
                        PhoneInfo(
                                isDefault: true,
                                userInfo: UserInfo(
                                        firstName: "Vasya",
                                        lastName: "Pupkin",
                                        avatarImageUrl: ""),
                                phoneNumber: "+11111234567",
                                nickName: "@vpCanada",
                                place: "mobile",
                                sortOrder: 0,
                                phoneCode: 1,
                                locationCode: 111,
                                ext: nil),
                        PhoneInfo(
                                isDefault: false,
                                userInfo: UserInfo(
                                        firstName: "Man",
                                        lastName: "One",
                                        avatarImageUrl: "https://images.unsplash.com/photo-1563351672-62b74891a28a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=aejaz-memon-6erzQwfnCuo-unsplash.jpg&w=640"), phoneNumber: "+68686868",
                                nickName: nil,
                                place: "Mobile",
                                sortOrder: 0,
                                phoneCode: 0,
                                locationCode: 0,
                                ext: nil)],
                    userPhoneInfo: PhoneInfo(
                            isDefault: true,
                            userInfo: UserInfo(
                                    firstName: "Vasya",
                                    lastName: "Pupkin",
                                    avatarImageUrl: ""),
                            phoneNumber: "+11111234567",
                            nickName: "@vpCanada",
                            place: "mobile",
                            sortOrder: 0,
                            phoneCode: 1,
                            locationCode: 111,
                            ext: nil),
                    title: "1234",
                    iconUrl: nil,
                    messages: [
                        MessageInfo(
                                id: "8",
                                message: "jjj bbbb ccccc vvvvv rrrrrr oooo pppp kkkk mmmm",
                                messageType: .audio,
                                messageStatus: .deliverd,
                                date: Date(),
                                sendFromPhoneNumber: "+68686868",
                                phoneNumbersHaveRead: ["+71111234567", "+787797868"])
                    ],
                    isUnread: true,
                    unreadCount: 8,
                    dateOfPin: Date(timeIntervalSince1970: 1606802400),
                    isMuted: false,
                    createdDate: Date(timeIntervalSince1970: 1606802400)),
            Conversation(
                    id: "10",
                    type: .dual,
                    members: [
                        PhoneInfo(
                                isDefault: true,
                                userInfo: UserInfo(
                                        firstName: "Vasya",
                                        lastName: "Pupkin",
                                        avatarImageUrl: ""),
                                phoneNumber: "+11111234567",
                                nickName: "@vpCanada",
                                place: "mobile",
                                sortOrder: 0,
                                phoneCode: 1,
                                locationCode: 111,
                                ext: nil),
                        PhoneInfo(
                                isDefault: false,
                                userInfo: UserInfo(
                                        firstName: "David",
                                        lastName: "Now",
                                        avatarImageUrl: nil),
                                phoneNumber: "+33333333",
                                nickName: nil,
                                place: "Mobile",
                                sortOrder: 0,
                                phoneCode: 0,
                                locationCode: 0,
                                ext: nil)
                    ],
                    userPhoneInfo: PhoneInfo(
                            isDefault: true,
                            userInfo: UserInfo(
                                    firstName: "Vasya",
                                    lastName: "Pupkin",
                                    avatarImageUrl: ""),
                            phoneNumber: "+11111234567",
                            nickName: "@vpCanada",
                            place: "mobile",
                            sortOrder: 0,
                            phoneCode: 1,
                            locationCode: 111,
                            ext: nil),
                    title: "",
                    iconUrl: nil,
                    messages: [
                        MessageInfo(
                                id: "100",
                                message: "Hello, I'm David",
                                messageType: .video,
                                messageStatus: .notSent,
                                date: Date(),
                                sendFromPhoneNumber: "+33333333",
                                phoneNumbersHaveRead: []
                        )
                    ],
                    isUnread: false,
                    unreadCount: 0,
                    dateOfPin: nil,
                    isMuted: false,
                    createdDate: Date(timeIntervalSince1970: 1606802400)),
        ]
        chatState.conversations = conversations
        return chatState
    }
}
