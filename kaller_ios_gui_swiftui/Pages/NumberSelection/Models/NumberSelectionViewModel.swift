struct NumberSelectionInfo {
    let phoneNumber: String
    let voice: Bool
    let sms: Bool
    let fax: Bool
}
