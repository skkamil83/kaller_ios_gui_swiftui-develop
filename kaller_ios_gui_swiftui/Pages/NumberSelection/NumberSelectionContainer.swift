import SwiftUI

struct NumberSelectionContainerProps {
    let country: Country
    let location: Location
    let plan: PBXPlan
}

struct NumberSelectionContainer: View {
    let props: NumberSelectionContainerProps
    @State private var ableToShowMore = true
    @State private var phones: [NumberSelectionInfo] = []
    @State private var pushToShoppingCart = false
    @State private var phoneNumberSelected: String = ""

    var body: some View {
        NumberSelectionView(
                props: NumberSelectionViewProps(
                        location: props.location,
                        phones: phones,
                        ableToShowMore: ableToShowMore,
                        onShowMore: onShowMore,
                        onSelectedNumber: onSelectedNumber
                )
        )
                .onAppear() {
                    fetchPhoneNumbers()
                }
        shoppingCartLink(props: props)
    }

    private func onSelectedNumber(phone: NumberSelectionInfo) {
        phoneNumberSelected = phone.phoneNumber
        pushToShoppingCart.toggle()
    }

    private func fetchPhoneNumbers() {
        phones = [
            .init(phoneNumber: "+1 212 000 0001", voice: true, sms: true, fax: false),
            .init(phoneNumber: "+1 212 000 0002", voice: true, sms: false, fax: false),
            .init(phoneNumber: "+1 212 000 0003", voice: true, sms: true, fax: true),
            .init(phoneNumber: "+1 212 000 0004", voice: true, sms: true, fax: true),
            .init(phoneNumber: "+1 212 000 0005", voice: true, sms: true, fax: false),
            .init(phoneNumber: "+1 212 000 0006", voice: false, sms: true, fax: false),
            .init(phoneNumber: "+1 212 000 0007", voice: true, sms: false, fax: false),
            .init(phoneNumber: "+1 212 000 0008", voice: true, sms: true, fax: false),
            .init(phoneNumber: "+1 212 000 0009", voice: false, sms: true, fax: false),
            .init(phoneNumber: "+1 212 000 0010", voice: true, sms: true, fax: false),
            .init(phoneNumber: "+1 212 000 0011", voice: true, sms: false, fax: false),
            .init(phoneNumber: "+1 212 000 0012", voice: true, sms: true, fax: false),
            .init(phoneNumber: "+1 212 000 0013", voice: true, sms: true, fax: true),
        ]
    }

    private func onShowMore() {
        print(#function)
        if phones.count < 20 {
            phones += [
                .init(phoneNumber: "+1 212 000 0014", voice: true, sms: true, fax: false),
                .init(phoneNumber: "+1 212 000 0015", voice: true, sms: false, fax: false),
                .init(phoneNumber: "+1 212 000 0016", voice: true, sms: true, fax: true),
                .init(phoneNumber: "+1 212 000 0017", voice: true, sms: true, fax: true),
                .init(phoneNumber: "+1 212 000 0018", voice: true, sms: true, fax: false),
                .init(phoneNumber: "+1 212 000 0019", voice: false, sms: true, fax: false),
                .init(phoneNumber: "+1 212 000 0020", voice: true, sms: false, fax: false),
                .init(phoneNumber: "+1 212 000 0021", voice: true, sms: true, fax: false),
                .init(phoneNumber: "+1 212 000 0022", voice: false, sms: true, fax: false),
                .init(phoneNumber: "+1 212 000 0023", voice: true, sms: true, fax: false),
                .init(phoneNumber: "+1 212 000 0024", voice: true, sms: false, fax: false),
                .init(phoneNumber: "+1 212 000 0025", voice: true, sms: true, fax: false),
                .init(phoneNumber: "+1 212 000 0026", voice: true, sms: true, fax: true),
            ]
        }

        ableToShowMore = phones.count < 20
    }

    private func shoppingCartLink(props: NumberSelectionContainerProps) -> some View {
        NavigationLink(
                destination: GetLocalNumberShoppingCartContainer(
                        props: GetLocalNumberShoppingCartContainerProps(
                                country: props.country,
                                location: props.location,
                                phoneNumber: phoneNumberSelected,
                                plan: props.plan
                        )
                ),
                isActive: $pushToShoppingCart
        ) {
            EmptyView()
        }
    }
}
