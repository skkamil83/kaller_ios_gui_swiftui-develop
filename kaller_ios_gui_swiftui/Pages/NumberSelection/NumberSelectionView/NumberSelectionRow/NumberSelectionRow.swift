import SwiftUI

struct NumberSelectionRow: View {
    let props: NumberSelectionRowProps

    var body: some View {
        row(props: props)
    }

    private func row(props: NumberSelectionRowProps) -> some View {
        HStack(spacing: 0) {
            Text(props.viewModel.phoneNumber)
                    .textStyle(.body1Default1Light1LabelColor2CenterAligned)
                    .padding(.leading, Spacing.spacing16Pt)
            Spacer()
            Image(systemName: props.viewModel.voice ? "circle.fill" : "circle")
                    .textStyle(TextStyle(fontName: "SFProText-Regular", fontSize: 8, color: Color.black.opacity(0.87)))
                    .frame(width: UIScreen.main.bounds.width/6)
            Image(systemName: props.viewModel.sms ? "circle.fill" : "circle")
                    .textStyle(TextStyle(fontName: "SFProText-Regular", fontSize: 8, color: Color.black.opacity(0.87)))
                    .frame(width: UIScreen.main.bounds.width/6)
            Image(systemName: props.viewModel.fax ? "circle.fill" : "circle")
                    .textStyle(TextStyle(fontName: "SFProText-Regular", fontSize: 8, color: Color.black.opacity(0.87)))
                    .frame(width: UIScreen.main.bounds.width/6)
                    .padding(.trailing, Spacing.spacing16Pt)
        }
                .frame(height: 45)
                .contentShape(Rectangle())
                .onTapGesture {
                    props.onSelected()
                }
    }
}

struct NumberSelectionRow_Previews: PreviewProvider {
    static var previews: some View {
        NumberSelectionRow(
                props: NumberSelectionRowProps(
                        viewModel: NumberSelectionInfo(
                                phoneNumber: "+1 212 000 0001",
                                voice: true,
                                sms: false,
                                fax: true
                        ),
                        onSelected: {}
                )
        )
    }
}
