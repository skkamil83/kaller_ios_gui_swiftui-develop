struct NumberSelectionRowProps {
    let viewModel: NumberSelectionInfo
    let onSelected: () -> Void
}
