import SwiftUI

struct NumberSelectionView: View {
    let props: NumberSelectionViewProps

    var body: some View {
        list(props: props)
                .navigationTitle(navigationTitle(props: props))
                .navigationBarTitleDisplayMode(.inline)
    }

    private func navigationTitle(props: NumberSelectionViewProps) -> Text {
        Text("\(props.location.localizedName) (\(props.phones.count))")
    }

    private func list(props: NumberSelectionViewProps) -> some View {
        List {
            Section(header: selectNumber()) {
            }
            titlesSection(props: props)
        }
                .listStyle(PlainListStyle())
    }

    private func selectNumber() -> some View {
        VStack {
            Text("SELECT YOUR NUMBER")
                    .textStyle(.footnote1Default1Light6DarkGray)
                    .padding(EdgeInsets(top: 31, leading: Spacing.spacing16Pt, bottom: 0, trailing: 0))
        }
                .frame(width: UIScreen.main.bounds.width, height: 56, alignment: .leading)
                .background(Color.navigationBarBackgroundChrome)
    }

    private func titlesSection(props: NumberSelectionViewProps) -> some View {
        Section(header: titles()) {
            ForEach(0..<props.phones.count, id: \.self) { index in
                let phone = props.phones[index]
                NumberSelectionRow(
                        props: NumberSelectionRowProps(
                                viewModel: phone,
                                onSelected: {
                                    props.onSelectedNumber(phone)
                                }
                        )
                )
                        .listRowInsets(EdgeInsets())
            }
            if props.ableToShowMore {
                showMoreButton(props: props)
            }
        }
                .textCase(nil)
    }

    private func titles() -> some View {
        HStack(spacing: 0) {
            Text("Number")
                    .textStyle(.caption12Bold1Light1LabelColor)
                    .padding(.leading, Spacing.spacing16Pt)
            Spacer()
            Text("Voice")
                    .textStyle(.caption12Bold1Light1LabelColor)
                    .frame(width: UIScreen.main.bounds.width/6)
            Text("SMS")
                    .textStyle(.caption12Bold1Light1LabelColor)
                    .frame(width: UIScreen.main.bounds.width/6)
            Text("Fax")
                    .textStyle(.caption12Bold1Light1LabelColor)
                    .frame(width: UIScreen.main.bounds.width/6)
                    .padding(.trailing, Spacing.spacing16Pt)
        }
                .frame(width: UIScreen.main.bounds.width, height: 28, alignment: .leading)
                .background(Color.fillsLightSecondaryFill)
    }

    private func showMoreButton(props: NumberSelectionViewProps) -> some View {
        VStack(spacing: 0) {
            Text("Show More Numbers")
                    .textStyle(.body1Default1Light5Blue)
                    .padding(.leading, Spacing.spacing16Pt)
                    .frame(width: UIScreen.main.bounds.width, height: 44, alignment: .leading)
            Divider()
        }
                .listRowInsets(EdgeInsets(top: -1, leading: -1, bottom: -1, trailing: -1))
                .background(Color.white)
                .onTapGesture {
                    props.onShowMore()
                }
    }
}

struct NumberSelectionView_Previews: PreviewProvider {
    static var previews: some View {
        NumberSelectionView(
                props: NumberSelectionViewProps(
                        location: Location(
                                localizedName: "New York",
                                phoneCode: nil,
                                locationCode: nil,
                                previewImageUrl: ""
                        ),
                        phones: [
                            NumberSelectionInfo(
                                    phoneNumber: "+1 212 000 0001",
                                    voice: true,
                                    sms: false,
                                    fax: true
                            )
                        ],
                        ableToShowMore: true,
                        onShowMore: {},
                        onSelectedNumber: { _ in }
                )
        )
    }
}
