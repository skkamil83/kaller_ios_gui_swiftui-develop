struct NumberSelectionViewProps {
    let location: Location
    let phones: [NumberSelectionInfo]
    let ableToShowMore: Bool
    let onShowMore: () -> Void
    let onSelectedNumber: (NumberSelectionInfo) -> Void
}
