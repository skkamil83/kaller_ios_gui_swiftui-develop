import SwiftUI

struct ContactsPageContainerSample: View {

    var body: some View {

        ContactsPageContainer(
                props: ContactsPageContainerProps(
                        onSelectDefaultPhoneNumber: {
                            print("onSelectDefaultPhoneNumber")
                        }
                )
        )
                .environmentObject(getUserInfoState())
                .environmentObject(getContactsState())
                .environmentObject(getCountryState())
                .navigationBarHidden(true)
    }

    private func getCountryState() -> CountryState {
        let countryState = CountryState()
        countryState.countries = CountryFetcher().countries
        return countryState
    }

    private func getUserInfoState() -> UserInfoState {
        let userInfoState = UserInfoState()
        userInfoState.accountInfo = AccountInfo(
                phones: [
                    PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                account: Account(id: 1, name: "Account name", createDate: Date()),
                devices: []
        )
        return userInfoState
    }

    private func getContactsState() -> ContactsState {
        let contactsState = ContactsState()
        contactsState.addressBooks = [
            AppConstant.localAddressBookId: [
                Contact(firstName: "Richard", lastName: "Miles", company: nil, avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Richard", lastName: "Miles", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@richard", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)]),
                Contact(firstName: "Alex", lastName: "Pro", company: nil, avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Alex", lastName: "Pro", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@alex", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)]),
                Contact(firstName: "Eugene", lastName: "Sir", company: nil, avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Eugene", lastName: "Sir", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@eugene", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)])
            ],
            "+11111234567": [
                Contact(firstName: "Khoa", lastName: "Oi", company: "ok_Company", avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Khoa", lastName: "Oi", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@khoa", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)]),
                Contact(firstName: "Kazan", lastName: "CS", company: nil, avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Kazan", lastName: "CS", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@kazan", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)])
            ],
            "+71111234567": [
                Contact(firstName: "Tada", lastName: "No", company: nil, avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "no_company", lastName: "Oi", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@khoa", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)]),
                Contact(firstName: "Mama", lastName: nil, company: nil, avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Kazan", lastName: "CS", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@kazan", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)])
            ],
            "+21111234567": [
                Contact(firstName: "Nano", lastName: "", company: nil, avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "no_company", lastName: "Oi", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@khoa", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)]),
                Contact(firstName: "Micro", lastName: "phone", company: "Microphone Company", avatarUrl: nil,
                        phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Kazan", lastName: "CS", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678", nickName: "@kazan", place: "Mobile", sortOrder: 1, phoneCode: 44, locationCode: 44, ext: nil)])
            ]
        ]

        return contactsState
    }
}
