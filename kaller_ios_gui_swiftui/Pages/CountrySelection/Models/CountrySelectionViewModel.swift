//
//  CountrySelectionViewModel.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 12/12/2020.
//

import Foundation

struct CountrySelectionViewModel {
    let country: Country
    let searchIndex: String
    
    init(country: Country) {
        self.country = country
        if let phoneCode = country.phoneCode{
            self.searchIndex = "\(country.localizedName.lowercased())+\(phoneCode)"
        }else{
            self.searchIndex = "\(country.localizedName.lowercased())"
        }
    }
}
