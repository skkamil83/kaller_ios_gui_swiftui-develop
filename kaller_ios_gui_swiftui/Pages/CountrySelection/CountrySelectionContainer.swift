//
//  CountrySelectionContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 11/12/2020.
//

import SwiftUI

struct CountrySelectionContainer: View {
    let props: CountrySelectionContainerProps
    @State private var countries: [CountrySelectionViewModel]
    
    init(props: CountrySelectionContainerProps) {
        self.props = props
        self._countries = .init(wrappedValue: CountryFetcher().countries.filter({$0.phoneCode != nil}).map(CountrySelectionViewModel.init))
    }
    
    var body: some View {
        CountrySelectionView(
            props: CountrySelectionViewProps(
                countries: countries,
                onSelection: props.onSelected
            )
        )
    }
}

struct CountrySelectionContainer_Previews: PreviewProvider {
    static var previews: some View {
        CountrySelectionContainer(
            props: CountrySelectionContainerProps(
                onSelected: {_ in}
            )
        )
    }
}
