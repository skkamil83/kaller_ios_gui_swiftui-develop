//
//  CountrySelectionContainerProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 11/12/2020.
//

import SwiftUI

struct CountrySelectionContainerProps{
    let onSelected: (Country) -> Void
}
