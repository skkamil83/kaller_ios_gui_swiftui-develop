//
//  CountrySelectionView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import SwiftUI

struct CountrySelectionView: View {
    @Environment(\.presentationMode) var presentationMode
    let props: CountrySelectionViewProps
    @State private var searchText = ""
    @State private var isShowTitle = true
    
    var body: some View {
        VStack(spacing: 0){
            header()
            list(props: props)
        }
    }
    
    private func list(props: CountrySelectionViewProps) -> some View{
        ScrollView{
            LazyVStack(spacing: 0){
                let countries = CountrySelectionViewHelper.countriesFiltered(countries: props.countries, searchText: searchText)
                ForEach(countries, id: \.searchIndex){ country in
                    CountrySelectionRow(
                        props: CountrySelectionRowProps(
                            country: country.country
                        )
                    )
                    .onTapGesture {
                        props.onSelection(country.country)
                        presentationMode.wrappedValue.dismiss()
                    }
                }
            }
        }
    }
    
    private func searchView() -> some View{
        SearchBarView(
            props: SearchBarViewProps(
                query: $searchText,
                placeHolder: "Search",
                onFocus: { onFocus in
                    isShowTitle = !onFocus
                }
            )
        )
        .frame(height: 36)
        .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))
    }
    
    private func titleView () -> some View{
        ZStack{
            HStack(spacing: 0){
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }){
                    Text("Cancel")
                        .font(TextStyle.body3Button1Light5Blue.font)
                        .foregroundColor(TextStyle.body3Button1Light5Blue.color)
                }
                .padding(.leading, 17)
                Spacer()
            }
            Text("Select Country")
                .font(TextStyle.body2Bold1Light1LabelColor.font)
                .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
        }
    }
    
    private func header() -> some View{
        VStack(spacing: 0){
            if isShowTitle{
                titleView()
                    .padding(.top, Spacing.spacing16Pt)
            }
            searchView()
                .padding(EdgeInsets(top: Spacing.spacing16Pt, leading: 0, bottom: Spacing.spacing16Pt, trailing: 0))
            Divider()
        }
        .background(Color.navigationBarBackgroundChrome)
    }
}

struct CountrySelectionView_Previews: PreviewProvider {
    static var previews: some View {
        CountrySelectionView(
            props: CountrySelectionViewProps(
                countries: [
                    CountrySelectionViewModel(
                        country: Country(
                            localizedName: "Afghanistan",
                            iso: "AF",
                            phoneCode: 93,
                            countryCode: 4,
                            flagImageName: "flag_AF"
                        )
                    ),
                    CountrySelectionViewModel(
                        country:  Country(
                            localizedName: "Afghanistan",
                            iso: "AF",
                            phoneCode: 93,
                            countryCode: 4,
                            flagImageName: "flag_AF"
                        )
                    )
                ],
                onSelection: {_ in}
            )
        )
    }
}
