//
//  CountrySelectionViewHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 11/12/2020.
//

import Foundation

class CountrySelectionViewHelper{
    static func countriesFiltered(countries: [CountrySelectionViewModel], searchText: String) -> [CountrySelectionViewModel]{
        if searchText.isEmpty{
            return countries
        }
        let searchString = searchText.lowercased().trimmingCharacters(in: .whitespaces)
        return countries.filter({$0.searchIndex.contains(searchString)})
    }
}
