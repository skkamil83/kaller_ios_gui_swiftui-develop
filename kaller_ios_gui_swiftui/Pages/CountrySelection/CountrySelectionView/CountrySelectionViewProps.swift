//
//  CountrySelectionViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

struct CountrySelectionViewProps{
    let countries: [CountrySelectionViewModel]
    let onSelection: (Country) -> Void
}
