//
//  CountrySelectionRowProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import Foundation

struct CountrySelectionRowProps{
    let country: Country
}
