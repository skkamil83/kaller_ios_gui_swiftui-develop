//
//  CountrySelectionRow.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 10/12/2020.
//

import SwiftUI

struct CountrySelectionRow: View {
    let props: CountrySelectionRowProps
    
    var body: some View {
        VStack(spacing: 0){
            Spacer()
            HStack(spacing: 0){
                flag(props: props)
                countryName(props: props)
                phoneCode(props: props)
                Spacer()
            }
            Spacer()
            Divider()
                .padding(.leading, 16)
        }
        .background(Color.white)
        .frame(width: UIScreen.main.bounds.width, height: 45)
    }
    
    private func flag(props: CountrySelectionRowProps) -> some View{
        CircleIcon(
            props: CircleIconProps(
                size: 21,
                style: .filled(imageName: props.country.flagImageName)
            )
        )
        .padding(.leading, 16)
    }
    
    private func countryName(props: CountrySelectionRowProps) -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return Text(props.country.localizedName)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.leading, 8)
    }
    
    private func phoneCode(props: CountrySelectionRowProps) -> some View{
        return Text(CountrySelectionRowHelper.getPhoneCode(country: props.country))
            .font(TextStyle.body1Default1Light2SecondaryLabelColor.font)
            .foregroundColor(TextStyle.body1Default1Light2SecondaryLabelColor.color)
            .padding(.leading, 8)
    }
}

struct CountrySelectionRow_Previews: PreviewProvider {
    static var previews: some View {
        CountrySelectionRow(
            props: CountrySelectionRowProps(
                country: Country(
                    localizedName: "Afghanistan",
                    iso: "AF",
                    phoneCode: 93,
                    countryCode: 4,
                    flagImageName: "flag_AF"
                )
            )
        )
    }
}
