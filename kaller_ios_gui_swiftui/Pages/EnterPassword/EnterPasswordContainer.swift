//
//  EnterPasswordContainer.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import SwiftUI

struct EnterPasswordContainerProps{
    let email: String
}

struct EnterPasswordContainer: View {
    let props: EnterPasswordContainerProps
    @State private var password = ""
    @State private var isShowIncorrectPassAlert = false
    
    var body: some View {
        EnterPasswordView(
            props: EnterPasswordViewProps(
                email: props.email,
                onForgotPassword: onForgotPassword,
                onPasswordChanged: {password in
                    self.password = password
                },
                onNext: onNext
            )
        )
        .alert(isPresented: $isShowIncorrectPassAlert, content: {
            alertinCorrectPassword()
        })
    }
    
    private func onForgotPassword(){
        print(#function)
    }
    
    private func onNext(){
        isShowIncorrectPassAlert = true
    }
    
    private func alertinCorrectPassword() -> Alert{
        Alert(
            title: Text("You entered an incorrect email, password, or both."),
            message: nil,
            dismissButton: .default(Text("Try Again")
            )
        )
    }
}

struct EnterPasswordContainer_Previews: PreviewProvider {
    static var previews: some View {
        EnterPasswordContainer(
            props: EnterPasswordContainerProps(
                email: "someone@example.com"
            )
        )
    }
}
