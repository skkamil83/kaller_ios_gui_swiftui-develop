//
//  EnterPasswordView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import SwiftUI

struct EnterPasswordView: View {
    let props:  EnterPasswordViewProps
    @State private var password = ""
    @State private var isShowPassword = false
    
    var body: some View {
        VStack(spacing: 0){
            enterPassWordText()
            desscriptionText()
            textFieldPassword()
            Spacer()
                .frame(maxHeight: 84)
            forgotPassword(props: props)
            Spacer()
        }
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                emailToolbar(props: props)
            }
        })
        .navigationBarItems(
            trailing: nextButton(props: props)
        )
        .navigationBarTitleDisplayMode(.large)
    }
    
    private func enterPassWordText() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 28, color: .black)
        return Text("Enter password")
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .padding(.top, -28)
    }
    
    private func desscriptionText() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return Text("The password is used by extensions or admin to access the phone number.")
            .frame(height: 44)
            .font(textStyle.font)
            .foregroundColor(textStyle.color)
            .multilineTextAlignment(.center)
            .lineLimit(2)
            .padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
    }
    
    private func textFieldPassword() -> some View{
        let textStyle = TextStyle(fontName: "SFProDisplay-Regular", fontSize: 20, color: .black)
        return
            VStack(spacing: 0){
                Spacer()
                HStack(spacing: 0){
                    Spacer()
                        .frame(width: 42.4)
                    
                    if isShowPassword{
                        TextField("Password", text: $password)
                            .multilineTextAlignment(.center)
                            .font(textStyle.font)
                            .foregroundColor(textStyle.color)
                            .disableAutocorrection(true)
                    }else{
                        SecureField("Password", text: $password)
                            .multilineTextAlignment(.center)
                            .font(textStyle.font)
                            .foregroundColor(textStyle.color)
                    }
                    
                    Button(action: {
                        isShowPassword.toggle()
                    }){
                        Image(systemName: isShowPassword ? "eye" : "eye.slash")
                            .font(TextStyle.body1Default1Light2SecondaryLabelColor.font)
                            .foregroundColor(TextStyle.body1Default1Light2SecondaryLabelColor.color)
                    }
                    .frame(width: 26)
                    .padding(.trailing, 16.4)
                }
                Spacer()
                Divider()
            }
        .frame(height: 44)
        .padding(EdgeInsets(top: 79, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func forgotPassword(props: EnterPasswordViewProps) -> some View{
        Button(action: props.onForgotPassword){
            Text("Forgot password?")
                .font(TextStyle.subheadline1Default1Light5Blue.font)
                .foregroundColor(TextStyle.subheadline1Default1Light5Blue.color)
        }
    }
    
    private func emailToolbar(props: EnterPasswordViewProps) -> some View{
        Text(props.email)
            .font(TextStyle.body2Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
    }
    
    private func nextButton(props: EnterPasswordViewProps) ->  some View{
        let grayTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.tintsDisabledLight)
        let blueTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.primaryBlue)
        let ableToNext = !password.isEmpty
        
        return Button(action: props.onNext){
            Text("Next")
                .font(ableToNext ? blueTextStyle.font : grayTextStyle.font)
                .foregroundColor(ableToNext ? blueTextStyle.color : grayTextStyle.color)
        }
        .disabled(!ableToNext)
    }
}

struct EnterPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        EnterPasswordView(
            props: EnterPasswordViewProps(
                email: "someone@example.com",
                onForgotPassword: {},
                onPasswordChanged: {_ in},
                onNext: {}
            )
        )
    }
}
