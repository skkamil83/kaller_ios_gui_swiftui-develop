//
//  EnterPasswordViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 14/12/2020.
//

import SwiftUI

struct EnterPasswordViewProps{
    let email: String
    let onForgotPassword: () -> Void
    let onPasswordChanged: (String) -> Void
    let onNext: () -> Void
}
