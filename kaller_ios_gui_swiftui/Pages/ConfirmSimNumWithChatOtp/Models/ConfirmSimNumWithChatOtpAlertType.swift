//
//  ConfirmOtpOldUserAlertType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

enum ConfirmSimNumWithChatOtpAlertType: Identifiable{
    var id: String{
        switch self {
        case .incorrectCode:
            return "incorrectCode"
        case .expiredCode:
            return "expiredCode"
        }
    }

    case incorrectCode
    case expiredCode
}
