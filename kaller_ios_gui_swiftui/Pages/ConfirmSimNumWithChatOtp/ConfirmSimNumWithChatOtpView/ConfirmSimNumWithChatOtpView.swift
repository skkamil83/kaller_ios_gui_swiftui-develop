//
//  ConfirmOTPOldUserView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

import SwiftUI

struct ConfirmSimNumWithChatOtpView: View {
    let props: ConfirmSimNumWithChatOtpViewProps
    @State private var code = ""
    
    var body: some View {
        VStack(spacing: 0){
            checkKallermessagesText()
            descriptionText()
            textFieldCode()
            Spacer()
                .frame(maxHeight: 84)
            sendCodeViaSMSView(props: props)
            Spacer()
        }
        .navigationBarTitleDisplayMode(.large)
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                phoneNumberToolbar(props: props)
            }
        })
        .navigationBarItems(
            trailing: nextButton(props: props)
        )
    }
    
    private func checkKallermessagesText() -> some View{
        return Text("Check Kaller messages")
            .font(TextStyle.title11Default1Light1LabelColor2CenterAligned.font)
            .foregroundColor(TextStyle.title11Default1Light1LabelColor2CenterAligned.color)
            .padding(.top, -28)
    }
    
    private func descriptionText() -> some View{
        return Text("We have sent a message with a code to Kaller Channel on your other device.")
            .frame(height: 44)
            .font(TextStyle.body1Default1Light1LabelColor2CenterAligned.font)
            .foregroundColor(TextStyle.body1Default1Light1LabelColor2CenterAligned.color)
            .multilineTextAlignment(.center)
            .lineLimit(2)
            .padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
    }
    
    private func textFieldCode() -> some View{
        return VStack(spacing: 0){
            Spacer()
            TextField("Code", text: $code)
                .font(TextStyle.title31Default1Light1LabelColor.font)
                .foregroundColor(TextStyle.title31Default1Light1LabelColor.color)
                .textContentType(.oneTimeCode)
                .keyboardType(.numberPad)
                .multilineTextAlignment(.center)
            Spacer()
            Divider()
        }
        .frame(height: 44)
        .padding(EdgeInsets(top: 79, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func phoneNumberToolbar(props: ConfirmSimNumWithChatOtpViewProps) -> some View{
        Text(props.phoneNumber)
            .font(TextStyle.body2Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
    }
    
    private func nextButton(props: ConfirmSimNumWithChatOtpViewProps) ->  some View{
        let textStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: .primaryBlue)
        let isCodeValid = !code.isEmpty
        let isInProgress = props.verificationResult == .inProgress
        
        return Button(action: {
            props.onNext(code)
        }){
            Text("Next")
                .font(textStyle.font)
        }
        .disabled(!isCodeValid || isInProgress)
    }
    
    private func sendCodeViaSMSView(props: ConfirmSimNumWithChatOtpViewProps) -> some View{
        NavigationLink(
            destination: VerifyPhoneNumberNewUserContainer(
                props: VerifyPhoneNumberNewUserContainerProps(
                    phoneNumber: props.phoneNumber
                )
            )
        ) {
            Text("Send the code via SMS")
                .font(TextStyle.subheadline1Default1Light5Blue.font)
                .foregroundColor(TextStyle.subheadline1Default1Light5Blue.color)
        }
    }
}

struct ConfirmOTPOldUserView_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmSimNumWithChatOtpView(
            props: ConfirmSimNumWithChatOtpViewProps(
                phoneNumber: "+972 50 999 9999",
                onNext: {_ in},
                verificationResult: .notVerified
            )
        )
    }
}
