//
//  ConfirmOTPOldUserViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

import Foundation

struct ConfirmSimNumWithChatOtpViewProps{
    let phoneNumber: String
    let onNext: (_ code: String) -> Void
    let verificationResult: AuthVerificationResult
}
