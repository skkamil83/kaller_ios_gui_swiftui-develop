import SwiftUI

struct ConfirmSimNumWithChatOtpContainerProps{
    let phoneNumber: String
}

struct ConfirmSimNumWithChatOtpContainer: View {
    @State private var alert: ConfirmSimNumWithChatOtpAlertType?
    let props: ConfirmSimNumWithChatOtpContainerProps
    
    var body: some View {
        ConfirmSimNumWithChatOtpView(
            props: ConfirmSimNumWithChatOtpViewProps(
                phoneNumber: props.phoneNumber,
                onNext: onNext(code:),
                verificationResult: .notVerified
            )
        )
        .alert(item: $alert) { item in alertView(type: item)}
    }
    
    private func onNext(code: String){
        alert = .incorrectCode
    }
    
    private func alertView(type: ConfirmSimNumWithChatOtpAlertType) -> Alert{
        switch type{
        case .incorrectCode:
            return alertIncorrectCode()
        case .expiredCode:
            return alertExpiredCode()
        }
    }
    
    private func alertIncorrectCode() -> Alert{
        Alert(
            title: Text("You have entered an invalid code. Please try again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func alertExpiredCode() -> Alert{
        Alert(
            title: Text("The code has expired. Please login again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
}
