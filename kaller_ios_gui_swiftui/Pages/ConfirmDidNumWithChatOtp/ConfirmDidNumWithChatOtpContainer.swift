import SwiftUI

struct ConfirmDidNumWithChatOtpContainerProps{
    let phoneNumber: String
}

struct ConfirmDidNumWithChatOtpContainer: View {
    let props: ConfirmDidNumWithChatOtpContainerProps
    @State private var alert: ConfirmDidNumWithChatOtpAlertType?
    
    var body: some View {
        ConfirmDidNumWithChatOtpView(
            props: ConfirmDidNumWithChatOtpViewProps(
                phoneNumber: props.phoneNumber,
                onNext: onNext(code:),
                verificationResult: .notVerified
            )
        )
        .alert(item: $alert) { item in alertView(type: item)}
    }
    
    private func onNext(code: String){
        alert = .incorrectCode
    }
    
    private func alertView(type: ConfirmDidNumWithChatOtpAlertType) -> Alert{
        switch type{
        case .incorrectCode:
            return alertIncorrectCode()
        case .expiredCode:
            return alertExpiredCode()
        }
    }
    
    private func alertIncorrectCode() -> Alert{
        Alert(
            title: Text("You have entered an invalid code. Please try again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func alertExpiredCode() -> Alert{
        Alert(
            title: Text("The code has expired. Please login again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
}
