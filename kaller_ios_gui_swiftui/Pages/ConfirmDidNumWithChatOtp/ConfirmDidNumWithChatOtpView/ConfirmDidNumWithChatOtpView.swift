import SwiftUI

struct ConfirmDidNumWithChatOtpView: View {
    let props: ConfirmDidNumWithChatOtpViewProps
    @State private var code = ""
    
    var body: some View {
        VStack(spacing: 0){
            checkKallermessagesText()
            descriptionText()
            textFieldCode()
            Spacer()
                .frame(maxHeight: 84)
            loginWithPasswordView(props: props)
            Spacer()
        }
        .navigationBarTitleDisplayMode(.large)
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                phoneNumberToolbar(props: props)
            }
        })
        .navigationBarItems(
            trailing: nextButton(props: props)
        )
    }
    
    private func checkKallermessagesText() -> some View{
        return Text("Check Kaller messages")
            .font(TextStyle.title11Default1Light1LabelColor2CenterAligned.font)
            .foregroundColor(TextStyle.title11Default1Light1LabelColor2CenterAligned.color)
            .padding(.top, -28)
    }
    
    private func descriptionText() -> some View{
        return Text("We have sent a message with a code to Kaller Channel on your other device.")
            .frame(height: 44)
            .font(TextStyle.body1Default1Light1LabelColor2CenterAligned.font)
            .foregroundColor(TextStyle.body1Default1Light1LabelColor2CenterAligned.color)
            .multilineTextAlignment(.center)
            .lineLimit(2)
            .padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
    }
    
    private func textFieldCode() -> some View{
        return VStack(spacing: 0){
            Spacer()
            TextField("Code", text: $code)
                .multilineTextAlignment(.center)
                .font(TextStyle.title31Default1Light1LabelColor.font)
                .foregroundColor(TextStyle.title31Default1Light1LabelColor.color)
                .textContentType(.oneTimeCode)
                .keyboardType(.numberPad)
            Spacer()
            Divider()
        }
        .frame(height: 44)
        .padding(EdgeInsets(top: 79, leading: 32, bottom: 0, trailing: 32))
    }
    
    private func phoneNumberToolbar(props: ConfirmDidNumWithChatOtpViewProps) -> some View{
        Text(props.phoneNumber)
            .font(TextStyle.body2Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
    }
    
    private func nextButton(props: ConfirmDidNumWithChatOtpViewProps) ->  some View{
        let textStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: .primaryBlue)
        let isCodeValid = !code.isEmpty
        let isInProgress = props.verificationResult == .inProgress
        
        return Button(action: {
            props.onNext(code)
        }){
            Text("Next")
                .font(textStyle.font)
        }
        .disabled(!isCodeValid || isInProgress)
    }
    
    private func loginWithPasswordView(props: ConfirmDidNumWithChatOtpViewProps) -> some View{
        NavigationLink(
            destination: ConfirmDidNumWithPasswordContainer(
                props: ConfirmDidNumWithPasswordContainerProps(
                    phoneNumber: props.phoneNumber
                )
            )
        ) {
            Text("Login with password")
                .font(TextStyle.subheadline1Default1Light5Blue.font)
                .foregroundColor(TextStyle.subheadline1Default1Light5Blue.color)
        }
    }
}
