struct ConfirmDidNumWithChatOtpViewProps{
    let phoneNumber: String
    let onNext: (_ code: String) -> Void
    let verificationResult: AuthVerificationResult
}
