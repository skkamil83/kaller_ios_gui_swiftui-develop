
enum ConfirmDidNumWithChatOtpAlertType: Identifiable{
    var id: String{
        switch self {
        case .incorrectCode:
            return "incorrectCode"
        case .expiredCode:
            return "expiredCode"
        }
    }

    case incorrectCode
    case expiredCode
}
