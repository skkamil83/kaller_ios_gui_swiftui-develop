import SwiftUI

struct ForgotPasswordContainerProps{
    let phoneNumber: String
}

struct ConfirmEmailWithOtpContainer: View {
    @State private var alert: ConfirmEmailWithOtpAlertType?
    @State private var didResendCode = false
    @State private var waitingTimeCountDown = 0
    @State private var openSetNewPassword = false
    
    let props: ForgotPasswordContainerProps
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    let timeToWait: Int = 180
    
    var body: some View {
        ConfirmEmailWithOtpView(
            props: ConfirmEmailWithOtpViewProps(
                phoneNumber: props.phoneNumber,
                waitingTime: waitingTimeCountDown,
                verificationResult: .notVerified,
                onNext: onNext(password:),
                sendCodeAgain: sendCodeAgain
            )
        )
        .alert(item: $alert) { item in alertView(type: item)}
        .onReceive(timer){ _ in onTimerTick()}
        setNewPasswordLink(props: props)
    }
    
    private func onNext(password: String){
        openSetNewPassword.toggle()
//        onWrongCode()
    }
    
    private func onWrongCode(){
        alert = .inCorrectCode
    }
    
    private func sendCodeAgain(){
        if(didResendCode){
            waitingTimeCountDown = timeToWait
            alert = .waitForFewMinutes
            didResendCode = false
        }else{
            alert = .resendCode
            didResendCode = true
        }
    }
    
    private func onTimerTick() {
        if(waitingTimeCountDown > 0){
            waitingTimeCountDown -= 1
        }
    }
    
    private func setNewPasswordLink(props: ForgotPasswordContainerProps) -> some View{
        NavigationLink(
            destination: SetNewPasswordContainer(
                props: SetNewPasswordContainerProps(
                    phoneNumber: props.phoneNumber
                )
            ),
            isActive: $openSetNewPassword
        ) {
            EmptyView()
        }
    }
    
    private func alertView(type: ConfirmEmailWithOtpAlertType) -> Alert{
        switch type{
        case .resendCode:
            return alertResendCode()
        case .waitForFewMinutes:
            return alertWaitForFewMinutes()
        case .inCorrectCode:
            return alertIncorrectCode()
        }
    }
    
    private func alertResendCode() -> Alert{
        Alert(
            title: Text("Recovery code resent."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func alertWaitForFewMinutes() -> Alert{
        Alert(
            title: Text("Please wait a few minutes to receive your code before trying again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
    
    private func alertIncorrectCode() -> Alert{
        Alert(
            title: Text("You have entered an invalid code. Please try again."),
            message: nil,
            dismissButton: .default(Text("OK"))
        )
    }
}

struct ForgotPasswordContainer_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmEmailWithOtpContainer(
            props: ForgotPasswordContainerProps(
                phoneNumber: "+972 50 999 9999"
            )
        )
    }
}
