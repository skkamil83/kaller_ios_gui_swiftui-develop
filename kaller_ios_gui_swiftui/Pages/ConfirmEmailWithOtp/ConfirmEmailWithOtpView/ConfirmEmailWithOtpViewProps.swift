//
//  ForgotPasswordViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

import Foundation

struct ConfirmEmailWithOtpViewProps{
    let phoneNumber: String
    let waitingTime: Int
    let verificationResult: AuthVerificationResult
    let onNext: (_ code: String) -> Void
    let sendCodeAgain: () -> Void
}
