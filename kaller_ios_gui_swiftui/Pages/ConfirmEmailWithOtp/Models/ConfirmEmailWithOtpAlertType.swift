//
//  ForgotPasswordAlertType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 22/12/2020.
//

import Foundation

enum ConfirmEmailWithOtpAlertType: Identifiable{
    var id: String{
        switch self {
        case .resendCode:
            return "resendCode"
        case .waitForFewMinutes:
            return "waitForFewMinutes"
        case .inCorrectCode:
            return "inCorrectCode"
        }
    }
    
    case resendCode
    case waitForFewMinutes
    case inCorrectCode
}
