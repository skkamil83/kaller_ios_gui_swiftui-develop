import SwiftUI

struct ContentView: View {
    @State private var openApp = false
    private let appStore = AppStore(
            applicationState: ApplicationState(),
            userInfoState: UserInfoState(),
            contactsState: ContactsState(),
            recentsState: RecentsState(),
            countryState: CountryState(),
            pbxPlanState: PBXPlanState(),
            chatState: ChatState()
    )

    var body: some View {
        if openApp {
            ContentContainer(
                    appStore: appStore,
                    appState: appStore.applicationState
            )
        } else {
            sandboxContainer()
                    .environmentObject(UserInfoState())
                    .environmentObject(ContactsState())
                    .environmentObject(RecentsState())
                    .environmentObject(ContactsState())
                    .environmentObject(PBXPlanState())
                    .environmentObject(ChatState())
        }
    }

    private func sandboxContainer() -> some View {
        TabView {
            ComponentsContainer()
                    .tabItem { Image(systemName: "1.square.fill"); Text("Components") }
                    .tag(0)

            StylesContainer()
                    .tabItem { Image(systemName: "2.square.fill"); Text("Styles") }
                    .tag(1)

            PagesContainer(props: PageContainerProps(
                    onOpenApp: { openApp.toggle() }
            ))
                    .tabItem { Image(systemName: "3.square.fill"); Text("Pages") }
                    .tag(2)
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
