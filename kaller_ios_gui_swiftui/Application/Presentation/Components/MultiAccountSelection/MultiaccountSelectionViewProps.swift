//
//  MultiaccountButtonProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 19/11/2020.
//

import Foundation

struct MultiaccountSelectionViewProps{
    let style: MultiaccountSelectionStyle
    let accountInfo: AccountInfo
    let onOutSideTap: () -> Void
    let onButtonCallTap: () -> Void
    let onSelectedCallingContact: (PhoneInfo?) -> Void
    let onSelectedMessageContact: (PhoneInfo?) -> Void
    let onSelectedDefalutNumber: () -> Void
}
