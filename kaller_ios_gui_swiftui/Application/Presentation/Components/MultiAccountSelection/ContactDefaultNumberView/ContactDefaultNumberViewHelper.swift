//
//  ContactDefaultNumberViewHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 17/12/2020.
//

import Foundation

class ContactDefaultNumberViewHelper{
    static func getAvatarStyleOfDefaultNumber(accountInfo: AccountInfo) -> AvatarViewProps.Style{
        guard let defaultPhoneInfo = accountInfo.defaultPhone else{
            return .image(imageName: "avatarUnknown")
        }
        if defaultPhoneInfo.userInfo.avatarImageUrl?.isEmpty ?? true{
            return .name(
                firstName: defaultPhoneInfo.userInfo.firstName ?? "",
                lastName: defaultPhoneInfo.userInfo.lastName ?? "",
                company: "")
        }else{
            return .bitmapImage(
                urlString: defaultPhoneInfo.userInfo.avatarImageUrl!)
        }
    }
}
