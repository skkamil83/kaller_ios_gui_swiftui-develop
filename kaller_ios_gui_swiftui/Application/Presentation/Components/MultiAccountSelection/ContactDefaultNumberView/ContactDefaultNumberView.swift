//
//  ContactDefaultNumberView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 20/11/2020.
//

import SwiftUI

struct ContactDefaultNumberView: View {
    let props: ContactDefaultNumberViewProps
    
    var body: some View {
        HStack(spacing: 0){
            ZStack{
                avatar(props: props)
                HStack{
                    Spacer()
                    VStack{
                        Spacer()
                        SimIcon(props: SimIconProps(number: getSimNumber(props: props), size: CGSize(width: 14, height: 18)))
                            .padding(EdgeInsets(top: 0, leading: 0, bottom: -3, trailing: -3))
                    }

                }
            }
            .frame(width: 44, height: 44)
            .padding(Spacing.spacing16Pt)
            VStack(alignment: .leading){
                Spacer()
                Text("MY DEFAULT NUMBER")
                    .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                    .foregroundColor(TextStyle.footnote1Default1Light2SecondaryLabelColor.color)
                    .frame(height: 18)
                Text(props.accountInfo.defaultPhone?.phoneNumber ?? "")
                    .font(phoneNumberTextStyle().font)
                    .foregroundColor(phoneNumberTextStyle().color)
                    .frame(height: 22)
                Spacer()
            }
            Spacer()
        }
        .frame(height: 70)
        .background(Color.white)
    }
    
    private func avatar(props: ContactDefaultNumberViewProps) -> some View{
        let avatarStyle = ContactDefaultNumberViewHelper.getAvatarStyleOfDefaultNumber(accountInfo: props.accountInfo)
        return AnyView(
            AvatarView(
                props: AvatarViewProps(
                    size: 44,
                    style: avatarStyle
                )
            )
        )
    }
    
    private func phoneNumberTextStyle() -> TextStyle{
        return TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: Color.black)
    }
    
    private func getSimNumber(props: ContactDefaultNumberViewProps) -> Int{
        if let defaultPhone = props.accountInfo.defaultPhone{
            return props.accountInfo.getSimNumber(phone: defaultPhone) ?? 1
        }else{
            return 1
        }
    }
}

struct ContactDefaultNumberView_Previews: PreviewProvider {
    static var previews: some View {
        ContactDefaultNumberView(
            props: ContactDefaultNumberViewProps(
                accountInfo: AccountInfo(
                    phones: [
                        PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                        PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                        PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil),],
                    account: Account(id: 1, name: "Account name", createDate: Date()),
                    devices: []
                )
            )
        )
    }
}
