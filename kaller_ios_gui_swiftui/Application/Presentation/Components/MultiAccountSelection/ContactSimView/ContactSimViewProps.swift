//
//  ContactSimViewProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 20/11/2020.
//

import Foundation

struct ContactSimViewProps{
    let phoneInfo: PhoneInfo
    let number: Int
    let style: Style
    let isShowDivider: Bool
    
    enum Style{
        case contactPhone
        case userName
    }
}
