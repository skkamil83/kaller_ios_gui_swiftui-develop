//
//  ContactSimView.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 20/11/2020.
//

import SwiftUI

struct ContactSimView: View {
    let props: ContactSimViewProps
    var body: some View {
        HStack(spacing: 0){
            SimIcon(props: SimIconProps(number: props.number, size: CGSize(width: 16, height: 21)))
                .padding(EdgeInsets(top: 0, leading: 30, bottom: 0, trailing: 30))
            VStack(alignment: .leading){
                Spacer()
                Text(props.phoneInfo.place)
                    .font(TextStyle.subheadline1Default1Light2SecondaryLabelColor.font)
                    .foregroundColor(TextStyle.subheadline1Default1Light2SecondaryLabelColor.color)
                    .frame(height: 20)
                Text(getContent(props: props))
                    .font(phoneNumberTextStyle().font)
                    .foregroundColor(phoneNumberTextStyle().color)
                    .frame(height: 22)
                Spacer()
                if props.isShowDivider{
                    Divider()
                }
            }
            Spacer()
        }
        .frame(height: 62)
        .background(Color.white)
    }
    
    private func phoneNumberTextStyle() -> TextStyle{
        return TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
    }
    
    private func getContent(props: ContactSimViewProps) -> String{
        switch props.style{
        case .contactPhone:
            return props.phoneInfo.phoneNumber
        case .userName:
            return props.phoneInfo.nickName ?? ""
        }
    }
}

struct ContactSimView_Previews: PreviewProvider {
    static var previews: some View {
        ContactSimView(props: ContactSimViewProps(
                phoneInfo: PhoneInfo(
                    isDefault: true,
                    userInfo: UserInfo(
                        firstName: nil,
                        lastName: nil,
                        avatarImageUrl: nil),
                    phoneNumber: "+33 1 99 99 99 99",
                    nickName: "@olegkh",
                    place: "Russia, Moscow",
                    sortOrder: 1,
                    phoneCode: 0,
                    locationCode: 0,
                    ext: nil
                ),
                number: 1,
                style: .contactPhone,
                isShowDivider: true))
    }
}
