//
//  MultiaccountButton.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 19/11/2020.
//

import SwiftUI

struct MultiaccountSelectionView: View {
    let props: MultiaccountSelectionViewProps
    @State private var actionType: ExpandedType = .callFrom
    @State private var isShowUsername = false
    
    enum ExpandedType{
        case callFrom, messageFrom
    }
    
    var body: some View {
        VStack(spacing: 0){
            Spacer()
            VStack(spacing: 0){
                VStack(spacing: 0){
                    topSection()

                    if isShowUsername {
                        usernameList(props: props)
                    } else {
                        if actionType == .callFrom {
                            phoneList(props: props)
                        } else {
                            phoneList(props: props)
                        }
                    }
                    
                    if !(getPhoneInfoIncludeUsername(props: props).isEmpty || isShowUsername) {
                        usernameSection()
                    }

                    if !isShowUsername {
                        bottomSection()
                    }
                }
                .background(Color.white)
                .cornerRadius(14)

                bottomView(props: props)
            }
            .frame(width: UIScreen.main.bounds.width - 20)
            .padding(EdgeInsets(top: getTopAndBottomPadding(props: props), leading: 0, bottom: getTopAndBottomPadding(props: props), trailing: 0))
        }
        .frame(width: UIScreen.main.bounds.width)
        .background(Blur(style: .systemUltraThinMaterialDark))
        .onTapGesture {
            props.onOutSideTap()
        }
    }
    
    private func topSection() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return
            VStack(spacing: 0){
                HStack(spacing: 0){
                    Image(actionType == .messageFrom ? "viewsActionSheetsXIconMessage" : "viewsActionSheetsXIconCall")
                        .frame(width: 76, height: 62)
                        .opacity(isShowUsername ? 0 :1)
                        .transition(.opacity)
                    Text(isShowUsername ? "Usernames" : actionType == .messageFrom ? "Message from" :  "Call from")
                        .font(textStyle.font)
                        .foregroundColor(textStyle.color)
                    Spacer()
                    Image("viewsActionSheetsXIconDown")
                        .padding(.leading, 24)
                        .rotationEffect(Angle(degrees: 180))
                }
                .frame(height: 62)
                .background(Color.materialsBackgroundsLight1Thick)
                .onTapGesture {
                    isShowUsername = false
                }
                Divider()
                    .padding(.leading, Spacing.spacing16Pt)
            }
    }
    
    private func bottomSection() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return
            VStack(spacing: 0){
                Divider()
                    .padding(.leading, Spacing.spacing16Pt)
                HStack(spacing: 0){
                    Image(actionType != .messageFrom ? "viewsActionSheetsXIconMessage" : "viewsActionSheetsXIconCall")
                        .frame(width: 76, height: 62)
                    Text(actionType != .messageFrom ? "Message from" : "Call from")
                        .font(textStyle.font)
                        .foregroundColor(textStyle.color)
                    Spacer()
                    Image("viewsActionSheetsXIconDown")
                        .padding(.trailing, 24)
                }
                .frame(height: 62)
                .background(Color.materialsBackgroundsLight1Thick)
                .onTapGesture {
                    actionType = actionType == .callFrom ? .messageFrom : .callFrom
                }
            }
    }
    
    private func usernameSection() -> some View{
        let textStyle = TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black)
        return
            VStack(spacing: 0){
                Divider()
                    .padding(.leading, Spacing.spacing16Pt)
                HStack{
                    Text("Usernames")
                        .padding(.leading, 76)
                        .font(textStyle.font)
                        .foregroundColor(textStyle.color)
                    Spacer()
                    Image("viewsActionSheetsXIconDown")
                        .padding(.trailing, 24)
                }
                .frame(height: 62)
                .background(Color.white)
                .onTapGesture {
                    isShowUsername = true
                }
            }
    }
    
    private func usernameList(props: MultiaccountSelectionViewProps) -> some View{
        let phones = getPhoneInfoIncludeUsername(props: props)
        return
            ScrollView{
                LazyVStack(spacing: 0){
                    ForEach(0..<phones.count, id: \.self) {  index in
                        let phoneInfo = phones[index]
                        ContactSimView(
                                props: ContactSimViewProps(
                                        phoneInfo: phoneInfo,
                                        number: props.accountInfo.getSimNumber(phone: phoneInfo) ?? 0,
                                        style: .userName,
                                        isShowDivider: index < phones.count - 1
                                )
                        )
                            .onTapGesture {
                                if(actionType == .callFrom){
                                    props.onSelectedCallingContact(phoneInfo)
                                }else{
                                    props.onSelectedMessageContact(phoneInfo)
                                }
                            }
                    }
                }
            }
            .frame(minHeight: 0, maxHeight: CGFloat(phones.count) * 62)
            .transition(.move(edge: .bottom))
            .animation(.easeOut)
    }
    
    private func phoneList(props: MultiaccountSelectionViewProps) -> some View{
        return
            ScrollView{
                LazyVStack(spacing: 0){
                    ForEach(0..<props.accountInfo.sortedPhones.count, id: \.self) {  index in
                        let phoneInfo = props.accountInfo.sortedPhones[index]
                        ContactSimView(
                                props: ContactSimViewProps(
                                        phoneInfo: phoneInfo,
                                        number: props.accountInfo.getSimNumber(phone: phoneInfo) ?? 0,
                                        style: .contactPhone,
                                        isShowDivider: index < props.accountInfo.sortedPhones.count - 1
                                )
                        )
                            .onTapGesture {
                                if(actionType == .callFrom){
                                    props.onSelectedCallingContact(phoneInfo)
                                }else{
                                    props.onSelectedMessageContact(phoneInfo)
                                }
                            }
                    }
                }
            }
            .frame(minHeight: 0, maxHeight: CGFloat(props.accountInfo.sortedPhones.count) * 62)
            .transition(.move(edge: .bottom))
            .animation(.easeOut)
    }
    
    private func bottomView(props: MultiaccountSelectionViewProps) -> some View{
        switch props.style{
        case .selectPhoneInfo(let isFree):
            return
                AnyView(
                    Button(action: props.onButtonCallTap){
                        CircleIcon(
                                props: CircleIconProps(
                                        size: getButtonCallSize(),
                                        style: .filled(imageName: isFree ? "systemDialerButtonCallFree" : "systemDialerButtonCall")
                                ))
                                .shadow(color: Color.black.opacity(0.33), radius: 4, x: 0, y: 0)
                    }
                    .padding(EdgeInsets(top: 18, leading: 0, bottom: 0, trailing: 0))
                )
            
        case .selectDefaultPhoneInfo:
            return AnyView(
                ContactDefaultNumberView(props: ContactDefaultNumberViewProps(accountInfo: props.accountInfo))
                    .cornerRadius(14)
                    .padding(EdgeInsets(top: 8, leading: 0, bottom: 0, trailing: 0))
                    .onTapGesture {
                        props.onSelectedDefalutNumber()
                    }
            )
        }
    }
    
    private func getPhoneInfoIncludeUsername(props: MultiaccountSelectionViewProps) -> [PhoneInfo]{
        props.accountInfo.sortedPhones.filter({$0.nickName != nil})
    }
    
    private func getButtonCallSize() -> CGFloat{
        return (UIScreen.main.bounds.width - (48 * 2) - (27 * 2))/3
    }
    
    private func getTopAndBottomPadding(props: MultiaccountSelectionViewProps) -> CGFloat{
        switch props.style{
        case .selectPhoneInfo:
            switch UIScreen.main.bounds.height{
            case 926:
                return 130
            case 896:
                return 129
            case 844:
                return 124
            case 812:
                return 124
            case 736:
                return 67
            case 667:
                return 64
            case 568:
                return 61
            default:
                return 100
            }
        case .selectDefaultPhoneInfo:
            return 67
        }
    }
}

struct MultiaccountButton_Previews: PreviewProvider {
    static var previews: some View {
        MultiaccountSelectionView(
            props: MultiaccountSelectionViewProps(
                style: .selectDefaultPhoneInfo,
                accountInfo: AccountInfo(
                    phones: [
                        PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                        PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                        PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                    account: Account(id: 1, name: "Account name", createDate: Date()),
                    devices: []
                ),
                onOutSideTap: {
                    print("outside tap")
                },
                onButtonCallTap: {},
                onSelectedCallingContact: {_ in },
                onSelectedMessageContact: {_  in },
                onSelectedDefalutNumber: {}
            )
        )
    }
}
