//
//  MultiaccountSelectionStyle.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 24/11/2020.
//

import Foundation

enum MultiaccountSelectionStyle {
    case selectPhoneInfo(_ isFree: Bool)
    case selectDefaultPhoneInfo
}
