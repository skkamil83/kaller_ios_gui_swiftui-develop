import Foundation

struct RecentsInfoCallLogGroupViewModel{
    let date: Date
    let rate: Double
    let currencySymbol: String
    let recentCalls: [RecentCall]
    
    var recentCallsSorted: [RecentCall]{
        return recentCalls.sorted { (first, second) -> Bool in
            return first.callStartDate > second.callStartDate
        }
    }
}
