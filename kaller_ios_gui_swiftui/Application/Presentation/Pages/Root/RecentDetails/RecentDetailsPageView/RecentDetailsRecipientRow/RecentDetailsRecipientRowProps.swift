struct RecentDetailsRecipientRowProps{
    let recentViewModel: RecentViewModel
    let onAddContact: () -> Void
    let onViewInfo: () -> Void
    let openContactCard: () -> Void
}
