import SwiftUI

struct RecentDetailsRecipientRow: View {
    let props: RecentDetailsRecipientRowProps
    
    var body: some View {
        VStack(spacing: 0){
            Divider()
            contentView(props: props)
            Divider()
        }
        .background(Color.white)
        .onTapGesture {
            props.openContactCard()
        }
    }
    
    private func contentView(props: RecentDetailsRecipientRowProps) -> some View{
        HStack(spacing: 0){
            AvatarView(
                props: AvatarViewProps(
                    size: 44,
                    style: AvatarViewHelper.getAvatarStyle(
                        from: props.recentViewModel.contact
                    )
                )
            )
            .padding(.leading, 16)
            
            VStack(alignment: .leading, spacing: 0){
                Text(RecentRowHelper.getNameOfPartner(recentViewModel: props.recentViewModel))
                    .frame(height: 22)
                    .font(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black).font)
                    .foregroundColor(.black)
                    .padding(.top, 3)
                Text(props.recentViewModel.place)
                    .frame(height: 20)
                    .font(TextStyle.subheadline1Default1Light2SecondaryLabelColor.font)
                    .foregroundColor(TextStyle.subheadline1Default1Light2SecondaryLabelColor.color)
            }
            .padding(.leading, 16)
            
            Spacer()
            
            rightButton(props: props)
                .padding(.trailing, 16)
            
        }
        .frame(height: 44)
        .padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))
    }
    
    private func rightButton(props: RecentDetailsRecipientRowProps) -> some View{
        Button(action: {
            if(props.recentViewModel.contact != nil){
                props.onViewInfo()
            }else{
                props.onAddContact()
            }
        }){
            Image(props.recentViewModel.contact != nil ? "symbolInfo" : "symbolPlus")
        }
    }
}

struct RecentsInfoOppositeRow_Previews: PreviewProvider {
    static var previews: some View {
        let recentCall1 = RecentCall( callId: "1", callType: .outcoming, phoneFromNumber: "+71111234567", phoneToNumber: "+44 20 1234 5678", callStartDate: Date(),
                                      callEndDate: Date(), isExtension: false, isMissed: false,
                                      isCanceled: false,
                                      fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Dad", lastName: "", company: nil, avatarUrl: nil, phones: []),
                                      billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        return RecentDetailsRecipientRow(
            props: RecentDetailsRecipientRowProps (
                recentViewModel: RecentViewModel(
                    calls: [recentCall1],
                    displayDate: "18:21",
                    isMissed: false,
                    isCanceled: false, callType: .outcoming,
                    contact: Contact(firstName: "Dad",lastName: "", company: nil, avatarUrl: nil,
                                     phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Dad", lastName: "", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "Mobile",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]),
                    isExtension: false,
                    place: "Mobile"
                ),
                onAddContact: {},
                onViewInfo: {},
                openContactCard: {}
            )
        )
    }
}
