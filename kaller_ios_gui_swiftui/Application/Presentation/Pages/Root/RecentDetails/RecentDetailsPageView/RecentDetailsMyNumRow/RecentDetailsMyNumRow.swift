import SwiftUI

struct RecentDetailsMyNumRow: View {
    let props: RecentDetailsMyNumRowProps
    
    var body: some View {
        VStack(spacing: 0){
            Divider()
            contentView(props: props)
            Divider()
        }
    }
    
    private func contentView(props: RecentDetailsMyNumRowProps) -> some View{
        HStack(spacing: 0){
            AvatarView(
                props: AvatarViewProps(
                    size: 44,
                    style: AvatarViewHelper.getAvatarStyle(
                        from: props.phoneInfo
                    )
                )
            )
            .padding(.leading, 16)
            
            VStack(alignment: .leading, spacing: 0){
                Text("MY PHONE NUMBER")
                    .frame(height: 18)
                    .font(TextStyle.footnote1Default1Light2SecondaryLabelColor.font)
                    .foregroundColor(TextStyle.footnote1Default1Light2SecondaryLabelColor.color)
                Text(props.phoneInfo.phoneNumber)
                    .frame(height: 22)
                    .font(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black).font)
                    .foregroundColor(.black)
            }
            .padding(.leading, 16)
            
            Spacer()
            
            SimIcon(
                props: SimIconProps(
                        number: props.numberOfSim,
                    size: CGSize(width: 16, height: 21)
                )
            )
            .padding(.trailing, 19)
        }
        .frame(height: 44)
        .padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))
    }
}

struct RecentsInfoMyNumRow_Previews: PreviewProvider {
    static var previews: some View {
        RecentDetailsMyNumRow(
            props: RecentDetailsMyNumRowProps(
                numberOfSim: 1,
                phoneInfo: PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil)
            )
        )
    }
}
