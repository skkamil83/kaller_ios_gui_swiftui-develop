//
//  RecentsInfoCallLog.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 23/12/2020.
//

import SwiftUI

struct RecentDetailsCallLog: View {
    let props: RecentDetailsCallLogProps
    
    var body: some View {
        VStack(spacing: 4){
            header(props: props)
            list(props: props)
        }
        .padding(.top, 8)
    }
    
    private func header(props: RecentDetailsCallLogProps) -> some View{
        HStack{
            Text(props.callLogGroup.date.formattedTitleDate())
                .font(TextStyle.subheadline1Default1Light1LabelColor.font)
                .foregroundColor(TextStyle.subheadline1Default1Light1LabelColor.color)
            Spacer()
            if(props.isShowRate){
                Text("Rate (min.): \(props.callLogGroup.rate == 0 ? "Free" : "\(props.callLogGroup.currencySymbol)\(props.callLogGroup.rate)")")
                    .font(TextStyle.caption12Bold1Light1LabelColor.font)
                    .foregroundColor(TextStyle.caption12Bold1Light1LabelColor.color)
            }
        }
    }
    
    private func list(props: RecentDetailsCallLogProps) -> some View{
        LazyVStack(alignment: .leading, spacing: 4){
            ForEach(props.callLogGroup.recentCallsSorted, id: \.callId){ recentCall in
                ZStack(alignment: .leading){
                    Text(RecentDetailsCallLogHelper.getTime(date: recentCall.callStartDate))
                        .font(TextStyle.caption11Default1Light6DarkGray.font)
                        .foregroundColor(TextStyle.caption11Default1Light6DarkGray.color)

                    HStack(spacing: 2){
                        Image(RecentDetailsCallLogHelper.getBillingTypeImageName(recentCall: recentCall))
                            .frame(width: 16, height: 16)
                        Text(RecentDetailsCallLogHelper.getBillingTypeTitle(billingType: recentCall.billingInfo.type))
                            .font(TextStyle.caption11Default1Light6DarkGray.font)
                            .foregroundColor(TextStyle.caption11Default1Light6DarkGray.color)
                    }
                    .padding(.leading, 98)
                    HStack{
                        Spacer()
                        Text(RecentDetailsCallLogHelper.getBillingInfo(billInfo: recentCall.billingInfo))
                            .font(TextStyle.caption11Default1Light6DarkGray.font)
                            .foregroundColor(TextStyle.caption11Default1Light6DarkGray.color)
                    }
                }
            }
        }
    }
}

struct RecentsInfoCallLog_Previews: PreviewProvider {
    static var previews: some View {
        RecentDetailsCallLog(
            props: RecentDetailsCallLogProps(
                callLogGroup: RecentsInfoCallLogGroupViewModel(
                    date: Date(),
                    rate: 0.05,
                    currencySymbol: "$",
                    recentCalls: [
                        RecentCall(
                            callId: "1",
                            callType: .outcoming,
                            phoneFromNumber:"+71111234567",
                            phoneToNumber: "+44 20 1234 5678",
                            callStartDate: Date(),
                            callEndDate: Date(),
                            isExtension: false,
                            isMissed: false,
                            isCanceled: false,
                            fromCountry: nil,
                            fromExt: nil,
                            contact: Contact( firstName: "Dad", lastName: "", company: nil, avatarUrl: nil, phones: []),
                            billingInfo: BillingInfo(type: .appToPhone, pricePerMinute: 0.05, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
                    ]),
                isShowRate: true
            )
        )
    }
}
