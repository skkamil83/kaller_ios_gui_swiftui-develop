import Foundation

class RecentDetailsCallLogHelper{
    
    static func getTime(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }
    
    static func getBillingInfo(billInfo: BillingInfo) -> String{
        if(billInfo.pricePerMinute == 0){
            return "Free\u{200A}/\u{200A}\(billInfo.minutes) min."
        }else{
            return "\(billInfo.currency.symbol)\(billInfo.pricePerMinute * Double(billInfo.minutes))\u{200A}/\u{200A}\(billInfo.minutes) min."
        }
    }
    
    static func getBillingTypeTitle(billingType: BillingType) -> String{
        switch billingType{
        case .appToApp:
            return "Kaller to Kaller"
        case .appToPhone:
            return "Kaller to Phone"
        case .phoneToApp:
            return "Phone to Kaller"
        case .phoneToPhone:
            return "Phone to Phone"
        }
    }
    
    static func getBillingTypeImageName(recentCall: RecentCall) -> String{
        if recentCall.isMissed{
            return recentCall.billingInfo.type == .appToApp ? "CallLightMiss" : "CallLightMissGsm"
        }
        if recentCall.callType == .outcoming{
            return recentCall.billingInfo.type == .appToApp ? "CallLightOutgoing" : "CallLightOutgoingGsm"
        }else{
            return recentCall.billingInfo.type == .appToApp ? "CallLightIn" : "CallLightInGsm"
        }
    }
}
