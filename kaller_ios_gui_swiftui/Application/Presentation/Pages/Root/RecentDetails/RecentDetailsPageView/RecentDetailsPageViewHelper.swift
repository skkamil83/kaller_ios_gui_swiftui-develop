import Foundation

class RecentDetailsPageViewHelper{
    static func getTitle(recentViewModel: RecentViewModel) -> String{
        if(recentViewModel.isMissed){
            if recentViewModel.isCanceled{
                return "Canceled Call Info"
            }else{
                return "Missed Call Info"
            }
        }else{
            if(recentViewModel.callType == .incoming){
                return "Incoming Call Info"
            }else{
                return "Outgoing Call Info"
            }
        }
    }
    
    static func getPhoneInfoOfUser(recentViewModel: RecentViewModel, accountInfo: AccountInfo) -> PhoneInfo?{
        guard let recentCall = recentViewModel.calls.first else {return nil}
        let phone = recentViewModel.callType == .incoming ? recentCall.phoneToNumber : recentCall.phoneFromNumber
        return accountInfo.phones.first(where: {$0.phoneNumber == phone})
    }
    
    static func groupRecentCallsByDate(recentCalls: [RecentCall]) -> [RecentsInfoCallLogGroupViewModel]{
        let calendar = Calendar.current
        let groups = Dictionary(grouping: recentCalls, by: { calendar.startOfDay(for: $0.callStartDate) })
        var viewModels = [RecentsInfoCallLogGroupViewModel]()
        for group in groups{
            let date = group.key
            let calls = group.value
            let rate = calls.first?.billingInfo.pricePerMinute ?? 0
            let symbol = calls.first?.billingInfo.currency.symbol ?? ""
            viewModels.append(
                RecentsInfoCallLogGroupViewModel(
                    date: date,
                    rate: rate,
                    currencySymbol: symbol,
                    recentCalls: calls
                )
            )
        }
        viewModels.sort { (first, second) -> Bool in
            return first.date > second.date
        }
        return viewModels
    }
}
