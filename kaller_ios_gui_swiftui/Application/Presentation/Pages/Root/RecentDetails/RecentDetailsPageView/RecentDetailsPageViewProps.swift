struct RecentDetailsPageViewProps {
    let accountInfo: AccountInfo
    let recentViewModel: RecentViewModel
    let onCancel: () -> Void
    let addNewContact: () -> Void
    let onViewInfo: (Contact) -> Void
    let openContactCard: () -> Void
}
