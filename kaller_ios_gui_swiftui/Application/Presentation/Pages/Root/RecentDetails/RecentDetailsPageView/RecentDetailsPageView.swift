import SwiftUI

struct RecentDetailsPageView: View {
    let props: RecentDetailsPageViewProps
    @State private var listHeight: CGFloat = .zero
    
    var body: some View {
        VStack(spacing: 0){
            Spacer()
                .frame(height: 50)
            Spacer()
            VStack(spacing: 8){
                VStack(spacing: 0){
                    title(props: props)
                    fromView(props: props)
                    toView(props: props)
                    callLog(props: props)
                }
                .frame(width: UIScreen.main.bounds.width - 20)
                .background(Color(red: 253/255, green: 253/255, blue: 253/255, opacity: 0.92))
                .cornerRadius(14)
                cancelButton(props: props)
            }
            Spacer()
            Spacer()
                .frame(height: 50)
        }
        .frame(width: UIScreen.main.bounds.width)
        .background(Blur(style: .systemUltraThinMaterialDark))
        .edgesIgnoringSafeArea(.all)
        
    }
    
    private func title(props: RecentDetailsPageViewProps) -> some View{
        Text(RecentDetailsPageViewHelper.getTitle(recentViewModel: props.recentViewModel))
            .font(TextStyle.body2Bold1Light1LabelColor.font)
            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
            .padding(.top, 13)
    }
    
    private func fromView(props: RecentDetailsPageViewProps) -> some View{
        VStack(alignment: .leading, spacing: 0){
            Text("FROM")
                .font(TextStyle.footnote1Default1Light1LabelColor.font)
                .foregroundColor(TextStyle.footnote1Default1Light1LabelColor.color)
                .padding(EdgeInsets(top: 16, leading: 16, bottom: 7, trailing: 0))
            if(props.recentViewModel.callType == .incoming){
                RecentDetailsRecipientRow(
                    props: RecentDetailsRecipientRowProps(
                        recentViewModel: props.recentViewModel,
                        onAddContact: props.addNewContact,
                        onViewInfo: {
                            if let contact = props.recentViewModel.contact{
                                props.onViewInfo(contact)
                            }
                        },
                        openContactCard: props.openContactCard
                    )
                )
            }else{
                if let phoneInfo = RecentDetailsPageViewHelper.getPhoneInfoOfUser(recentViewModel: props.recentViewModel, accountInfo: props.accountInfo){
                    RecentDetailsMyNumRow(
                        props: RecentDetailsMyNumRowProps(
                            numberOfSim: props.accountInfo.getSimNumber(phone: phoneInfo) ?? 1,
                            phoneInfo: phoneInfo
                        )
                    )
                }
            }
        }
    }
    
    private func toView(props: RecentDetailsPageViewProps) -> some View{
        VStack(alignment: .leading, spacing: 0){
            Text("TO")
                .font(TextStyle.footnote1Default1Light1LabelColor.font)
                .foregroundColor(TextStyle.footnote1Default1Light1LabelColor.color)
                .padding(EdgeInsets(top: 7, leading: 16, bottom: 7, trailing: 0))
            if(props.recentViewModel.callType == .outcoming){
                RecentDetailsRecipientRow(
                    props: RecentDetailsRecipientRowProps(
                        recentViewModel: props.recentViewModel,
                        onAddContact: props.addNewContact,
                        onViewInfo: {
                            if let contact = props.recentViewModel.contact{
                                props.onViewInfo(contact)
                            }
                        },
                        openContactCard: props.openContactCard
                    )
                )
            }else{
                if let phoneInfo = RecentDetailsPageViewHelper.getPhoneInfoOfUser(recentViewModel: props.recentViewModel, accountInfo: props.accountInfo){
                    RecentDetailsMyNumRow(
                        props: RecentDetailsMyNumRowProps(
                            numberOfSim: props.accountInfo.getSimNumber(phone: phoneInfo) ?? 1,
                            phoneInfo: phoneInfo
                        )
                    )
                }
            }
        }
    }
    
    private func callLog(props: RecentDetailsPageViewProps) -> some View{
        let groups = RecentDetailsPageViewHelper.groupRecentCallsByDate(recentCalls: props.recentViewModel.calls)
        return
            ScrollView{
                LazyVStack(spacing: 0){
                    ForEach(0..<groups.count, id: \.self){ index in
                        RecentDetailsCallLog(props: RecentDetailsCallLogProps(callLogGroup: groups[index], isShowRate: index == 0))
                    }
                }
                .background(collectListHeight())
            }
            .frame(maxHeight: listHeight)
            .padding(EdgeInsets(top: 4, leading: 16, bottom: 12, trailing: 16))
    }
    
    private func cancelButton(props: RecentDetailsPageViewProps) -> some View{
        Button(action: props.onCancel){
            Text("Cancel")
                .font(TextStyle.title32Bold1Light5Blue.font)
                .foregroundColor(TextStyle.title32Bold1Light5Blue.color)
        }
        .frame(width: UIScreen.main.bounds.width - 20, height: 56)
        .background(Color.white)
        .cornerRadius(14)
    }
    
    private func collectListHeight() -> some View{
        return GeometryReader{ geometry -> Color in
            let rect = geometry.frame(in: .global)
            DispatchQueue.main.async {
                if(listHeight == 0){
                    listHeight = rect.size.height
                }
            }
            return Color.clear
        }
    }
}
