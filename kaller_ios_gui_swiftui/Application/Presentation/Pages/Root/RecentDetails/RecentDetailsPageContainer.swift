import SwiftUI

struct RecentDetailsPageContainerProps {
    let recentViewModel: RecentViewModel
    let close: () -> Void
}

struct RecentDetailsPageContainer: View {
    @EnvironmentObject private var userInfoState: UserInfoState
    let props: RecentDetailsPageContainerProps
    
    var body: some View {
        RecentDetailsPageView(
            props: RecentDetailsPageViewProps(
                accountInfo: userInfoState.accountInfo!,
                recentViewModel: props.recentViewModel,
                onCancel: props.close,
                addNewContact: addNewContact,
                onViewInfo: viewInfo(contact:),
                openContactCard: openContactCard
            )
        )
    }
    
    private func addNewContact(){
        print(#function)
    }
    
    private func viewInfo(contact: Contact) -> Void{
        print(#function)
    }
    
    private func openContactCard(){
        print(#function)
    }
}
