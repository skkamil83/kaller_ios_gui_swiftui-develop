import SwiftUI

struct ChatsPageContainerProps {
    let onSelectDefaultNumber: () -> Void
    let onSwitchEditMode: (_ isEditMode: Bool, _ items: [BottomTabbarActionViewModel]) -> Void
}

struct ChatsPageContainer: View {
    @EnvironmentObject private var userInfoState: UserInfoState
    @EnvironmentObject private var countryState: CountryState
    @EnvironmentObject private var chatState: ChatState

    @State private var filterType: PhoneNumberFilterType = .allNumbers
    @State private var isRouteToArchivedConversationsPage = false
    let props: ChatsPageContainerProps

    var body: some View {
        NavigationView {
            ZStack {
                ChatsPageView(
                        props: ChatsPageViewProps(
                                accountInfo: userInfoState.accountInfo!,
                                filterType: filterType,
                                countries: CountryFetcher().countries,
                                archive: ConversationsHelper.buildChatListArchivedViewModel(archivedConversations: chatState.archivedConversations),
                                conversations: ConversationsHelper.buildConversationViewModels(conversations: chatState.conversations ?? [], isArchived: false, filterType: filterType, defaultPhone: userInfoState.accountInfo?.defaultPhone, accountInfo: userInfoState.accountInfo!),
                                onFilter: onFilter,
                                onSelectDefaultNumber: props.onSelectDefaultNumber,
                                onMute: onMuteConversation,
                                onDelete: onDeleteConversations,
                                onClear: onClearConversations,
                                onArchive: onArchiveConversations,
                                onRead: onReadConversations,
                                onUnread: onUnreadConversation,
                                onPin: onPinConversation,
                                onCreateNewChat: onCreateNewChat,
                                onSwitchEditMode: props.onSwitchEditMode,
                                onSelectedArchivedConvarsations: onSelectedArchivedConvarsations,
                                onHideArchivedConversations: onHideArchivedConversations,
                                onPinArchivedConversations: onPinArchivedConversations,
                                onSelectedConversation: onSelectedConversation
                        )
                )
                archivedConversationsLink(props: props)
            }
        }
    }

    private func onFilter(filter: PhoneNumberFilterType) {
        filterType = filter
    }

    private func onMuteConversation(isMuted: Bool, id: String) {
        if let index = chatState.conversations?.firstIndex(where: { $0.id == id }) {
            chatState.conversations?[index].isMuted = isMuted
        }
    }

    private func onDeleteConversations(ids: [String]) {
        for id in ids {
            if let index = chatState.conversations?.firstIndex(where: { $0.id == id }) {
                chatState.conversations?.remove(at: index)
            }
        }
    }

    private func onClearConversations(ids: [String]) {
        for id in ids {
            if let index = chatState.conversations?.firstIndex(where: { $0.id == id }) {
                chatState.conversations?[index].messages.removeAll()
            }
        }
    }

    private func onArchiveConversations(ids: [String]) {
        for id in ids {
            if let index = chatState.conversations?.firstIndex(where: { $0.id == id }) {
                chatState.conversations?[index].dateOfPin = nil
                if let conversation = chatState.conversations?[index] {
                    chatState.archivedConversations?.conversations.append(conversation)
                    chatState.conversations?.remove(at: index)
                }
            }
        }
        
        updateStateOfArchivedConersations()
    }

    private func onReadConversations(ids: [String]) {
        for id in ids {
            if let index = chatState.conversations?.firstIndex(where: { $0.id == id }) {
                chatState.conversations![index].isUnread = false
                chatState.conversations![index].unreadCount = 0
                for i in 0..<chatState.conversations![index].messages.count {
                    if !chatState.conversations![index].messages[i].phoneNumbersHaveRead.contains(chatState.conversations![index].userPhoneInfo.phoneNumber) {
                        chatState.conversations![index].messages[i].phoneNumbersHaveRead.append(chatState.conversations![index].userPhoneInfo.phoneNumber)
                    }
                }
            }
        }
    }

    private func onUnreadConversation(id: String) {
        if let index = chatState.conversations?.firstIndex(where: { $0.id == id }) {
            chatState.conversations![index].isUnread = true
        }
    }

    private func onPinConversation(date: Date?, id: String) {
        if let index = chatState.conversations?.firstIndex(where: { $0.id == id }) {
            chatState.conversations![index].dateOfPin = date
        }
    }

    private func onCreateNewChat() {
        print(#function)
    }

    private func onSelectedArchivedConvarsations() {
        isRouteToArchivedConversationsPage = true
    }

    private func archivedConversationsLink(props: ChatsPageContainerProps) -> some View {
        NavigationLink(
                destination: ArchivedConversationsPageContainer(
                        props: ArchivedConversationsPageContainerProps(
                                onSwitchEditMode: props.onSwitchEditMode
                        )
                ),
                isActive: $isRouteToArchivedConversationsPage
        ) {

        }
    }

    private func onHideArchivedConversations() {
        chatState.archivedConversations?.isPinned = false
    }

    private func onPinArchivedConversations() {
        chatState.archivedConversations?.isPinned = true
    }
    
    private func updateStateOfArchivedConersations() {
        if chatState.archivedConversations?.conversations.first?.isUnread != nil {
            chatState.archivedConversations?.isUnread = true
        }
        
        var unreadCount = 0
        for conversation in chatState.archivedConversations?.conversations ?? [] {
            unreadCount += conversation.unreadCount
        }
        chatState.archivedConversations?.unreadCount = unreadCount
    }

    private func onSelectedConversation(id: String) {
        print("\(#function): \(id)")
    }
}
