enum ChatPageEditActionType: Identifiable {

    var id: String {
        return title.lowercased()
    }

    case read
    case readAll
    case archive
    case delete

    var title: String {
        switch self {
        case .read:
            return "Read"
        case .readAll:
            return "Read All"
        case .archive:
            return "Archive"
        case .delete:
            return "Delete"
        }
    }
}
