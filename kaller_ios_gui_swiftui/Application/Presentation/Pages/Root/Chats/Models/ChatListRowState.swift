enum ChatListRowState {
    case normal
    case edit(_ isSelected: Bool)
}
