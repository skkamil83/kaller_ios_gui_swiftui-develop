import Foundation

struct ConversationViewModel {
    let id: String
    let avatar: AvatarViewProps.Style
    let title: String
    let conversationType: ConversationType
    let numberOfSim: Int
    let isShowSimNumber: Bool
    let message: String
    let messageType: MessageType
    let messageStatus: MessageStatus?
    let date: Date
    let isArchived: Bool
    let isUnread: Bool
    let unreadCount: Int
    let dateOfPin: Date?
    let isMuted: Bool
    var isPinned: Bool {
        return dateOfPin != nil
    }
}
