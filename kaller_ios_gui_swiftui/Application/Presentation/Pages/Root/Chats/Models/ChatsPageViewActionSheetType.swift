enum ChatsPageViewActionSheetType: Identifiable {
    var id: String {
        switch self {
        case .filter:
            return "filter"
        case .delete(_):
            return "delete"
        }
    }
    case filter
    case delete([String])
}
