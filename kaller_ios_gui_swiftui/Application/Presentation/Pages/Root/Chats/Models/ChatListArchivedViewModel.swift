import Foundation

struct ChatListArchivedViewModel {
    let date: Date
    let isUnread: Bool
    let unreadCount: Int
    let isPinned: Bool
    let content: String
    let messageStatus: MessageStatus?
}
