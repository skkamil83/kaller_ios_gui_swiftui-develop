class ChatsPageViewHelper {
    static func getFlagImageName(phoneCode: Int, countries: [Country]) -> String? {
        return countries.first(where: { $0.phoneCode == phoneCode })?.flagImageName
    }
}
