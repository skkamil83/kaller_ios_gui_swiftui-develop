import SwiftUI

struct HiddenArchivedConversationsView: View {
    var body: some View {
        HStack(spacing: 0) {
            Spacer()
            Image(systemName: "archivebox.fill")
                    .textStyle(.caption12Bold2Dark1LabelColor)
            Text(" Archive hidden. Swipe down to see the archive.")
                    .textStyle(.caption12Bold2Dark1LabelColor)
            Spacer()
        }
                .frame(height: 32)
                .background(Color.black.opacity(0.63))
    }
}

struct HiddenArchivedConversationsView_Previews: PreviewProvider {
    static var previews: some View {
        HiddenArchivedConversationsView()
    }
}
