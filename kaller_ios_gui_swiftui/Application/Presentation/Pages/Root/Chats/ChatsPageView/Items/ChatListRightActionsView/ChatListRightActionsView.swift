import SwiftUI

struct ChatListRightActionsView: View {
    let props: ChatListRightActionsViewProps
    var body: some View {
        view(props: props)
    }

    private func view(props: ChatListRightActionsViewProps) -> some View {
        SimpleActionsRow(
                props: SimpleActionsRowProps(
                        items: [
                            .init(
                                    id: "mute",
                                    title: props.isMuted ? "Unmute" : "Mute",
                                    color: .graysSystemGray4Light,
                                    icon: .sytemImageName(props.isMuted ? "speaker.wave.2.fill" : "speaker.slash.fill"),
                                    action: {
                                        props.onMute(!props.isMuted)
                                    }),
                            .init(
                                    id: "delete",
                                    title: "Delete",
                                    color: Color.red700,
                                    icon: .sytemImageName("trash.fill"),
                                    action: props.onDelete),
                            .init(
                                    id: "archive",
                                    title: props.isArchived ? "Unarchive" : "Archive",
                                    color: Color.blue800,
                                    icon: props.isArchived ? .imageName("iconsUnarchive") : .sytemImageName("archivebox.fill"),
                                    action: {
                                        props.onArchive(!props.isArchived)
                                    }
                            )
                        ],
                        alignment: .trailing,
                        contentViewWidth: props.contentViewWidth
                )
        )
    }
}

struct ChatListActionsView_Previews: PreviewProvider {
    static var previews: some View {
        ChatListRightActionsView(
                props: ChatListRightActionsViewProps(
                        isMuted: true,
                        isArchived: true,
                        onMute: { _ in },
                        onDelete: {},
                        onArchive: { _ in },
                        contentViewWidth: { _ in }
                )
        )
    }
}
