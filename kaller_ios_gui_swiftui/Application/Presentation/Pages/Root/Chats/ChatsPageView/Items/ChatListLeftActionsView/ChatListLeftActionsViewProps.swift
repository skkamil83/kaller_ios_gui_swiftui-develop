import SwiftUI

struct ChatListLeftActionsViewProps {
    let isArchived: Bool
    let isRead: Bool
    let isPinned: Bool
    let onRead: (Bool) -> Void
    let onPin: (Date?) -> Void
    let contentViewWidth: (CGFloat) -> Void
}
