import SwiftUI

struct ChatListRightActionsViewProps {
    let isMuted: Bool
    let isArchived: Bool
    let onMute: (Bool) -> Void
    let onDelete: () -> Void
    let onArchive: (Bool) -> Void
    let contentViewWidth: (CGFloat) -> Void
}
