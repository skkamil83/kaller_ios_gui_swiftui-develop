import SwiftUI

struct ChatListLeftActionsView: View {
    let props: ChatListLeftActionsViewProps

    var body: some View {
        view(props: props)
    }

    private func view(props: ChatListLeftActionsViewProps) -> some View {
        SimpleActionsRow(
                props: SimpleActionsRowProps(
                        items: props.isArchived ? [readItem(props: props)] : [readItem(props: props), pinItem(props: props)],
                        alignment: .leading,
                        contentViewWidth: props.contentViewWidth
                )
        )
    }

    private func readItem(props: ChatListLeftActionsViewProps) -> SimpleActionsRowViewModel {
        SimpleActionsRowViewModel(
                id: "read",
                title: props.isRead ? "Unread" : "Read",
                color: .blue800,
                icon: .imageName(props.isRead ? "iconsUnread" : "iconsRead"),
                action: {
                    props.onRead(!props.isRead)
                }
        )
    }

    private func pinItem(props: ChatListLeftActionsViewProps) -> SimpleActionsRowViewModel {
        SimpleActionsRowViewModel(
                id: "pin",
                title: props.isPinned ? "Unpin" : "Pin",
                color: .graysSystemGray4Light,
                icon: .sytemImageName(props.isPinned ? "pin.slash.fill" : "pin.fill"),
                action: {
                    props.isPinned ? props.onPin(nil) : props.onPin(Date())
                }
        )
    }
}

struct ChatListLeftActionsView_Previews: PreviewProvider {
    static var previews: some View {
        ChatListLeftActionsView(
                props: ChatListLeftActionsViewProps(
                        isArchived: true,
                        isRead: true,
                        isPinned: true,
                        onRead: { _ in },
                        onPin: { _ in },
                        contentViewWidth: { _ in }
                )
        )
    }
}
