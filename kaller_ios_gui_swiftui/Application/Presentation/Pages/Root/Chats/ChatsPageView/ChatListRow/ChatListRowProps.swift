import Foundation

struct ChatListRowProps {
    let conversation: ConversationViewModel
    let state: ChatListRowState
    let onMute: (Bool) -> Void
    let onDelete: () -> Void
    let onArchive: (Bool) -> Void
    let onRead: () -> Void
    let onUnread: () -> Void
    let onPin: (Date?) -> Void
}
