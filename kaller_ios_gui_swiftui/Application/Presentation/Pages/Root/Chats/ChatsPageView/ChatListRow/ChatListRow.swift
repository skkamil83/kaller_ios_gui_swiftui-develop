import SwiftUI

struct ChatListRow: View {
    @State private var offset: CGSize = .zero
    @State private var isShowedRightActionsView = false
    @State private var isShowedLeftActionView = false
    @State private var isInRightActionsView = false
    @State private var isInLeftActionsView = false
    @State private var widthToShowRightActionsView: CGFloat = 0
    @State private var widthToShowLeftActionsView: CGFloat = 0
    let props: ChatListRowProps

    var body: some View {
        ZStack {
            if isShowedLeftActionView {
                leftActionsView(props: props)
            }
            if isShowedRightActionsView {
                rightActionsView(props: props)
            }
            HStack(spacing: 0) {
                checkMark(props: props)
                conversation(props: props)
            }
                    .frame(height: 76)
                    .background(Color.white)
                    .offset(x: getOffset(props: props).width)
                    .gesture(
                            DragGesture()
                                    .onChanged(onDrag)
                                    .onEnded({ _ in onEndDrag() })
                    )
        }
    }

    private func onDrag(gesture: DragGesture.Value) {
        if isInRightActionsView {
            let newOffsetWidth = -widthToShowRightActionsView + gesture.translation.width
            if newOffsetWidth <= 0 {
                offset.width = newOffsetWidth
            }
        } else if isInLeftActionsView {
            let newOffsetWidth = widthToShowLeftActionsView + gesture.translation.width
            if newOffsetWidth > 0 {
                offset.width = newOffsetWidth
            }
        } else if abs(gesture.translation.width) > abs(gesture.translation.height) {
            if gesture.translation.width > 0 {
                isShowedRightActionsView = false
                isShowedLeftActionView = true
            } else {
                isShowedRightActionsView = true
                isShowedLeftActionView = false
            }
            offset = gesture.translation
        }
    }

    private func onEndDrag() {
        if offset.width < -widthToShowRightActionsView / 2 {
            withAnimation {
                isInRightActionsView = true
                offset = CGSize(width: -widthToShowRightActionsView, height: 0)
            }
        } else if offset.width > widthToShowLeftActionsView / 2 {
            withAnimation {
                isInLeftActionsView = true
                offset = CGSize(width: widthToShowLeftActionsView, height: 0)
            }
        } else {
            withAnimation {
                isInRightActionsView = false
                isInLeftActionsView = false
                self.offset = .zero
            }
        }
    }

    private func checkMark(props: ChatListRowProps) -> some View {
        switch props.state {
        case .normal:
            return AnyView(EmptyView())
        case .edit(let isSelected):
            return AnyView(Image("\(isSelected ? "editingSelectedLight" : "editingUnselectedLight")")
                    .padding(.leading, Spacing.spacing16Pt)
            )
        }
    }

    private func conversation(props: ChatListRowProps) -> some View {
        ConversationRow(
                props: ConversationRowProps(
                        conversation: props.conversation
                )
        )
    }


    private func rightActionsView(props: ChatListRowProps) -> some View {
        ChatListRightActionsView(
                props: ChatListRightActionsViewProps(
                        isMuted: props.conversation.isMuted,
                        isArchived: props.conversation.isArchived,
                        onMute: props.onMute,
                        onDelete: props.onDelete,
                        onArchive: props.onArchive,
                        contentViewWidth: { width in
                            widthToShowRightActionsView = width
                        }
                )
        )
    }

    private func leftActionsView(props: ChatListRowProps) -> some View {
        ChatListLeftActionsView(
                props: ChatListLeftActionsViewProps(
                        isArchived: props.conversation.isArchived,
                        isRead: !props.conversation.isUnread,
                        isPinned: props.conversation.isPinned,
                        onRead: { isRead in
                            isRead ? props.onRead() : props.onUnread()
                        },
                        onPin: props.onPin,
                        contentViewWidth: { width in
                            widthToShowLeftActionsView = width
                        }
                )
        )
    }

    private func getOffset(props: ChatListRowProps) -> CGSize {
        switch props.state {
        case .normal:
            return offset
        case .edit(_):
            DispatchQueue.main.async {
                offset = .zero
            }
            return .zero
        }
    }
}

struct ChatListRow_Previews: PreviewProvider {
    static var previews: some View {
        ChatListRow(
                props: ChatListRowProps(
                        conversation: ConversationViewModel(
                                id: "2",
                                avatar: .name(firstName: "Alex", lastName: "Sir", company: ""),
                                title: "Alex Sir",
                                conversationType: .dual,
                                numberOfSim: 1,
                                isShowSimNumber: true,
                                message: "Hello!",
                                messageType: .text,
                                messageStatus: .deliverd,
                                date: Date(),
                                isArchived: true,
                                isUnread: true,
                                unreadCount: 0,
                                dateOfPin: nil,
                                isMuted: false),
                        state: .normal,
                        onMute: { _ in },
                        onDelete: {},
                        onArchive: { _ in },
                        onRead: {},
                        onUnread: {},
                        onPin: { _ in }
                )
        )
    }
}
