import SwiftUI

struct ChatsPageView: View {
    @State private var searchText = ""
    @State private var defaultListFrame: CGRect = .zero
    @State private var isShowFilteredView = false
    @State private var isShowArchivedConversations = false
    @State private var isShowFilteredActionSheet = false
    @State private var actionSheet: ChatsPageViewActionSheetType?
    @State private var isShowHiddenArchivedConversationsView = false
    @State private var isInEditMode = false {
        didSet {
            props.onSwitchEditMode(isInEditMode, bottomActions)
        }
    }
    @State private var conversationIdsSelected = [String]() {
        didSet {
            props.onSwitchEditMode(isInEditMode, bottomActions)
        }
    }
    @State private var bottomActions: [BottomTabbarActionViewModel] = []{
        didSet {
            if oldValue != bottomActions {
                props.onSwitchEditMode(isInEditMode, bottomActions)
            }
        }
    }
    
    private let filteredViewHeight: CGFloat = 44
    private let rowHeight: CGFloat = 76
    let props: ChatsPageViewProps

    var body: some View {
        ZStack {
            updateBottomActions(props: props)
            ScrollView {
                VStack(spacing: 0) {
                    searchBar()
                    filterButton(props: props)
                    Divider()
                    archive(props: props)
                    ZStack {
                        list(props: props)
                        if isShowHiddenArchivedConversationsView {
                            hiddenArchivedConversationsView()
                        }
                    }
                }
                        .background(collectListFrame())
            }
            if props.conversations.isEmpty && props.archive == nil {
                emptyContactView()
            }
        }
                .navigationBarTitle(Text("Messages"), displayMode: .large)
                .toolbar(content: {
                    ToolbarItem(placement: .principal) {
                        defaultNumber(props: props)
                    }
                })
                .navigationBarItems(
                        leading: leadingButton(props: props),
                        trailing: isInEditMode ? nil : newMessButton(props: props)
                )
                .actionSheet(item: $actionSheet) { item in
                    switch item {
                    case .filter:
                        return filterActionSheet(props: props)
                    case .delete(let ids):
                        return deleteActionSheet(props: props, ids: ids)
                    }
                }
    }
    
    private func updateBottomActions(props: ChatsPageViewProps) -> some View{
        let actions = getBottomActions(props: props)
        DispatchQueue.main.async {
            self.bottomActions = actions
        }
        return EmptyView()
    }

    private func searchBar() -> some View {
        SearchBarView(
                props: SearchBarViewProps(
                        query: $searchText,
                        placeHolder: "Search",
                        onFocus: { isFocus in

                        }
                )
        )
                .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: Spacing.spacing14Pt, trailing: Spacing.spacing16Pt))
    }

    private func archive(props: ChatsPageViewProps) -> some View {
        if let archive = props.archive, isInEditMode == false {
            let isShowed = archive.isPinned || isShowArchivedConversations
            return AnyView(
                    ChatListArchiveRow(
                            props: ChatListArchiveRowProps(
                                    viewModel: archive,
                                    onHide: onHideArchivedConversationsRow,
                                    onPin: props.onPinArchivedConversations
                            )
                    )
                            .frame(height: isShowed ? rowHeight : 0)
                            .opacity(isShowed ? 1 : 0)
                            .onTapGesture {
                                props.onSelectedArchivedConvarsations()
                            }
            )
        }
        return AnyView(EmptyView())
    }

    private func list(props: ChatsPageViewProps) -> some View {
        LazyVStack {
            ForEach(props.conversations, id: \.self.id) { conversation in
                ChatListRow(
                        props: ChatListRowProps(
                                conversation: conversation,
                                state: isInEditMode ? .edit(conversationIdsSelected.contains(conversation.id)) : .normal,
                                onMute: { isMute in
                                    props.onMute(isMute, conversation.id)
                                },
                                onDelete: {
                                    actionSheet = .delete([conversation.id])
                                },
                                onArchive: { isArchived in
                                    if isArchived {
                                        props.onArchive([conversation.id])
                                    }
                                },
                                onRead: {
                                    props.onRead([conversation.id])
                                },
                                onUnread: {
                                    props.onUnread(conversation.id)
                                },
                                onPin: { date in
                                    props.onPin(date, conversation.id)
                                }
                        )
                )
                        .onTapGesture {
                            if isInEditMode {
                                if let index = conversationIdsSelected.firstIndex(where: { $0 == conversation.id }) {
                                    conversationIdsSelected.remove(at: index)
                                } else {
                                    conversationIdsSelected.append(conversation.id)
                                }
                            } else {
                                props.onSelectedConversation(conversation.id)
                            }
                        }
            }
        }
    }

    private func defaultNumber(props: ChatsPageViewProps) -> some View {
        if let defaultPhone = props.accountInfo.defaultPhone {
            return AnyView(
                    PhoneNumberToolBar(
                            props: PhoneNumberToolBarProps(
                                    flagImageName: ChatsPageViewHelper.getFlagImageName(
                                            phoneCode: defaultPhone.phoneCode,
                                            countries: props.countries),
                                    phoneNumber: defaultPhone.phoneNumber,
                                    onSelectPhoneNumber: props.onSelectDefaultNumber
                            )
                    )
            )
        } else {
            return AnyView(EmptyView())
        }
    }

    private func newMessButton(props: ChatsPageViewProps) -> some View {
        Button(action: props.onCreateNewChat) {
            Image(systemName: "square.and.pencil")
                    .textStyle(.body3Button1Light5Blue)
        }
    }

    private func leadingButton(props: ChatsPageViewProps) -> some View {
        Button(action: {
            isInEditMode = !isInEditMode
            if !isInEditMode {
                conversationIdsSelected.removeAll()
            }
        }) {
            Text(isInEditMode ? "Done" : "Edit")
                    .textStyle(.body3Button1Light5Blue)
        }
    }

    private func readAllBottomAction(props: ChatsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ChatPageEditActionType.readAll.id,
                title: ChatPageEditActionType.readAll.title,
                isDisable: false,
                color: nil,
                action: { _ in
                    props.onRead(props.conversations.filter({ $0.isUnread }).map({ $0.id }))
                }
        )
    }

    private func readBottomAction(props: ChatsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ChatPageEditActionType.read.id,
                title: ChatPageEditActionType.read.title,
                isDisable: ConversationsHelper.checkReadButtonInTabbarIsDisable(conversations: props.conversations, idsSelected: conversationIdsSelected),
                color: nil,
                action: { _ in
                    props.onRead(conversationIdsSelected)
                    conversationIdsSelected.removeAll()
                }
        )
    }

    private func archiveBottomAction(props: ChatsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ChatPageEditActionType.archive.id,
                title: ChatPageEditActionType.archive.title,
                isDisable: conversationIdsSelected.isEmpty,
                color: nil,
                action: { _ in
                    props.onArchive(conversationIdsSelected)
                    conversationIdsSelected.removeAll()
                }
        )
    }

    private func deleteBottomAction(props: ChatsPageViewProps) -> BottomTabbarActionViewModel {
        BottomTabbarActionViewModel(
                id: ChatPageEditActionType.delete.id,
                title: ChatPageEditActionType.delete.title,
                isDisable: conversationIdsSelected.isEmpty,
                color: nil,
                action: { _ in
                    actionSheet = .delete(conversationIdsSelected)
                }
        )
    }

    private func filterButton(props: ChatsPageViewProps) -> some View {
        PhoneNumberFilterView(
                props: PhoneNumberFilterViewProps(
                        filterType: props.filterType,
                        onSelected: {
                            actionSheet = .filter
                        }
                )
        )
                .frame(height: isShowFilteredView ? filteredViewHeight : 0)
                .opacity(isShowFilteredView ? 1 : 0)
    }

    private func hiddenArchivedConversationsView() -> some View {
        VStack {
            HiddenArchivedConversationsView()
            Spacer()
        }
    }

    private func emptyContactView() -> some View {
        VStack(spacing: Spacing.spacing8Pt) {
            Spacer()
            Text("No Messages")
                    .textStyle(.title21Default1Light2SecondaryLabelColor)
                    .multilineTextAlignment(.center)
            Text("Tap on \(Image(systemName: "square.and.pencil")) to start a new chat. ")
                    .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                    .multilineTextAlignment(.center)
            Spacer()
        }
                .frame(maxHeight: UIScreen.main.bounds.height)
                .edgesIgnoringSafeArea(.all)
                .animation(nil)
    }

    private func filterActionSheet(props: ChatsPageViewProps) -> ActionSheet {
        ActionSheet(
                title: Text("Filter My Contacts by"),
                buttons: [
                    .default(Text("All Numbers")) {
                        props.onFilter(.allNumbers)
                    },
                    .default(Text("Default Number")) {
                        props.onFilter(.defaultNumber)
                    },
                    .cancel()
                ]
        )
    }

    private func deleteActionSheet(props: ChatsPageViewProps, ids: [String]) -> ActionSheet {
        ActionSheet(
                title: Text(ConversationsHelper.getTitleOfDeleteActionSheet(conversations: props.conversations, idsSelected: ids)),
                buttons: [
                    .default(Text("Clear")) {
                        props.onClear(ids)
                        conversationIdsSelected.removeAll()
                    },
                    .destructive(Text("Delete")) {
                        props.onDelete(ids)
                        conversationIdsSelected.removeAll()
                    },
                    .cancel()
                ]
        )
    }

    private func collectListFrame() -> some View {
        GeometryReader { geometry -> Color in
            if isInEditMode {
                DispatchQueue.main.async {
                    isShowFilteredView = false
                }
                return Color.clear
            }
            let rect = geometry.frame(in: .global)
            DispatchQueue.main.async {
                if defaultListFrame == .zero && rect.origin.y > 0 {
                    defaultListFrame = rect
                }
                if (rect.origin.y - defaultListFrame.origin.y > filteredViewHeight) {
                    isShowFilteredView = true
                    isShowArchivedConversations = true
                } else if (rect.origin.y - defaultListFrame.origin.y < -filteredViewHeight * 3) {
                    isShowFilteredView = false
                    isShowArchivedConversations = false
                }
            }
            return Color.clear
        }
    }

    private func getBottomActions(props: ChatsPageViewProps) -> [BottomTabbarActionViewModel] {
        if ConversationsHelper.checkIsShowReadAllInTabbar(conversations: props.conversations, idsSelected: conversationIdsSelected) {
            return bottomActionsWithReadAll()
        } else {
            return bottomActionWithRead()
        }
    }

    private func bottomActionsWithReadAll() -> [BottomTabbarActionViewModel] {
        [readAllBottomAction(props: props),
         archiveBottomAction(props: props),
         deleteBottomAction(props: props)]
    }

    private func bottomActionWithRead() -> [BottomTabbarActionViewModel] {
        [readBottomAction(props: props),
         archiveBottomAction(props: props),
         deleteBottomAction(props: props)]
    }

    private func onHideArchivedConversationsRow() {
        isShowArchivedConversations = false
        props.onHideArchivedConversations()
        withAnimation {
            isShowHiddenArchivedConversationsView = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                withAnimation {
                    isShowHiddenArchivedConversationsView = false
                }
            }
        }
    }
}

struct ChatsPageView_Previews: PreviewProvider {
    static var previews: some View {
        ChatsPageView(
                props: ChatsPageViewProps(
                        accountInfo: AccountInfo(
                                phones: [
                                    PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                                account: Account(id: 1, name: "Account name", createDate: Date()),
                                devices: []),
                        filterType: .allNumbers,
                        countries: CountryFetcher().countries,
                        archive: nil,
                        conversations: [],
                        onFilter: { _ in },
                        onSelectDefaultNumber: {},
                        onMute: { _, _ in },
                        onDelete: { _ in },
                        onClear: { _ in },
                        onArchive: { _ in },
                        onRead: { _ in },
                        onUnread: { _ in },
                        onPin: { _, _ in },
                        onCreateNewChat: {},
                        onSwitchEditMode: { _, _ in },
                        onSelectedArchivedConvarsations: {},
                        onHideArchivedConversations: {},
                        onPinArchivedConversations: {},
                        onSelectedConversation: { _ in }
                )
        )
    }
}
