import Foundation
import SwiftUI

struct ChatsPageViewProps {
    let accountInfo: AccountInfo
    let filterType: PhoneNumberFilterType
    let countries: [Country]
    let archive: ChatListArchivedViewModel?
    let conversations: [ConversationViewModel]
    let onFilter: (PhoneNumberFilterType) -> Void
    let onSelectDefaultNumber: () -> Void
    let onMute: (_ isMute: Bool, _ id: String) -> Void
    let onDelete: (_ ids: [String]) -> Void
    let onClear: (_ ids: [String]) -> Void
    let onArchive: (_ ids: [String]) -> Void
    let onRead: (_ ids: [String]) -> Void
    let onUnread: (_ id: String) -> Void
    let onPin: (_ date: Date?, _ id: String) -> Void
    let onCreateNewChat: () -> Void
    let onSwitchEditMode: (_ isEditMode: Bool, _ items: [BottomTabbarActionViewModel]) -> Void
    let onSelectedArchivedConvarsations: () -> Void
    let onHideArchivedConversations: () -> Void
    let onPinArchivedConversations: () -> Void
    let onSelectedConversation: (_ id: String) -> Void
}
