import Foundation

struct ChatListArchiveRowProps {
    let viewModel: ChatListArchivedViewModel
    let onHide: () -> Void
    let onPin: () -> Void
}
