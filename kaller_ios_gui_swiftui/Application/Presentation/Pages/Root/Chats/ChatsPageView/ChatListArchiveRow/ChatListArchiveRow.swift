import SwiftUI

struct ChatListArchiveRow: View {
    let props: ChatListArchiveRowProps
    @State private var offset: CGSize = .zero
    @State private var isInRightActionsView = false
    @State private var isInLeftActionsView = false
    @State private var widthToShowRightActionsView: CGFloat = 0
    @State private var widthToShowLeftActionsView: CGFloat = 0

    var body: some View {
        ZStack {
            leftAction(props: props)
                    .offset(x: -UIScreen.main.bounds.width)
            HStack(spacing: 0) {
                icon()
                info(props: props)
            }
                    .frame(width: UIScreen.main.bounds.width, height: 76)
                    .background(Color.white)
            rightAction(props: props)
                    .offset(x: UIScreen.main.bounds.width)
        }
                .offset(x: offset.width)
                .gesture(
                        DragGesture()
                                .onChanged(onDrag)
                                .onEnded({ _ in onEndDrag() })
                )
    }

    private func icon() -> some View {
        AvatarView(
                props: .init(
                        size: 44,
                        style: .whiteGlyph(
                                systemImageName: "archivebox.fill"
                        )
                )
        )
                .padding(.leading, Spacing.spacing16Pt)
    }

    private func info(props: ChatListArchiveRowProps) -> some View {
        ConversationRowInfo(
                props: ConversationRowInfoProps(
                        title: "Archived Conversations",
                        status: props.viewModel.messageStatus,
                        time: props.viewModel.date,
                        isMuted: false,
                        isPinned: false,
                        isUnread: props.viewModel.isUnread,
                        unreadCount: props.viewModel.unreadCount,
                        message: props.viewModel.content,
                        messageType: nil
                )
        )
    }

    private func rightAction(props: ChatListArchiveRowProps) -> some View {
        SimpleActionsRow(
                props: SimpleActionsRowProps(
                        items: [
                            .init(id: "right",
                                    title: "\(props.viewModel.isPinned ? "Hide" : "Pin")",
                                    color: .blue800,
                                    icon: .sytemImageName("\(props.viewModel.isPinned ? "eye.slash.fill" : "pin.fill")"),
                                    action: onHideOrPin
                            )
                        ],
                        alignment: .leading,
                        contentViewWidth: { width in
                            widthToShowRightActionsView = width
                        }
                )
        )
    }

    private func leftAction(props: ChatListArchiveRowProps) -> some View {
        SimpleActionsRow(
                props: SimpleActionsRowProps(
                        items: [
                            .init(id: "left",
                                    title: "\(props.viewModel.isPinned ? "Hide" : "Pin")",
                                    color: .blue800,
                                    icon: .sytemImageName("\(props.viewModel.isPinned ? "eye.slash.fill" : "pin.fill")"),
                                    action: onHideOrPin
                            )
                        ],
                        alignment: .trailing,
                        contentViewWidth: { width in
                            widthToShowLeftActionsView = width
                        }
                )
        )
    }

    private func onDrag(gesture: DragGesture.Value) {
        if isInRightActionsView {
            let newOffsetWidth = -widthToShowRightActionsView + gesture.translation.width
            if newOffsetWidth <= 0 {
                offset.width = newOffsetWidth
            }
        } else if isInLeftActionsView {
            let newOffsetWidth = widthToShowLeftActionsView + gesture.translation.width
            if newOffsetWidth > 0 {
                offset.width = newOffsetWidth
            }
        } else if abs(gesture.translation.width) > abs(gesture.translation.height) {
            offset = gesture.translation
        }
    }

    private func onEndDrag() {
        if offset.width > UIScreen.main.bounds.width * 2 / 3 {
            withAnimation {
                offset = CGSize(width: UIScreen.main.bounds.width, height: 0)
                onHideOrPin()
            }
        } else if offset.width < -UIScreen.main.bounds.width * 2 / 3 {
            withAnimation {
                offset = CGSize(width: -UIScreen.main.bounds.width, height: 0)
                onHideOrPin()
            }
        } else if offset.width < -widthToShowRightActionsView / 2 {
            withAnimation {
                isInRightActionsView = true
                offset = CGSize(width: -widthToShowRightActionsView, height: 0)
            }
        } else if offset.width > widthToShowLeftActionsView / 2 {
            withAnimation {
                isInLeftActionsView = true
                offset = CGSize(width: widthToShowLeftActionsView, height: 0)
            }
        } else {
            withAnimation {
                isInRightActionsView = false
                isInLeftActionsView = false
                self.offset = .zero
            }
        }
    }

    private func onHideOrPin() {
        isInRightActionsView = false
        isInLeftActionsView = false
        self.offset = .zero
        props.viewModel.isPinned ? props.onHide() : props.onPin()
    }
}

struct ChatListArchiveRow_Previews: PreviewProvider {
    static var previews: some View {
        ChatListArchiveRow(
                props: ChatListArchiveRowProps(
                        viewModel: ChatListArchivedViewModel(
                                date: Date(),
                                isUnread: true,
                                unreadCount: 2,
                                isPinned: true,
                                content: "Alex, Eugene, Valeriy",
                                messageStatus: nil
                        ),
                        onHide: {},
                        onPin: {}
                )
        )
    }
}
