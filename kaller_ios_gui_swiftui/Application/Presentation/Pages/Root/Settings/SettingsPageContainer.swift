import SwiftUI

struct SettingsPageContainerProps {

}

struct SettingsPageContainer: View {
    let props: SettingsPageContainerProps

    var body: some View {
        NavigationView {
            SettingsPageView(
                    props: SettingsPageViewProps()
            )
        }
    }
}