import SwiftUI

struct RootContainer: View {
    enum OverlayPageType {
        case selectUserDefaultNumber(makeCall: (PhoneInfo?) -> Void, makeMessage: (PhoneInfo?) -> Void)
        case selectUserCurrentNumber(isFree: Bool, makeCall: (PhoneInfo?) -> Void, makeMessage: (PhoneInfo?) -> Void)
        case recentDetails(recent: RecentViewModel)
        case selectDefaultPhoneNumber
        case bottomTabbarAction(items: [BottomTabbarActionViewModel])
        case none
    }

    @State private var overlayPageType = OverlayPageType.none

    var body: some View {
        appContent()
                .overlay(
                        overlayPage()
                                .ignoresSafeArea()
                )
    }

    private func openOverlayPage(type: OverlayPageType) {
        overlayPageType = type
    }

    private func closeOverlayPage() {
        withAnimation {
            overlayPageType = .none
        }
    }

    private func appContent() -> some View {
        TabView {
            keypadPage()
                    .tabItem { Image(systemName: "1.square.fill"); Text("Keypad") }
                    .tag(0)

            recentsPage()
                    .tabItem { Image(systemName: "2.square.fill"); Text("Recents") }
                    .tag(1)

            contactsPage()
                    .tabItem { Image(systemName: "3.square.fill"); Text("Contacts") }
                    .tag(2)

            chatsPage()
                    .tabItem { Image(systemName: "4.square.fill"); Text("Messages") }
                    .tag(3)

            settingsPage()
                    .tabItem { Image(systemName: "5.square.fill"); Text("Settings") }
                    .tag(4)
        }
    }

    private func keypadPage() -> some View {
        KeypadPageContainer(
                props: KeypadPageContainerProps(
                        openSelectDefaultNumberPage: { (makeCall, makeMessage) in
                            openOverlayPage(type: .selectUserDefaultNumber(makeCall: makeCall, makeMessage: makeMessage))
                        },
                        openSelectCurrentNumber: { (isFree, makeCall, makeMessage) in
                            openOverlayPage(type: .selectUserCurrentNumber(isFree: isFree, makeCall: makeCall, makeMessage: makeMessage))
                        }
                )
        )
    }

    private func recentsPage() -> some View {
        RecentsPageContainer(
                props: RecentsPageContainerProps(
                        openRecentDetails: { recent in
                            openOverlayPage(type: .recentDetails(recent: recent))
                        }
                )
        )
    }

    private func contactsPage() -> some View {
        ContactsPageContainer(
            props: ContactsPageContainerProps(
                onSelectDefaultPhoneNumber: {
                    openOverlayPage(type: .selectDefaultPhoneNumber)
                }
            )
        )
    }

    private func chatsPage() -> some View {
        ChatsPageContainer(
            props: ChatsPageContainerProps(
                onSelectDefaultNumber: {
                    openOverlayPage(type: .selectDefaultPhoneNumber)
                },
                onSwitchEditMode: { isEdit, items  in
                    if isEdit {
                        openOverlayPage(type: .bottomTabbarAction(items: items))
                    } else {
                        closeOverlayPage()
                    }
                }
            )
        )
    }

    private func settingsPage() -> some View {
        SettingsPageContainer(
                props: SettingsPageContainerProps()
        )
    }

    private func selectCurrentNumberPage(
            isFree: Bool,
            makeCall: @escaping (PhoneInfo?) -> Void,
            makeMessage: @escaping (PhoneInfo?) -> Void
    ) -> some View {
        SelectCurrentNumberPageContainer(
                props: SelectCurrentNumberPageContainerProps(
                        isFreeCall: isFree,
                        onClose: closeOverlayPage,
                        onMakeCall: { phoneInfoSelected in
                            makeCall(phoneInfoSelected)
                            closeOverlayPage()
                        },
                        onMakeMessage: { phoneInfoSelected in
                            makeMessage(phoneInfoSelected)
                            closeOverlayPage()
                        }
                )
        )
    }

    private func selectDefaultNumberPage(
        makeCall: @escaping (PhoneInfo?) -> Void,
        makeMessage: @escaping (PhoneInfo?) -> Void
    ) -> some View {
        SelectDefaultNumberPageContainer(
            props: SelectDefaultNumberPageContainerProps(
                onClose: closeOverlayPage,
                onMakeCall: { phoneInfoSelected in
                    makeCall(phoneInfoSelected)
                    closeOverlayPage()
                },
                onMakeMessage: { phoneInfoSelected in
                    makeMessage(phoneInfoSelected)
                    closeOverlayPage()
                },
                onSelectedDefalutNumber: {
                    openOverlayPage(type: .selectDefaultPhoneNumber)
                }
            )
        )
    }

    private func overlayPage() -> some View {
        switch overlayPageType {
        case .selectUserCurrentNumber(let isFree, let makeCall, let makeMessage):
            return AnyView(
                    selectCurrentNumberPage(isFree: isFree, makeCall: makeCall, makeMessage: makeMessage)
            )
        case .selectUserDefaultNumber(let makeCall, let makeMessage):
            return AnyView(
                    selectDefaultNumberPage(makeCall: makeCall, makeMessage: makeMessage)
            )
        case .recentDetails(let recent):
            return AnyView(
                    RecentDetailsPageContainer(
                            props: RecentDetailsPageContainerProps(
                                recentViewModel: recent,
                                close: closeOverlayPage
                            )
                    )
            )
        case .selectDefaultPhoneNumber:
            return AnyView(
                SelectDefaultNumberContainer(
                    props: SelectDefaultNumberContainerProps(
                        onClose: closeOverlayPage
                    )
                )
            )
        case .bottomTabbarAction(let items):
            return AnyView(
                BottomTabbarActionView(
                    props: BottomTabbarActionViewProps(
                        items: items
                    )
                )
            )
        case .none:
            return AnyView(
                    EmptyView()
            )
        }
    }
}
