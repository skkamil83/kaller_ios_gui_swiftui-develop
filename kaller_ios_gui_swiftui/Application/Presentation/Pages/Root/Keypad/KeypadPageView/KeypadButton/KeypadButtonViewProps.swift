import SwiftUI

struct KeypadButtonProps{
    let size: CGFloat
    let style: Style
    let isHandleLongPress: Bool
    let isShadowWhenLongPress: Bool
    let onAction: () -> Void
    var onLongPressAction: (() -> Void)? = nil

    enum Style{
        case titleWithSubTitle(title: String, subtitle: String, normalColor: Color = .graysSystemGray5Light, highlightColor: Color = .graysSystemGray3Li2, textColor: Color = .black)
        case titleWithSystemImage(title: String, systemImageName: String, normalColor: Color = .graysSystemGray5Light, highlightColor: Color = .graysSystemGray3Li2, textColor: Color = .black)
        case image(normalState: String, highlightState: String, isResizeAble: Bool)
    }
}
