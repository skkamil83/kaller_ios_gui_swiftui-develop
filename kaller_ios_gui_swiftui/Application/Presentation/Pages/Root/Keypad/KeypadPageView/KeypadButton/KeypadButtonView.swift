import SwiftUI

struct KeypadButtonView: View{
    let props: KeypadButtonProps
    @State private var isLongPressed: Bool = false

    var body: some View {
        button()
                .buttonStyle(KeypadButtonStyle(props: props, isLongPressed: $isLongPressed))
                .shadow(color: (isLongPressed && props.isShadowWhenLongPress) ? Color.black.opacity(0.5) : .clear, radius: 5, x: 0, y: 0)
                .simultaneousGesture(
                        LongPressGesture()
                                .onEnded({ _ in
                                    if props.isHandleLongPress {
                                        isLongPressed = true
                                        props.onLongPressAction?()
                                    }
                                })
                )
    }

    private func button() -> some View {
        Button(
                action: {
                    isLongPressed
                            ? isLongPressed = false
                            : props.onAction()
                },
                label: {}
        )
    }
}

struct KeypadButtonStyle: ButtonStyle{
    let props: KeypadButtonProps
    @Binding var isLongPressed: Bool

    func makeBody(configuration: Configuration) -> some View {
        switch props.style{

        case .titleWithSubTitle(let title, let subtitle, let normalColor, let highlightColor, let textColor):
            return AnyView(
                    titleWithSubtitle(
                            title: title,
                            subtitle: subtitle,
                            configuration: configuration,
                            normalColor: normalColor,
                            highlightColor: highlightColor,
                            textColor: textColor
                    )
            )

        case .image(let normalState, let highlightState, let isResizeAble):
            return AnyView(
                    image(
                            normalState: normalState,
                            highlightState: highlightState,
                            isResizeAble: isResizeAble,
                            size: props.size,
                            configuration: configuration
                    )
            )
            
        case .titleWithSystemImage(let title, let systemImageName, let normalColor, let highlightColor, let textColor):
            return AnyView(
                titleWithSystemImage(
                    title: title,
                    systemImageName: systemImageName,
                    configuration: configuration,
                    normalColor: normalColor,
                    highlightColor: highlightColor,
                    textColor: textColor
                )
            )
        }
    }

    private func image(
            normalState: String,
            highlightState: String,
            isResizeAble: Bool,
            size :CGFloat,
            configuration: Configuration
    ) -> some View {
        VStack{}
                .frame(width: size, height: size)
                .background(
                        (isResizeAble ?
                                (isLongPressed ? Image(normalState) : configuration.isPressed ? Image(highlightState) : Image(normalState)).resizable() :
                                (isLongPressed ? Image(normalState) : configuration.isPressed ? Image(highlightState) : Image(normalState))
                        )
                                .aspectRatio(contentMode: .fit)
                )

    }

    private func titleWithSubtitle(title: String, subtitle: String, configuration: Configuration, normalColor: Color, highlightColor: Color, textColor: Color) -> some View {
        let titleTxtStyle = titleTextStyle(size: props.size, color: textColor)
        let subTitleTxtStyle = subTitleTextStyle(size: props.size, color: textColor)
        return ZStack{
            VStack{
                Text(title)
                        .frame(height: getTitleHeight(size: props.size))
                        .font(titleTxtStyle.font)
                        .foregroundColor(titleTxtStyle.color)
                        .minimumScaleFactor(0.1)
                        .padding(.top, getPaddingTopForTitle(size: props.size))
                Spacer()
            }
            VStack{
                Spacer()
                Text(subtitle)
                        .frame(height: getSubTitleHeight(size: props.size))
                        .font(subTitleTxtStyle.font)
                        .foregroundColor(subTitleTxtStyle.color)
                        .minimumScaleFactor(0.1)
                        .padding(.bottom, getPaddingBottomForSubTitle(size: props.size))
            }
        }
                .frame(width: props.size, height: props.size)
                .background(isLongPressed ? normalColor : configuration.isPressed ? highlightColor : normalColor)
                .cornerRadius(props.size/2)
    }
    
    private func titleWithSystemImage(title: String, systemImageName: String, configuration: Configuration, normalColor: Color, highlightColor: Color, textColor: Color) -> some View {
        let titleTxtStyle = titleTextStyle(size: props.size, color: textColor)
        let subTitleTxtStyle = subTitleTextStyle(size: props.size, color: textColor)
        return ZStack{
            VStack{
                Text(title)
                        .frame(height: getTitleHeight(size: props.size))
                        .font(titleTxtStyle.font)
                        .foregroundColor(titleTxtStyle.color)
                        .minimumScaleFactor(0.1)
                        .padding(.top, getPaddingTopForTitle(size: props.size))
                Spacer()
            }
            VStack{
                Spacer()
                Image(systemName: systemImageName)
                        .frame(height: getSubTitleHeight(size: props.size))
                        .font(subTitleTxtStyle.font)
                        .foregroundColor(subTitleTxtStyle.color)
                        .minimumScaleFactor(0.1)
                        .padding(.bottom, getPaddingBottomForSubTitle(size: props.size))
            }
        }
                .frame(width: props.size, height: props.size)
                .background(isLongPressed ? normalColor : configuration.isPressed ? highlightColor : normalColor)
                .cornerRadius(props.size/2)
    }

    private func getTitleHeight(size: CGFloat) -> CGFloat{
        41 * size / 75
    }

    private func getSubTitleHeight(size: CGFloat) -> CGFloat{
        13 * size / 75
    }

    private func getPaddingTopForTitle(size: CGFloat) -> CGFloat{
        11 * size / 75
    }

    private func getPaddingBottomForSubTitle(size: CGFloat) -> CGFloat{
        14 * size / 75
    }
    
    private func titleTextStyle(size: CGFloat, color: Color) -> TextStyle{
        return TextStyle(
            fontName: "SFProDisplay-Regular",
            fontSize: Float(getTitleHeight(size: size) * 36 / 41),
            color: color
        )
    }
    
    private func subTitleTextStyle(size: CGFloat, color: Color) -> TextStyle{
        return TextStyle(
            fontName: "SFProText-Bold",
            fontSize: Float(getSubTitleHeight(size: size) * 10 / 13),
            color: color
        )
    }
}
