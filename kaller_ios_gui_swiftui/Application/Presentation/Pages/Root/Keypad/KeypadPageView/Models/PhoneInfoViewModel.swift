struct PhoneInfoViewModel {
    let phoneNumber: String
    let countryFlagImageName: String?
    let name: String?
    let place: String?
}