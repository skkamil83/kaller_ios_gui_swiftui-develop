struct KeypadPageViewProps{
    let phoneFromInfo: PhoneInfoViewModel?
    let phoneToInfo: PhoneInfoViewModel?
    let pricing: Float
    let onNumTap: (String) -> Void
    let onBackSpaceTap: () -> Void
    let onBackSpaceLongTap: () -> Void
    let onCall: () -> Void
    let onLongPressCallButton: () -> Void
    let onCreateNewContact: () -> Void
    let onAddToExistingContact: () -> Void
    let onSelectPhoneTo: () -> Void
}
