struct KeypadViewProps {
    let style: Style
    let onCallingAction: () -> Void
    let onBackSpaceAction: () -> Void
    var onCallingLongPressAction: (() -> Void)? = nil
    var onBackSpaceLongPressAction: (() -> Void)? = nil
    let onTypingAction: (String) -> Void

    enum Style{
        case free
        case unFree(price: String)
    }
}