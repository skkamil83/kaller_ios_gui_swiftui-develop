import SwiftUI

struct KeypadView: View {
    let props: KeypadViewProps

    var body: some View {
        let buttonSize = getButtonSize()

        VStack(spacing: Spacing.keyHorizontalFix){
            HStack(spacing: Spacing.keyVerticalFix){
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "1", subtitle: ""),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "1")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "2", subtitle: "A B C"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "2")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "3", subtitle: "D E F"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "3")
                                }))
            }

            HStack(spacing: Spacing.keyVerticalFix){
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "4", subtitle: "G H I"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "4")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "5", subtitle: "J K L"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "5")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "6", subtitle: "M N O"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "6")
                                }))
            }

            HStack(spacing: Spacing.keyVerticalFix){
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "7", subtitle: "P Q R S"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "7")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "8", subtitle: "T U V"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "8")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .titleWithSubTitle(title: "9", subtitle: "W X Y Z"),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "9")
                                }))
            }

            HStack(spacing: Spacing.keyVerticalFix){
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .image(normalState: "systemDialerButtonAsterisk", highlightState: "systemDialerButtonAsteriskPressed", isResizeAble: true),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "*")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                            style: .titleWithSystemImage(title: "0", systemImageName: "plus"),
                                isHandleLongPress: true,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "0")
                                },
                                onLongPressAction: {
                                    onTypingAction(text: "+")
                                }))
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .image(normalState: "systemDialerButtonOctocorp", highlightState: "systemDialerButtonOctocorpPressed", isResizeAble: true),
                                isHandleLongPress: false,
                                isShadowWhenLongPress: false,
                                onAction: {
                                    onTypingAction(text: "#")
                                }))
            }

            HStack(spacing: Spacing.keyVerticalFix){
                Text(getPriceText(props: props))
                        .frame(width: buttonSize, height: buttonSize, alignment: .center)
                        .font(TextStyle.body1Default1Light1LabelColor2CenterAligned.font)
                        .foregroundColor(TextStyle.body1Default1Light1LabelColor2CenterAligned.color)
                        .minimumScaleFactor(0.1)
                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .image(normalState: getCallButtonImageInNormal(props: props), highlightState: getCallButtonImageInHighLight(props: props), isResizeAble: true),
                                isHandleLongPress: true,
                                isShadowWhenLongPress: false,
                                onAction: props.onCallingAction,
                                onLongPressAction: props.onCallingLongPressAction
                        ))

                KeypadButtonView(
                        props: KeypadButtonProps(
                                size: buttonSize,
                                style: .image(normalState: "systemDialerButtonBackspace", highlightState: "systemDialerButtonBackspacePressed", isResizeAble: false),
                                isHandleLongPress: true,
                                isShadowWhenLongPress: false,
                                onAction: props.onBackSpaceAction,
                                onLongPressAction: props.onBackSpaceLongPressAction
                        ))
            }
        }
    }

    private func onTypingAction(text: String){
        props.onTypingAction(text)
    }

    private func getCallButtonImageInNormal(props: KeypadViewProps) -> String{
        switch props.style{
        case .free:
            return "systemDialerButtonCallFree"
        case .unFree:
            return "systemDialerButtonCall"
        }
    }

    private func getCallButtonImageInHighLight(props: KeypadViewProps) -> String{
        switch props.style{
        case .free:
            return "systemDialerButtonCallFreePressed"
        case .unFree:
            return "systemDialerButtonCallPressed"
        }
    }

    private func getPriceText(props: KeypadViewProps) -> String{
        switch props.style{
        case .free:
            return ""
        case .unFree(price: let price):
            return price
        }
    }

    private func getButtonSize() -> CGFloat{
        return (UIScreen.main.bounds.width - (48 * 2) - (27 * 2))/3
    }

}
