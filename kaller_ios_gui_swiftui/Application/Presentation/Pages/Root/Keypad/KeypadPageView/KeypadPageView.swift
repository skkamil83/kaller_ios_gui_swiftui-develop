import SwiftUI

struct KeypadPageView: View {
    let props: KeypadPageViewProps
    @State private var isShowingActionSheet: Bool = false

    var body: some View {
        GeometryReader(content: { geometry in
            VStack{
                VStack{
                    phoneTo()
                    Spacer()
                    phoneToInfo()
                }
                        .frame(height: Spacing.keypadVertical72Pt)
                .padding(EdgeInsets(top: getPaddingTopAndBottom(layoutHeight: geometry.size.height), leading: 32, bottom: 0, trailing: 32))

                Spacer()

                keypad()
                        .padding(.bottom, getPaddingTopAndBottom(layoutHeight: geometry.size.height))
            }
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .center)
                    .actionSheet(isPresented: $isShowingActionSheet, content: {
                        actionSheet()
                    })
                    .toolbar(content: {
                        ToolbarItem(placement: .principal) {
                            phoneFromInfo()
                        }

                    }).animation(nil)

        })
    }

    private func phoneFromInfo() -> some View {
        PhoneNumberToolBar(
                props: PhoneNumberToolBarProps(
                        flagImageName: props.phoneFromInfo?.countryFlagImageName,
                        phoneNumber: props.phoneFromInfo?.phoneNumber,
                        onSelectPhoneNumber: props.onSelectPhoneTo
                )
        )
    }

    private func keypad() -> some View {
        KeypadView(
                props: KeypadViewProps(
                        style: props.pricing == 0 ? . free : .unFree(price: "$\(props.pricing)"),
                        onCallingAction: props.onCall,
                        onBackSpaceAction: props.onBackSpaceTap,
                        onCallingLongPressAction: props.onLongPressCallButton,
                        onBackSpaceLongPressAction: props.onBackSpaceLongTap,
                        onTypingAction: { text in
                            props.onNumTap(text)
                        }
                )
        )
    }

    private func phoneToInfo() -> some View {
        KeypadPhoneNumberInfoView(
                props: KeypadPhoneNumberInfoViewProps(
                        style: getPhoneNumberInfoStyle(phoneToInfo: props.phoneToInfo),
                        actionAddNumber: {
                            isShowingActionSheet.toggle()
                        }))
    }

    private func phoneTo() -> some View {
        Text(props.phoneToInfo?.phoneNumber ?? "")
                .frame(height: Spacing.keypadVerticalPhoneNumber)
                .font(TextStyle.keypadNumber.font)
                .foregroundColor(TextStyle.keypadNumber.color)
                .truncationMode(.head)
                .minimumScaleFactor(28/36)
    }

    private func actionSheet() -> ActionSheet{
        ActionSheet(
                title:
                Text("Add to contact")
                        .font(TextStyle.footnote2Bold1Light2SecondaryLabelColor.font)
                        .foregroundColor(TextStyle.footnote2Bold1Light2SecondaryLabelColor.color),
                message: nil,
                buttons:
                [.default(
                        Text("Create New Contact")
                                .font(TextStyle.title31Default1Light5Blue.font)
                                .foregroundColor(TextStyle.title31Default1Light5Blue.color),
                        action: props.onCreateNewContact),
                    .default(
                            Text("Add to Existing Contact")
                                    .font(TextStyle.title31Default1Light5Blue.font)
                                    .foregroundColor(TextStyle.title31Default1Light5Blue.color),
                            action: props.onAddToExistingContact),
                    .cancel(
                            Text("Cancel")
                                    .font(TextStyle.title32Bold1Light5Blue.font)
                                    .foregroundColor(TextStyle.title32Bold1Light5Blue.color)
                    )]
        )

    }

    //todo move it to view helper
    private func getPhoneNumberInfoStyle(phoneToInfo: PhoneInfoViewModel?) -> KeypadPhoneNumberInfoViewProps.Style{
        if props.phoneToInfo?.phoneNumber == nil {
            return .none
        } else if
                let info = phoneToInfo,
                let flagImgName = info.countryFlagImageName
        {
            if let name = phoneToInfo?.name {
                return .flagWithName(
                        flagImageName: flagImgName,
                        name: name,
                        place: info.place ?? ""
                )
            } else {
                return .flagWithAddNumber(flagImageName: flagImgName)
            }

        } else {
            return .onlyAddNumber
        }
    }

    private func getKeypadHeight() -> CGFloat{
        return ((UIScreen.main.bounds.width - (48 * 2) - (27 * 2))/3) * 5 + 14 * 4
    }

    private func getPaddingTopAndBottom(layoutHeight: CGFloat) -> CGFloat{
        return 5 * (layoutHeight - getKeypadHeight() - 72)/17
    }
}
