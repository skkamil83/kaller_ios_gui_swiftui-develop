import SwiftUI

struct KeypadPhoneNumberInfoViewProps {
    let style: Style
    let actionAddNumber: () -> Void

    enum Style{
        case none
        case onlyAddNumber
        case flagWithAddNumber(flagImageName: String)
        case flagWithName(flagImageName: String, name: String, place: String)
    }
}
