import SwiftUI

struct KeypadPhoneNumberInfoView: View {
    let props: KeypadPhoneNumberInfoViewProps

    var body: some View {
        switch props.style{
        case .onlyAddNumber:
            return AnyView(
                    addNumber()
            )

        case .flagWithAddNumber(let flagImageName):
            return AnyView(
                    addNumberWithFlag(flagImageName: flagImageName)
            )

        case .flagWithName(let flagImageName, let name, let place):
            return AnyView(
                    flagWithName(flagImageName: flagImageName, name: name, place: place)
            )

        case .none:
            return AnyView(EmptyView())
        }
    }

    private func addNumber() -> some View {
        HStack{
            Button(
                    action: addNumberTap,
                    label: {
                        Text("Add Number")
                                .font(TextStyle.keypadUnknowContact.font)
                                .foregroundColor(TextStyle.keypadUnknowContact.color)
                    })
        }
    }

    private func addNumberWithFlag(flagImageName: String) -> some View {
        HStack{
            CircleIcon(props: CircleIconProps(size: Spacing.keypadHorizontalPhoneNameFlag, style: .filled(imageName: flagImageName)))
            Button(
                    action: addNumberTap,
                    label: {
                        Text("Add Number")
                                .font(TextStyle.keypadUnknowContact.font)
                                .foregroundColor(TextStyle.keypadUnknowContact.color)
                    })
        }
    }

    private func flagWithName(flagImageName: String, name: String, place: String) -> some View {
        HStack{
            CircleIcon(props: CircleIconProps(size: Spacing.keypadHorizontalPhoneNameFlag, style: .filled(imageName: flagImageName)))
            Text(name)
                    .foregroundColor(TextStyle.keypadPhoneName.color)
                    .font(TextStyle.keypadPhoneName.font)
                    + Text(" \(place)")
                    .foregroundColor(TextStyle.keypadPhoneLabel.color)
                    .font(TextStyle.keypadPhoneLabel.font)
        }
    }

    private func addNumberTap() {
        props.actionAddNumber()
    }
}