import SwiftUI

struct KeypadPageContainerProps {
    let openSelectDefaultNumberPage: (
            _ makeCall: @escaping (PhoneInfo?) -> Void,
            _ makeMessage: @escaping (PhoneInfo?) -> Void
    ) -> Void

    let openSelectCurrentNumber: (
            _ isFree: Bool,
            _ makeCall: @escaping (PhoneInfo?) -> Void,
            _ makeMessage: @escaping (PhoneInfo?) -> Void
    ) -> Void
}

struct KeypadPageContainer: View {
    let props: KeypadPageContainerProps

    private let pricingPlans : [Float] = [
        0,
        0.05,
        0.06
    ]

    @EnvironmentObject private var userInfoState: UserInfoState
    @EnvironmentObject private var contactsState: ContactsState
    @EnvironmentObject private var countryState: CountryState

    @State private var openCaller = false
    @State private var phoneToNumber: String = ""

    var body: some View {
        NavigationView {
            KeypadPageView(
                    props: KeypadPageViewProps(
                            phoneFromInfo: combinePhoneFromInfo(
                                    accountInfo: userInfoState.accountInfo,
                                    countries: countryState.countries
                            ),
                            phoneToInfo: detectContact(
                                    phoneNumber: phoneToNumber,
                                    allContacts: contactsState.localContacts,
                                    allCountries: countryState.countries
                            ),
                            pricing: detectPricing(
                                    fromNumber: getUserNumber()?.phoneNumber ?? "",
                                    toNumber: phoneToNumber,
                                    pricingPlans: pricingPlans
                            ),
                            onNumTap: addNumToPhoneToNumber,
                            onBackSpaceTap: removeNumFromPhoneToNumber,
                            onBackSpaceLongTap: cleanPhoneToNumber,
                            onCall: call,
                            onLongPressCallButton: showPhoneFromNumberSelection,
                            onCreateNewContact: createNewContact,
                            onAddToExistingContact: addPhoneToNumberToExistingContact,
                            onSelectPhoneTo: showSetDefaultPhoneToSelection
                    )
            )
                    .navigationBarTitle("", displayMode: .inline)
        }
    }

    private func getUserNumber() -> PhoneInfo? {
        userInfoState.accountInfo?.defaultPhone
    }

    private func getContactPhones() -> [PhoneInfo] {
        contactsState.localContacts.flatMap { $0.phones }
    }

    private func addNumToPhoneToNumber(text: String){
        phoneToNumber += text
    }

    private func removeNumFromPhoneToNumber(){
        phoneToNumber = String(phoneToNumber.dropLast())
    }

    private func cleanPhoneToNumber(){
        phoneToNumber = ""
    }

    //todo move it to helper
    private func combinePhoneFromInfo(accountInfo: AccountInfo?, countries: [Country]?) -> PhoneInfoViewModel? {
        guard let accountInfo = accountInfo else {
            //todo log error
            return nil
        }

        let defaultPhone = accountInfo.defaultPhone
        let country = countries?.first { $0.phoneCode == defaultPhone?.phoneCode }

        return PhoneInfoViewModel(
                phoneNumber: defaultPhone?.phoneNumber ?? "",
                countryFlagImageName: country?.flagImageName,
                name: defaultPhone?.nickName,
                place: defaultPhone?.place
        )
    }

    // TODO: move it to view helper and refactor logic
    private func detectContact(phoneNumber: String, allContacts: [Contact]?, allCountries: [Country]?) -> PhoneInfoViewModel? {
        //some logic here  (it's a clean helper func)
        // TODO extract normalization to helper

        let normalizedPhoneTo = phoneNumber.trimmingCharacters(in: .whitespaces)

        if normalizedPhoneTo.isEmpty {
            return nil
        }

        let contact = allContacts?
                .first { $0.phones.compactMap { $0.phoneNumber }
                        .contains(normalizedPhoneTo)}

        if
                let cont = contact,
                let phoneInfo = (cont.phones.first { $0.phoneNumber == phoneNumber })
        {
            let country = allCountries?
                    .first { $0.phoneCode == phoneInfo.phoneCode }

            return PhoneInfoViewModel(
                    phoneNumber: phoneToNumber,
                    countryFlagImageName: country?.flagImageName,
                    name: cont.firstName ?? "" + (cont.lastName ?? ""), //todo refactor and move to helper
                    place: phoneInfo.place
            )
        } else {
            if let country = detectCountryByPhone(normalizedPhoneTo) {
                return PhoneInfoViewModel(
                        phoneNumber: phoneToNumber,
                        countryFlagImageName: country.flagImageName,
                        name: nil,
                        place: nil
                )
            } else {
                return PhoneInfoViewModel(
                        phoneNumber: phoneToNumber,
                        countryFlagImageName: nil,
                        name: nil,
                        place: nil
                )
            }
        }
    }

    //todo implement logic and move it to helper
    private func detectCountryByPhone(_ normalizedPhone: String) -> Country? {
        if normalizedPhone.count > 2 && normalizedPhone.prefix(2) == "+1" {
            return countryState.countries?.first { $0.phoneCode == 1}
        }

        return nil
    }

    private func detectPricing(fromNumber: String, toNumber: String, pricingPlans: [Float]) -> Float {
        //do some logic here  (it's a clean helper func)

        if toNumber.prefix(2) == "+1" {
            return pricingPlans[1]
        }

        if toNumber.prefix(2) == "+7" {
            return pricingPlans[2]
        }

        return pricingPlans[0]
    }

    private func call(){
        print(#function)
        openCaller.toggle()
    }

    private func showPhoneFromNumberSelection(){
        print(#function)
        if let accountInfo = userInfoState.accountInfo{
            let price = detectPricing(
                fromNumber: getUserNumber()?.phoneNumber ?? "",
                toNumber: phoneToNumber,
                pricingPlans: pricingPlans
            )
            props.openSelectCurrentNumber(
                price == 0,
                { phoneInfoToMakeCall in
                    makeCall(phoneInfo: phoneInfoToMakeCall)
                },
                { phoneInfoToMakeMessage in
                    makeMessage(phoneInfo: phoneInfoToMakeMessage)
                }
            )
        }
    }

    private func showSetDefaultPhoneToSelection(){
        props.openSelectDefaultNumberPage(
            { phoneInfoToMakeCall in
                makeCall(phoneInfo: phoneInfoToMakeCall)
            },
            { phoneInfoToMakeMessage in
                makeMessage(phoneInfo: phoneInfoToMakeMessage)
            }
        )
    }

    private func createNewContact(){
        print("\(#function) \(phoneToNumber)")
    }

    private func addPhoneToNumberToExistingContact() {
        print("\(#function) \(phoneToNumber)")
    }
    
    private func makeCall(phoneInfo: PhoneInfo?) {
        print("\(#function) with \(phoneInfo?.phoneNumber ?? "")")
    }
    
    private func makeMessage(phoneInfo: PhoneInfo?) {
        print("\(#function) with \(phoneInfo?.phoneNumber ?? "")")
    }
}
