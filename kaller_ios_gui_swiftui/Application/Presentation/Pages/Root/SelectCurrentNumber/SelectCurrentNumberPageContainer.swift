import SwiftUI

struct SelectCurrentNumberPageContainerProps {
    let isFreeCall: Bool
    let onClose: () -> Void
    let onMakeCall: (PhoneInfo?) -> Void
    let onMakeMessage: (PhoneInfo?) -> Void
}

struct SelectCurrentNumberPageContainer: View {
    var props: SelectCurrentNumberPageContainerProps
    @EnvironmentObject private var userInfoState: UserInfoState

    var body: some View {
        MultiaccountSelectionView(
            props: MultiaccountSelectionViewProps(
                style: .selectPhoneInfo(props.isFreeCall),
                accountInfo: userInfoState.accountInfo!,
                onOutSideTap: props.onClose,
                onButtonCallTap: props.onClose,
                onSelectedCallingContact: props.onMakeCall,
                onSelectedMessageContact: props.onMakeMessage,
                onSelectedDefalutNumber: {}
            )
        )
    }
}
