enum ContactsPageViewActionSheetType: Identifiable {
    case filter
    case addContact

    var id: String {
        switch self {
        case .filter:
            return "filter"
        case .addContact:
            return "addContact"
        }
    }

}
