import SwiftUI

enum ContatctsPageTabbarType: String, Identifiable, CaseIterable {
    case myContacts
    case employees
    case clients

    var id: String {
        return self.rawValue
    }

    var title: String {
        switch self {
        case .myContacts:
            return "My Contacts"
        case .employees:
            return "Employees"
        case .clients:
            return "Clients"
        }
    }

    var emptyListTitle: String {
        switch self {
        case .myContacts:
            return "No Contacts"
        case .employees:
            return "No Employees"
        case .clients:
            return "No Clients"
        }
    }

    var emptyListSubtitle: String {
        switch self {
        case .myContacts:
            return "Private contacts visible only to you."
        case .employees:
            return "No Employees"
        case .clients:
            return "Client cards visible to all\nemployees of your company."
        }
    }

}
