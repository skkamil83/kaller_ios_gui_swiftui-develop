import SwiftUI

struct ContactsPageView: View {
    let props: ContactsPageViewProps

    @State private var isFocusSearchBar = false
    @State private var characterJumper: Character = "#"
    @State private var isShowJumpList = false
    @State private var searchText = ""
    @State private var tabbarType: ContatctsPageTabbarType = .myContacts
    @State private var isShowEmpty = false
    @State private var actionSheetType: ContactsPageViewActionSheetType?

    var body: some View {
        ZStack {
            list(props: props)
            if isShowJumpList && searchText.isEmpty {
                alphabetJumper()
            }
            if isFocusSearchBar && searchText.isEmpty {
                contentShader()
            }
            if isShowEmpty && searchText.isEmpty {
                emptyContactView()
            }
        }
                .toolbar(content: {
                    ToolbarItem(placement: .principal) {
                        header(props: props)
                    }
                })
                .navigationBarItems(trailing: addContactButton())
                .navigationBarHidden(isFocusSearchBar)
                .navigationBarTitle("Contacts", displayMode: .large)
                .actionSheet(item: $actionSheetType) { type in
                    switch type {
                    case .filter:
                        return filterActionSheet(props: props)
                    case .addContact:
                        return addContactActionSheet(props: props)
                    }
                }
    }

    private func header(props: ContactsPageViewProps) -> some View {
        if let defaultPhone = props.accountInfo.defaultPhone {
            return AnyView(
                    PhoneNumberToolBar(
                            props: PhoneNumberToolBarProps(
                                    flagImageName: ContactsPageViewHelper.getFlagImageName(phoneCode: defaultPhone.phoneCode, countries: props.countries),
                                    phoneNumber: defaultPhone.phoneNumber,
                                    onSelectPhoneNumber: props.onSelectDefaultPhoneNumber
                            )
                    )
            )
        } else {
            return AnyView(EmptyView())
        }
    }

    private func contentShader() -> some View {
        Color.black
                .opacity(isFocusSearchBar ? 0.4 : 0)
                .edgesIgnoringSafeArea(.bottom)
                .padding(.top, isFocusSearchBar ? 44 : 0)
    }

    private func list(props: ContactsPageViewProps) -> some View {
        ContactsPageList(
                props: ContactsPageListProps(
                        accountInfo: props.accountInfo,
                        scrollToAlphabet: $characterJumper,
                        isFocusSearchBar: $isFocusSearchBar,
                        searchText: $searchText,
                        tabbarType: $tabbarType,
                        filterType: props.filterType,
                        phoneContacts: props.phoneContacts,
                        cloudContacts: props.cloudContacts,
                        employeeContacts: props.employeeContacts,
                        clientContacts: props.clientContacts,
                        onSelectContact: props.onSelectContact,
                        needJumpList: { isNeeded in
                            isShowJumpList = isNeeded
                        },
                        onFilter: {
                            actionSheetType = .filter
                        },
                        needShowEmpty: { isEmpty in
                            isShowEmpty = isEmpty
                        }
                )
        )
    }

    private func addContactButton() -> some View {
        Button(action: {
            actionSheetType = .addContact
        }) {
            Image(systemName: "plus")
                    .textStyle(.body3Button1Light5Blue)
        }
    }

    private func alphabetJumper() -> some View {
        AlphabetJumper(
                props: AlphabetJumperProps(
                        onJump: { char in
                            characterJumper = char
                        }
                )
        )
    }

    private func emptyContactView() -> some View {
        VStack(spacing: Spacing.spacing8Pt) {
            Spacer()
            Text(tabbarType.emptyListTitle)
                    .textStyle(.title21Default1Light2SecondaryLabelColor)
                    .multilineTextAlignment(.center)
            Text(tabbarType.emptyListSubtitle)
                    .textStyle(.subheadline1Default1Light2SecondaryLabelColor)
                    .multilineTextAlignment(.center)
            Spacer()
        }
                .frame(maxHeight: UIScreen.main.bounds.height)
                .edgesIgnoringSafeArea(.all)
                .animation(nil)
    }


    private func filterActionSheet(props: ContactsPageViewProps) -> ActionSheet {
        ActionSheet(
                title: Text("Filter My Contacts by"),
                buttons: [
                    .default(Text("All Numbers")) {
                        props.onFilter(.allNumbers)
                    },
                    .default(Text("Default Number")) {
                        props.onFilter(.defaultNumber)
                    },
                    .cancel()
                ]
        )
    }

    private func addContactActionSheet(props: ContactsPageViewProps) -> ActionSheet {
        var buttons = [Alert.Button]()
        if (props.accountInfo.defaultPhone?.isExtension ?? false) == false {
            if ContactsService.ableToAccessContacts() {
                buttons = [createContactButton(props: props), addEmployeeButton(props: props), addClientButton(props: props), .cancel()]
            } else {
                buttons = [addEmployeeButton(props: props), addClientButton(props: props), .cancel()]
            }
        } else {
            if ContactsService.ableToAccessContacts() {
                buttons = [createContactButton(props: props), addClientButton(props: props), .cancel()]
            } else {
                buttons = [addClientButton(props: props), .cancel()]
            }
        }

        return ActionSheet(
                title: Text("New Contact"),
                message: nil,
                buttons: buttons
        )
    }

    private func createContactButton(props: ContactsPageViewProps) -> Alert.Button {
        Alert.Button.default(Text("Create Contact")) {
            props.onCreateContact()
        }
    }

    private func addEmployeeButton(props: ContactsPageViewProps) -> Alert.Button {
        Alert.Button.default(Text("Add Employee")) {
            props.onAddEmployee()
        }
    }

    private func addClientButton(props: ContactsPageViewProps) -> Alert.Button {
        Alert.Button.default(Text("Add Client")) {
            props.onAddClient()
        }
    }
}

struct ContactsPageView_Previews: PreviewProvider {
    static var previews: some View {
        ContactsPageView(
                props: ContactsPageViewProps(
                        accountInfo: AccountInfo(
                                phones: [
                                    PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: "2"),
                                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                                    PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
                                account: Account(id: 1, name: "Account name", createDate: Date()),
                                devices: []
                        ),
                        filterType: .allNumbers,
                        countries: [],
                        phoneContacts: [
                            .init(firstName: "hello", lastName: "mark", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "ok", lastName: "true", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: nil, lastName: "fine", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "gogo", lastName: "now", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "good", lastName: "bye", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "never", lastName: "loose", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "Adam", lastName: "Eva", company: nil, avatarUrl: nil, phones: [])
                        ],
                        cloudContacts: [
                            .init(firstName: "hello", lastName: "mark", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "ok", lastName: "true", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: nil, lastName: "fine", company: "Ok123", avatarUrl: nil, phones: []),
                            .init(firstName: "gogo", lastName: "now", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "good", lastName: "bye", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "never", lastName: "loose", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "Adam", lastName: "Eva", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "hello", lastName: "mark", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "ok", lastName: "true", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: nil, lastName: "fine", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "gogo", lastName: "now", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "good", lastName: "bye", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "never", lastName: "loose", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "Adam", lastName: "Eva", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "no", lastName: "one", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "every", lastName: "body", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "kind", lastName: "of", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "number", lastName: "one", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "pow", lastName: "wow", company: nil, avatarUrl: nil, phones: []),
                            .init(firstName: "cmg", lastName: nil, company: nil, avatarUrl: nil, phones: [])
                        ],
                        employeeContacts: [],
                        clientContacts: [],
                        onSelectContact: { _ in },
                        onFilter: { _ in },
                        onCreateContact: {},
                        onAddEmployee: {},
                        onAddClient: {},
                        onSelectDefaultPhoneNumber: {}
                )
        )
    }
}
