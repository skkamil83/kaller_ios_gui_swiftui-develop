import SwiftUI

struct AlphabetJumperProps {
    let onJump: (Character) -> Void
}
