import SwiftUI

struct AlphabetJumper: View {
    let props: AlphabetJumperProps

    var body: some View {
        alphabetJumper()
    }

    private func alphabetJumper() -> some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                VStack {
                    ForEach(JumpListUtils.getCharacters(), id: \.self) { char in
                        Button(action: {
                            props.onJump(char)
                        }) {
                            Text(String(char))
                                    .textStyle(.init(fontName: "SFProText-Semibold", fontSize: 11, color: .primaryBlue))
                        }
                    }
                }
                        .padding(.trailing, 1)
            }
            Spacer()
        }
                .edgesIgnoringSafeArea(.all)
    }
}
