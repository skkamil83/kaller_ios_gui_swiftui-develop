import Contacts

class ContactsPageViewHelper {
    static func getFlagImageName(phoneCode: Int, countries: [Country]) -> String {
        return countries.first {
            $0.phoneCode == phoneCode
        }?.flagImageName ?? ""
    }
}
