struct ContactsPageViewProps {
    let accountInfo: AccountInfo
    let filterType: PhoneNumberFilterType
    let countries: [Country]
    let phoneContacts: [Contact]
    let cloudContacts: [Contact]
    let employeeContacts: [Contact]
    let clientContacts: [Contact]
    let onSelectContact: (Contact) -> Void
    let onFilter: (PhoneNumberFilterType) -> Void
    let onCreateContact: () -> Void
    let onAddEmployee: () -> Void
    let onAddClient: () -> Void
    let onSelectDefaultPhoneNumber: () -> Void
}
