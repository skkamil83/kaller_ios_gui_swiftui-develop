class ContactsPageSearchingListHelper {
    static func getOtherResults(contacts: [Contact], searchText: String) -> [Contact] {
        let text = searchText.lowercased().trimmingCharacters(in: .whitespaces)
        return contacts.filter { (contact) -> Bool in
            guard let company = contact.company else {
                return false
            }
            return company.lowercased().split(separator: " ").first(where: { String($0).hasPrefix(text) }) != nil
        }
    }

    static func getTopNameMatches(contacts: [Contact], searchText: String) -> [Contact] {
        let text = searchText.lowercased().trimmingCharacters(in: .whitespaces)
        return contacts.filter { (contact) -> Bool in
            if let firstName = contact.firstName {
                if firstName.lowercased().hasPrefix(text) {
                    return true
                }
            }
            if let lastName = contact.lastName {
                return lastName.lowercased().hasPrefix(text)
            }
            return false
        }
    }
}
