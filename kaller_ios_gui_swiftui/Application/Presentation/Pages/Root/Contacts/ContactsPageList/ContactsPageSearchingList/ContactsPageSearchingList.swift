import SwiftUI

struct ContactsPageSearchingList: View {
    let props: ContactsPageSearchingListProps
    @State private var topNameMatchesCount: Int = 0
    @State private var otherResultsCount: Int = 0

    var body: some View {
        ZStack {
            background()
            list(props: props)
            if topNameMatchesCount == 0 && otherResultsCount == 0 {
                emptyText()
            }
        }
    }

    private func background() -> some View {
        Color.navigationBarBackgroundChrome
                .edgesIgnoringSafeArea(.all)
    }

    private func list(props: ContactsPageSearchingListProps) -> some View {
        VStack(spacing: 0) {
            Divider()
            ScrollView {
                VStack(spacing: 0) {
                    topMatches(props: props)
                    otherResults(props: props)
                }
            }
        }
    }

    private func topMatches(props: ContactsPageSearchingListProps) -> some View {
        let contacts = ContactsPageSearchingListHelper.getTopNameMatches(contacts: props.contacts, searchText: props.searchText)
        DispatchQueue.main.async {
            topNameMatchesCount = contacts.count
        }
        return ForEach(0..<contacts.count, id: \.self) { index in
            LazyVStack(spacing: 0) {
                if index == 0 {
                    section(title: "Top Name Matches".uppercased())
                }
                topNameMatchRow(contact: contacts[index], isLastRow: index == contacts.count - 1)
                        .onTapGesture {
                            props.onSelected(contacts[index])
                        }
            }
        }
    }

    private func otherResults(props: ContactsPageSearchingListProps) -> some View {
        let contacts = ContactsPageSearchingListHelper.getOtherResults(contacts: props.contacts, searchText: props.searchText)
        DispatchQueue.main.async {
            otherResultsCount = contacts.count
        }
        return ForEach(0..<contacts.count, id: \.self) { index in
            LazyVStack(spacing: 0) {
                if index == 0 {
                    section(title: "other results".uppercased())
                }
                otherResultRow(contact: contacts[index], isLastRow: index == contacts.count - 1)
                        .onTapGesture {
                            props.onSelected(contacts[index])
                        }
            }
        }
    }

    private func section(title: String) -> some View {
        VStack(alignment: .leading, spacing: 0) {
            Spacer()
            Text(title)
                    .textStyle(.footnote1Default1Light6DarkGray)
                    .padding(EdgeInsets(top: 0, leading: Spacing.spacing16Pt, bottom: 7, trailing: 0))
            Divider()
        }
                .frame(width: UIScreen.main.bounds.width, height: 56)
    }

    private func topNameMatchRow(contact: Contact, isLastRow: Bool) -> some View {
        VStack(spacing: 0) {
            ContactsPageListRow(
                    props: ContactsPageListRowProps(
                            contact: contact
                    )
            )
            Divider()
                    .padding(.leading, isLastRow ? 0 : Spacing.spacing16Pt)
        }
                .background(Color.white)
    }

    private func otherResultRow(contact: Contact, isLastRow: Bool) -> some View {
        ContactsPageSearchingOtherResultRow(
                props: ContactsPageSearchingOtherResultRowProps(
                        contact: contact,
                        isLastRow: isLastRow,
                        searchText: props.searchText
                )
        )
    }

    private func emptyText() -> some View {
        VStack(spacing: 0) {
            Divider()
            Text("No Results")
                    .textStyle(.title21Default1Light2SecondaryLabelColor)
                    .frame(maxHeight: UIScreen.main.bounds.height)
        }
    }
}
