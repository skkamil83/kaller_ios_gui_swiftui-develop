import SwiftUI

struct ContactsPageSearchingListProps {
    let searchText: String
    let contacts: [Contact]
    let onSelected: (Contact) -> Void
}
