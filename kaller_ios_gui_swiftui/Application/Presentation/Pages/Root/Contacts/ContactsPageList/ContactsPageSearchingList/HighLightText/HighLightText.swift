import SwiftUI

struct HighLightText: View {
    let props: HighLightTextProps

    var body: some View {
        HStack(spacing: 0) {
            let viewModels = HighLightTextHelper.getHighLightTextViewModels(text: props.text, highlightTextTarget: props.highLight.trimmingCharacters(in: .whitespacesAndNewlines))
            ForEach(0..<viewModels.count, id: \.self) { index in
                let viewModel = viewModels[index]
                Text(viewModel.text)
                        .textStyle(viewModel.isHighLight ? .footnote1Default1Light1LabelColor : .footnote1Default1Light2SecondaryLabelColor)
            }
        }
    }
}

struct HighLightText_Previews: PreviewProvider {
    static var previews: some View {
        HighLightText(
                props: HighLightTextProps(
                        text: "Microsoft Google",
                        highLight: "Microsoft"
                )
        )
    }
}
