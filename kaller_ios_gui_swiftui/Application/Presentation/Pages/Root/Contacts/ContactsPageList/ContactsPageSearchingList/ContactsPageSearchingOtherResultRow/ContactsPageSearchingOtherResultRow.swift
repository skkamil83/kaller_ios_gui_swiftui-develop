import SwiftUI

struct ContactsPageSearchingOtherResultRow: View {
    let props: ContactsPageSearchingOtherResultRowProps

    var body: some View {
        view(props: props)
    }

    private func view(props: ContactsPageSearchingOtherResultRowProps) -> some View {
        let contact = props.contact
        let firstText = (contact.lastName ?? "").isEmpty ? "" : (contact.firstName ?? "")
        var secondText = (contact.lastName ?? "").isEmpty ? (contact.firstName ?? "") : (contact.lastName ?? "")
        if !firstText.isEmpty {
            secondText = " " + secondText
        }
        if !firstText.isEmpty || !secondText.isEmpty {
            return AnyView(
                    twoRows(firstText: firstText, secondText: secondText, company: props.contact.company ?? "", searchText: props.searchText)
            )
        } else {
            return AnyView(
                    companyRow(props: props)
            )
        }
    }

    private func companyRow(props: ContactsPageSearchingOtherResultRowProps) -> some View {
        VStack(spacing: 0) {
            HStack {
                Text(props.contact.company ?? "")
                        .font(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black).font)
                        .foregroundColor(.black)
                        .padding(.leading, Spacing.spacing16Pt)
                Spacer()
            }
                    .frame(height: 44)
                    .contentShape(Rectangle())
            Divider()
                    .padding(.leading, props.isLastRow ? 0 : Spacing.spacing16Pt)
        }
                .background(Color.white)
    }

    private func twoRows(firstText: String, secondText: String, company: String, searchText: String) -> some View {
        VStack(spacing: 0) {
            ZStack {
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        (Text(firstText)
                                .font(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black).font)
                                .foregroundColor(.black)
                                + Text(secondText)
                                .font(TextStyle.body2Bold1Light1LabelColor.font)
                                .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
                        )
                                .padding(.leading, Spacing.spacing16Pt)
                        Spacer()
                    }
                            .padding(.top, Spacing.spacing4Pt)
                    Spacer()
                }
                VStack {
                    Spacer()
                    HStack(spacing: 0) {
                        HighLightText(props: HighLightTextProps(text: company, highLight: searchText))
                                .padding(.leading, Spacing.spacing16Pt)
                        Spacer()
                    }
                            .padding(.bottom, 3)
                }
            }
                    .frame(height: 44)
                    .contentShape(Rectangle())
            Divider()
                    .padding(.leading, props.isLastRow ? 0 : Spacing.spacing16Pt)
        }
                .background(Color.white)
    }
}

struct ContactsPageSearchingOtherResultRow_Previews: PreviewProvider {
    static var previews: some View {
        ContactsPageSearchingOtherResultRow(
                props: ContactsPageSearchingOtherResultRowProps(
                        contact: Contact(firstName: "Ok", lastName: nil, company: "Microsoft Google", avatarUrl: nil, phones: []),
                        isLastRow: false,
                        searchText: "m"
                )
        )
    }
}
