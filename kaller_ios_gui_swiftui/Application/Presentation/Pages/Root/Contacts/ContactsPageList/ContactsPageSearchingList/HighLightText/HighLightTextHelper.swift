import Foundation

class HighLightTextHelper {
    static func getHighLightTextViewModels(text: String, highlightTextTarget: String) -> [HighLightTextViewModel] {
        if highlightTextTarget.isEmpty {
            return [HighLightTextViewModel(text: text, isHighLight: false)]
        }
        var viewModels = [HighLightTextViewModel]()
        let subTexts = text.split(separator: " ")
        for subText in subTexts {
            let space = subText == subTexts.last ? "" : " "
            if (subText.lowercased().hasPrefix(highlightTextTarget.lowercased())) {
                viewModels.append(HighLightTextViewModel(text: String(subText.dropLast(subText.count - highlightTextTarget.count)), isHighLight: true))
                viewModels.append(HighLightTextViewModel(text: "\(String(subText.dropFirst(highlightTextTarget.count)))\(space)", isHighLight: false))
            } else {
                viewModels.append(HighLightTextViewModel(text: "\(String(subText))\(space)", isHighLight: false))
            }
        }
        return viewModels
    }
}
