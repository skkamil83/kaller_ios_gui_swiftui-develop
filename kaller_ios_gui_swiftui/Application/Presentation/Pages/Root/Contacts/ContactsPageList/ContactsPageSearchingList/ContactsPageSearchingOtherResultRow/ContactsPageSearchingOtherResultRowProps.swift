struct ContactsPageSearchingOtherResultRowProps {
    let contact: Contact
    let isLastRow: Bool
    let searchText: String
}
