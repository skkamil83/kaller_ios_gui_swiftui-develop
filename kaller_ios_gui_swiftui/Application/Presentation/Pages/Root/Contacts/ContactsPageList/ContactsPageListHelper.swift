import Foundation

class ContactsPageListHelper {
    static func getRoleTitle(isExtension: Bool?) -> String {
        guard let isExtension = isExtension else {
            return ""
        }
        return isExtension ? "Extension" : "Owner"
    }
}

