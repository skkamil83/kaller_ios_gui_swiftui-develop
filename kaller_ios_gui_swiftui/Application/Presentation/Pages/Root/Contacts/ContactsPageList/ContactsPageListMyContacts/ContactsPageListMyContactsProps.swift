import SwiftUI

struct ContactsPageListMyContactsProps {
    let phoneContacts: [Contact]
    let cloudContacts: [Contact]
    let isFocusSearchBar: Bool
    let onSelected: (Contact) -> Void
    let needJumpList: (Bool) -> Void
    let needShowEmpty: (Bool) -> Void
}
