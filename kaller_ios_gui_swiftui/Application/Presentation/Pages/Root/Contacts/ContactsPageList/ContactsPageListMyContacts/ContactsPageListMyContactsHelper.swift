import Contacts

class ContactsPageListMyContactsHelper {
    static func contactsSorted(contacts: [Contact]) -> [Contact] {
        return contacts.sorted { (first, second) -> Bool in
            let firstText = (first.lastName ?? "").isEmpty ? (first.firstName ?? "") : (first.lastName ?? "")
            let secondText = (second.lastName ?? "").isEmpty ? (second.firstName ?? "") : (second.lastName ?? "")
            return firstText < secondText
        }
    }

    static func groupAndSortContacts(contacts: [Contact]) -> [Character: [Contact]] {
        return Dictionary(grouping: contacts, by: { (element: Contact) in
            var character: Character = "#"
            if let lastName = element.lastName, !lastName.isEmpty {
                character = lastName.uppercased().first ?? "#"
            } else if let firstName = element.firstName, !firstName.isEmpty {
                character = firstName.uppercased().first ?? "#"
            }
            if (character >= "A" && character <= "Z") {
                return character
            } else {
                return "#"
            }
        })
    }

    static func mergeContacts(phoneContacts: [Contact], cloudContacts: [Contact]) -> [Contact] {
        return ContactsService.ableToAccessContacts() ? phoneContacts + cloudContacts : cloudContacts
    }

    static func isNeedJumpList(phoneContacts: [Contact], cloudContacts: [Contact]) -> Bool {
        return mergeContacts(phoneContacts: phoneContacts, cloudContacts: cloudContacts).count >= 6
    }
}
