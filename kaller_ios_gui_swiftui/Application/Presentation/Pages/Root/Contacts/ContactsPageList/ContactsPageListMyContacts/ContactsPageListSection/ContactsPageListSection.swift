import SwiftUI

struct ContactsPageListSection: View {
    let props: ContactsPageListSectionProps

    var body: some View {
        section(character: props.character)
    }

    private func section(character: Character) -> some View {
        HStack {
            Text(String(character))
                    .textStyle(.body2Bold1Light1LabelColor)
                    .padding(.leading, Spacing.spacing16Pt)
            Spacer()
        }
                .frame(height: 28)
                .background(Color.fillsLightSecondaryFill)
    }
}

struct ContactsPageListSection_Previews: PreviewProvider {
    static var previews: some View {
        ContactsPageListSection(
                props: ContactsPageListSectionProps(character: "W")
        )
    }
}
