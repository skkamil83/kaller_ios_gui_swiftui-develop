import SwiftUI

struct ContactsPageListRow: View {
    let props: ContactsPageListRowProps

    var body: some View {
        row(contact: props.contact)
    }

    private func row(contact: Contact) -> some View {
        let firstText = (contact.lastName ?? "").isEmpty ? "" : (contact.firstName ?? "")
        var secondText = (contact.lastName ?? "").isEmpty ? (contact.firstName ?? "") : (contact.lastName ?? "")
        if !firstText.isEmpty {
            secondText = " " + secondText
        }
        return HStack {
            (Text(firstText)
                    .font(TextStyle(fontName: "SFProText-Regular", fontSize: 17, color: .black).font)
                    .foregroundColor(.black)
                    + Text(secondText)
                            .font(TextStyle.body2Bold1Light1LabelColor.font)
                            .foregroundColor(TextStyle.body2Bold1Light1LabelColor.color)
            )
                    .padding(.leading, Spacing.spacing16Pt)
            Spacer()
        }
                .frame(width: UIScreen.main.bounds.width, height: 44)
                .contentShape(Rectangle())
    }
}
