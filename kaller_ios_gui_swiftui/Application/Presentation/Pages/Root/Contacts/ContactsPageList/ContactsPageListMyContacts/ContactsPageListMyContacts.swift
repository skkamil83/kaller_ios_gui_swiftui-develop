import SwiftUI

struct ContactsPageListMyContacts: View {
    let props: ContactsPageListMyContactsProps

    var body: some View {
        list(props: props)
                .onAppear() {
                    props.needJumpList(ContactsPageListMyContactsHelper.isNeedJumpList(phoneContacts: props.phoneContacts, cloudContacts: props.cloudContacts))
                }
    }

    private func list(props: ContactsPageListMyContactsProps) -> some View {
        if ContactsPageListMyContactsHelper.isNeedJumpList(phoneContacts: props.phoneContacts, cloudContacts: props.cloudContacts) {
            return AnyView(listWithSection(props: props))
        } else {
            return AnyView(listWithoutSection(props: props))
        }
    }

    private func listWithSection(props: ContactsPageListMyContactsProps) -> some View {
        let contactsToGroup = ContactsPageListMyContactsHelper.mergeContacts(phoneContacts: props.phoneContacts, cloudContacts: props.cloudContacts)
        let groups = ContactsPageListMyContactsHelper.groupAndSortContacts(contacts: contactsToGroup)
        props.needShowEmpty(groups.isEmpty && ContactsService.ableToAccessContacts())

        if groups.isEmpty {
            return AnyView(accessContactWithKallerSection(props: props))
        } else {
            return AnyView(listWithSectionRows(groups: groups, props: props))
        }
    }

    private func listWithSectionRows(groups: [Character: [Contact]], props: ContactsPageListMyContactsProps) -> some View {
        let characters = JumpListUtils.getCharacters()

        return ForEach(characters, id: \.self) { char in
            if let contacts = groups[char] {
                let contactsSorted = ContactsPageListMyContactsHelper.contactsSorted(contacts: contacts)
                ForEach(0..<contactsSorted.count, id: \.self) { index in
                    rowWithSection(
                            props: props,
                            character: char,
                            contact: contactsSorted[index],
                            isShowSection: index == 0,
                            isLastItem: index == contactsSorted.count - 1
                    )
                            .onTapGesture {
                                props.onSelected(contactsSorted[index])
                            }
                }
            } else if char == characters.first {
                accessContactWithKallerSection(props: props)
            }
            if char == characters.last {
                contactsCounter(props: props)
            }
        }
    }

    private func listWithoutSection(props: ContactsPageListMyContactsProps) -> some View {
        let contacts = ContactsPageListMyContactsHelper.mergeContacts(phoneContacts: props.phoneContacts, cloudContacts: props.cloudContacts)
        let contactsSorted = ContactsPageListMyContactsHelper.contactsSorted(contacts: contacts)
        props.needShowEmpty(contactsSorted.isEmpty && ContactsService.ableToAccessContacts())

        if contactsSorted.isEmpty {
            return AnyView(accessContactWithKallerSection(props: props))
        } else {
            return AnyView(listWithoutSectionRows(contactsSorted: contactsSorted, props: props))
        }
    }

    private func listWithoutSectionRows(contactsSorted: [Contact], props: ContactsPageListMyContactsProps) -> some View {
        ForEach(0..<contactsSorted.count, id: \.self) { index in
            if index == 0 {
                accessContactWithKallerSection(props: props)
            }
            ContactsPageListRow(
                    props: ContactsPageListRowProps(
                            contact: contactsSorted[index]
                    )
            )
                    .listRowInsets(EdgeInsets())
                    .onTapGesture {
                        props.onSelected(contactsSorted[index])
                    }
            if index == contactsSorted.count - 1 {
                contactsCounter(props: props)
            }
        }
    }

    private func accessContactWithKallerSection(props: ContactsPageListMyContactsProps) -> some View {
        if props.isFocusSearchBar {
            return AnyView(EmptyView())
        } else {
            let isShowAccessContacts = !ContactsService.ableToAccessContacts()
            let isShowKallerCloudSection = (props.phoneContacts.isEmpty || !ContactsService.ableToAccessContacts()) && !props.cloudContacts.isEmpty
            if !isShowAccessContacts && !isShowKallerCloudSection {
                return AnyView(EmptyView())
            }
            return AnyView(
                    VStack(spacing: 0) {
                        if isShowAccessContacts {
                            AccessContactsWarning(
                                    props: ContactsPageListAccessContactsProps()
                            )
                        }
                        if isShowKallerCloudSection {
                            kallerCloudSection()
                        }
                    }
                            .listRowInsets(EdgeInsets())
            )
        }
    }

    private func rowWithSection(props: ContactsPageListMyContactsProps, character: Character, contact: Contact, isShowSection: Bool, isLastItem: Bool) -> some View {
        VStack(spacing: 0) {
            if isShowSection {
                if character == JumpListUtils.getCharacters().first {
                    accessContactWithKallerSection(props: props)
                }
                ContactsPageListSection(
                        props: ContactsPageListSectionProps(
                                character: character
                        )
                )
            }
            ContactsPageListRow(
                    props: ContactsPageListRowProps(
                            contact: contact
                    )
            )
            if !isLastItem {
                Divider()
                        .padding(.leading, Spacing.spacing16Pt)
            }
        }
                .listRowInsets(EdgeInsets())
                .background(Color.white)
    }

    private func contactsCounter(props: ContactsPageListMyContactsProps) -> some View {
        let contacts = ContactsPageListMyContactsHelper.mergeContacts(phoneContacts: props.phoneContacts, cloudContacts: props.cloudContacts)

        return ContactsPageListCounter(
                props: ContactsPageListCounterProps(
                        number: contacts.count
                )
        )
    }

    private func kallerCloudSection() -> some View {
        HStack {
            Text("contacts stored in the kaller cloud".uppercased())
                    .textStyle(.footnote1Default1Light1LabelColor)
                    .padding(.leading, 16)
            Spacer()
        }
                .frame(width: UIScreen.main.bounds.width, height: 32)
                .background(Color.navigationBarBackgroundChrome)
    }
}
