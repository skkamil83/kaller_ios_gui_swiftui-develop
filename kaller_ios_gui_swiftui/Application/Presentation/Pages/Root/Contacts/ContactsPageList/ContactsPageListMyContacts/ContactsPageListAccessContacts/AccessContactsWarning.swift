import SwiftUI

struct AccessContactsWarning: View {
    @State private var isShowAccessContactsAlert = false
    let props: ContactsPageListAccessContactsProps

    var body: some View {
        accessToContacts()
                .alert(isPresented: $isShowAccessContactsAlert, content: {
                    accessContactsAlert()
                })
    }

    private func accessToContacts() -> some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack(spacing: 5) {
                Image(systemName: "exclamationmark.circle.fill")
                        .textStyle(.title31Default1Light7SystemRed)
                Text("Access to Contacts")
                        .textStyle(.title32Bold1Light1LabelColor)
                Spacer()
            }
                    .padding(EdgeInsets(top: 16, leading: Spacing.spacing16Pt, bottom: 0, trailing: Spacing.spacing16Pt))
            Text("Contacts of your device will be stored in an encrypted form on the Kaller cloud, which will allow you to have access to them from any device. You can delete all contacts from the cloud at any given time.")
                    .textStyle(.footnote1Default1Light1LabelColor)
                    .padding(EdgeInsets(top: Spacing.spacing8Pt, leading: Spacing.spacing16Pt, bottom: Spacing.spacing12Pt, trailing: Spacing.spacing16Pt))
            Divider()
                    .padding(.leading, Spacing.spacing16Pt)
            Text("Allow in Settings")
                    .textStyle(.body1Default1Light5Blue)
                    .onTapGesture {
                        isShowAccessContactsAlert.toggle()
                    }
                    .padding(EdgeInsets(top: Spacing.spacing12Pt, leading: Spacing.spacing16Pt, bottom: Spacing.spacing10Pt, trailing: Spacing.spacing16Pt))
            Divider()
        }
    }

    private func accessContactsAlert() -> Alert {
        Alert(
                title: Text("Please Allow Access"),
                message: Text("Kaller does not have access to your device’s contacts."),
                primaryButton: Alert.Button.cancel(Text("Not Now")),
                secondaryButton: Alert.Button.default(Text("Settings")) {
                    openSettingsButton()
                }
        )
    }

    private func openSettingsButton() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}

struct ContactsPageListAccessContacts_Previews: PreviewProvider {
    static var previews: some View {
        AccessContactsWarning(
                props: ContactsPageListAccessContactsProps()
        )
    }
}
