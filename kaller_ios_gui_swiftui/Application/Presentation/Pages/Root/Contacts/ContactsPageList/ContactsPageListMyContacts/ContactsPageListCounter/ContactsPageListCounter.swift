import SwiftUI

struct ContactsPageListCounter: View {
    let props: ContactsPageListCounterProps

    var body: some View {
        contactsCounter(props: props)
    }

    private func contactsCounter(props: ContactsPageListCounterProps) -> some View {
        if props.number == 0 {
            return AnyView(EmptyView())
        } else {
            let text = props.number > 1 ? "\(props.number) Contacts" : "\(props.number) Contact"
            return AnyView(
                    VStack(spacing: 0) {
                        Divider()
                                .padding(.leading, Spacing.spacing16Pt)
                        Text(text)
                                .textStyle(.title31Default1Light2SecondaryLabelColor)
                                .frame(height: 44)
                    }
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                            .listRowInsets(EdgeInsets())
                            .background(Color.white)
            )
        }
    }

}

struct ContactsPageListCounter_Previews: PreviewProvider {
    static var previews: some View {
        ContactsPageListCounter(
                props: ContactsPageListCounterProps(number: 1)
        )
    }
}
