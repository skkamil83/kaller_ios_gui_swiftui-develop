import SwiftUI

struct ContactsPageListProps {
    let accountInfo: AccountInfo
    let scrollToAlphabet: Binding<Character>
    let isFocusSearchBar: Binding<Bool>
    let searchText: Binding<String>
    let tabbarType: Binding<ContatctsPageTabbarType>
    let filterType: PhoneNumberFilterType
    let phoneContacts: [Contact]
    let cloudContacts: [Contact]
    let employeeContacts: [Contact]
    let clientContacts: [Contact]
    let onSelectContact: (Contact) -> Void
    let needJumpList: (Bool) -> Void
    let onFilter: () -> Void
    let needShowEmpty: (Bool) -> Void
}
