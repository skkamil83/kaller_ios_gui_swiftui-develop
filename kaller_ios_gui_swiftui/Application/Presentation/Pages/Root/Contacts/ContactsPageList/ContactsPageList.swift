import SwiftUI

struct ContactsPageList: View {
    @Binding private var scrollToAlphabet: Character
    @Binding private var isFocusSearchBar: Bool
    @Binding private var searchText: String
    @Binding private var tabbarType: ContatctsPageTabbarType
    @State private var scrollViewProxy: ScrollViewProxy?
    @State private var isShowFilteredView = false
    @State private var defaultOwnerContactFrame: CGRect = .zero

    let props: ContactsPageListProps
    private let filteredViewHeight: CGFloat = 44

    init(props: ContactsPageListProps) {
        self.props = props
        self._scrollToAlphabet = props.scrollToAlphabet
        self._isFocusSearchBar = props.isFocusSearchBar
        self._searchText = props.searchText
        self._tabbarType = props.tabbarType
    }

    var body: some View {
        ScrollViewReader { scrollViewProxy in
            let _ = setScrollViewProxy(scrollViewProxy: scrollViewProxy)
            List {
                Section(header: searchBar()) {
                }
                if searchText.isEmpty {
                    Section(header: tabbarView(props: props)) {
                        ownerRow(props: props)
                        listContent(props: props)
                    }
                }
            }
                    .listStyle(PlainListStyle())
                    .frame(maxHeight: !searchText.isEmpty ? 36 : UIScreen.main.bounds.height)
            if !searchText.isEmpty {
                searchingList(props: props)
            }
        }
                .onChange(of: scrollToAlphabet) { value in
                    withAnimation {
                        scrollViewProxy?.scrollTo(value)
                    }
                }
    }

    private func searchBar() -> some View {
        SearchBarView(
                props: SearchBarViewProps(
                        query: $searchText,
                        placeHolder: "Search",
                        onFocus: { isFocus in
                            isFocusSearchBar = isFocus
                        }
                )
        )
                .padding(
                        EdgeInsets(
                                top: 0,
                                leading: Spacing.spacing16Pt,
                                bottom: isFocusSearchBar ? Spacing.spacing8Pt : props.clientContacts.isEmpty ? 15 : 0,
                                trailing: Spacing.spacing16Pt
                        ))
                .background(Color.white)
                .listRowInsets(EdgeInsets())
                .textCase(nil)
    }

    private func searchingList(props: ContactsPageListProps) -> some View {
        ContactsPageSearchingList(
                props: ContactsPageSearchingListProps(
                        searchText: searchText,
                        contacts: props.cloudContacts + props.phoneContacts,
                        onSelected: props.onSelectContact
                )
        )
    }

    private func tabbarView(props: ContactsPageListProps) -> some View {
        if isFocusSearchBar || props.clientContacts.isEmpty {
            return AnyView(
                    EmptyView()
            )
        } else {
            return AnyView(
                    VStack(spacing: 0) {
                        UpperTabbar(
                                props: UpperTabbarProps(
                                        tabs: ContatctsPageTabbarType.allCases.map {
                                            UpperTabModel.init(id: $0.id, title: $0.title)
                                        },
                                        onSelected: { id in
                                            if let type = ContatctsPageTabbarType(rawValue: id) {
                                                self.tabbarType = type
                                            }
                                        }
                                )
                        )
                                .padding(.top, Spacing.spacing12Pt)
                        Divider()
                    }
                            .background(Color.white)
                            .listRowInsets(EdgeInsets())
                            .textCase(nil)
            )
        }
    }

    private func filterButton(props: ContactsPageListProps) -> some View {
        PhoneNumberFilterView(
                props: PhoneNumberFilterViewProps(
                        filterType: props.filterType,
                        onSelected: props.onFilter
                )
        )
                .listRowInsets(EdgeInsets())
    }

    private func ownerRow(props: ContactsPageListProps) -> some View {
        if isFocusSearchBar {
            return AnyView(EmptyView())
        } else {
            return AnyView(
                    VStack(spacing: 0) {
                        filterButton(props: props)
                                .frame(height: isShowFilteredView ? filteredViewHeight : 0)
                                .opacity(isShowFilteredView ? 1 : 0)
                        if isShowFilteredView {
                            Divider()
                        }
                        HStack(spacing: 0) {
                            OwnerIcon(
                                    props: OwnerIconProps(
                                            avatarStyle: AvatarViewHelper.getAvatarStyle(from: props.accountInfo.defaultPhone),
                                            firstName: props.accountInfo.defaultPhone?.userInfo.firstName ?? "",
                                            lastName: props.accountInfo.defaultPhone?.userInfo.lastName ?? "",
                                            role: ContactsPageListHelper.getRoleTitle(isExtension: props.accountInfo.defaultPhone?.isExtension)
                                    )
                            )
                                    .padding(.leading, Spacing.spacing16Pt)
                            Spacer()
                        }
                        Divider()
                    }
                            .background(collectOwnerInfoFrame())
                            .listRowInsets(EdgeInsets())
            )
        }
    }

    private func listContent(props: ContactsPageListProps) -> some View {
        switch tabbarType {
        case .myContacts:
            return AnyView(myContacts(props: props))
        case .employees:
            return AnyView(EmptyView())
        case .clients:
            return AnyView(EmptyView())
        }
    }

    private func myContacts(props: ContactsPageListProps) -> some View {
        ContactsPageListMyContacts(
                props: ContactsPageListMyContactsProps(
                        phoneContacts: props.phoneContacts,
                        cloudContacts: props.cloudContacts,
                        isFocusSearchBar: isFocusSearchBar,
                        onSelected: props.onSelectContact,
                        needJumpList: props.needJumpList,
                        needShowEmpty: props.needShowEmpty
                )
        )
    }

    private func setScrollViewProxy(scrollViewProxy: ScrollViewProxy) -> ScrollViewProxy {
        DispatchQueue.main.async {
            self.scrollViewProxy = scrollViewProxy
        }
        return scrollViewProxy
    }

    private func collectOwnerInfoFrame() -> some View {
        return GeometryReader { geometry -> Color in
            let rect = geometry.frame(in: .global)
            DispatchQueue.main.async {
                if defaultOwnerContactFrame == .zero {
                    defaultOwnerContactFrame = rect
                }
                if (rect.origin.y - defaultOwnerContactFrame.origin.y > filteredViewHeight) {
                    withAnimation {
                        isShowFilteredView = true
                    }
                } else if (rect.origin.y - defaultOwnerContactFrame.origin.y < -filteredViewHeight * 3) {
                    withAnimation {
                        isShowFilteredView = false
                    }
                }
            }
            return Color.white
        }
    }
}
