import SwiftUI

struct ContactsPageContainerProps {
    let onSelectDefaultPhoneNumber: () -> Void
}

struct ContactsPageContainer: View {
    @EnvironmentObject private var userInfoState: UserInfoState
    @EnvironmentObject private var contactsState: ContactsState
    @EnvironmentObject private var countryState: CountryState
    @State private var filterType: PhoneNumberFilterType = .allNumbers

    let props: ContactsPageContainerProps

    var body: some View {
        NavigationView {
            ContactsPageView(
                    props: ContactsPageViewProps(
                            accountInfo: userInfoState.accountInfo!,
                            filterType: filterType,
                            countries: countryState.countries ?? [],
                            phoneContacts: contactsState.localContacts,
                            cloudContacts: ContactsPageContainerHelper.filterCloudContacts(
                                    filter: filterType,
                                    addressBooks: contactsState.addressBooks ?? [:],
                                    accountInfo: userInfoState.accountInfo!
                            ),
                            employeeContacts: [],
                            clientContacts: [],
                            onSelectContact: onSelectContact,
                            onFilter: onFilter,
                            onCreateContact: onCreateContact,
                            onAddEmployee: onAddEmployee,
                            onAddClient: onAddClient,
                            onSelectDefaultPhoneNumber: props.onSelectDefaultPhoneNumber
                    )
            )
        }
    }

    private func onSelectContact(contact: Contact) {
        print(#function)
    }

    private func onFilter(filter: PhoneNumberFilterType) {
        filterType = filter
    }

    private func onCreateContact() {
        print(#function)
    }

    private func onAddEmployee() {
        print(#function)
    }

    private func onAddClient() {
        print(#function)
    }
}
