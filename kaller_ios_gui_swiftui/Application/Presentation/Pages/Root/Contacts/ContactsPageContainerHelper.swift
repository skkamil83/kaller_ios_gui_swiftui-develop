class ContactsPageContainerHelper {
    static func filterCloudContacts(filter: PhoneNumberFilterType, addressBooks: [String: [Contact]], accountInfo: AccountInfo) -> [Contact] {
        switch filter {
        case .defaultNumber:
            if let defaultPhone = accountInfo.defaultPhone {
                return addressBooks[defaultPhone.phoneNumber] ?? []
            }
            return []
        case .allNumbers:
            var contacts = [Contact]()
            for phoneInfo in accountInfo.phones {
                contacts += addressBooks[phoneInfo.phoneNumber] ?? []
            }
            return contacts
        }
    }
}
