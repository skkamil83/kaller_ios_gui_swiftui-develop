import SwiftUI

struct SelectDefaultNumberPageContainerProps {
    let onClose: () -> Void
    let onMakeCall: (PhoneInfo?) -> Void
    let onMakeMessage: (PhoneInfo?) -> Void
    let onSelectedDefalutNumber: () -> Void
}

struct SelectDefaultNumberPageContainer: View {
    let props: SelectDefaultNumberPageContainerProps
    @EnvironmentObject private var userInfoState: UserInfoState

    var body: some View {
        MultiaccountSelectionView(
            props: MultiaccountSelectionViewProps(
                style: .selectDefaultPhoneInfo,
                accountInfo: userInfoState.accountInfo!,
                onOutSideTap: props.onClose,
                onButtonCallTap: props.onClose,
                onSelectedCallingContact: props.onMakeCall,
                onSelectedMessageContact: props.onMakeMessage,
                onSelectedDefalutNumber: props.onSelectedDefalutNumber
            )
        )
    }
}
