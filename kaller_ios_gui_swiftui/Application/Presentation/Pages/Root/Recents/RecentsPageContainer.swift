import SwiftUI

struct RecentsPageContainer: View {
    let props: RecentsPageContainerProps

    @EnvironmentObject private var userInfoState: UserInfoState
    @EnvironmentObject private var recentsState: RecentsState
    @EnvironmentObject private var contactsState: ContactsState

    var body: some View {
        RecentsPageView(
            props: RecentsPageViewProps(
                accountInfo: userInfoState.accountInfo!,
                recents: RecentsPageContainerHelper.getRecentViewModels(
                    recentCalls: recentsState.recents,
                    contacts: contactsState.localContacts),
                onRecentTap: callTo(_:),
                onRecentInfoTap: openRecentCallDetails(_:),
                removeRecents: removeRecents(_:))
        )
    }

    private func callTo(_ recentCall: RecentCall) {
        print("\(#function) \(recentCall.callType == .incoming ? recentCall.phoneFromNumber : recentCall.phoneToNumber)")
    }

    private func openRecentCallDetails(_ recent: RecentViewModel) {
        props.openRecentDetails(recent)
    }
    
    private func removeRecents(_ recents: [RecentViewModel]) {
        var recentCalls = [RecentCall]()
        for recent in recents{
            recentCalls += recent.calls
        }
        removeRecentCalls(recentCalls)
    }

    private func removeRecentCalls(_ recentCalls: [RecentCall]) {
        for recentCall in recentCalls{
            if let index = recentsState.recents?.firstIndex(where: {$0.callId == recentCall.callId}){
                recentsState.recents?.remove(at: index)
            }
        }
    }
}

struct RecentsPageContainer_Previews: PreviewProvider {
    static var previews: some View {
        RecentsPageContainer(
            props: RecentsPageContainerProps(
                openRecentDetails: {_ in}
            )
        )
        .environmentObject(UserInfoState())
        .environmentObject(RecentsState())
        .environmentObject(ContactsState())
    }
}
