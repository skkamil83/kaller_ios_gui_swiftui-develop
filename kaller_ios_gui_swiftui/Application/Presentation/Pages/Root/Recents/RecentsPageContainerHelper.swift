//
//  RecentsPageContainerHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 08/12/2020.
//

import Foundation

class RecentsPageContainerHelper{
    
    static func getRecentViewModels(recentCalls: [RecentCall]?, contacts: [Contact]?) -> [RecentViewModel]{
        guard let recentCalls = recentCalls, recentCalls.count > 0 else {return []}
        
        let recentCallsSorted = recentCalls.sorted { (first, second) -> Bool in
            return first.callStartDate > second.callEndDate
        }
        
        var recentViewModels = [RecentViewModel]()
        var tempRecentCalls = [recentCallsSorted.first!]
        if recentCalls.count == 1{
            if let viewModel = createRecentViewModelFromRecentCallsSameGroup(recentCalls: tempRecentCalls, contacts: contacts){
                recentViewModels.append(viewModel)
            }
        }else{
            for i in 1..<recentCallsSorted.count{
                if( recentCallsAreSameGroup(first: recentCallsSorted[i - 1], second: recentCallsSorted[i])){
                    tempRecentCalls.append(recentCallsSorted[i])
                }else{
                    if let viewModel = createRecentViewModelFromRecentCallsSameGroup(recentCalls: tempRecentCalls, contacts: contacts){
                        recentViewModels.append(viewModel)
                    }
                    tempRecentCalls = [recentCallsSorted[i]]
                }
                
                if(i == recentCallsSorted.count - 1){
                    if let viewModel = createRecentViewModelFromRecentCallsSameGroup(recentCalls: tempRecentCalls, contacts: contacts){
                        recentViewModels.append(viewModel)
                    }
                }
            }
        }
        return recentViewModels
    }
    
    static func createRecentViewModelFromRecentCallsSameGroup(recentCalls: [RecentCall], contacts: [Contact]?) -> RecentViewModel?{
        if let recentCall = recentCalls.first{
            return RecentViewModel(
                calls: recentCalls,
                displayDate: recentCall.callStartDate.formattedDate(),
                isMissed: recentCall.isMissed,
                isCanceled: recentCall.isCanceled,
                callType: recentCall.callType,
                contact: recentCall.contact,
                isExtension: recentCall.isExtension,
                place: placeFromRecentCall(recentCall: recentCall, contacts: contacts))
        }
        return nil
    }
    
    static func recentCallsAreSameGroup(first: RecentCall, second: RecentCall) -> Bool{
        if(first.callType != second.callType) {return false}
        if(first.phoneFromNumber != second.phoneFromNumber) {return false}
        if(first.phoneToNumber != second.phoneToNumber) {return false}
        if(isAppToApp(recentCall: first) != isAppToApp(recentCall: second)) {return false}
        if(first.isMissed != second.isMissed) {return false}
        if(first.isCanceled != second.isCanceled) {return false}
        return true
    }
    
    static func isAppToApp(recentCall: RecentCall) -> Bool{
        return recentCall.billingInfo.type == .appToApp
    }
    
    static func placeFromRecentCall(recentCall: RecentCall, contacts: [Contact]?) -> String{
        if let contacts = contacts{
            let phoneNumber = recentCall.callType == .incoming ? recentCall.phoneFromNumber : recentCall.phoneToNumber
            
            for contact in contacts{
                if let phoneInfo = contact.phones.first(where: {$0.phoneNumber == phoneNumber}){
                    return phoneInfo.place
                }
            }
        }
        
        if recentCall.isExtension{
            if let fromExt = recentCall.fromExt{
                return fromExt
            }
        }
        
        return recentCall.fromCountry ?? ""
    }
}
