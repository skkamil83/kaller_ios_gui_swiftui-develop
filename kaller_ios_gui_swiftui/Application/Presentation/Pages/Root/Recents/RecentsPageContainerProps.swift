//
//  RecentsPageContainerProps.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 08/12/2020.
//

import Foundation

struct RecentsPageContainerProps {
    let openRecentDetails: (RecentViewModel) -> Void
}
