//
//  RecentsActionSheetType.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 08/12/2020.
//

import SwiftUI

enum RecentsActionSheetType: Identifiable{
    var id: String{
        switch self {
        case .clearAll:
            return "clearAll"
        case .filter:
            return "filter"
        }
    }
    case clearAll
    case filter
}
