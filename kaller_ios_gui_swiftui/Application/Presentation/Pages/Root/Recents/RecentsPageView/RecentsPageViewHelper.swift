//
//  RecentsPageViewHelper.swift
//  kaller_ios_gui_swiftui
//
//  Created by Thanh Vo on 08/12/2020.
//

import Foundation

class RecentsPageViewHelper{
    static func recentViewModelsFiltered(accountInfo: AccountInfo, recents: [RecentViewModel], recentListCallType: RecentListCallType, recentListFilterType: RecentListFilterType) -> [RecentViewModel]{
        var data = recents
        if(recentListFilterType == .defaultNumber){
            data = data.filter({ (recentViewModel) -> Bool in
                guard let recentCall = recentViewModel.calls.first, let defaultPhoneNumber = accountInfo.defaultPhone else {return false}
                let userPhoneNumber = recentCall.callType == .incoming ? recentCall.phoneToNumber : recentCall.phoneFromNumber
                return userPhoneNumber == defaultPhoneNumber.phoneNumber
            })
        }
        if(recentListCallType == .missed){
            data = data.filter({$0.isMissed})
        }
        return data
    }
}
