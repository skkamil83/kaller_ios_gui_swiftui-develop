import SwiftUI

struct RecentsPageView: View {
    let props: RecentsPageViewProps
    @State private var isEdit = false
    @State private var actionSheetType: RecentsActionSheetType?
    @State private var recentListCallType: RecentListCallType = .all
    @State private var recentListFilterType: RecentListFilterType = .all
    
    var body: some View {
        NavigationView {
            recentsList(props: props)
                .navigationTitle("Recents")
                .toolbar(content: {
                    ToolbarItem(placement: .principal) {
                        toolBar()
                    }
                })
                .navigationBarItems(
                    leading: leftNavigationButton(),
                    trailing: rightNavigationButton()
                )
                .actionSheet(item: $actionSheetType){ item in
                    switch item{
                    case .clearAll:
                        return actionSheetClearAll(props: props)
                    case .filter:
                        return actionSheetFilter(props: props)
                    }
                }
        }
    }
    
    private func recentsList(props: RecentsPageViewProps) -> some View{
        RecentsListView(
            props: RecentsListViewProps(
                filterType: recentListFilterType,
                recentCalls: RecentsPageViewHelper.recentViewModelsFiltered(
                    accountInfo: props.accountInfo,
                    recents: props.recents,
                    recentListCallType: recentListCallType,
                    recentListFilterType: recentListFilterType
                ),
                accountInfo: props.accountInfo,
                state: isEdit ? .edit : .view,
                onDelete: {call in
                    props.removeRecents([call])
                },
                onShowInfo: props.onRecentInfoTap,
                onSelected: props.onRecentTap,
                onFilter: {
                    actionSheetType = .filter
                }
            )
        )
    }
    
    private func toolBar() -> some View{
        let selecetdTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 14, color: .black)
        let unselectedTextStyle = TextStyle(fontName: "SFProText-Medium", fontSize: 13, color: .black)
        let binding = Binding<Int>(
            get: {
                recentListCallType.rawValue
            },
            set: { newValue in
                recentListCallType = RecentListCallType(rawValue: newValue) ?? .all
            }
        )
        
        return Picker(selection: binding, label: Text("What is your favorite color?")) {
            ForEach(0..<2) { index in
                Text(index == 0 ? "All" : "Missed")
                    .font(recentListCallType.rawValue == index ? selecetdTextStyle.font : unselectedTextStyle.font)
                    .foregroundColor(recentListCallType.rawValue == index ? selecetdTextStyle.color : unselectedTextStyle.color)
            }
        }
        .pickerStyle(SegmentedPickerStyle())
        .frame(width: 152, height: 32)
    }
    
    private func rightNavigationButton() -> some View{
        let doneTextStyle = TextStyle(fontName: "SFProText-Semibold", fontSize: 17, color: Color.primaryBlue)
        return Button(action: {
            withAnimation{
                isEdit.toggle()
            }
        }){
            Text("\(isEdit ? "Done" : "Edit")")
                .font( isEdit ? doneTextStyle.font : TextStyle.body3Button1Light5Blue.font)
                .foregroundColor( isEdit ? doneTextStyle.color : TextStyle.body3Button1Light5Blue.color)
        }
    }
    
    private func leftNavigationButton() -> some View{
        if isEdit{
            return AnyView(Button(
                action: {
                    actionSheetType = .clearAll
                }){
                Text("Clear")
                    .font(TextStyle.body3Button1Light5Blue.font)
                    .foregroundColor(TextStyle.body3Button1Light5Blue.color)
            })
        }else{
            return AnyView(EmptyView())
        }
    }
    
    private func actionSheetClearAll(props: RecentsPageViewProps) ->  ActionSheet{
        ActionSheet(
            title:
                Text("")
                .font(TextStyle.footnote2Bold1Light2SecondaryLabelColor.font)
                .foregroundColor(TextStyle.footnote2Bold1Light2SecondaryLabelColor.color),
            message: nil,
            buttons:
                [.destructive(
                    Text("Clear All Recents")
                        .font(TextStyle.title31Default1Light5Blue.font)
                        .foregroundColor(TextStyle.title31Default1Light5Blue.color),
                    action: {
                        props.removeRecents(RecentsPageViewHelper.recentViewModelsFiltered(
                            accountInfo: props.accountInfo,
                            recents: props.recents,
                            recentListCallType: recentListCallType,
                            recentListFilterType: recentListFilterType
                        ))
                    }),
                 .cancel(
                    Text("Cancel")
                        .font(TextStyle.title32Bold1Light5Blue.font)
                        .foregroundColor(TextStyle.title32Bold1Light5Blue.color))
                ]
        )
    }
    
    private func actionSheetFilter(props: RecentsPageViewProps) ->  ActionSheet{
        ActionSheet(
            title:
                Text("Filter Recent calls by")
                .font(TextStyle.footnote2Bold1Light2SecondaryLabelColor.font)
                .foregroundColor(TextStyle.footnote2Bold1Light2SecondaryLabelColor.color),
            message: nil,
            buttons:
                [.default(
                    Text("All Numbers")
                        .font(TextStyle.title31Default1Light5Blue.font)
                        .foregroundColor(TextStyle.title31Default1Light5Blue.color),
                    action: {
                        recentListFilterType = .all
                    }),
                 .default(
                    Text("Default Number")
                        .font(TextStyle.title31Default1Light5Blue.font)
                        .foregroundColor(TextStyle.title31Default1Light5Blue.color),
                    action: {
                        recentListFilterType = .defaultNumber
                    }),
                 .cancel(
                    Text("Cancel")
                        .font(TextStyle.title32Bold1Light5Blue.font)
                        .foregroundColor(TextStyle.title32Bold1Light5Blue.color))
                ]
        )
    }
}

struct RecentsPageView_Previews: PreviewProvider {
    static var previews: some View {
        let userInfo = AccountInfo(
            phones: [
                PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Vasya", lastName: "Pupkin", avatarImageUrl: ""), phoneNumber: "+11111234567", nickName: "@vpCanada", place: "mobile", sortOrder: 0, phoneCode: 1, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Kevin", lastName: "Rocker", avatarImageUrl: ""), phoneNumber: "+71111234567", nickName: "@vpRus", place: "mobile", sortOrder: 1, phoneCode: 7, locationCode: 111, ext: nil),
                PhoneInfo(isDefault: false, userInfo: UserInfo(firstName: "Omg", lastName: "Ken", avatarImageUrl: ""), phoneNumber: "+21111234567", nickName: nil, place: "home", sortOrder: 2, phoneCode: 2, locationCode: 111, ext: nil)],
            account: Account(id: 1, name: "Account name", createDate: Date()),
            devices: []
        )
        let recentCall = RecentCall( callId: "1", callType: .incoming, phoneFromNumber: "+44 20 1234 5678", phoneToNumber: "+11111234567", callStartDate: Date(),
                                     callEndDate: Date(), isExtension: false, isMissed: false,
                                     isCanceled: false,
                                     fromCountry: nil, fromExt: nil, contact: Contact( firstName: "Jay", lastName: "D", company: nil, avatarUrl: nil, phones: []),
                                     billingInfo: BillingInfo(type: .appToApp, pricePerMinute: 2, minutes: 20, currency: .usd(symbol: "$", code: "USD")))
        
        let recentCallViewModel = RecentViewModel( calls: [recentCall, recentCall], displayDate: "16:04", isMissed: false, isCanceled: false, callType: .incoming,
                                                   contact: Contact(firstName: "Jayyyyyyyy",lastName: "Daaaaaaaaaaa", company: nil,avatarUrl: nil,
                                                                    phones: [PhoneInfo(isDefault: true, userInfo: UserInfo(firstName: "Jayyyyyyyy", lastName: "Daaaaaaaaaaa", avatarImageUrl: nil), phoneNumber: "+44 20 1234 5678",nickName: "@jayd",place: "United Kingdom",sortOrder: 1,phoneCode: 44,locationCode: 44, ext: nil)]), isExtension: false, place: "United Kingdom")
        
        return RecentsPageView(props: RecentsPageViewProps(accountInfo: userInfo, recents: [recentCallViewModel], onRecentTap: {_ in}, onRecentInfoTap: {_ in}, removeRecents: {_ in}))
    }
}
