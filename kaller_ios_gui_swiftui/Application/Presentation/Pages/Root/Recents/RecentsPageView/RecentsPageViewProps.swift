struct RecentsPageViewProps {
    let accountInfo: AccountInfo
    let recents: [RecentViewModel]
    let onRecentTap: (RecentCall) -> Void
    let onRecentInfoTap: (RecentViewModel) -> Void
    let removeRecents: ([RecentViewModel]) -> Void
}
