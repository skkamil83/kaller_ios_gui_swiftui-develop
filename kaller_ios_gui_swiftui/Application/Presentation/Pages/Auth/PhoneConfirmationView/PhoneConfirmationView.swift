import SwiftUI

struct PhoneConfirmationView: View {
    let props: PhoneConfirmationViewProps

    @State private var pin = ""

    var body: some View {
        VStack {
            TextField("Pin", text: $pin)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
            Button(action: {
                props.verifyPin(pin)
            }) {
                Text("Verify")
            }
        }
    }
}