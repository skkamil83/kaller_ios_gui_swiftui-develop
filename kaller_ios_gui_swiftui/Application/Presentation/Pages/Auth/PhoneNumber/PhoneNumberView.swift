import SwiftUI

struct PhoneNumberView: View {
    private let props: PhoneNumberViewProps

    init (props: PhoneNumberViewProps) {
        self.props = props
    }

    @State private var phone = ""

    var body: some View {
        VStack {
            TextField("Phone number", text: $phone)
                    .font(.title)
                    .keyboardType(.numberPad)
                    .textFieldStyle(RoundedBorderTextFieldStyle())

            Button(action: { props.savePhone(phone) }) {
                Text("Next")
                        .font(.title)
            }
        }
    }
}