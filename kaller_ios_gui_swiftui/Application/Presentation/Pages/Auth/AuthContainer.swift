import SwiftUI

struct AuthContainer: View {
    private enum AuthStep {
        case termsOfUse
        case phone
        case verification
    }

    @EnvironmentObject private var countryState: CountryState
    @State private var step = AuthStep.termsOfUse

    var body: some View {
        VStack {
            if step == .termsOfUse {
                TermsOfUseView(
                        props: TermsOfUseViewProps(
                                acceptTermsOfUse: self.acceptTermsOfUse
                        )
                )
            } else if step == .phone {
                PhoneNumberView(
                        props: PhoneNumberViewProps(
                                countries: self.countryState.countries,
                                savePhone: self.savePhone
                        )
                )

            } else if step == .verification {
                PhoneConfirmationView(
                        props: PhoneConfirmationViewProps(
                                verifyPin: authenticate
                        )
                )
            } else {
                ProgressView() {
                    Text("Something went wrong!")
                }
            }
        }
    }

    private func savePhone(_ phone: String) {
        print("phone entered: \(phone)")
        withAnimation {
            self.step = .verification
        }
    }

    private func acceptTermsOfUse() {
        print("Terms of use accepted")
        withAnimation {
            self.step = .phone
        }
    }

    private func authenticate(_ pin: String) {
        print("verify pin: \(pin)")
        ActionDispatcher.emitAsync(ApplicationAction.logIn)
    }
}