import SwiftUI

struct TermsOfUseView: View {
    let props: TermsOfUseViewProps

    var body: some View {
        VStack {
            Text("Read and accept terms of use!")
                    .font(.title)

            Button(action: props.acceptTermsOfUse) {
                Text("Accept")
            }
        }
    }
}