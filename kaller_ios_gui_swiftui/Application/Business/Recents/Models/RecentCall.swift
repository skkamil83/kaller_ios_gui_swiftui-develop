import Foundation

struct RecentCall {
    let callId: String
    let callType: CallType
    let phoneFromNumber: String
    let phoneToNumber: String
    let callStartDate: Date
    let callEndDate: Date
    let isExtension: Bool
    let isMissed: Bool
    let isCanceled: Bool
    let fromCountry: String?
    let fromExt: String?
    let contact: Contact?
    let billingInfo: BillingInfo
}
