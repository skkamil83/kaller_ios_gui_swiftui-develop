let recentsReducer: Reducer<RecentsState, RecentsAction> = Reducer { state, action in
    switch action {

    case RecentsAction.fetchRecentsSuccess(let recents):
        state.recents = recents
    }
}