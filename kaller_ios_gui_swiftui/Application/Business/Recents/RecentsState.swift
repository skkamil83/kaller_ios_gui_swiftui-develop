import Foundation

class RecentsState: ReduxState {
    var recents: [RecentCall]? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: recents, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: recents)
        }
    }
}