enum RecentsAction: ReduxAction {
    case fetchRecentsSuccess(recents: [RecentCall])
}