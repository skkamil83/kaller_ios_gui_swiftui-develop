import Foundation

class PBXPlanState: ReduxState {
    var pbxPlans: [PBXPlan]? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: pbxPlans, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: pbxPlans)
        }
    }
}
