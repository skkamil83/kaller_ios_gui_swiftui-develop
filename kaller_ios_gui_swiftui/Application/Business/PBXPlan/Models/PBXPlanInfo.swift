struct PBXPlanInfo {
    let name: String
    let setupPrice: Double
    let numberPricePerMonth: Double
    let cloudPricePetMonth: Double
    let currency: CurrencyType
}
