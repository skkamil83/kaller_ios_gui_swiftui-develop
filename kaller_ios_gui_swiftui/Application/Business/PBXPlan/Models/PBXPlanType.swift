enum PBXPlanType {
    case basic
    case extended
    case maximum
    
    var info: PBXPlanInfo {
        switch self {
        case .basic:
            return .init(
                name: "Basic",
                setupPrice: 0,
                numberPricePerMonth: 2,
                cloudPricePetMonth: 0,
                currency: .usd(symbol: "$", code: "USD"))
        case .extended:
            return .init(
                name: "Extended",
                setupPrice: 0,
                numberPricePerMonth: 2,
                cloudPricePetMonth: 10,
                currency: .usd(symbol: "$", code: "USD"))
        case .maximum:
            return .init(
                name: "Maximum",
                setupPrice: 0,
                numberPricePerMonth: 2,
                cloudPricePetMonth: 30,
                currency: .usd(symbol: "$", code: "USD"))
        }
    }
}
