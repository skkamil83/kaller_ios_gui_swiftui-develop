enum CurrencyType {
    case usd(symbol: String, code: String)
    var symbol: String{
        switch self {
        case .usd(let symbol, _):
            return symbol
        }
    }
    var code: String{
        switch self {
        case .usd(_, let code):
            return code
        }
    }
}
