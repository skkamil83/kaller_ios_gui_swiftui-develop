enum BillingType {
    case appToApp
    case appToPhone
    case phoneToApp
    case phoneToPhone
}