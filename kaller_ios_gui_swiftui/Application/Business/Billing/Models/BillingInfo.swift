struct BillingInfo {
    let type: BillingType
    let pricePerMinute: Double
    let minutes: Int
    let currency: CurrencyType
}