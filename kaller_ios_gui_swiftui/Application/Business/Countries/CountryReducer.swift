let countryReducer: Reducer<CountryState, CountryAction> = Reducer { state, action in
    switch action {

    case CountryAction.fetchCountriesSuccess(let countries):
        state.countries = countries

    }
}