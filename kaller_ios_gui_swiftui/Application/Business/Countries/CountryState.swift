import Foundation

class CountryState: ReduxState {
    var countries: [Country]? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: countries, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: countries)
        }
    }
}