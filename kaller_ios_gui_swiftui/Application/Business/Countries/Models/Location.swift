import Foundation

struct Location {
    let localizedName: String
    let phoneCode: Int?
    let locationCode: Int?
    let previewImageUrl: String
}
