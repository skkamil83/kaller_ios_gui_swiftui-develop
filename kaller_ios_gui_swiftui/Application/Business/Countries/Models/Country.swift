import SwiftUI

struct Country: Equatable {
    let localizedName: String
    let iso: String
    let phoneCode: Int?
    let countryCode: Int?
    let flagImageName: String
}
