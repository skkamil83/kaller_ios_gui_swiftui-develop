enum CountryAction: ReduxAction {
    case fetchCountriesSuccess(countries: [Country])
}