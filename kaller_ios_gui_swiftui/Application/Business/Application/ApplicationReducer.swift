let applicationReducer: Reducer<ApplicationState, ApplicationAction> = Reducer { state, action in
    switch action {

    case ApplicationAction.logIn:
        state.isLoggedIn = true

    case ApplicationAction.logOut:
        state.isLoggedIn = false

    case ApplicationAction.loginProcessFailed:
        state.isLoggedIn = false
    }
}