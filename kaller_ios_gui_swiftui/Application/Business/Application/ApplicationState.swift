import Foundation

class ApplicationState: ReduxState {
    var isLoggedIn: Bool? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: isLoggedIn, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: isLoggedIn)
        }
    }

    var countries: [Country]? = nil
}