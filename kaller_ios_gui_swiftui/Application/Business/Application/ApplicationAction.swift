enum ApplicationAction: ReduxAction {
    case logIn
    case logOut
    case loginProcessFailed
}