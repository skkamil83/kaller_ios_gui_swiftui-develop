import Foundation

class AppStore: ActionDispatcherSubscriber {
    private(set) var applicationState: ApplicationState
    private(set) var userInfoState: UserInfoState
    private(set) var contactsState: ContactsState
    private(set) var recentsState: RecentsState
    private(set) var countryState: CountryState
    private(set) var pbxPlanState: PBXPlanState
    private(set) var chatState: ChatState
    
    init(
            applicationState: ApplicationState,
            userInfoState: UserInfoState,
            contactsState: ContactsState,
            recentsState: RecentsState,
            countryState: CountryState,
            pbxPlanState: PBXPlanState,
            chatState: ChatState
    ) {
        self.applicationState = applicationState
        self.userInfoState = userInfoState
        self.contactsState = contactsState
        self.recentsState = recentsState
        self.countryState = countryState
        self.pbxPlanState = pbxPlanState
        self.chatState = chatState
        
        ActionDispatcher.subscribe(self)
    }

    func notify<Action>(_ action: Action) {
        if let action = action as? ApplicationAction {
            applicationReducer.reduce(&applicationState, action)
            return
        }

        if let action = action as? UserInfoAction {
            userInfoReducer.reduce(&userInfoState, action)
            return
        }

        if let action = action as? ContactsAction {
            contactsReducer.reduce(&contactsState, action)
            return
        }

        if let action = action as? RecentsAction {
            recentsReducer.reduce(&recentsState, action)
            return
        }

        if let action = action as? CountryAction {
            countryReducer.reduce(&countryState, action)
            return
        }
        
        if let action = action as? PBXPlanAction {
            pbxlanReducer.reduce(&pbxPlanState, action)
        }
        
        if let action = action as? ChatAction {
            chatReducer.reduce(&chatState, action)
        }
    }

    deinit {
        Logger.log(sender: self, message: "deInit")
    }
}
