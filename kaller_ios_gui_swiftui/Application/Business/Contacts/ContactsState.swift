import Foundation

class ContactsState: ReduxState {
    
    var addressBooks: [String: [Contact]]? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: addressBooks, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: addressBooks)
        }
    }
    
    var localContacts: [Contact] {
        get {
            return addressBooks?[AppConstant.localAddressBookId] ?? []
        }
        set {
            addressBooks?[AppConstant.localAddressBookId] = newValue
        }
    }
}
