import Foundation

struct PhoneHelper {
    static func sortPhones(_ phones: [PhoneInfo]) -> [PhoneInfo] {
        phones.sorted { $0.sortOrder < $1.sortOrder}
    }
    static func getDefaultPhone(_ phones: [PhoneInfo]) -> PhoneInfo? {
        sortPhones(phones).first
    }
    static func getSimNumber(phones: [PhoneInfo], phone: PhoneInfo) -> Int? {
        guard
            let index = (sortPhones(phones).firstIndex { $0.phoneNumber == phone.phoneNumber })
        else {
            return nil
        }
        return index + 1
    }
}
