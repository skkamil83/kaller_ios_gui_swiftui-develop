let contactsReducer: Reducer<ContactsState, ContactsAction> = Reducer { state, action in
    switch action {
    case .fetchPhoneNumberContactsSuccess(phoneNumber: let phoneNumber, contacts: let contacts):
        if state.addressBooks != nil {
            state.addressBooks![phoneNumber] = contacts
        } else {
            state.addressBooks = [phoneNumber: contacts]
        }
    }
}
