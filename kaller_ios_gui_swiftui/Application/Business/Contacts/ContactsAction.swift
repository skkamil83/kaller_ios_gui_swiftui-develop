enum ContactsAction: ReduxAction {
    case fetchPhoneNumberContactsSuccess(phoneNumber: String, contacts: [Contact])
}
