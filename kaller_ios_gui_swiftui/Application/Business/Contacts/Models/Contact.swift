import Contacts

struct Contact {
    let firstName: String?
    let lastName: String?
    let company: String?
    let avatarUrl: String?
    let phones: [PhoneInfo]

    var defaultPhone: PhoneInfo? {
        get { phones.first(where: {$0.isDefault}) }
    }
}
