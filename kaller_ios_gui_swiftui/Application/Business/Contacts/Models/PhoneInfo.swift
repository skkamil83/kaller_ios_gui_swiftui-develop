struct PhoneInfo {
    var isDefault: Bool
    let userInfo: UserInfo
    let phoneNumber: String
    let nickName: String?
    let place: String
    var sortOrder: Int
    let phoneCode: Int // todo figure out, me we have to calc it from phone just in time
    let locationCode: Int // todo figure out, me we have to calc it from phone just in time
    let ext: String?
}

extension PhoneInfo: Identifiable {
    var id: String { phoneNumber }
    var isExtension: Bool {
        return ext != nil
    }
}
