let chatReducer: Reducer<ChatState, ChatAction> = Reducer { state, action in
    switch action {
    case .fetchConversationsSuccess(conversations: let conversations):
        state.conversations = conversations
    case .fetchArchivedConversationsSuccess(archivedConversations: let archivedConversations):
        state.archivedConversations = archivedConversations
    }
}
