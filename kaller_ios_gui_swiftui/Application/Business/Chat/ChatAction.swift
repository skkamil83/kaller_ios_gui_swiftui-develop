enum ChatAction: ReduxAction {
    case fetchConversationsSuccess(conversations: [Conversation])
    case fetchArchivedConversationsSuccess(archivedConversations: ArchivedConversations)
}
