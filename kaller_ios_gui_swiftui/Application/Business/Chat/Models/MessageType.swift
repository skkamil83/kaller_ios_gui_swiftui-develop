enum MessageType {
    case text
    case photo
    case video
    case location
    case contact
    case audio

    var iconName: String? {
        switch self {
        case .text:
            return nil
        case .photo:
            return "camera"
        case .video:
            return "play.circle"
        case .location:
            return "location"
        case .contact:
            return "person.circle"
        case .audio:
            return "mic"
        }
    }
}
