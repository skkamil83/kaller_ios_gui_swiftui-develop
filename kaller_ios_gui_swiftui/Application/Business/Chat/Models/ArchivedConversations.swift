struct ArchivedConversations {
    var isUnread: Bool
    var unreadCount: Int
    var isPinned: Bool
    var conversations: [Conversation]
}
