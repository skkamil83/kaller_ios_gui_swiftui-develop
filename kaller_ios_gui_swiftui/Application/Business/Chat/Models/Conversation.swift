import Foundation

struct Conversation {
    let id: String
    let type: ConversationType
    let members: [PhoneInfo]
    let userPhoneInfo: PhoneInfo
    let title: String?
    let iconUrl: String?
    var messages: [MessageInfo]
    var isUnread: Bool
    var unreadCount: Int
    var dateOfPin: Date?
    var isMuted: Bool
    var createdDate: Date
    var isPinned: Bool {
        return dateOfPin != nil
    }
    var newestMessage: MessageInfo? {
        return messages.sorted {
            $0.date > $1.date
        }.first
    }
}
