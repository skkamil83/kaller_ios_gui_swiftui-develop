enum MessageStatus {
    case notSent
    case sent
    case deliverd
    case seen
    case viaSMS

    var imageName: String {
        switch self {
        case .notSent:
            return "iconsWaiting"
        case .sent:
            return "iconsNotDeliverd"
        case .deliverd:
            return "iconsDeliverd"
        case .seen:
            return "iconsSeen"
        case .viaSMS:
            return "iconsSms"
        }
    }
}
