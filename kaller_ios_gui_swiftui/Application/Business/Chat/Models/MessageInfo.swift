import Foundation

struct MessageInfo {
    let id: String
    let message: String
    let messageType: MessageType
    let messageStatus: MessageStatus
    let date: Date
    let sendFromPhoneNumber: String
    var phoneNumbersHaveRead: [String]
}
