import Foundation

class ChatState: ReduxState {
    var conversations: [Conversation]? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: conversations, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: conversations)
        }
    }

    var archivedConversations: ArchivedConversations? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: archivedConversations, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: archivedConversations)
        }
    }
}
