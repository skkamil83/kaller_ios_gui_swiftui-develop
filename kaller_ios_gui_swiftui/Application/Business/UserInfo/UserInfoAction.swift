enum UserInfoAction: ReduxAction {
    case fetchUserInfoSuccess(info: AccountInfo)
}
