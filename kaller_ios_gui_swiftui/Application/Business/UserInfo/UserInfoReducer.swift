let userInfoReducer: Reducer<UserInfoState, UserInfoAction> = Reducer { state, action in
    switch action {

    case UserInfoAction.fetchUserInfoSuccess(let accountInfo):
        state.accountInfo = accountInfo
    }
}
