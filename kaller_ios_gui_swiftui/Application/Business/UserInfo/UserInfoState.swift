import Foundation

class UserInfoState: ReduxState {
    var accountInfo: AccountInfo? = nil {
        willSet {
            handleWillChange(sender: self, name: #function, oldValue: accountInfo, newValue: newValue)
        }
        didSet {
            handleDidChange(sender: self, name: #function, oldValue: oldValue, newValue: accountInfo)
        }
    }
}
