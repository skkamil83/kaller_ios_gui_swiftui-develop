import Combine
import Foundation

protocol Effect {
    func apply()
}

extension Effect {
    static func handleError(error: Error, data: [String: Any] = [:]) {
        Logger.log(error: error)
    }
}

protocol ActionDispatcherSubscriber {
    func notify<Action: ReduxAction>(_ action: Action)
}

struct HistoryItem {
    let action: ReduxAction?
    let sideEffect: Effect?

    init(action: ReduxAction) {
        self.action = action
        self.sideEffect = nil
    }

    init(effect: Effect) {
        self.action = nil
        self.sideEffect = effect
    }

    func toString() -> String {
        if action != nil {
            return "\(action!)"
        } else if sideEffect != nil {
            return "\(sideEffect!)"
        } else {
            return "nil"
        }
    }
}

class ActionDispatcher {
    private static var subscribers: [ActionDispatcherSubscriber] = []
    private(set) static var history: [HistoryItem] = []

    class func subscribe(_ subscriber: ActionDispatcherSubscriber) {
        subscribers.append(subscriber)
    }

    class func emitSync<Action: ReduxAction>(_ action: Action) {
        self.history.append(HistoryItem(action: action))
        Logger.log(action)
        DispatchQueue.global(qos: .default).sync {
            subscribers.forEach {
                $0.notify(action)
            }
        }
    }

    class func emitAsync<Action: ReduxAction>(_ action: Action) {
        self.history.append(HistoryItem(action: action))
        Logger.log(action)
        DispatchQueue.main.async {
            subscribers.forEach {
                $0.notify(action)
            }
        }
    }

    class func emitAsync<Action: ReduxAction>(_ action: Action, deadline: DispatchTime) {
        self.history.append(HistoryItem(action: action))
        Logger.log(action)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            subscribers.forEach {
                $0.notify(action)
            }
        }
    }

    class func emitSync<E: Effect>(_ effect: E) {
        self.history.append(HistoryItem(effect: effect))
        Logger.log(effect)
        DispatchQueue.global(qos: .default).sync {
            effect.apply()
        }
    }

    class func emitAsync<E: Effect>(_ effect: E) {
        self.history.append(HistoryItem(effect: effect))
        Logger.log(effect)
        DispatchQueue.global(qos: .background).async {
            effect.apply()
        }
    }
}

protocol ReduxAction {}

open class ReduxState: ObservableObject {
    public let objectWillChange = ObservableObjectPublisher()
    public let objectDidChange = ObservableObjectPublisher()

    public func handleWillChange(sender: Any, name: String, oldValue: Any?, newValue: Any?) {
        DispatchQueue.main.async { [weak self] in
            self?.objectWillChange.send()
        }
        Logger.log(state: self, name: name, oldValue: oldValue, newValue: newValue)
    }

    public func handleDidChange(sender: Any, name: String, oldValue: Any?, newValue: Any?) {
        DispatchQueue.main.async { [weak self] in
            self?.objectDidChange.send()
        }
        Logger.log(state: self, name: name, oldValue: oldValue, newValue: newValue)
    }
}

class Logger {

    class func log(error: NSException, data: [String: Any] = [:]) {
        #if DEBUG
        print(
                """
                Error: \(type(of: error)) at \(currDate): name: \(error.name) 
                    reason: \(error.reason ?? "nil")
                    data: \(data)
                    callstack: \n\(error.callStackSymbols.joined(separator: "\n"))
                """
        )
        #endif
    }

    class func log(error: Error) {
        #if DEBUG
        if let error = error as? IAppError {
            print("Error: \(type(of: error)) at \(currDate): \(error.toString())")
        } else {
            print("Error: \(type(of: error)) at \(currDate): \(error.localizedDescription)")
        }
        #endif
    }

    class func log(state: ReduxState, name: String, oldValue: Any?, newValue: Any?) {
        #if DEBUG
        let mirror = Mirror(reflecting: state)
        let message =
                """
                '\(name)' changes
                  from: \(oldValue ?? "nil")
                  to: \(newValue ?? "nil")
                """
        // print("\(mirror.subjectType) at \(currDate): \(message)")
        #endif
    }

    class func log(sender: Any, message: String) {
        #if DEBUG
        let mirror = Mirror(reflecting: sender)
        print("\(mirror.subjectType) at \(currDate): \(message)")
        #endif
    }

    class func log(sender: Any, message: String, data: String) {
        #if DEBUG
        let mirror = Mirror(reflecting: sender)
        print("\(mirror.subjectType) at \(currDate): \(message): \n\t \(data)")
        #endif
    }

    class func log<Action: ReduxAction>(_ action: Action) {
        #if DEBUG
        print("Action at \(currDate): \(type(of: action)).\(action)")
        #endif
    }

    class func log(_ effect: Effect) {
        #if DEBUG
        print("Effect at \(currDate): \(type(of: effect)).\(effect)")
        #endif
    }

    private class var currDate: String {
        "\(Date().toString(as: DateFormat.defaultTime))"
    }
}

struct Reducer<State: ReduxState, Action: ReduxAction> {
    let reduce: (inout State, Action) -> Void
}