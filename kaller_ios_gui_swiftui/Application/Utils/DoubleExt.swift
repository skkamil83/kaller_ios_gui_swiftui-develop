extension Double {
    func toString(decimalCount: Int = 1) -> String {
        String(format: "%.\(decimalCount)f", self)
    }
    
    func toFitString() -> String {
        if self - Double(Int(self)) == 0 {
            return self.toString(decimalCount: 0)
        } else {
            return "\(self)"
        }
    }
}
