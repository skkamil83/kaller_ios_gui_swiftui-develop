import Contacts

class ContactsService {
    
    static func ableToAccessContacts() -> Bool {
        return CNContactStore.authorizationStatus(for: .contacts) == .authorized
    }
    
    static func fetchCNContacts() -> [CNContact] {
        var contacts = [CNContact]()
        let keys = [CNContactGivenNameKey,
                    CNContactFamilyNameKey,
                    CNContactOrganizationNameKey,
                    CNContactPhoneNumbersKey,
                    CNContactNicknameKey,
                    CNContactDepartmentNameKey,
                    CNContactImageDataAvailableKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: keys)
        request.sortOrder = CNContactSortOrder.familyName
        
        let contactStore = CNContactStore()
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                contacts.append(contact)
            }
        }
        catch {
            
        }
        return contacts
    }
    
    static func fetchLocalContacts() -> [Contact] {
        let cnContacts = fetchCNContacts()
        var contacts = [Contact]()
        for cnContact in cnContacts {
            print(cnContact)
            contacts.append(buildContact(cnContact: cnContact))
        }
        return contacts
    }
    
    static func buildContact(cnContact: CNContact) -> Contact {
        var phones = [PhoneInfo]()
        for i in 0..<cnContact.phoneNumbers.count{
            let cnPhone = cnContact.phoneNumbers[i]
            let value = cnPhone.value
            let phoneInfo = PhoneUtils.tryFormatPhone(rawNumber: value.stringValue, format: .e164)
            phones.append(
                PhoneInfo(
                    isDefault: false,
                    userInfo: UserInfo(
                        firstName: cnContact.givenName,
                        lastName: cnContact.familyName,
                        avatarImageUrl: nil
                    ),
                    phoneNumber: value.stringValue,
                    nickName: cnContact.nickname,
                    place: cnPhone.label ?? "",
                    sortOrder: i,
                    phoneCode: Int(phoneInfo.phoneCode ?? 0),
                    locationCode: 0,
                    ext: nil
                )
            )
        }
        return Contact(
            firstName: cnContact.givenName,
            lastName: cnContact.familyName,
            company: cnContact.organizationName,
            avatarUrl: nil,
            phones: phones)
    }
    
}

