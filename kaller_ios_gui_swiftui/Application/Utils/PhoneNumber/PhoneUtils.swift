import Foundation
import PhoneNumberKit

enum PhoneFormat {
    case international
    case e164
}

struct PhoneFormattingResult {
    let phone: String
    let parsed: Bool
    let phoneCode: UInt64?
    let region: String?

    init(
            phone: String,
            parsed: Bool = false,
            phoneCode: UInt64? = nil,
            region: String? = nil
    ) {
        self.phone = phone
        self.parsed = parsed
        self.phoneCode = phoneCode
        self.region = region
    }
}

class PhoneUtils {
    private static let phoneNumberKit = PhoneNumberKit()

    static func cleanPhoneNumber(_ phoneNum: String) -> String {
        phoneNum.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
    }

    static func getFormattedStringWithoutOccurrences(phone: String) -> String {
        var result = phone

        result = result.replacingOccurrences(of: "(", with: "")
        result = result.replacingOccurrences(of: ")", with: "")
        result = result.replacingOccurrences(of: "-", with: " ")

        return result
    }

    static func tryFormatPhone(rawNumber: String, format: PhoneFormat, withRegion: String? = nil) -> PhoneFormattingResult {
        do {
            var phoneNumber: PhoneNumber

            if let region = withRegion {
                phoneNumber = try phoneNumberKit.parse(rawNumber, withRegion: region)
            } else {
                phoneNumber = try phoneNumberKit.parse(rawNumber)
            }

            let formatted: String!
            switch format {
            case .e164:
                formatted = phoneNumberKit.format(phoneNumber, toType: .e164)
            case .international:
                formatted = phoneNumberKit.format(phoneNumber, toType: .international)
            }

            return PhoneFormattingResult(
                    phone: formatted,
                    parsed: true,
                    phoneCode: phoneNumber.countryCode,
                    region: phoneNumber.regionID
            )
        } catch {
            return PhoneFormattingResult(phone: rawNumber, parsed: false)
        }
    }
}