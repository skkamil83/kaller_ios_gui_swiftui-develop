import Foundation

enum DateFormat: String {
    case defaultDate = "dd/mm/yy"
    case defaultTime = "HH:mm:ss"
}

extension Date {
    func toString(as format: DateFormat, timezone: TimeZone? = nil) -> String {
        let formatter = DateFormatter()

        formatter.timeZone = timezone ?? TimeZone(secondsFromGMT: 0)
        formatter.locale = NSLocale(localeIdentifier: "en_us_POSIX") as Locale

        formatter.dateFormat = format.rawValue

        return formatter.string(from: self)
    }

    func toString(as format: DateFormat, timezone: String) -> String {
        if let tzone = TimeZone(identifier: timezone) {
            return toString(as: format, timezone: tzone)
        }

        return toString(as: format)
    }
    
    func formattedDate() -> String{
        let calendar = Calendar.current
        let dateFormatter = DateFormatter()
        
        if(calendar.isDateInToday(self)){
            dateFormatter.dateFormat = "HH:mm"
            return dateFormatter.string(from: self)
            
        } else if (calendar.isDateInYesterday(self)){
            return "Yesterday"
            
        } else {
            let currentComponents = calendar.dateComponents([.weekOfYear], from: Date())
            let dateComponents = calendar.dateComponents([.weekOfYear], from: self)
            
            if let currentWeekOfYear = currentComponents.weekOfYear, let dateWeekOfYear = dateComponents.weekOfYear{
                if(currentWeekOfYear == dateWeekOfYear){
                    dateFormatter.dateFormat = "EEEE"
                    return dateFormatter.string(from: self)
                }
            }
            
            if calendar.isDate(Date(), equalTo: self, toGranularity: .month){
                dateFormatter.dateFormat = "dd.MM"
                return dateFormatter.string(from: self)
            }
            
            dateFormatter.dateFormat = "dd.MM.yyy"
            return dateFormatter.string(from: self)
        }
    }
    
    func formattedTitleDate() -> String{
        let calendar = Calendar.current
        let dateFormatter = DateFormatter()
        
        if(calendar.isDateInToday(self)){
            return "Today"
            
        } else if (calendar.isDateInYesterday(self)){
            return "Yesterday"
            
        } else {
            let currentComponents = calendar.dateComponents([.weekOfYear], from: Date())
            let dateComponents = calendar.dateComponents([.weekOfYear], from: self)
            
            if let currentWeekOfYear = currentComponents.weekOfYear, let dateWeekOfYear = dateComponents.weekOfYear{
                if(currentWeekOfYear == dateWeekOfYear){
                    dateFormatter.dateFormat = "EEEE"
                    return dateFormatter.string(from: self)
                }
            }
            
            if calendar.isDate(Date(), equalTo: self, toGranularity: .month){
                dateFormatter.dateFormat = "dd MMMM"
                return dateFormatter.string(from: self)
            }
            
            dateFormatter.dateFormat = "dd MMMM yyyy"
            return dateFormatter.string(from: self)
        }
    }
}

extension String {
    static let timeZoneRegex = "[+-]\\d{2}[:]\\d{2}"

    func asDate(format: DateFormat = DateFormat.defaultDate) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
        formatter.locale = NSLocale(localeIdentifier: "en_us_POSIX") as Locale
        guard let result = formatter.date(from: self) else {
            return nil
        }

        return result
    }

    var asDateIgnoreTimeZone: Date? {
        let dateStringIgnoringTimeZone =
                self.replacingOccurrences(of: String.timeZoneRegex, with: "", options: .regularExpression)
                        .replacingOccurrences(of: " AM", with: "")
                        .replacingOccurrences(of: " PM", with: "")
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = DateFormat.defaultDate.rawValue

        return formatter.date(from: dateStringIgnoringTimeZone)
    }
}

extension Int{
    func secondsToHoursMinutes() -> String {
        let hours = self/3600
        let minutes = (self%3600) / 60
        let seconds = (self%3600) % 60
        if hours > 0 {
            return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        }else{
            return String(format:"%02i:%02i", minutes, seconds)
        }
    }
    
    func secondsToHoursMinutesSeconds() -> String {
        let hours = self/3600
        let minutes = (self%3600)/60
        let seconds = (self%3600)%60
        if(hours > 0){
            return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        }else if minutes > 0{
            return String(format:"%02i:%02i", minutes, seconds)
        }else{
            return String(format:"%02i", seconds)
        }
    }
}
