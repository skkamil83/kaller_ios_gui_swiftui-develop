class ConversationsHelper {
    
    static func filterConversation(conversations: [Conversation], filterType: PhoneNumberFilterType, defaultPhone: PhoneInfo?) -> [Conversation] {
        switch filterType {
        case .allNumbers:
            return conversations
        case .defaultNumber:
            if let defaultPhone = defaultPhone {
                return conversations.filter({ $0.userPhoneInfo.id == defaultPhone.id })
            } else {
                return []
            }
        }
    }

    static func buildConversationViewModels(conversations: [Conversation], isArchived: Bool, filterType: PhoneNumberFilterType, defaultPhone: PhoneInfo?, accountInfo: AccountInfo) -> [ConversationViewModel] {
        var viewModels = [ConversationViewModel]()

        let conversationsFiltered = filterConversation(conversations: conversations, filterType: filterType, defaultPhone: defaultPhone)
        let conversationsSorted = conversationsFiltered.sorted { (first, second) -> Bool in
            if let firstDateOfPin = first.dateOfPin, let secondDateOfPin = second.dateOfPin {
                return firstDateOfPin > secondDateOfPin
            } else if first.dateOfPin != nil {
                return true
            } else if second.dateOfPin != nil {
                return false
            } else {
                return first.newestMessage?.date ?? first.createdDate > second.newestMessage?.date ?? second.createdDate
            }
        }
        for conversation in conversationsSorted {
            if let newestMessage = conversation.newestMessage {
                viewModels.append(ConversationViewModel(
                        id: conversation.id,
                        avatar: getAvatar(conversation: conversation),
                        title: getTitleOfConversation(conversation: conversation),
                        conversationType: conversation.type,
                        numberOfSim: accountInfo.getSimNumber(phone: conversation.userPhoneInfo) ?? 0,
                        isShowSimNumber: filterType == .allNumbers,
                        message: newestMessage.message,
                        messageType: newestMessage.messageType,
                        messageStatus: conversation.userPhoneInfo.phoneNumber != newestMessage.sendFromPhoneNumber ? nil : newestMessage.messageStatus,
                        date: newestMessage.date,
                        isArchived: isArchived,
                        isUnread: conversation.isUnread,
                        unreadCount: conversation.unreadCount,
                        dateOfPin: conversation.dateOfPin,
                        isMuted: conversation.isMuted)
                )
            } else {
                viewModels.append(ConversationViewModel(
                        id: conversation.id,
                        avatar: getAvatar(conversation: conversation),
                        title: getTitleOfConversation(conversation: conversation),
                        conversationType: conversation.type,
                        numberOfSim: accountInfo.getSimNumber(phone: conversation.userPhoneInfo) ?? 0,
                        isShowSimNumber: filterType == .allNumbers,
                        message: "",
                        messageType: .text,
                        messageStatus: nil,
                        date: conversation.createdDate,
                        isArchived: false,
                        isUnread: false,
                        unreadCount: 0,
                        dateOfPin: nil,
                        isMuted: false)
                )
            }
        }
        return viewModels
    }

    static func buildChatListArchivedViewModel(archivedConversations: ArchivedConversations?) -> ChatListArchivedViewModel? {
        guard let archivedConversations = archivedConversations,
              let newestMessage = getNewestMessageInArchived(archivedConversations: archivedConversations)
                else {
            return nil
        }

        return ChatListArchivedViewModel(
                date: newestMessage.date,
                isUnread: archivedConversations.isUnread,
                unreadCount: archivedConversations.unreadCount,
                isPinned: archivedConversations.isPinned,
                content: getContentInArchived(archivedConversations: archivedConversations),
                messageStatus: newestMessage.messageStatus
        )
    }

    static func getNewestMessageInArchived(archivedConversations: ArchivedConversations?) -> MessageInfo? {
        guard let archivedConversations = archivedConversations
                else {
            return nil
        }
        var messages = [MessageInfo]()
        for conversation in archivedConversations.conversations {
            messages += conversation.messages
        }
        return messages.sorted {
            $0.date > $1.date
        }.first
    }

    static func getContentInArchived(archivedConversations: ArchivedConversations?) -> String {
        return archivedConversations?.conversations.map({ getTitleOfConversation(conversation: $0) }).joined(separator: ", ") ?? ""
    }

    static func getAvatar(conversation: Conversation) -> AvatarViewProps.Style {
        if let iconUrl = conversation.iconUrl {
            return .bitmapImage(urlString: iconUrl)
        }
        switch conversation.type {
        case .dual:
            let phoneInfo = conversation.members.first(where: { $0.id != conversation.userPhoneInfo.id })
            return AvatarViewHelper.getAvatarStyle(from: phoneInfo)
        case .group:
            return .image(imageName: "avatarGroup")
        case .favorite:
            return .whiteGlyph(systemImageName: "star.fill")
        }
    }

    static func getTitleOfConversation(conversation: Conversation) -> String {
        if let title = conversation.title, !title.isEmpty {
            return title
        } else {
            switch conversation.type {
            case .dual:
                if let phoneInfo = conversation.members.first(where: { $0.id != conversation.userPhoneInfo.id }) {
                    return phoneInfo.userInfo.fullName
                }
                return ""
            case .group:
                return ""
            case .favorite:
                return "Favorite Messages"
            }
        }
    }
    
    static func getTitleOfDeleteActionSheet(conversations: [ConversationViewModel], idsSelected: [String]) -> String {
        if idsSelected.count > 1 {
            return "\(idsSelected.count) Conversations selected"
        } else {
            if let conversation = conversations.first(where: { $0.id == idsSelected.first }) {
                return "Would you like to delete or clear this conversation with \(conversation.title)?"
            }
        }
        return ""
    }

    static func checkIsShowReadAllInTabbar(conversations: [ConversationViewModel], idsSelected: [String]) -> Bool {
        if conversations.first(where: { $0.isUnread }) != nil {
            for id in idsSelected {
                if conversations.first(where: { $0.id == id })?.isUnread != nil {
                    return false
                }
            }
            return true
        }
        return false
    }

    static func checkReadButtonInTabbarIsDisable(conversations: [ConversationViewModel], idsSelected: [String]) -> Bool {
        if conversations.first(where: { $0.isUnread }) != nil {
            for id in idsSelected {
                if conversations.first(where: { $0.id == id })?.isUnread != nil {
                    return false
                }
            }
        }
        return true
    }
}
