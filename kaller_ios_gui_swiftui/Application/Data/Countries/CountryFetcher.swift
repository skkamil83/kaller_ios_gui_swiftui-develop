import CountryKit

class CountryFetcher {
    private static let imgFlagPrefix = "flag_"
    private lazy var countryLib =  CountryKit()

    func getCountryByName(_ name: String) -> Country? {
        countries.first { $0.localizedName.lowercased().contains(name.lowercased()) }
    }

    func getCountryByCountryCode(_ countryCode: Int) -> Country? {
        countries.first { $0.countryCode == countryCode }
    }

    var countries: [Country] {
        get {
            countryLib.countries
                    .filter { !Self.excludedIsoCodes.contains($0.iso)}
                    .map { Country(
                            localizedName: $0.localizedName,
                            iso: $0.iso,
                            phoneCode: $0.phoneCode,
                            countryCode: $0.countryCode,
                            flagImageName: "\(Self.imgFlagPrefix)\($0.iso)"
                    ) }
        }
    }

    var countriesAvailableToWorkWith: [Country] {
        countries
                .filter { Self.availableIsoCodes.contains($0.iso) }
    }

    private static let availableIsoCodes = [
        "CA", // Canada
        "IL", // Israel,
        "GB", // UK
        "US", // US
    ]
    
    private static let highlightedIsoCodes = [
        "CA", // Canada
        "IL", // Israel
    ]

    private static let excludedIsoCodes = [
        "AQ", // Antarctica
        "AC", // Ascension island,
        "EA", // Ceuta and melilla
        "DG", // Diego Garcia
        "SH", // St. Helena
        "GS", // So. Georgia and Sandwich isl.
        "TA", // Tristan da Cunha
    ]
}
