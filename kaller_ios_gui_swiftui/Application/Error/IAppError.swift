import Foundation

protocol IAppError: Error {
    var message: String { get }
    var callStack: [String] { get }
    var sender: Mirror { get }
    var error: Error? { get }

    func toString() -> String
    func data() -> [String: String]
}