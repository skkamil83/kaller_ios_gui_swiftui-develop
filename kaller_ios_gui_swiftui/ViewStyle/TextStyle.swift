// This file was generated using Prism
import SwiftUI

extension TextStyle {
    // iOS doesn't have built-in support for "Text Styles", per-se.
    //
    // This works given a custom `TextStyle` struct to hold these
    // text styling properties and allowing to apply a custom .textStyle
    // modifier to `View`s in your app. See `TextStyle+SwiftUI.swift`
    // for more inspiration around this.
    static let body1Default1Light1LabelColor2CenterAligned = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .black
    )

    static let body1Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .secondaryLabel
    )

    static let body1Default1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .tertiaryLabel
    )

    static let body1Default1Light5Blue = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .primaryBlue
    )

    static let body1Default1Light6SystemRed = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .tintsSystemRedLight
    )

    static let body1Default2Dark1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .white
    )

    static let body2Bold1Light1LabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 17,
        color: .black
    )

    static let body2Bold1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 17,
        color: .secondaryLabel
    )

    static let body2Bold1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 17,
        color: .tertiaryLabel
    )

    static let body2Bold2Dark1LabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 17,
        color: .white
    )

    static let body2Bold2Dark1LabelColor2CenterAligned = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 17,
        color: .white
    )

    static let body2Dark2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .secondaryLabelDark
    )

    static let body3Button1Light5Blue = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 17,
        color: .primaryBlue
    )

    static let body21Default1Light1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 16,
        color: .black
    )

    static let body22Bold1Light1LabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 16,
        color: .black
    )

    static let button1DefaultLight5Blue = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .primaryBlue
    )

    static let buttonBlueGlyph = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 16,
        color: .primaryBlue
    )

    static let cameraIcon = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 28,
        color: .white
    )

    static let cameraSourceWhite = TextStyle(
        fontName: "SFCompactText-Regular",
        fontSize: 15,
        color: .white
    )

    static let cameraSourceYellow = TextStyle(
        fontName: "SFCompactText-Regular",
        fontSize: 15,
        color: .yellowDark
    )

    static let caption11Default1Light1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 12,
        color: .black
    )

    static let caption11Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 12,
        color: .secondaryLabel
    )

    static let caption11Default1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 12,
        color: .tertiaryLabel
    )

    static let caption11Default1Light5Blue = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 12,
        color: .primaryBlue
    )

    static let caption11Default1Light6DarkGray = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 12,
        color: .graysSystemGray3Dark
    )

    static let caption11Default2Dark1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 12,
        color: .white
    )

    static let caption12Bold1Light1LabelColor = TextStyle(
        fontName: "SFProText-Medium",
        fontSize: 12,
        color: .black
    )

    static let caption12Bold2Dark1LabelColor = TextStyle(
        fontName: "SFProText-Medium",
        fontSize: 12,
        color: .white
    )

    static let caption13Semi1Light6Disabled = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 12,
        color: .tintsDisabledLight
    )

    static let caption14Light1Light1LabelColor = TextStyle(
        fontName: "SFProText-Light",
        fontSize: 12,
        color: .black
    )

    static let caption21Default1Light1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 11,
        color: .black
    )

    static let caption21Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 11,
        color: .secondaryLabel
    )

    static let caption21Default1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 11,
        color: .tertiaryLabel
    )

    static let footnote1Default1Light1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 13,
        color: .black
    )

    static let footnote1Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 13,
        color: .secondaryLabel
    )

    static let footnote1Default1Light5Blue = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 13,
        color: .primaryBlue
    )

    static let footnote1Default1Light6Alert = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 13,
        color: .black
    )

    static let footnote1Default1Light6DarkGray = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 13,
        color: .graysSystemGray2Dark
    )

    static let footnote2Bold1Light1LabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 13,
        color: .black
    )

    static let footnote2Bold1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 13,
        color: .secondaryLabel
    )

    static let footnote21Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 13,
        color: .secondaryLabel
    )

    static let iconFile32Dark1LabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 10,
        color: .white
    )

    static let iconFile42Dark1LabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 7,
        color: .white
    )

    static let iconSIM12Dark1LabelColor = TextStyle(
        fontName: "SFProText-Medium",
        fontSize: 14,
        color: .white
    )

    static let iconSIM22Dark1LabelColor = TextStyle(
        fontName: "SFProText-Medium",
        fontSize: 10,
        color: .white
    )

    static let iconSIM32Dark1LabelColor = TextStyle(
        fontName: "SFCompactText-Medium",
        fontSize: 7,
        color: .white
    )

    static let keypadNumber = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 36,
        color: .black
    )

    static let keypadNumber320 = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 27,
        color: .black
    )

    static let keypadNumber414 = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 42,
        color: .black
    )

    static let keypadNumberDark = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 36,
        color: .white
    )

    static let keypadPhoneLabel = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 19,
        color: .black
    )

    static let keypadPhoneName = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 19,
        color: .secondaryLabel
    )

    static let keypadTips = TextStyle(
        fontName: "SFProText-Bold",
        fontSize: 10,
        color: .black
    )

    static let keypadTips320 = TextStyle(
        fontName: "SFProText-Bold",
        fontSize: 8,
        color: .black
    )

    static let keypadTips414 = TextStyle(
        fontName: "SFProText-Bold",
        fontSize: 12,
        color: .black
    )

    static let keypadTipsDark = TextStyle(
        fontName: "SFProText-Bold",
        fontSize: 10,
        color: .white
    )

    static let keypadUnknowContact = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 19,
        color: .primaryBlue
    )

    static let largeTitle2Bold1Light1LabelColor = TextStyle(
        fontName: "SFProDisplay-Bold",
        fontSize: 34,
        color: .black
    )

    static let largeTitle2Dark1LabelColor = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 34,
        color: .white
    )

    static let messageEmoji = TextStyle(
        fontName: "AppleColorEmoji",
        fontSize: 48,
        color: .secondaryLabel
    )

    static let sFProText10PtPrimaryBlue = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 10,
        color: .primaryBlue
    )

    static let subheadline1Default1Light1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .black
    )

    static let subheadline1Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .secondaryLabel
    )

    static let subheadline1Default1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .tertiaryLabel
    )

    static let subheadline1Default1Light5Blue = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .primaryBlue
    )

    static let subheadline1Default1Light6Red = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .tintsSystemRedLight
    )

    static let subheadline1Default2Dark1LabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .white
    )

    static let subheadline1Default2Dark3TertiaryLabelColor = TextStyle(
        fontName: "SFProText-Regular",
        fontSize: 15,
        color: .tertiaryLabelDark
    )

    static let subheadline2Bold1Light1LabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 15,
        color: .black
    )

    static let subheadline2Bold1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 15,
        color: .secondaryLabel
    )

    static let subheadline2Bold1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 15,
        color: .tertiaryLabel
    )

    static let subheadline2Bold1Light5Blue = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 15,
        color: .primaryBlue
    )

    static let subheadline2Bold1Light6SystemRed = TextStyle(
        fontName: "SFProText-Semibold",
        fontSize: 15,
        color: .tintsSystemRedLight
    )

    static let title11Default1Light1LabelColor2CenterAligned = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 28,
        color: .black
    )

    static let title21Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 22,
        color: .secondaryLabel
    )

    static let title23Semi1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProDisplay-Semibold",
        fontSize: 22,
        color: .black
    )

    static let title31Default1Light1LabelColor = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 20,
        color: .black
    )

    static let title31Default1Light2SecondaryLabelColor = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 20,
        color: .secondaryLabel
    )

    static let title31Default1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 20,
        color: .tertiaryLabel
    )

    static let title31Default1Light5Blue = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 20,
        color: .primaryBlue
    )

    static let title31Default1Light6Disabled = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 20,
        color: .tintsDisabledLight
    )

    static let title31Default1Light7SystemRed = TextStyle(
        fontName: "SFProDisplay-Regular",
        fontSize: 20,
        color: .tintsSystemRedLight
    )

    static let title32Bold1Light1LabelColor = TextStyle(
        fontName: "SFProDisplay-Semibold",
        fontSize: 20,
        color: .black
    )

    static let title32Bold1Light3TertiaryLabelColor = TextStyle(
        fontName: "SFProDisplay-Semibold",
        fontSize: 20,
        color: .tertiaryLabel
    )

    static let title32Bold1Light5Blue = TextStyle(
        fontName: "SFProDisplay-Semibold",
        fontSize: 20,
        color: .primaryBlue
    )

    static let title33Medium1Light1LabelColor = TextStyle(
        fontName: "SFProDisplay-Medium",
        fontSize: 20,
        color: .black
    )

}
