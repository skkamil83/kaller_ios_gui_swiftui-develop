// This file was generated using Prism
import SwiftUI

extension Color {
    static let secondaryLabelDark = Color(r: 235, g: 235, b: 245, alpha: 0.60)
    static let avatarGradient = Color(r: 174, g: 174, b: 178, alpha: 1.00)
//    static let black = Color(r: 0, g: 0, b: 0, alpha: 1.00)
    static let blue700 = Color(r: 100, g: 187, b: 237, alpha: 1.00)
    static let blue800 = Color(r: 89, g: 175, b: 222, alpha: 1.00)
    static let darkBlue = Color(r: 9, g: 130, b: 186, alpha: 1.00)
    static let darkGreen = Color(r: 0, g: 171, b: 63, alpha: 1.00)
    static let fillsLightSecondaryFill = Color(r: 120, g: 120, b: 128, alpha: 0.16)
    static let fillsLightTertiaryFillColor = Color(r: 118, g: 118, b: 128, alpha: 0.12)
    static let graysSystemGray1Light50 = Color(r: 142, g: 142, b: 147, alpha: 0.50)
    static let graysSystemGray2Dark = Color(r: 99, g: 99, b: 102, alpha: 1.00)
    static let graysSystemGray3Dark = Color(r: 72, g: 72, b: 74, alpha: 1.00)
    static let graysSystemGray3Li2 = Color(r: 199, g: 199, b: 204, alpha: 1.00)
    static let graysSystemGray4Dark = Color(r: 58, g: 58, b: 60, alpha: 1.00)
    static let graysSystemGray4Light = Color(r: 209, g: 209, b: 214, alpha: 1.00)
    static let graysSystemGray5Light = Color(r: 229, g: 229, b: 234, alpha: 1.00)
    static let iOsSystemSeparatorsLightSeparatorCol = Color(r: 185, g: 185, b: 187, alpha: 1.00)
    static let keypadCallButtonShadow = Color(r: 0, g: 0, b: 0, alpha: 0.33)
    static let materialsBackgroundsLight1Thick = Color(r: 250, g: 250, b: 250, alpha: 0.93)
    static let materialsBackgroundsLight2Thin = Color(r: 227, g: 227, b: 227, alpha: 0.65)
    static let navigationBarBackgroundChrome = Color(r: 249, g: 249, b: 249, alpha: 0.94)
    static let navigationBarSeparator = Color(r: 0, g: 0, b: 0, alpha: 0.30)
    static let primaryBlue = Color(r: 63, g: 160, b: 213, alpha: 1.00)
    static let red700 = Color(r: 255, g: 98, b: 89, alpha: 1.00)
    static let secondaryLabel = Color(r: 60, g: 60, b: 67, alpha: 0.60)
    static let tertiaryLabel = Color(r: 60, g: 60, b: 67, alpha: 0.30)
    static let tertiaryLabelDark = Color(r: 235, g: 235, b: 245, alpha: 0.30)
    static let tintsDisabledLight = Color(r: 153, g: 153, b: 153, alpha: 1.00)
    static let tintsSystemGreenLig2 = Color(r: 52, g: 199, b: 89, alpha: 1.00)
    static let tintsSystemRedLight = Color(r: 255, g: 59, b: 48, alpha: 1.00)
//    static let white = Color(r: 255, g: 255, b: 255, alpha: 1.00)
    static let yellowDark = Color(r: 255, g: 214, b: 10, alpha: 1.00)
}

private extension Color {
    init(r: Double, g: Double, b: Double, alpha: Double) {
        self.init(red: r / 255, green: g / 255, blue: b / 255, opacity: alpha)
    }
}
