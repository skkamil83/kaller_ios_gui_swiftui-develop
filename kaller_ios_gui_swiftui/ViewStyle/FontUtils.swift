import SwiftUI

extension View {
    func textStyle(_ textStyle: TextStyle) -> some View {
        self.font(textStyle.font)
                .foregroundColor(textStyle.color)
    }
}
extension Text {
    func textStyle(_ textStyle: TextStyle) -> some View {
        self.font(textStyle.font)
                .foregroundColor(textStyle.color)
    }
}


struct TextStyle {
    let font: Font
    let color: Color
    init(
            fontName: String,
            fontSize: Float,
            color: Color
    ) {
        let fontSize = CGFloat(fontSize)

        if fontName.prefix(2) == "SF" {
            self.font = Self.getSystemFont(fontName: fontName, fontSize: fontSize)
        } else {
            self.font = Font.custom(fontName, size: fontSize)
        }

        self.color = color
    }


    private static func getSystemFont(fontName: String, fontSize: CGFloat) -> Font {
        var font = getSizedSystemFont(size: fontSize)

        font = applyWeightToSystemFont(font: font, fontName: fontName)

        font = applyItalicToSystemFont(font: font, fontName: fontName)

        font = applyItalicToSystemFont(font: font, fontName: fontName)

        return font
    }

    private static func getSizedSystemFont(size: CGFloat) -> Font {
        switch size {
        case 11:
            return .caption2
        case 12:
            return .caption
        case 13:
            return .footnote
        case 15:
            return .subheadline
        case 16:
            return .callout
        case 17:
            return .body //body and headline are the same but headline is semibold
        case 20:
            return .title3
        case 22:
            return .title2
        case 28:
            return .title
        case 34:
            return .largeTitle
        default:
            return Font.system(size: size)
        }
    }

    private static func applyWeightToSystemFont(font: Font, fontName: String) -> Font {
        if fontName.contains("-Bold") {
            return font.weight(.bold)
        } else if (fontName.contains("-Regular")) {
            return font.weight(.regular)
        } else if (fontName.contains("-Semibold")) {
            return font.weight(.semibold)
        } else if (fontName.contains("-Medium")) {
            return font.weight(.medium)
        } else if (fontName.contains("-Light")) {
            return font.weight(.light)
        } else if (fontName.contains("-Heavy")) {
            return font.weight(.heavy)
        } else if (fontName.contains("-Ultralight")) {
            return font.weight(.ultraLight)
        } else if (fontName.contains("-Thin")) {
            return font.weight(.thin)
        } else {
            return font
        }
    }

    private static func applyItalicToSystemFont(font: Font, fontName: String) -> Font {
        if fontName.contains("Italic") {
            return font.italic()
        }

        return font
    }
}

struct FontStyleExample: Identifiable {
    let id: String
    let style: TextStyle
}
