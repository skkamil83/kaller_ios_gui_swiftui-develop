import SwiftUI

struct FontStylesView: View {
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack {
                ForEach(FontStyleExample.models) { model in
                    Text(model.id)
                        .textStyle(model.style)
                }
            }
        }
    }
}

