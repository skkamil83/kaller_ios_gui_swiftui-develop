import SwiftUI

struct StylesContainer: View {
    var body: some View {
        NavigationView {
            List {
                NavigationLink(destination: FontStylesView()) {
                    Text("Font styles")
                }
            }
                    .navigationBarTitle("Components", displayMode: .inline)
        }
    }
}

