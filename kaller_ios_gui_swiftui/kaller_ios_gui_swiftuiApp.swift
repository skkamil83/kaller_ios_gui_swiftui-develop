//
//  kaller_ios_gui_swiftuiApp.swift
//  kaller_ios_gui_swiftui
//
//  Created by Alexis Grigorev on 09.11.2020.
//

import SwiftUI

@main
struct kaller_ios_gui_swiftuiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
